<?php
/**
 * Template name:Defibrillator
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since jnext 1.0
 */
get_header('mega-menu'); ?>
<?php if ( has_post_thumbnail() ) :
    $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
endif;
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

    <div id="section1" class="header-overlap">
        <!-- course-banner-->
        <div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
        <div class="banner-content">
            <div class="wrap">
                <?php if($smalltitle){ ?>
                    <h6><?php echo $smalltitle; ?></h6>
                <?php } ?>
                <h1><?php echo $largetitle; ?></h1>
            </div>
        </div>
    </div>

<?php
while ( have_posts() ) : the_post();
?>
    <div id="maincontent" class="general-template">
        <h1 class="head-text"><?php the_field('title'); ?></h1>
        <div class="wrap">
                <div class="wrap-inner">
                <div class="w-50">
                    <?php
                        $aed_image = get_field('aed_image');
                        if($aed_image) {
                            echo '<img src="'.$aed_image['url'].'">';
                        }
                    ?>
                </div>
                <div class="w-50">
                    <?php the_field('why_purchase_with_us'); ?>
            </div>
            </div>
        </div>

        <div id="section1a" class="header-overlap">
            <div class="buttons">
                <div class="wrap">
                    <?php
                        $buttons = get_field('buttons');
                        foreach($buttons as $button) {
                            echo '<div class="w-33"><i class="fa fa-check-square-o"></i> '.$button['text'].'</div>';
                        }
                    ?>
                </div>
            </div>
            <!-- course-banner-->
            <div class="lower-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
            <div class="banner-content">
                <div class="wrap">
                    <?php the_content(); ?>
                    <p><a class="btn btn-primary" href="/shop">Buy Now</a></p>
                </div>
            </div>
        </div>

        <div class="wrap">
            <div class="wrap-inner blocks">
            <h1 class="head-text"><?php the_field('lower_header'); ?></h1>
                <?php
                $buttons = get_field('blocks');
                foreach($buttons as $button) {
                    echo '<div class="w-33"><div class="stat">'.$button['stat'].'</div><div class="text">'.$button['text'].'</div></div>';
                }
                ?>
            </div>
        </div>
    </div>
<?php
    endwhile; // End of the loop.
?>

    <style>
        p {
            margin-bottom: 16px;
        }

        .entry-content ol ul {
            margin-top: 0;
            margin-bottom: 16px;
        }
    </style>

<?php get_footer();
