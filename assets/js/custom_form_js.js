//plugin for hash detect and hash history on IE 7/6
(function($, window, undefined) {
    '$:nomunge';
    var str_hashchange = 'hashchange',
        doc = document,
        fake_onhashchange,
        special = $.event.special,
        doc_mode = doc.documentMode,
        supports_onhashchange = 'on' + str_hashchange in window && (doc_mode === undefined || doc_mode > 7);
    function get_fragment(url) {
        url = url || location.href;
        return '#' + url.replace(/^[^#]*#?(.*)$/, '$1');
    };
    $.fn[str_hashchange] = function(fn) {
        return fn ? this.bind(str_hashchange, fn) : this.trigger(str_hashchange);
    };
    $.fn[str_hashchange].delay = 50;
    special[str_hashchange] = $.extend(special[str_hashchange], {
        setup: function() {
            if (supports_onhashchange) {
                return false;
            }
            $(fake_onhashchange.start);
        },

        teardown: function() {
            if (supports_onhashchange) {
                return false;
            }
            $(fake_onhashchange.stop);
        }
    });
    fake_onhashchange = (function() {
        var self = {},
            timeout_id,

            last_hash = get_fragment(),

            fn_retval = function(val) {
                return val;
            },
            history_set = fn_retval,
            history_get = fn_retval;
        self.start = function() {
            timeout_id || poll();
        };
        self.stop = function() {
            timeout_id && clearTimeout(timeout_id);
            timeout_id = undefined;
        };
        function poll() {
            var hash = get_fragment(),
                history_hash = history_get(last_hash);

            if (hash !== last_hash) {
                history_set(last_hash = hash, history_hash);

                $(window).trigger(str_hashchange);

            } else if (history_hash !== last_hash) {
                location.href = location.href.replace(/#.*/, '') + history_hash;
            }

            timeout_id = setTimeout(poll, $.fn[str_hashchange].delay);
        };
        //$.noConflict();
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
        $.browser.msie && !supports_onhashchange && (function() {
            var iframe,
                iframe_src;
            self.start = function() {
                if (!iframe) {
                    iframe_src = $.fn[str_hashchange].src;
                    iframe_src = iframe_src && iframe_src + get_fragment();

                    iframe = $('<iframe tabindex="-1" title="empty"/>').hide()

                        .one('load', function() {
                            iframe_src || history_set(get_fragment());
                            poll();
                        })

                        .attr('src', iframe_src || 'javascript:0')

                        .insertAfter('body')[0].contentWindow;

                    doc.onpropertychange = function() {
                        try {
                            if (event.propertyName === 'title') {
                                iframe.document.title = doc.title;
                            }
                        } catch (e) {}
                    };

                }
            };
            self.stop = fn_retval;
            history_get = function() {
                return get_fragment(iframe.location.href);
            };
            history_set = function(hash, history_hash) {
                var iframe_doc = iframe.document,
                    domain = $.fn[str_hashchange].domain;

                if (hash !== history_hash) {
                    iframe_doc.title = doc.title;
                    iframe_doc.open();
                    domain && iframe_doc.write('<scr'+'ipt>document.domain="' + domain + '"</scr' + '' + 'ipt>');
                    iframe_doc.close();
                    iframe.location.hash = hash;
                }
            };
        })();
        return self;
    })();
})(jQuery, window);

var BDR_M_AdSettings = {
    'host': window.location.protocol+"//ad.betteroffers.com",

    "context": "adbetteroffers",
    "target": "https://www.betteroffers.com",
    "targetMortgage": "betteroffers/mleadof",
    "targetPurchase": "betteroffers/homepurchase",
    "adOfferId":"",
    "adLine":"",
    "adTitle": "",
    "showExtraPage": false,
    "showExtraPageOnZip": false,
    "extraPage": "https://www.betteroffers.com/landing/ratetable.html",
    "adSubTitle": "",
    "adSubmitBtn": window.location.protocol+"://ad.betteroffers.com/images/rtrbuttonsm.png",
    "adBackBtn": "https://ad.betteroffers.com/images/backar.png",
    "adOfferRLID": "mortgage_ad",
    "adLocalStorage": true,
    "adLocalContext": 'betteroffers',
    "adLocalMortgagePath": 'betteroffers/mlead',
    "adLocalPurchasePath": 'betteroffers/homepurchase',
    "addLeadId": true
};

var BDR_M_AdMortgageImg = (function() {

    var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var digi = /^\d{5}$/;
    var phonereg1 = /^\d{3}$/;
    var phonereg3 = /^\d{4}$/;
    var mortgageProcent = 100;
    var mortgageValueStep = 5000;
    var cashValueStep = 5000, sliderRangeInc = 5000;
    var totalMortgageProcent = 100;
    var wassubmitted = false;
    var showExtraPage = false;
    var showExtraPageOnZip = false;
    var extraPage = "ratetable.html";
    var progressid = '';
    var laststep = 0;
    var wasPopFired = false;
    var backButtonFired = false;
    var savePStep = false;
    var launch = new Date().getTime();
    var showDebtStep = true;
    var homeSliderTooltip, mortgageSliderTooltip, rateSliderTooltip, cashSliderTooltip, cardSliderTooltip,
        taxSliderTooltip,studentdebtSliderTooltip,homepriceSliderTooltip,downpaySliderTooltip, incomeTooltip,
        paymentSliderTooltip;
    var animationSetup = {speed: 500, forward: 'right', backward: 'left'};

    createRandomNumber();

    var mortgageHouseSliderRanges = [{
        min: 1,
        max: 16,
        mul: 1
    }, {
        min: 17,
        max: 22,
        mul: 2
    },  {
        min: 23,
        max: 34,
        mul: 5
    },{
        min: 35,
        max: 44,
        mul: 10
    },{
        min: 45,
        max: 46,
        mul: 100
    }];
    var mortgageBalanceSliderRanges = [{
        min: 1,
        max: 19,
        mul: 1
    }, {
        min: 20,
        max: 24,
        mul: 2
    },  {
        min: 25,
        max: 34,
        mul: 5
    },{
        min: 35,
        max: 44,
        mul: 10
    },{
        min: 45,
        max: 46,
        mul: 100
    }];
    var mortgageHouseSliderMin = 60000, mortgageHouseSliderMax = 2000000;
    var mortgageBalanceSliderMin = 5000, mortgageBalanceSliderMax = 2000000;

    var procentStep0 = 0;
    var procentStep1 = 14;
    var procentStep2 = 28;
    var procentStep3 = 42;
    var procentStep4 = 58;
    var procentStep4_new = 65;
    var procentStep5 = 72;
    var procentStep6 = 80;
    var procentStep7 = 90;
    var procentStep8 = 44;
    var procentStep9 = 49;
    var procentStep10 = 55;
    var procentStep11 = 60;
    var procentStep12 = 67;
    var procentStep13 = 73;
    var procentStep14 = 78;
    var procentStep15 = 85;
    var procentStep16 = 92;
    var procentStep17 = 97;
    var thankyou_preo = 100;



    var mortgageSteps = {
        slide0: {
            next: 'slide1',
            progress: procentStep0
        },
        slide1: {
            /* prev: 'slide0',*/
            next: [
                {
                    step: 'slide_location_onsite',
                    cond: where_train2
                }, {
                    step: 'slide_location_public'
                }
            ],

            progress: procentStep1
        },
        slide_location_onsite: {
            prev: 'slide1',
            next: 'slide2site',
            valid: check_location,
            progress: procentStep1
        },
        slide_location_public: {
            prev: 'slide1',
            next: 'slide2site',
            valid: check_location,
            progress: procentStep1
        },
        slide2site: {
            prev: [
                {
                    step: 'slide_location_onsite',
                    cond: where_train2
                }, {
                    step: 'slide_location_public'
                }
            ],
            next: [
                {
                    step: 'slide3public',
                    cond: site_participants
                }, {
                    step: 'slide4publicmore'
                }
            ],
            valid: check_course_details,
            progress: procentStep2
        },
        slide2public: {
            prev: 'slide_location_public',
            next: [
                {
                    step: 'slide3public',
                    cond: public_participants
                }, {
                    step: 'slide4publicmore'
                }
            ],
            valid: check_course_details,
            progress: procentStep2
        },
        slide3public: {
            prev: [
                {
                    step: 'slide2site',
                    cond: where_train2
                }, {
                    step: 'slide2public'
                }
            ],
            next:[
                {
                    step: 'slide6',
                    cond: are_one_of
                }, {
                    step: 'slide4publicless',
                    cond: where_train2
                }, {
                    step: 'slide5'
                }
            ],
            valid:gereral_info,
            progress: procentStep3
        },
        slide6: {
            prev: 'slide3public',
            next: 'slide5',
            valid:participant_valid,
            progress: procentStep4_new
        },
        slide3site: {
            prev: 'slide2site',
            next: 'slide4publicless',
            valid:gereral_info,
            progress: procentStep3
        },
        slide4publicless: {
            prev: 'slide3public',
            next: 'slide5',
            valid:address_valid,
            progress: procentStep5
        },

        slide4publicmore: {
            prev: [
                {
                    step: 'slide2site',
                    cond: where_train2
                }, {
                    step: 'slide2public'
                }
            ],
            finish: true,
            valid: ajaxSubmitMortgageLead,
            progress: procentStep6
        },
        thankyou: {
            prev: 'slide1',
            finish: true,
            valid: ajaxSubmitMortgageLead,
        },
        slide5: {
            prev: 'slide4publicless',
            // next: 'slidePreview',
            // valid: check_services,
            finish: ajaxSubmitMortgageLead,
            progress: procentStep6
        },
        slidePreview:{
            prev: 'slide5',
            finish: ajaxSubmitMortgageLead,
            progress: procentStep6
        },

        slide7: {
            prev: 'slide4publicless',
            finish: ajaxSubmitMortgageLead,
            /*valid: ajaxSubmitMortgageLead,*/
            progress: procentStep6
        },
        slide8: {
            prev: 'slide7',
            next: 'slide9',
            /*valid: MortgagecheckStep14,*/
            progress: procentStep2
        },
        slide9: {
            prev: 'slide8',
            next: 'slide10',
            progress: procentStep2
        },
        slide10: {
            prev: 'slide9',
            next: 'slide11',
            valid:companyname_valid,
            progress: procentStep2
        },
        slide11: {
            prev: 'slide10',
            next: 'slide12',
            //valid:job_title_valid,
            progress: procentStep2
        },
        slide12: {
            prev: 'slide11',
            next: 'slide13',
            valid:street_valid,
            progress: procentStep2
        },
        slide13: {
            prev: 'slide12',
            next: 'slide14',
            valid:city_valid,
            progress: procentStep2
        },
        slide14: {
            prev: 'slide13',
            next: 'slide15',
            valid:state_valid,
            progress: procentStep2
        },
        slide15: {
            prev: 'slide14',
            next: 'slide16',
            valid:postcode_valid,
            progress: procentStep2
        },
        slide16: {
            prev: 'slide15',
            next: 'slide17',
            progress: procentStep2
        },
        slide17: {
            prev: 'slide16',
            next: 'slide18',
            progress: procentStep2
        },
        slide18: {
            prev: 'slide17',
            next: 'slide19',
            progress: procentStep2
        },
        slide19: {
            prev: 'slide18',
            finish: true,
            valid: ajaxSubmitMortgageLead,
            progress: procentStep2
        },
        /*slide19: {
            prev: 'slide1',
            next: 'slide20',
            progress: procentStep2
        },
        slide20: {
            prev: 'slide1',
            next: 'slide21',
            progress: procentStep2
        },*/

        slide4site: {
            prev: 'slide1',
            next: 'slide5site',
            progress: procentStep2
        },


        solarquote: {
            valid: checkSolarStep,
            finish: doSolarStep
        }
    };



    function init() {

        showExtraPage = BDR_M_AdSettings.showExtraPage;
        showExtraPageOnZip = BDR_M_AdSettings.showExtraPageOnZip;
        extraPage = BDR_M_AdSettings.extraPage;
        var isTouchDevice = 'ontouchstart' in window || navigator.msMaxTouchPoints;
        BDR_M_AdMortgageImg.isTouchDevice = isTouchDevice;

        jQuery(".refinance-slide .continue").bind("click", function(e) {
            goNext(e);
        });
        /*if (isTouchDevice) {} else {
            jQuery(".radio-item, .loan-radio-item").bind("mouseover", function(e) {
                jQuery(this).children('.hover').show();
            });
            jQuery(".radio-item, .loan-radio-item").bind("mouseout", function(e) {
                jQuery(this).children('.hover').hide();
            });
        }
        jQuery(".radio-item").bind("click", function(e) {
            if(e.target.nodeName && e.target.nodeName.toLowerCase() === 'input') {
                e.preventDefault();
                return;
            }
            checkRadioOp(this);
            if(jQuery(this).hasClass('radio-item-no-continue')) {}
            else {goNext(e);}
        });
        jQuery(".loan-radio-item").bind("click", function(e) {
            if(e.target.nodeName && e.target.nodeName.toLowerCase() === 'input') {
                e.preventDefault();
                return;
            }
            checkRadioOp(this);
            goNext(e);
        });*/

        var home = jQuery("#home");
        var mortgageBalance = jQuery("#mortgageBalance");
        var mortgageRate = jQuery("#mortgagerate");
        var cash = jQuery("#cash");


        if (backButtonFired) {
            backButtonFired = false;
            return;

        }
        progressid = '';
        laststep = 0;
        if(jQuery().progressbar && jQuery('#cfshowprogress').val() == 'true') {
            jQuery("#progressbar").progressbar({
                value: 1
            });
        }
        showProgressVal(procentStep1);
        if (jQuery("#headersPanel").length > 0) jQuery("#headersPanel").children("div").hide();
        if (jQuery("#footersPanel").length > 0) jQuery("#footersPanel").children("div").hide();
        jQuery(".mortgage-ad-panel .continue").bind("click", function(e) {
            goNext(e);
        });
        jQuery("#phone1, #phone2, #phone3, #mobile1, #mobile2, #mobile3, #zip, #zipR, #zipA, #phone, #altphone").bind("keypress", function(e) {
            return justDigit(e);
        });
        jQuery("#phone1").bind("keyup", function(e) {
            moveNext(this, 3, "phone2");
        });
        jQuery("#phone2").bind("keyup", function(e) {
            moveNext(this, 3, "phone3");
        });
        jQuery("#mobile1").bind("keyup", function(e) {
            moveNext(this, 3, "mobile2");
        });
        jQuery("#mobile2").bind("keyup", function(e) {
            moveNext(this, 3, "mobile3");
        });
        if (jQuery("#electricbill").length > 0) jQuery("#electricbill").bind("keypress", function(e) {
            return justDigit(e);
        });
        initTracking();
        //initMortgage();
        if(jQuery("#configsliders").val() == 'true') {
            initSliders();
        }
        if(jQuery("#cfanimationspeed").length > 0) {
            var speed = parseInt(jQuery("#cfanimationspeed").val());
            if(!isNaN(speed)) {
                animationSetup.speed = speed;
            }
        }
        if(jQuery("#cfanimationdir").length > 0) {
            var dir = jQuery("#cfanimationdir").val();
            animationSetup.forward = dir;
            if(dir === 'right') {animationSetup.backward = 'left';}
            else if(dir === 'left') {animationSetup.backward = 'right';}
            else if(dir === 'up') {animationSetup.backward = 'down';}
            else if(dir === 'down') {animationSetup.backward = 'up';}
        }
        initExtraPage();

        jQuery('#reverseInteres1, #reverseInteres2').bind('change', function(){reverseMortgageInteresChange(jQuery(this).val());});
        if(jQuery("#reverseInteres1").is(':checked')) reverseMortgageInteresChange("Yes");
        if(jQuery("#reverseInteres2").is(':checked')) reverseMortgageInteresChange("No");

        jQuery('#addressedit, #addresseditP, #addresseditA, #addresseditR').bind("click", function(e) {
            showEditAddress(this);
        });

        var initCheckFired = checkPrevFired();
        //if (!initCheckFired) location.hash = "#bmstepslide1";

        jQuery(window).hashchange(function() {
            hashChanged(window.location.hash);
        });
        jQuery("#loantype, #protype, #payment, #mortgagerate, #credit, #state, #fha, #card, #tax, #yearsinloan, #yearsinhome, #homeprice, #downpayment, #tframe, #income, #emplStatus").bind("change", function(e) {
            if (jQuery(this).val().length != 0) showValidStatus(this);
            else showInvalidStatus(this);
        });
        //jQuery("#electric1, #electric2").bind('change', solarOptionChanged);
        jQuery("#electric1").parent().bind('click', function(e) {solarOptionChanged();if(e.stopImmediatePropagation) e.stopImmediatePropagation();e.stopPropagation();});
        createTooltip(jQuery('.reverse-morgage-info abbr'));
        jQuery('.backBtn').bind('click', function(e){ history.back();});
        jQuery('#singlemortgage').bind('submit', showFinishState);
        if (BDR_M_AdSettings.fieldConfig) configureFields();
        if(jQuery("#cfenablepreset").val() == 'true') {
            presetFields(getQueryVariables());
        }
    }
    function submit_form(){
        alert('submit_form');
    }

    function justDigit(evt) {
        var eve = evt ? evt : window.event;
        var charCode = 0;
        if (eve) charCode = eve.charCode ? eve.charCode : (eve.keyCode ? eve.keyCode : (eve.which ? eve.which : 0));
        if (charCode == 46) return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
        else return true;
    }

    function justDigitFloat(evt) {
        var eve = evt ? evt : window.event;
        var charCode = 0;
        if (eve) charCode = eve.charCode ? eve.charCode : (eve.keyCode ? eve.keyCode : (eve.which ? eve.which : 0));
        if (charCode == 46) return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
        else return true;
    }

    function showProgressVal(val) {
        if(jQuery('#cfshowprogress').val() != 'true') {
            return;
        }
        jQuery("#progressbar > .ui-progressbar-value").animate({
            width: val + "%"
        }, 1000);
    };

    function goNext(e) {
        var currentType = jQuery("#typeOfLoan").val();
        if(!currentType) {
            currentType = "MORTGAGE_LOAN";
        }
        if (!e.preventDefault) e.preventDefault = function() {};
        var step = jQuery(".slide:visible");
        var stepId = jQuery(step).attr("id"), stepFlow = mortgageSteps;
        id = stepId.substring(5);

        var nextStep = stepFlow[stepId].next, valid = stepFlow[stepId].valid;
        if(valid) {
            var validStep = valid();
            if(!validStep) return;
        }
        if(!stepFlow[stepId].next && stepFlow[stepId].finish) {
            finish = stepFlow[stepId].finish;
            var validStep = finish();
            return;
        }
        if (BDR_M_AdMortgageImg.isTouchDevice) e.preventDefault();
        //alert(nextStep);
        if(typeof nextStep == 'string') {
            changeFormStep(stepId, nextStep, stepFlow[nextStep].progress);
        } else {
            for(var i=0; i<nextStep.length; i++) {
                var nextStepCriteria = nextStep[i];
                if(nextStepCriteria.cond) {
                    if(nextStepCriteria.cond()) {
                        changeFormStep(stepId, nextStepCriteria.step, stepFlow[nextStepCriteria.step].progress);
                        break;
                    }
                } else {
                    changeFormStep(stepId, nextStepCriteria.step, stepFlow[nextStepCriteria.step].progress);
                    break;
                }
            }
        }
        return;

    };

    function goBack(e, backbrowser) {
        var currentType = jQuery("#typeOfLoan").val();
        if(!currentType) {
            currentType = "MORTGAGE_LOAN";
        }
        if (!e.preventDefault) e.preventDefault = function() {};
        var step = jQuery(".slide:visible");
        var stepId = jQuery(step).attr("id"), stepFlow = mortgageSteps;
        if(stepId) {
            id = stepId.substring(5);

            var prevStep = stepFlow[stepId].prev;
            if (typeof prevStep === "object" && prevStep.constructor === Array) {
                for (var i = 0; i < prevStep.length; i++) {
                    var prevStepCriteria = prevStep[i];
                    if (prevStepCriteria.cond) {
                        if (prevStepCriteria.cond()) {
                            prevStep = prevStepCriteria.step;
                            break;
                        }
                    } else {
                        prevStep = prevStepCriteria.step;
                        break;
                    }
                }
            }
            if (BDR_M_AdMortgageImg.isTouchDevice) e.preventDefault();
            if (!prevStep) {
                return;
            }
            if (typeof prevStep == 'string') {
                changeFormStep(stepId, prevStep, stepFlow[prevStep].progress, animationSetup.backward);
            }
        }
    }


    function isReverseMortgageOp() {
        return jQuery('#reverseInteres1').is(':checked');
    }

    function isReverseMortgageCond() {
        var age = getAge( jQuery("#dob").val() );
        return age>61 && getLtv()<=0.7;
    }

    function advanceFromSolar() {
        window.location.href = 'finish/index.php?rp=2&statecode='+jQuery('#state').val();
        jQuery("#slide11aa").hide();
        jQuery("#slide11ab").hide();
        if(!jQuery('#reverseInteres1').is(':checked'))
        {
            // reverse mortgage "NO" -> show SSN
            show('11a',true);
        } else
        {
            show(14,true);
        }

    }

    function check_location(){
        var err = false;
        var wt = $("input[name='where_train']:checked").val();
        if('Public' == wt){
            if (!$('input[name="location_public"]:checked').val()) {
                err = true;
                jQuery( ".location_public_main" ).addClass( "error-main" );
                jQuery('.location_public_main .my-error').fadeIn('error');
            } else {
                err = false;
                jQuery( ".location_public_main" ).removeClass( "error-main" );
                jQuery('.location_public_main .my-error').fadeOut('error');

                if ($('input[name="location_public"]:checked').val() == 'Other location') {
                    if ($('input[name="other_location_public"]').val() == '') {
                        err = true;
                        jQuery( ".location_public_main" ).addClass( "error-main" );
                        jQuery('.location_public_main .other_location').fadeIn('error');
                    }
                }
            }
        }else{
            if (!$('input[name="location_site"]:checked').val()) {
                err = true;
                jQuery( ".location_site_main" ).addClass( "error-main" );
                jQuery('.location_site_main .my-error').fadeIn('error');
            } else {
                err = false;
                jQuery( ".location_site_main" ).removeClass( "error-main" );
                jQuery('.location_site_main .my-error').fadeOut('error');
                if ($('input[name="location_site"]:checked').val() == 'Other location') {
                    if ($('input[name="other_location_site"]').val() == '') {
                        err = true;
                        jQuery( ".location_site_main" ).addClass( "error-main" );
                        jQuery('.location_site_main .other_location').fadeIn('error');
                    }
                }
            }
        }
        return !err;
    }
    function check_course_details(){
        var err = false;
        var wt = $("input[name='where_train']:checked").val();

        if('Public' == wt){
            var err1 = false;
            var err2 = false;
            var err3 = false;
            if (!$('input[name="public_course"]:checked').val() ) {
                err1 = true;
                jQuery( ".public_course_main" ).addClass( "error-main" );
                jQuery('.public_course_main .my-error').fadeIn('error');
            } else {
                err1 = false;
                jQuery( ".public_course_main" ).removeClass( "error-main" );
                jQuery('.public_course_main .my-error').fadeOut('error');
            }
            if ($('select[name="public_participants"]').val() == "") {
                err2 = true;
                jQuery( ".public_participants_main" ).addClass( "error-main" );
                jQuery('.public_participants_main .my-error').fadeIn('error');
            } else {
                err2 = false;
                jQuery( ".public_participants_main" ).removeClass( "error-main" );
                jQuery('.public_participants_main .my-error').fadeOut('error');
            }
            if (document.getElementById('date_booking1').value == '')  {
                err3 = true;
                jQuery( ".site_date_main" ).addClass( "error-main" );
                jQuery('.site_date_main .my-error').fadeIn('error');
            }
            else
            {
                err3 = false;
                jQuery( ".site_date_main" ).removeClass( "error-main" );
                jQuery('.site_date_main .my-error').fadeOut('error');
            }
            if (document.getElementById('site_time').value == 'Select')  {
                err4 = true;
                jQuery( ".site_time_main" ).addClass( "error-main" );
                jQuery('.site_time_main .my-error').fadeIn('error');
            }
            else
            {
                err4 = false;
                jQuery( ".site_time_main" ).removeClass( "error-main" );
                jQuery('.site_time_main .my-error').fadeOut('error');
            }
            if(err3 == false && err2 == false && err1 == false && err4 == false)
            {
                var err = false;

            }
            else
            {
                var err = true;
            }
            if(err) {
                jQuery('html, body').animate({
                    scrollTop: (jQuery('.error-main').offset().top - 30)
                }, 2000);
            }
            var public_participants = jQuery("#public_participants").val();
            var where_train2 = jQuery("#where_train2").val();
            if(jQuery("#where_train1").is(":checked")) {
                var text_title = '<div class="legend-text input_title are_you">Are you one of the ' + public_participants + ' participants?</div><fieldset class="radio-item-group one_of_main"><div class="loan-radio-item loan_form2_item "><input id="one_of1" name="one_of" class="btn-radio" value="Yes"  type="radio" ><label class="label-bottom" for="one_of1">Yes</label><div class="radio-select"></div></div><div class="loan-radio-item loan_form2_item loantype-2"><input id="one_of2" name="one_of" class="btn-radio" value="No" type="radio"><label class="label-bottom" for="one_of2">No</label></div><div class="my-error" style="display: none;">Choose member</div></fieldset>';
                if(public_participants) {
                    jQuery('#are_you').html(text_title);
                }
                if(!err) {
                    var public_participants = jQuery("#public_participants").val();
                    parseInt(public_participants);
                    var course1 = jQuery("#public_course1").is(":checked");
                    var course2 = jQuery("#public_course2").is(":checked");
                    var course3 = jQuery("#public_course3").is(":checked");
                    var course4 = jQuery("#public_course4").is(":checked");

                    if(parseInt(public_participants) < 7  && course1) {
                        var price = 540;
                    }
                    if(parseInt(public_participants) < 7  && course2) {
                        var price = 1010;
                    }
                    if(parseInt(public_participants) < 7  && course3) {
                        var price = 1270;
                    }
                    if(parseInt(public_participants) < 7  && course4) {
                        var price = 1760;
                    }
                    if(price) {
                        var price_div = '<div class="price_html">Amount Payable : $' + price + '</div>';
                        jQuery('#price_final .price_inner').html(price_div);
                    }
                }
            }
        }else{
            var err1 = false;
            var err1 = false;
            var err2 = false;
            var err3 = false;
            if (!$('input[name="site_course"]:checked').val() ) {
                err1 = true;
                jQuery( ".site_course_main" ).addClass( "error-main" );
                jQuery('.site_course_main .my-error').fadeIn('error');
            } else {
                err1 = false;
                jQuery( ".site_course_main" ).removeClass( "error-main" );
                jQuery('.site_course_main .my-error').fadeOut('error');
            }
            if ($('select[name="site_participants"]').val() == "") {
                err2 = true;
                jQuery( ".site_participants_main" ).addClass( "error-main" );
                jQuery('.site_participants_main .my-error').fadeIn('error');
            } else {
                err2 = false;
                jQuery( ".site_participants_main" ).removeClass( "error-main" );
                jQuery('.site_participants_main .my-error').fadeOut('error');
            }
            if (document.getElementById('date_booking1').value == '')  {
                err3 = true;
                jQuery( ".site_date_main" ).addClass( "error-main" );
                jQuery('.site_date_main .my-error').fadeIn('error');
            }
            else
            {
                err3 = false;
                jQuery( ".site_date_main" ).removeClass( "error-main" );
                jQuery('.site_date_main .my-error').fadeOut('error');
            }
            if (document.getElementById('site_time').value == 'Select')  {
                err4 = true;
                jQuery( ".site_time_main" ).addClass( "error-main" );
                jQuery('.site_time_main .my-error').fadeIn('error');
            }
            else
            {
                err4 = false;
                jQuery( ".site_time_main" ).removeClass( "error-main" );
                jQuery('.site_time_main .my-error').fadeOut('error');
            }
            if(err3 == false && err2 == false && err1 == false && err4 == false)
            {
                var err = false;

            }
            else
            {
                var err = true;
            }
            if(err) {
                jQuery('html, body').animate({
                    scrollTop: (jQuery('.error-main').offset().top - 30)
                }, 2000);
            } else {
                var public_participants = jQuery("#site_participants").val();
                parseInt(public_participants);
                var course1 = jQuery("#site_course1").is(":checked");
                var course2 = jQuery("#site_course2").is(":checked");
                var course3 = jQuery("#site_course3").is(":checked");
                var course4 = jQuery("#site_course4").is(":checked");

                if(parseInt(public_participants) < 8  && course1) {var price = 540; }
                if(parseInt(public_participants) < 8  && course2) {var price = 1010; }
                if(parseInt(public_participants) < 8  && course3) {var price = 1270; }
                if(parseInt(public_participants) < 8  && course4) {var price = 1760; }
                // for 8 to 11
                if(parseInt(public_participants) > 7) {
                    var level_2  = parseInt(public_participants) - 7;
                    if(parseInt(public_participants) < 12 && parseInt(public_participants) > 7  && course1) {
                        var price = 540 + level_2 * 69;
                    }
                    if(parseInt(public_participants) < 12 && parseInt(public_participants) > 7  && course2) {
                        var price = 1010 + level_2 * 130;
                    }
                    if(parseInt(public_participants) < 12 && parseInt(public_participants) > 7  && course3) {
                        var price = 1270 + level_2 * 165;
                    }
                    if(parseInt(public_participants) < 12 && parseInt(public_participants) > 7  && course4) {
                        var price = 1760 + level_2 * 220;
                    }
                }
                if(parseInt(public_participants) > 11) {
                    var level_3  = parseInt(public_participants) - 11;
                    if(parseInt(public_participants) < 21 && parseInt(public_participants) > 11  && course1) {
                        var price = 540 + 4 * 69 + level_3 * 65;
                    }
                    if(parseInt(public_participants) < 21 && parseInt(public_participants) > 11  && course2) {
                        var price = 1010 + 4 * 130 + level_3 * 120;
                    }
                    if(parseInt(public_participants) < 21 && parseInt(public_participants) > 11  && course3) {
                        var price = 1270 + 4 * 165 + level_3 * 155;
                    }
                    if(parseInt(public_participants) < 21 && parseInt(public_participants) > 11  && course4) {
                        var price = 1760 + 4 * 220 + level_3 * 210;
                    }
                }
                if(price) {
                    var price_div = '<div class="peice_html">Amount Payable : $' + price + '</div>';
                    jQuery('#price_final .price_inner').html(price_div);
                }
            }
        }
        return !err;
    }
    function location_public(){
        var err = false;
        if (!$('input[name=location_public]:checked').val()) {
            err = true;
            jQuery( ".location_public_main" ).addClass( "error-main" );
            jQuery('.location_public_main .my-error').fadeIn('error');
        }
        else
        {
            //alert($('input[name=location_public]').val());
            err = false;
            jQuery( ".location_public_main" ).removeClass( "error-main" );
            jQuery('.location_public_main .my-error').fadeOut('error');
            if ($('#slide2public input[name=other_location_public]').val() == 'Other location') {
                if (!$('input[name=other_location]').val()) {
                    jQuery( ".location_public_main" ).addClass( "error-main" );
                    jQuery('.location_public_main .other_location').fadeIn('error');
                }
            }
        }
        if (!$('input[name=public_course]:checked').val() ) {
            err = true;
            jQuery( ".public_course_main" ).addClass( "error-main" );
            jQuery('.public_course_main .my-error').fadeIn('error');
        }
        else
        {
            err = false;
            jQuery( ".public_course_main" ).removeClass( "error-main" );
            jQuery('.public_course_main .my-error').fadeOut('error');
        }
        if ($('select[name=public_participants]').val() == "Select") {
            err = true;
            jQuery( ".public_participants_main" ).addClass( "error-main" );
            jQuery('.public_participants_main .my-error').fadeIn('error');
        }
        else
        {
            err = false;
            jQuery( ".public_participants_main" ).removeClass( "error-main" );
            jQuery('.public_participants_main .my-error').fadeOut('error');
        }
        if (document.getElementById('date_booking1').value == '')  {
            err = true;
            jQuery( ".public_course_main" ).addClass( "error-main" );
            jQuery('.public_course_main .my-error').fadeIn('error');
        }
        else
        {
            err = false;
            jQuery( ".public_course_main" ).removeClass( "error-main" );
            jQuery('.public_course_main .my-error').fadeOut('error');
        }
        if(err)
        {
            jQuery('html, body').animate({
                scrollTop: (jQuery('.error-main').offset().top - 30)
            }, 2000);
        }
        var public_participants = jQuery("#public_participants").val();
        var where_train2 = jQuery("#where_train2").val();
        console.log(public_participants);
        if(jQuery("#where_train1").is(":checked"))
        {
            var text_title = '<div class="legend-text input_title are_you">Are you one of the ' + public_participants + ' participants?</div><fieldset class="radio-item-group one_of_main"><div class="loan-radio-item loan_form2_item "><input id="one_of1" name="one_of" class="btn-radio" value="Yes"  type="radio" ><label class="label-bottom" for="one_of1">Yes</label><div class="radio-select"></div></div><div class="loan-radio-item loan_form2_item loantype-2"><input id="one_of2" name="one_of" class="btn-radio" value="No" type="radio"><label class="label-bottom" for="one_of2">No</label></div><div class="my-error" style="display: none;">Choose member</div></fieldset>';
            if(public_participants)
            {
                jQuery('#are_you').html(text_title);
            }
            if(!err)
            {
                var public_participants = jQuery("#public_participants").val();
                parseInt(public_participants);
                var course1 = jQuery("#public_course1").is(":checked");
                var course2 = jQuery("#public_course2").is(":checked");
                var course3 = jQuery("#public_course3").is(":checked");
                var course4 = jQuery("#public_course4").is(":checked");

                if(parseInt(public_participants) < 7  && course1)
                {
                    var price = 540;
                }
                if(parseInt(public_participants) < 7  && course2)
                {
                    var price = 1010;
                }
                if(parseInt(public_participants) < 7  && course3)
                {
                    var price = 1270;
                }
                if(parseInt(public_participants) < 7  && course4)
                {
                    var price = 1760;
                }
                if(price)
                {
                    var price_div = '<div class="price_html">Amount Payable : $' + price + '</div>';
                    jQuery('#price_final .price_inner').html(price_div);
                }
            }
        }
        return !err;
    }
    function location_site(){
        var err = false;
        if (!$('input[name=location_site]:checked').val()) {
            err = true;
            jQuery( ".location_site_main" ).addClass( "error-main" );
            jQuery('.location_site_main .my-error').fadeIn('error');
        }
        else
        {
            //alert($('input[name=location_public]').val());
            err = false;
            jQuery( ".location_site_main" ).removeClass( "error-main" );
            jQuery('.location_site_main .my-error').fadeOut('error');
            if ($('#slide2public input[name=other_location_site]').val() == 'Other location') {
                if (!$('input[name=other_location]').val()) {
                    //alert('hello');
                    jQuery( ".location_site_main" ).addClass( "error-main" );
                    jQuery('.location_site_main .other_location').fadeIn('error');
                }
            }
        }
        if (!$('input[name=site_course]:checked').val() ) {
            err = true;
            jQuery( ".site_course_main" ).addClass( "error-main" );
            jQuery('.site_course_main .my-error').fadeIn('error');
        }
        else
        {
            err = false;
            jQuery( ".site_course_main" ).removeClass( "error-main" );
            jQuery('.site_course_main .my-error').fadeOut('error');
        }
        if ($('select[name=site_participants]').val() == "Select") {
            err = true;
            jQuery( ".site_participants_main" ).addClass( "error-main" );
            jQuery('.site_participants_main .my-error').fadeIn('error');
        }
        else
        {
            err = false;
            jQuery( ".site_participants_main" ).removeClass( "error-main" );
            jQuery('.site_participants_main .my-error').fadeOut('error');
        }
        if (document.getElementById('date_booking1').value == '')  {
            err = true;
            jQuery( ".site_course_main" ).addClass( "error-main" );
            jQuery('.site_course_main .my-error').fadeIn('error');
        }
        else
        {
            err = false;
            jQuery( ".site_course_main" ).removeClass( "error-main" );
            jQuery('.site_course_main .my-error').fadeOut('error');
        }
        alert(err);
        if(err)
        {

            jQuery('html, body').animate({
                scrollTop: (jQuery('.error-main').offset().top - 30)
            }, 2000);}
        if(!err)
        {
            var public_participants = jQuery("#site_participants").val();
            parseInt(public_participants);
            var course1 = jQuery("#site_course1").is(":checked");
            var course2 = jQuery("#site_course2").is(":checked");
            var course3 = jQuery("#site_course3").is(":checked");
            var course4 = jQuery("#site_course4").is(":checked");

            if(parseInt(public_participants) < 8  && course1)
            {
                var price = 540;
            }
            if(parseInt(public_participants) < 8  && course2)
            {
                var price = 1010;
            }
            if(parseInt(public_participants) < 8  && course3)
            {
                var price = 1270;
            }
            if(parseInt(public_participants) < 8  && course4)
            {
                var price = 1760;
            }
            // for 8 to 11
            if(parseInt(public_participants) > 7)
            {
                var level_2  = parseInt(public_participants) - 7;
                if(parseInt(public_participants) < 12 && parseInt(public_participants) > 7  && course1)
                {		//alert(level_2 * 69)
                    var price = 540 + level_2 * 69;
                }
                if(parseInt(public_participants) < 12 && parseInt(public_participants) > 7  && course2)
                {
                    var price = 1010 + level_2 * 130;
                }
                if(parseInt(public_participants) < 12 && parseInt(public_participants) > 7  && course3)
                {
                    var price = 1270 + level_2 * 165;
                }
                if(parseInt(public_participants) < 12 && parseInt(public_participants) > 7  && course4)
                {
                    var price = 1760 + level_2 * 220;
                }
            }
            if(parseInt(public_participants) > 11)
            {
                var level_3  = parseInt(public_participants) - 11;
                if(parseInt(public_participants) < 21 && parseInt(public_participants) > 11  && course1)
                {
                    var price = 540 + 4 * 69 + level_3 * 65;
                }
                if(parseInt(public_participants) < 21 && parseInt(public_participants) > 11  && course2)
                {
                    var price = 1010 + 4 * 130 + level_3 * 120;
                }
                if(parseInt(public_participants) < 21 && parseInt(public_participants) > 11  && course3)
                {
                    var price = 1270 + 4 * 165 + level_3 * 155;
                }
                if(parseInt(public_participants) < 21 && parseInt(public_participants) > 11  && course4)
                {
                    var price = 1760 + 4 * 220 + level_3 * 210;
                }
            }
            if(price)
            {
                var price_div = '<div class="peice_html">Amount Payable : $' + price + '</div>';
                jQuery('#price_final .price_inner').html(price_div);
            }

        }

        return !err;
    }
    jQuery( ".select_arrow" ).click(function() {
        //jQuery(this).children('option:not(.init)').slideToggle(200);
        jQuery(this).toggleClass("arrow-down-bg");
    });

    function checkStep0() {
        var err = false;
        if (jQuery.trim(jQuery("#typeOfLoan").val()).length == 0) {
            showReq('typeOfLoan');
            err = true;
        } else {
            hideReq('typeOfLoan');
        }
        return !err;
    };

    function checkSingleField(fieldId, options) {
        if (jQuery.trim(jQuery('#'+fieldId).val()).length == 0 ) {
            showReq(fieldId, options);
            return false;
        }
        else {
            hideReq(fieldId, options);
        }
        return true;
    }
    jQuery(function (){

        jQuery("#preview").click(function(){
            previewAllData();
        });
    });
    function previewAllData(){
        flag = true;
        var where_train = jQuery('#where_train1').is(':checked');
        var where_train_value = jQuery('[name=where_train]').val();
        if(where_train == true)
        {
            var where_train_value = jQuery('#where_train1').val();
            var location_public = jQuery('[name=location_public]').val();
            var public_participants = jQuery('[name=public_participants]').val();
        }
        else
        {
            var where_train_value = jQuery('#where_train2').val();
            var location_public = jQuery('[name=location_site]').val();
            var public_participants = jQuery('[name=site_participants]').val();
        }
        var public_course = jQuery('[name=public_course]').val();
        var public_date = jQuery('[name=public_date]').val();
        var public_time = jQuery('[name=session_time]').val();
        var public_firstname = jQuery('[name=public_firstname]').val();
        var public_lastname = jQuery('[name=public_lastname]').val();
        var email = jQuery('[name=public_email]').val();
        var public_contact = jQuery('[name=public_contact]').val();
        var company = jQuery('[name=company]').val();
        var job_title = jQuery('[name=job_title]').val();
        var street = jQuery('[name=street]').val();
        var city = jQuery('[name=city]').val();
        var state = jQuery('[name=state]').val();
        var postcode = jQuery('[name=postcode]').val();
        var comment = jQuery('[name=comment]').val();
        //var services = jQuery('[name=services[]]').val();
        var services = [];
        var services_indi ="";
        jQuery(".services:checked").each(function(){services_indi += $(this).val()+",</br/> "; services.push(jQuery(this).val());});
        //alert(where_train);
        var review_content = '';
        review_content += '<div class="review"><label>Where do you want to train? : </label>';
        review_content += '<span>'+where_train_value+'</span></div><div class="review"><label>Location : </label>';
        review_content += '<span>'+location_public+'<span></div>';
        review_content += '<div class="review"><label>How many participants do you want us to train? : </label>';
        review_content += '<span>'+public_participants+'</span></div>';
        review_content += '<div class="review"><label>Choose your course : </label>';
        review_content += '<span>'+public_course+'</span></div>';
        /*review_content += '<div class="review"><label>Training Date  : </label>';
        review_content += '<span>'+public_date+'</span></div>';*/
        review_content += '<div class="review"><label>Training Time : </label>';
        review_content += '<span>'+public_time+'</span></div>';
        review_content += '<div class="review"><label>First Name : </label>';
        review_content += '<span>'+public_firstname+'</span></div>';
        review_content += '<div class="review"><label>Last Name : </label>';
        review_content += '<span>'+public_lastname+'</span></div>';
        review_content += '<div class="review"><label>Email : </label>';
        review_content += '<span>'+email+'</span></div>';
        review_content += '<div class="review"><label>Contact Number  : </label>';
        review_content += '<span>'+public_contact+'</span></div>';
        review_content += '<div class="review"><label>Company Name : </label>';
        review_content += '<span>'+company+'</span></div>';
        review_content += '<div class="review"><label>Position / Job Title : </label>';
        review_content += '<span>'+job_title+'</span></div>';
        review_content += '<div class="review"><label>Building Number & Street name : </label>';
        review_content += '<span>'+street+'</span></div>';
        review_content += '<div class="review"><label>City, Suburb or Town : </label>';
        review_content += '<span>'+city+'</span></div>';
        review_content += '<div class="review"><label>State / Territory  : </label>';
        review_content += '<span>'+state+'</span></div><div class="review">';
        review_content += '<label>Postcode : </label><span>'+postcode+'</span></div>';
        review_content += '<div class="review"><label>Comments, questions or other information : </label>';
        review_content += '<span>'+comment+'</span></div>';
        review_content += '<div class="review"><label> Services Date  : </label>';
        review_content += '<span>'+services_indi+'</span></div>';
        //var review_content .= '<div class="review"><label>Location</label>'+location_public+'</div>';
        //var review_content .= '<div class="review"><label>Where do you want to train?</label>'+where_train+'</div>';
        jQuery('#review_content').replaceWith(review_content);
    }
    function MortgagecheckStep5() {
        var err = false, slider = jQuery('#configsliders').val() == 'true';
        if (jQuery.trim(jQuery("#home").val()).length == 0 || jQuery("#home").val() == "0") {
            showReq('home');
            if(slider) showReq('homelab', null, 'requiredError2');
            err = true;
        } else {hideReq('home');if(slider) hideReq('homelab', null, 'requiredError2');}
        return !err;
    };

    function MortgagecheckStep6() {
        var err = false, slider = jQuery('#configsliders').val() == 'true';
        if (jQuery.trim(jQuery("#mortgageBalance").val()).length == 0) {
            showReq('mortgageBalance');
            if(slider) showReq('mortgageBalancelab', null, 'requiredError2');
            err = true;
        } else {hideReq('mortgageBalance');if(slider) hideReq('mortgageBalancelab', null, 'requiredError2');}
        return !err;
    };

    function MortgagecheckStep7() {
        var err = false, slider = jQuery('#configsliders').val() == 'true';
        if (jQuery.trim(jQuery("#mortgagerate").val()).length == 0 || jQuery("#mortgagerate").val() == "0") {
            showReq('mortgagerate');
            if(slider) showReq('mortgageratelab', null, 'requiredError2');
            err = true;
        } else {hideReq('mortgagerate');if(slider) hideReq('mortgageratelab', null, 'requiredError2');}
        return !err;
    };

    function MortgagecheckStep8() {
        var err = false, slider = jQuery('#configsliders').val() == 'true';
        if (jQuery.trim(jQuery("#cash").val()).length == 0) {
            showReq('cash');
            if(slider) showReq('cashlab', null, 'requiredError2');
            err = true;
        } else {hideReq('cash');if(slider) hideReq('cashlab', null, 'requiredError2');}
        return !err;
    };

    function MortgagecheckStep10()  {

        var err = true, limit = 18;
        var dob = jQuery("#dob").val();
        jQuery("#age-error").hide();
        jQuery("#age-error2").hide();

        if(dob.length==10)
        {
            var age = getAge(dob);
            if(age>100 || age < limit) {
                err = false;
                jQuery("#dob").css("border","1px solid #DA2E2F");
                jQuery("#dob").css("color","#DA2E2F");
                if(age<limit) {
                    jQuery("#age-error").show();
                } else if(age>100) {
                    jQuery("#age-error2").show();
                }
            } else {
                err = true;
                jQuery("#dob").css("border","1px solid #DDDDDD");
                jQuery("#dob").css("color","#888888");
            }

        } else
        {
            jQuery("#dob").css("border","1px solid #DA2E2F");
            jQuery("#dob").css("color","#DA2E2F");
            err = false;
        }


        return err;
    }

    function MortgagecheckStep10a() {
        var err = false;
        if(jQuery("#payment").is(':visible')) {
            err =  !(checkSingleField('payment'));
        }
        if(jQuery('#reverseInteres1').is(':visible')) {
            if(!jQuery('#reverseInteres1').is(':checked') && !jQuery('#reverseInteres2').is(':checked')) {showReq2('reverseInteres');err=true;}
            else hideReq2('reverseInteres');
        }
        return !err;
    }

    function MortgagecheckStep13(){
        var err = false;
        var lastname = jQuery.trim(jQuery("#public_lastname").val());
        if (lastname=='')
        {

            showReq('lastname');
            err = true;
            jQuery("#lastname-error").fadeIn();
            setTimeout(function()
            {
                jQuery("#lastname-error").fadeOut();
            }, 3500);

        } else hideReq('lastname');


        return !err;
    };
    function check_services(){
        var err = false;
        if (jQuery("input.services:checked").length <= 0) {
            err = true;
            jQuery( ".services_main" ).addClass( "error-main" );
            jQuery('.services_main .my-error').fadeIn('error');
        }else {
            err = false;
            jQuery('html, body').animate({
                scrollTop: (jQuery('.services_main').offset().top - 30)
            }, 2000);
        }

        var email = jQuery( ".email" ).val();
        jQuery( "#email1" ).val( email );
        jQuery( ".booking_form_div .btn" ).trigger( "click" );

        return !err;
    }
    function terms() {
        var err = false;
        if (!$('input[name=terms]:checked').val()) {
            err = true;
            jQuery( ".terms_main" ).addClass( "error-main" );
            jQuery('.terms_main .my-error').fadeIn('error');
        }
        if(err)
        {
            jQuery('html, body').animate({
                scrollTop: (jQuery('.error-main').offset().top - 30)
            }, 2000);
        }
        previewAllData();
        return !err;
    }

    function date_time_valid(){
        var err = false;
        var public_date = jQuery.trim(jQuery("#public_date").val());
        var public_time = jQuery.trim(jQuery("#public_time").val());
        if (public_date=='')
        {

            showReq('public_date');
            err = true;
            jQuery("#public_date-error").fadeIn();
            setTimeout(function()
            {
                jQuery("#public_date-error").fadeOut();
            }, 3500);

        } else hideReq('public_date');
        if (public_time=='')
        {

            showReq('public_time');
            err = true;
            jQuery("#public_time-error").fadeIn();
            setTimeout(function()
            {
                jQuery("#public_time-error").fadeOut();
            }, 3500);

        } else hideReq('public_time');

        return !err;
    };

    function gereral_info(){
        var err = false;

        var firstname = jQuery.trim(jQuery("#public_firstname").val());
        if (firstname=='')
        {

            showReq('firstname');
            err = true;
            //jQuery("#firstname-error").fadeIn();
            jQuery( ".firstname_main" ).addClass( "error-main" );
            jQuery('.firstname_main .my-error').fadeIn('error');


        } else
        {
            jQuery( ".firstname_main" ).removeClass( "error-main" );
            jQuery('.firstname_main .my-error').fadeOut('error');
        }

        var lastname = jQuery.trim(jQuery("#public_lastname").val());
        if (lastname=='')
        {

            showReq('lastname');
            err = true;
            jQuery( ".lastname_main" ).addClass( "error-main" );
            jQuery('.lastname_main .my-error').fadeIn('error');


        } else
        {
            jQuery( ".lastname_main" ).removeClass( "error-main" );
            jQuery('.lastname_main .my-error').fadeOut('error');
        }

        if (!jQuery("#email").val().match(emailFilter)) {
            showReq('email');
            err = true;
            jQuery( ".email_main" ).addClass( "error-main" );
            jQuery('.email_main .my-error').fadeIn('error');
        } else
        {
            jQuery( ".email_main" ).removeClass( "error-main" );
            jQuery('.email_main .my-error').fadeOut('error');
        }


        /* var company = jQuery.trim(jQuery("#company").val());
         if (company=='')
         {

             showReq('company');
             err = true;
             //jQuery("#company-error").fadeIn();
              jQuery( ".company_main" ).addClass( "error-main" );
             jQuery('.company_main .my-error').fadeIn('error');


         }else
         {
                 jQuery( ".company_main" ).removeClass( "error-main" );
                 jQuery('.company_main .my-error').fadeOut('error');
         }

          var job_title = jQuery.trim(jQuery("#job_title").val());
         if (job_title=='')
         {

             showReq('job_title');
             err = true;
             //jQuery("#job_title-error").fadeIn();
             jQuery( ".job_title_main" ).addClass( "error-main" );
             jQuery('.job_title_main .my-error').fadeIn('error');


         } else
         {
                 jQuery( ".job_title_main" ).removeClass( "error-main" );
                 jQuery('.job_title_main .my-error').fadeOut('error');
         }*/
        if(err)
        {
            jQuery('html, body').animate({
                scrollTop: (jQuery('.error-main').offset().top - 30)
            }, 2000);
        }
        return !err;
    }

    function companyname_valid(){
        var err = false;
        var company = jQuery.trim(jQuery("#company").val());
        if (company=='')
        {

            showReq('company');
            err = true;
            // jQuery("#company-error").fadeIn();
            jQuery( ".company_main" ).addClass( "error-main" );
            jQuery('.company_main .my-error').fadeIn('error');


        }  else
        {
            jQuery( ".job_title_main" ).removeClass( "error-main" );
            jQuery('.job_title_main .my-error').fadeOut('error');
        }


        return !err;
    };
    function firstname_valid(){
        var err = false;
        var company = jQuery.trim(jQuery("#company").val());
        if (company=='')
        {

            showReq('company');
            err = true;
            // jQuery("#company-error").fadeIn();
            jQuery( ".company_main" ).addClass( "error-main" );
            jQuery('.company_main .my-error').fadeIn('error');


        } else hideReq('company');


        return !err;
    };
    function job_title_valid(){
        var err = false;
        var job_title = jQuery.trim(jQuery("#job_title").val());
        if (job_title=='')
        {

            showReq('job_title');
            err = true;
            // jQuery("#job_title-error").fadeIn();
            jQuery( ".job_title_main" ).addClass( "error-main" );
            jQuery('.job_title_main .my-error').fadeIn('error');


        } else hideReq('job_title');


        return !err;
    };
    function address_valid(){
        var err = false;
        var street = jQuery.trim(jQuery("#street").val());
        if (street=='')
        {

            showReq('street');
            err = true;
            jQuery( ".street_main" ).addClass( "error-main" );
            jQuery('.street_main .my-error').fadeIn('error');


        } else
        {
            jQuery( ".street_main" ).removeClass( "error-main" );
            jQuery('.street_main .my-error').fadeOut('error');
        }
        var city = jQuery.trim(jQuery("#city").val());
        if (city=='')
        {

            showReq('city');
            err = true;
            jQuery( ".city_main" ).addClass( "error-main" );
            jQuery('.city_main .my-error').fadeIn('error');


        }  else
        {
            jQuery( ".city_main" ).removeClass( "error-main" );
            jQuery('.city_main .my-error').fadeOut('error');
        }

        var state = jQuery.trim(jQuery("#state").val());
        if (state=='')
        {

            showReq('state');
            err = true;
            jQuery( ".state_main" ).addClass( "error-main" );
            jQuery('.state_main .my-error').fadeIn('error');


        }  else
        {
            jQuery( ".state_main" ).removeClass( "error-main" );
            jQuery('.state_main .my-error').fadeOut('error');
        }

        var postcode = jQuery.trim(jQuery("#postcode").val());
        if (postcode=='')
        {
            showReq('postcode');
            err = true;
            jQuery( ".postcode_main" ).addClass( "error-main" );
            jQuery('.postcode_main .my-error').fadeIn('error');
        }  else
        {
            jQuery( ".postcode_main" ).removeClass( "error-main" );
            jQuery('.postcode_main .my-error').fadeOut('error');
        }

        if(err)
        {
            jQuery('html, body').animate({
                scrollTop: (jQuery('.error-main').offset().top - 30)
            }, 2000);
        }
        var email = jQuery( "#email" ).val();
        if(email = "")
        {
            email = 'test@gmail.com';
        }
        jQuery( "#email1" ).val( email );
        jQuery( ".booking_form_div .btn" ).trigger( "click" );
        return !err;
    }

    function participant_valid()
    {
        var err = true;
        var new_error = false;
        var div = document.getElementById('slide6');
        jQuery(div).find('input:text, input:password, input:file, select, textarea')
            .each(function() {
                var my_value = $(this).val();
                var my_id_new = "#" + $(this).attr('id');
                var my_id = $(this).attr('id');

                var postcode = jQuery.trim(jQuery(my_id_new).val());

                if (my_value == "")
                {
                    //showReq('my_id');

                    var new_error = true;
                    var my_id = $(this).attr('id');
                    err = true;
                    var main = "."+ my_id + '_main';
                    var error = "."+ my_id + '_main .my-error';

                    jQuery( main ).addClass( "error-main" );
                    jQuery(error).fadeIn('error');

                }  else
                {

                    var my_id = $(this).attr('id');
                    err = false;
                    var main = "."+ my_id + '_main';
                    var error = "."+ my_id + '_main .my-error';
                    jQuery( main ).removeClass( "error-main" );
                    jQuery(error).fadeOut('error');
                    if((my_id.includes('email')))
                    {
                        if (!jQuery(my_id_new).val().match(emailFilter)) {
                            var new_error = true;
                            var my_id = $(this).attr('id');
                            err = true;
                            var main = "."+ my_id + '_main';
                            var error = "."+ my_id + '_main .my-error';

                            jQuery( main ).addClass( "error-main" );
                            jQuery(error).fadeIn('error');
                        }
                    }
                }


            });
        //alert(new_error);
        if(new_error)
        {
            err = true;
        }
        if(err)
        {
            jQuery('html, body').animate({
                scrollTop: (jQuery('.error-main').offset().top - 30)
            }, 2000);
        }
        return !err;
    }

    function street_valid(){
        var err = false;
        var street = jQuery.trim(jQuery("#street").val());
        if (street=='')
        {

            showReq('street');
            err = true;
            jQuery("#street-error").fadeIn();
            setTimeout(function()
            {
                jQuery("#street-error").fadeOut();
            }, 3500);

        } else hideReq('street');


        return !err;
    };
    function city_valid(){
        var err = false;
        var city = jQuery.trim(jQuery("#city").val());
        if (city=='')
        {

            showReq('city');
            err = true;
            jQuery("#city-error").fadeIn();
            setTimeout(function()
            {
                jQuery("#city-error").fadeOut();
            }, 3500);

        } else hideReq('city');


        return !err;
    };
    function state_valid(){
        var err = false;
        var state = jQuery.trim(jQuery("#state").val());
        if (state=='')
        {

            showReq('state');
            err = true;
            jQuery("#state-error").fadeIn();
            setTimeout(function()
            {
                jQuery("#state-error").fadeOut();
            }, 3500);

        } else hideReq('state');


        return !err;
    };
    function postcode_valid(){
        var err = false;
        var postcode = jQuery.trim(jQuery("#postcode").val());
        if (postcode=='')
        {

            showReq('postcode');
            err = true;
            jQuery("#postcode-error").fadeIn();
            setTimeout(function()
            {
                jQuery("#postcode-error").fadeOut();
            }, 3500);

        } else hideReq('postcode');


        return !err;
    };
    function MortgagecheckStep14(){
        var err = false;
        if (!jQuery("#email").val().match(emailFilter)) {
            showReq('email');
            err = true;
            jQuery("#email-error").fadeIn();
            setTimeout(function(){
                jQuery("#email-error").fadeOut();
            }, 3500);
        } else hideReq('email');

        return !err;
    };

    function MortgagecheckStep15() {
        var err = false;
        if (!jQuery("#zip").val().match(digi)) {
            showReq('zip');
            err = true;
        }
        var zipState = zipToState(jQuery("#zip").val());
        if (zipState.length == 0) {
            showReq('zip');
            err = true;
        } else hideReq('zip');
        if (err) return !err;
        if (showExtraPageOnZip && !wasPopFired) {
            newWindow(jQuery("#zip").val(), zipState);
            return false;
        } else {
            extractZip(false, 'mortgage');
            return true;
        }
    };

    function MortgagecheckStep16() {
        var err = false;
        if (jQuery.trim(jQuery("#address").val()).length == 0) {
            showReq('address');
            err = true;
        } else hideReq('address');
        if (jQuery.trim(jQuery("#city").val()).length == 0) {
            showReq('city');
            err = true;
        } else hideReq('city');
        if (jQuery.trim(jQuery("#state").val()).length == 0) {
            showReq('state');
            err = true;
        } else hideReq('state');
        if (jQuery('#zip2').is(':visible')) {
            var zipVal = jQuery("#zip2").val();
            if (!zipVal.match(digi)) {
                showReq('zip2');
                err = true;
            }
            var zipState = zipToState(zipVal);
            if (zipState.length == 0) {
                showReq('zip2');
                err = true;
            } else hideReq('zip2');
            if (err === false) jQuery('#zip').val(jQuery('#zip2').val());
        }
        return !err;
    };

    function checkSolarStep()
    {
        var isok = true;
        if( !jQuery("#electric1").is(":checked") &&  !jQuery("#electric2").is(":checked") ) {isok = false;}
        if (jQuery("#electric1").is(":checked") && jQuery("#electricbill").val()=='')
        {
            isok = false;
            showReq('electricbill');
        } else
        {
            hideReq('electricbill');
        }
        return isok;
    };



    var showReq = function(id, options, css) {
        css = css || 'requiredError';
        jQuery("#" + id).addClass(css);
        if(options && options.showAllErr) { jQuery("#" + id).prevAll().addClass('requiredError'); }
    };

    var showReq2 = function(id, flagId) {
        jQuery("#" + id + "lab").addClass('requiredError');
        showInvalidStatus(jQuery("#" + id));
        if (flagId) showInvalidStatus(jQuery("#" + flagId));
    };

    var hideReq = function(id, options, css) {
        css = css || 'requiredError';
        jQuery("#" + id).removeClass(css);
        if(options && options.showAllErr) { jQuery("#" + id).prevAll().removeClass('requiredError'); }
    };

    var hideReq2 = function(id, flagId) {
        jQuery("#" + id + "lab").removeClass('requiredError');
        showValidStatus(jQuery("#" + id));
        if (flagId) showValidStatus(jQuery("#" + flagId));
    };


    function roundnice(i) {
        var g = i;
        var rest = g % 5000;
        return i - rest;
    };

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    };

    function checkPrevFired() {
        var params = getQueryVariables();
        if (params["adstarted"] && params["adstarted"] == "true") {
            wasPopFired = true;
            if (params["loantype"] && params["loantype"].length > 0) {
                var loantype = decodeURIComponent(params["loantype"]);
                if (loantype === "Home Purchase") {
                    checkRadioOp(jQuery('.loan-radio-item.loantype-0'));
                } else if (loantype === "Home Equity") {
                    checkRadioOp(jQuery('.loan-radio-item.loantype-2'));
                }
            }
            show(2, true);
            extractZip();
            return true;
        }
        return false;
    };

    function initTracking() {
        var params = getQueryVariables();
        if (params["rlid"] && params["rlid"].length > 0) {
            if (jQuery("#rlid").length > 0) jQuery("#rlid").val(params["rlid"]);
        }
        if (params["source"] && params["source"].length > 0) {
            if (jQuery("#rlid").length > 0) jQuery("#rlid").val(params["source"]);
        }
        if (params["aff_id"] && params["aff_id"].length > 0) {
            if (jQuery("#rlid").length > 0) jQuery("#rlid").val(params["aff_id"]);
        }
        if (params["affiliate_id"] && params["affiliate_id"].length > 0) {
            if (jQuery("#rlid").length > 0) jQuery("#rlid").val(params["affiliate_id"]);
        }
        if (params["CID"] && params["CID"].length > 0) {
            if (jQuery("#rlid").length > 0) jQuery("#rlid").val(params["CID"]);
        }
        if (params["subid"] && params["subid"].length > 0) {
            if (jQuery("#subid").length > 0) jQuery("#subid").val(params["subid"]);
        }
        if (params["offer_id"] && params["offer_id"].length > 0) {
            if (jQuery("#offerid").length > 0) jQuery("#offerid").val(params["offer_id"]);
        }
        if (params["keyword"] && params["keyword"].length > 0) {
            if (jQuery("#offerid").length > 0) jQuery("#offerid").val(params["keyword"]);
        }
        if (params["aff_sub"] && params["aff_sub"].length > 0) {
            if (jQuery("#affsub").length > 0) jQuery("#affsub").val(params["aff_sub"]);
        }
        if (params["Sub_ID"] && params["Sub_ID"].length > 0) {
            if (jQuery("#affsub").length > 0) jQuery("#affsub").val(params["Sub_ID"]);
        }
        if (params["aff_sub2"] && params["aff_sub2"].length > 0) {
            if (jQuery("#affsub2").length > 0) jQuery("#affsub2").val(params["aff_sub2"]);
        }
        if (params["Sub_ID2"] && params["Sub_ID2"].length > 0) {
            if (jQuery("#affsub2").length > 0) jQuery("#affsub2").val(params["Sub_ID2"]);
        }
        if (params["offer_file_id"] && params["offer_file_id"].length > 0) {
            if (jQuery("#offerfileid").length > 0) jQuery("#offerfileid").val(params["offer_file_id"]);
        }
        if (params["form_id"] && params["form_id"].length > 0) {
            if (jQuery("#formid").length > 0) jQuery("#formid").val(params["form_id"]);
        }
        if (params["type"] && params["type"].length > 0) {
            if (jQuery("#type").length > 0) jQuery("#type").val(params["type"]);
        }
        if (params["t"] && params["t"].length > 0) {
            if (jQuery("#t").length > 0) jQuery("#t").val(params["t"]);
        }
        initFieldConfig(params);
    };

    function initFieldConfig(params) {
        BDR_M_AdSettings.fieldConfig = BDR_M_AdSettings.fieldConfig || {};
        for(var key in params) {
            BDR_M_AdSettings.fieldConfig[key] = decodeURIComponent(params[key]);
        }
    }

    function getQueryVariables() {
        var vars = {};
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            vars[pair[0]] = pair[1];
        }
        return vars;
    };

    function initExtraPage() {
        var extraObj = jQuery("#extrapage");
        if (extraObj.length > 0) {
            var popPage = extraObj.val();
            if (popPage.length > 0) {
                extraPage = popPage;
                showExtraPage = true;
            }
        }
        var onzipObj = jQuery("#onzipextrapage");
        if (onzipObj.length) {
            if (onzipObj.val() == "true") {
                showExtraPageOnZip = true;
            }
        }
    };

    function zipToState(zip) {
        var state = "";
        if (zip >= '99501' && zip <= '99950') {
            state = 'AK';
        } else if (zip >= '35004' && zip <= '36925') {
            state = 'AL';
        } else if (zip >= '71601' && zip <= '72959') {
            state = 'AR';
        } else if (zip >= '75502' && zip <= '75502') {
            state = 'AR';
        } else if (zip >= '85001' && zip <= '86556') {
            state = 'AZ';
        } else if (zip >= '90001' && zip <= '96162') {
            state = 'CA';
        } else if (zip >= '80001' && zip <= '81658') {
            state = 'CO';
        } else if (zip >= '06001' && zip <= '06389') {
            state = 'CT';
        } else if (zip >= '06401' && zip <= '06928') {
            state = 'CT';
        } else if (zip >= '20001' && zip <= '20039') {
            state = 'DC';
        } else if (zip >= '20101' && zip <= '20199') {
            state = 'VA';
        } else if (zip >= '20042' && zip <= '20599') {
            state = 'DC';
        } else if (zip >= '20799' && zip <= '20799') {
            state = 'DC';
        } else if (zip >= '19701' && zip <= '19980') {
            state = 'DE';
        } else if (zip >= '32004' && zip <= '34997') {
            state = 'FL';
        } else if (zip >= '30001' && zip <= '31999') {
            state = 'GA';
        } else if (zip >= '39901' && zip <= '39901') {
            state = 'GA';
        } else if (zip >= '96701' && zip <= '96898') {
            state = 'HI';
        } else if (zip >= '50001' && zip <= '52809') {
            state = 'IA';
        } else if (zip >= '68119' && zip <= '68120') {
            state = 'IA';
        } else if (zip >= '83201' && zip <= '83876') {
            state = 'ID';
        } else if (zip >= '60001' && zip <= '62999') {
            state = 'IL';
        } else if (zip >= '46001' && zip <= '47997') {
            state = 'IN';
        } else if (zip >= '66002' && zip <= '67954') {
            state = 'KS';
        } else if (zip >= '40003' && zip <= '42788') {
            state = 'KY';
        } else if (zip >= '70001' && zip <= '71232') {
            state = 'LA';
        } else if (zip >= '71234' && zip <= '71497') {
            state = 'LA';
        } else if (zip >= '01001' && zip <= '02791') {
            state = 'MA';
        } else if (zip >= '05501' && zip <= '05544') {
            state = 'MA';
        } else if (zip >= '20331' && zip <= '20331') {
            state = 'MD';
        } else if (zip >= '20335' && zip <= '20797') {
            state = 'MD';
        } else if (zip >= '20812' && zip <= '21930') {
            state = 'MD';
        } else if (zip >= '03901' && zip <= '04992') {
            state = 'ME';
        } else if (zip >= '48001' && zip <= '49971') {
            state = 'MI';
        } else if (zip >= '55001' && zip <= '56763') {
            state = 'MN';
        } else if (zip >= '63001' && zip <= '65899') {
            state = 'MO';
        } else if (zip >= '38601' && zip <= '39776') {
            state = 'MS';
        } else if (zip >= '71233' && zip <= '71233') {
            state = 'MS';
        } else if (zip >= '59001' && zip <= '59937') {
            state = 'MT';
        } else if (zip >= '27006' && zip <= '28909') {
            state = 'NC';
        } else if (zip >= '58001' && zip <= '58856') {
            state = 'ND';
        } else if (zip >= '68001' && zip <= '68118') {
            state = 'NE';
        } else if (zip >= '68122' && zip <= '69367') {
            state = 'NE';
        } else if (zip >= '03031' && zip <= '03897') {
            state = 'NH';
        } else if (zip >= '07001' && zip <= '08989') {
            state = 'NJ';
        } else if (zip >= '87001' && zip <= '88441') {
            state = 'NM';
        } else if (zip >= '88901' && zip <= '89883') {
            state = 'NV';
        } else if (zip >= '06390' && zip <= '06390') {
            state = 'NY';
        } else if (zip >= '10001' && zip <= '14975') {
            state = 'NY';
        } else if (zip >= '43001' && zip <= '45999') {
            state = 'OH';
        } else if (zip >= '73001' && zip <= '73199') {
            state = 'OK';
        } else if (zip >= '73401' && zip <= '74966') {
            state = 'OK';
        } else if (zip >= '97001' && zip <= '97920') {
            state = 'OR';
        } else if (zip >= '15001' && zip <= '19640') {
            state = 'PA';
        } else if (zip >= '02801' && zip <= '02940') {
            state = 'RI';
        } else if (zip >= '29001' && zip <= '29948') {
            state = 'SC';
        } else if (zip >= '57001' && zip <= '57799') {
            state = 'SD';
        } else if (zip >= '37010' && zip <= '38589') {
            state = 'TN';
        } else if (zip >= '73301' && zip <= '73301') {
            state = 'TX';
        } else if (zip >= '75001' && zip <= '75501') {
            state = 'TX';
        } else if (zip >= '75503' && zip <= '79999') {
            state = 'TX';
        } else if (zip >= '88510' && zip <= '88589') {
            state = 'TX';
        } else if (zip >= '84001' && zip <= '84784') {
            state = 'UT';
        } else if (zip >= '20101' && zip <= '20199') {
            state = 'VA';
        } else if (zip >= '20040' && zip <= '20041') {
            state = 'VA';
        } else if (zip >= '20040' && zip <= '20167') {
            state = 'VA';
        } else if (zip >= '20042' && zip <= '20042') {
            state = 'VA';
        } else if (zip >= '22001' && zip <= '24658') {
            state = 'VA';
        } else if (zip >= '05001' && zip <= '05495') {
            state = 'VT';
        } else if (zip >= '05601' && zip <= '05907') {
            state = 'VT';
        } else if (zip >= '98001' && zip <= '99403') {
            state = 'WA';
        } else if (zip >= '53001' && zip <= '54990') {
            state = 'WI';
        } else if (zip >= '24701' && zip <= '26886') {
            state = 'WV';
        } else if (zip >= '82001' && zip <= '83128') {
            state = 'WY';
        }
        return state;
    };

    function ValidZip(zip, state) {
        var ZIP = parseInt(zip.substring(0, 3), 10);
        switch (state) {
            case "AL":
                ZIPrange = (ZIP >= 350 && ZIP <= 369);
                break;
            case "AK":
                ZIPrange = (ZIP >= 995 && ZIP <= 999);
                break;
            case "AZ":
                ZIPrange = (ZIP >= 850 && ZIP <= 865);
                break;
            case "AR":
                ZIPrange = ((ZIP >= 716 && ZIP <= 729) || (ZIP == 755));
                break;
            case "AS":
                ZIPrange = (ZIP == 967);
                break;
            case "CA":
                ZIPrange = (ZIP >= 900 && ZIP <= 966);
                break;
            case "CO":
                ZIPrange = (ZIP >= 800 && ZIP <= 816);
                break;
            case "CT":
                ZIPrange = (ZIP >= 60 && ZIP <= 69);
                break;
            case "DC":
                ZIPrange = (ZIP >= 200 && ZIP <= 205);
                break;
            case "DE":
                ZIPrange = (ZIP >= 197 && ZIP <= 199);
                break;
            case "FL":
                ZIPrange = ((ZIP >= 320 && ZIP <= 349) && (ZIP != 343 && ZIP != 345 && ZIP != 348));
                break;
            case "GA":
                ZIPrange = (ZIP >= 300 && ZIP <= 319);
                break;
            case "GU":
                ZIPrange = (ZIP == 969);
                break;
            case "HI":
                ZIPrange = (ZIP >= 967 && ZIP <= 968);
                break;
            case "ID":
                ZIPrange = (ZIP >= 832 && ZIP <= 838);
                break;
            case "IL":
                ZIPrange = (ZIP >= 600 && ZIP <= 629);
                break;
            case "IN":
                ZIPrange = (ZIP >= 460 && ZIP <= 479);
                break;
            case "IA":
                ZIPrange = (ZIP >= 500 && ZIP <= 528);
                break;
            case "KS":
                ZIPrange = (ZIP >= 660 && ZIP <= 679);
                break;
            case "KY":
                ZIPrange = (ZIP >= 400 && ZIP <= 427);
                break;
            case "LA":
                ZIPrange = (ZIP >= 700 && ZIP <= 714);
                break;
            case "ME":
                ZIPrange = (ZIP >= 39 && ZIP <= 49);
                break;
            case "MH":
                ZIPrange = (ZIP == 969);
                break;
            case "MD":
                ZIPrange = (ZIP >= 206 && ZIP <= 219);
                break;
            case "MA":
                ZIPrange = ((ZIP >= 10 && ZIP <= 27) || (ZIP == 55));
                break;
            case "MI":
                ZIPrange = (ZIP >= 480 && ZIP <= 499);
                break;
            case "MN":
                ZIPrange = (ZIP >= 550 && ZIP <= 567);
                break;
            case "MS":
                ZIPrange = (ZIP >= 386 && ZIP <= 397);
                break;
            case "MO":
                ZIPrange = (ZIP >= 630 && ZIP <= 658);
                break;
            case "MT":
                ZIPrange = (ZIP >= 590 && ZIP <= 599);
                break;
            case "NE":
                ZIPrange = (ZIP >= 680 && ZIP <= 693);
                break;
            case "NV":
                ZIPrange = (ZIP >= 889 && ZIP <= 898);
                break;
            case "NH":
                ZIPrange = (ZIP >= 30 && ZIP <= 38);
                break;
            case "NJ":
                ZIPrange = (ZIP >= 70 && ZIP <= 89);
                break;
            case "NM":
                ZIPrange = (ZIP >= 870 && ZIP <= 884);
                break;
            case "NY":
                ZIPrange = ((ZIP >= 90 && ZIP <= 149) || (ZIP == 4) || (ZIP == 63));
                break;
            case "NC":
                ZIPrange = (ZIP >= 269 && ZIP <= 289);
                break;
            case "ND":
                ZIPrange = (ZIP >= 580 && ZIP <= 588);
                break;
            case "MP":
                ZIPrange = (ZIP == 969);
                break;
            case "OH":
                ZIPrange = (ZIP >= 430 && ZIP <= 458);
                break;
            case "OK":
                ZIPrange = (ZIP >= 730 && ZIP <= 749);
                break;
            case "OR":
                ZIPrange = (ZIP >= 970 && ZIP <= 979);
                break;
            case "PA":
                ZIPrange = (ZIP >= 150 && ZIP <= 196);
                break;
            case "PR":
                ZIPrange = (ZIP >= 6 && ZIP <= 9);
                break;
            case "RI":
                ZIPrange = (ZIP >= 28 && ZIP <= 29);
                break;
            case "SC":
                ZIPrange = (ZIP >= 290 && ZIP <= 299);
                break;
            case "SD":
                ZIPrange = (ZIP >= 570 && ZIP <= 577);
                break;
            case "TN":
                ZIPrange = (ZIP >= 370 && ZIP <= 385);
                break;
            case "TX":
                ZIPrange = ((ZIP >= 750 && ZIP <= 799) || (ZIP == 885));
                break;
            case "UT":
                ZIPrange = (ZIP >= 840 && ZIP <= 847);
                break;
            case "VT":
                ZIPrange = (ZIP >= 50 && ZIP <= 59);
                break;
            case "VA":
                ZIPrange = ((ZIP >= 220 && ZIP <= 246) || (ZIP == 201));
                break;
            case "VI":
                ZIPrange = (ZIP == 8);
                break;
            case "WA":
                ZIPrange = (ZIP >= 980 && ZIP <= 994);
                break;
            case "WI":
                ZIPrange = (ZIP >= 530 && ZIP <= 549);
                break;
            case "WV":
                ZIPrange = (ZIP >= 247 && ZIP <= 268);
                break;
            case "WY":
                ZIPrange = (ZIP >= 820 && ZIP <= 831);
                break;
            case "AE":
                ZIPrange = (ZIP >= 90 && ZIP <= 98);
                break;
            case "AA":
                ZIPrange = (ZIP == 340);
                break;
            case "AP":
                ZIPrange = (ZIP >= 962 && ZIP <= 966);
                break;
            default:
                return false;
        }
        return ZIPrange;
    };

    function extractZip(asy, mainFlow) {
        var postObj = new Object();
        postObj.zipUtilType = "SCFROMZIP";
        postObj.zip = jQuery("#zip").val();
        postObj.operation = 'zip';
        if(mainFlow == 'personal') {postObj.zip = jQuery("#zipP").val();}
        else if(mainFlow == 'auto') {postObj.zip = jQuery("#zipA").val();}
        else if(mainFlow == 'reverse') {postObj.zip = jQuery("#zipR").val();}
        var asyn = true;
        if (asy) asyn = false;
        var zipState;
        jQuery.ajax({
            url: '//betteroffers.com/application/controller/controller.php',
            type: 'POST',
            data: postObj,
            dataType: 'jsonp',
            jsonpCallback: 'jsonprazip',
            cache: true,
            success: function(data, textStatus, jqXHR) {
                zipState = processZipResponse(data, mainFlow);
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });

        return zipState;
    };

    function processZipResponse(data, mainFlow) {
        var zipInfo = [];
        var stateIdSelect = '#state';
        var cityIdSelect = '#city';
        var zipIdSelect = '#zip';
        var zipId2Select = '#zip2';
        var addressIdDisplay = '#addresspop';
        var addressIdEdit = '#addressedit';
        if (data.state) {
            zipInfo[0] = data.state;
            jQuery(stateIdSelect).val(zipInfo[0]);
            jQuery(".state-name").html(jQuery(stateIdSelect + " option:selected").text());



            if( jQuery(stateIdSelect).val()=='OH')
            {
                var html_bad = "<strong>Notice for Ohio Residents</strong><br /><br />Until this website is authorized, no properties located in Ohio will be accepted through this site.<br /><br />";
                jQuery("#address-bad-text").html( html_bad );
            }


            if( jQuery(stateIdSelect).val()=='OH' )
            {
                jQuery("#loantypetitle").html("Choose Your Best Rate");

                jQuery("#address-normal").css("display","none");
                jQuery("#address-bad").css("display","block");

                jQuery(".back-option").css("display","none");
                jQuery(".progress-panel").css("display","none");
            } else
            {
                jQuery("#address-normal").css("display","block");
                jQuery("#address-bad").css("display","none");

                jQuery(".back-option").css("display","block");
                jQuery(".progress-panel").css("display","block");
            }
        }
        if (data.city) {
            zipInfo[1] = data.city;
            jQuery(cityIdSelect).val(data.city);
            jQuery(".city-name").html(data.city);
        }
        jQuery(zipId2Select).val(jQuery(zipIdSelect).val());
        if (data.city && data.state) {
            var addressLabel = "" + data.city + ", " + data.state + "  " + jQuery(zipIdSelect).val();
            jQuery(addressIdDisplay).html(addressLabel);
        } else {
            jQuery(addressIdEdit).trigger('click');
        }
        return zipInfo;
    };


    function changeFormStep(hStep, vStep, progressVal, dir) {
        jQuery("#" + hStep).hide();
        if (BDR_M_AdMortgageImg.isTouchDevice)
        {console.log('xxx');
            jQuery("#" + vStep).show();
            scrollInView(jQuery("#" + vStep));
        }  else
        {
            if(jQuery('#cfshowanimation').val() == 'true') {
                if(!dir) {dir = animationSetup.forward;}
                jQuery("#" + vStep).toggle( "slide", {direction: dir, complete: function(){scrollInView(jQuery("#" + vStep));}}, animationSetup.speed );
            } else {
                jQuery("#" + vStep).show();
                scrollInView(jQuery("#" + vStep));
            }
        }
        jQuery('.progress-panel').addClass('hidden');
        if(typeof progressVal != 'undefined') {
            jQuery('.progress-panel').removeClass('hidden');
            showProgressVal(progressVal);
        }
        if(vStep == 'slide1')
            jQuery('.progress-panel').addClass('hidden');

        if(dir !== animationSetup.backward)updateHash(vStep);
    };

    function scrollInView(stepElem) {
        var offset = stepElem.offset(), yPos = 0;
        var page = jQuery(window).scrollTop();
        if(offset.top < 0) {
            yPos = 0;
        } else {
            yPos = offset.top -50;
            if(yPos<0) yPos = 0;
        }
        if (BDR_M_AdMortgageImg.isTouchDevice) {
            setTimeout(function () {
                window.scrollTo(jQuery(window).scrollLeft(), yPos);
            }, 10);
        }
    }



    function isHomePurchase() {
        return jQuery("#loanPurpose1").val() == "purchase_rate" || jQuery("#loantype0").is(":checked");
    };

    function where_train2() {
        //alert(jQuery("#where_train2").is(":checked"))
        return  jQuery("#where_train2").is(":checked");

    };
    function public_participants() {
        //alert(jQuery("#where_train2").is(":checked"))
        //return  jQuery("#public_participants").is(":checked");
        var err = true;
        var public_participants = jQuery("#public_participants").val();
        if(public_participants == 'more than 5')
        {
            var err = false;
        }
        if(jQuery("#location_public3").is(":checked"))
        {
            var err = false;
        }
        if(jQuery("#public_course_multiple").is(":checked"))
        {
            var err = false;
        }

        return err;
    };
    function site_participants() {
        //alert(jQuery("#where_train2").is(":checked"))
        var err = true;
        var public_participants = jQuery("#site_participants").val();
        if(public_participants == 'more than 18')
        {
            var err = false;
        }
        if(jQuery("#location_site3").is(":checked"))
        {
            var err = false;
        }
        if(jQuery("#site_course_multiple").is(":checked"))
        {
            var err = false;
        }
        return err;
    };
    function not_are_one_of() {
        return !are_one_of();
    }
    function are_one_of() {
        //alert(jQuery("#where_train2").is(":checked"))
        var err = true;
        if(jQuery("#where_train1").is(":checked"))
        {
            var one_of = jQuery("#one_of1").is(":checked")
            var public_participants = jQuery("#public_participants").val();
            if(public_participants == '1' && one_of == true)
            {
                err = true;
            }else if(public_participants == '1' && one_of == false)	{
                //alert('done');
                err = false;
                var pre_html = '';
                pre_html += '<div class="legend-text input_title">Participant 1 First  Name</div>';;
                pre_html += '<fieldset class="radio-item-group ">';
                pre_html += '<div class="radio-item loan_form2_item firstname_main">';
                pre_html += '<input class="txtfield fullname" name="other_firstname[]" id="firstname1" placeholder="First  Name" value="" type="text">';
                pre_html += '<div class="my-error" style="display: none;">Enter First  Name</div></div></fieldset>';
                pre_html += '<div class="legend-text input_title">Participant 1 Last Name</div>';
                pre_html += '<fieldset class="radio-item-group "><div class="radio-item loan_form2_item lastname_main">';
                pre_html += '<input class="txtfield fullname" name="other_lastname[]" id="lastname1" placeholder=" Last Name" value="" type="text">';
                pre_html += '<div class="my-error" style="display: none;">Enter  Last Name</div></div></fieldset>';
                pre_html += '<div class="legend-text input_title"> Participant 1 E-mail</div>';
                pre_html += '<fieldset class="radio-item-group ">';
                pre_html += '<div class="radio-item loan_form2_item email_main">';
                pre_html += '<input class="txtfield email" name="other_email[]" id="email" placeholder="Your E-mail" value="" type="text">';
                pre_html += '<div class="my-error" style="display: none;">Enter Valid Email</div></div>';
                pre_html += '</fieldset>';
                var next_html = '';
                jQuery('#prehtml').html(pre_html);

            }
            else if(parseInt(public_participants) > 1)
            {
                err = false;
                if(one_of == false)
                {
                    var public_participants =  parseInt(public_participants) + 1;
                }
                var i;
                var next_html = "";
                for(i=1;i < public_participants;i++)
                {

                    var new_html = '';
                    new_html += '<div class="legend-text input_title">Participant ' + i + ' First  Name</div>';
                    new_html += '<fieldset class="radio-item-group "><div class="radio-item loan_form2_item firstname' + i + '_main">';
                    new_html += '<input class="txtfield " name="other_firstname[]" id="firstname' + i + '" placeholder="First  Name" value="" type="text">';
                    new_html += '<div class="my-error" style="display: none;">Enter First  Name</div></div></fieldset> ';
                    new_html += '<div class="legend-text input_title">Participant ' + i + ' Last Name</div>';
                    new_html += '<fieldset class="radio-item-group "><div class="radio-item loan_form2_item lastname' + i + '_main">';
                    new_html += '<input class="txtfield" name="other_lastname[]" id="lastname'+ i +'" placeholder=" Last Name" value="" type="text">';
                    new_html += '<div class="my-error" style="display: none;">Enter  Last Name</div></div></fieldset>';
                    new_html += '<div class="legend-text input_title">Participant ' + i + ' E-mail</div>';
                    new_html += '<fieldset class="radio-item-group "><div class="radio-item loan_form2_item email' + i + '_main">';
                    new_html += '<input class="txtfield email" name="other_email[]" id="cus_email'+ i + '" placeholder="Your E-mail" value="" type="text">';
                    new_html += '<div class="my-error" style="display: none;">Enter Valid Email</div></div></fieldset>';
                    var next_html = next_html + new_html;

                }
                jQuery('#prehtml').html(next_html);
            }

        }
        return !err;
    };
    function updateHash(id) {
        if(jQuery("#typeOfLoan").val() !== ''){
            window.location.hash = "#bmstep" + id ; //+ "?loantype="+jQuery("#typeOfLoan").val()
        }else{
            window.location.hash = "#bmstep" + id ; //+ "?loantype="
        }
    };

    function checkMilitary() {
        if (jQuery("#amil1").is(":checked")) {
            if (jQuery("#amil1").val() == "Yes") showExtraLoan();
            else hideExtraLoan();
        }
        if (jQuery("#amil2").is(":checked")) {
            if (jQuery("#amil2").val() == "Yes") showExtraLoan();
            else hideExtraLoan();
        }
    }

    function purchaseFoundHome() {
        if (jQuery("#hom1").is(":checked")) {
            if (jQuery("#hom1").val() == "Yes") return true;
        }
        return false;
    }

    function purchaseOwnHome() {
        if (jQuery("#ownh1").is(":checked")) {
            if (jQuery("#ownh1").val() == "Yes") return true;
        }
        return false;
    }

    function purchaseHaveRealtor() {
        if (jQuery("#rel1").is(":checked")) {
            if (jQuery("#rel1").val() == "Yes") return true;
        }
        return false;
    }

    function showExtraLoan() {
        if (document.getElementById("valoanpanel")) {
            jQuery("#valoanpanel").show();
        }
    }

    function hideExtraLoan(res) {
        if (document.getElementById("valoanpanel")) {
            jQuery("#valoanpanel").hide();
            if (res) jQuery("#valoan2").attr("checked", "checked");
        }
    }

    function hashChanged() {
        var hash = window.location.hash;
        if (hash.length == 0) hash = "#bmstepslide1";
        var step = jQuery(".slide:visible");
        var stepId = jQuery(step).attr("id");
        if(stepId != hash.substring(7)) {
            var e = {};
            e.preventDefault = function() {};
            //if(hash == "#bmstepslide1" /*|| stepId === 'slide1'*/) {return;}
            goBack(e, true);
        }
    }

    function checkForm() {
        var status = checkStep8() && checkStep9() && checkStep10();
        if (!status) return;
        if(isHomePurchase()) {
            var htarget = BDR_M_AdSettings.target + "/" + BDR_M_AdSettings.targetPurchase;
            if (BDR_M_AdSettings.adLocalStorage === true && BDR_M_AdSettings.adLocalPurchasePath.length > 0) htarget = window.location.protocol + "//" + window.location.host + "/" + BDR_M_AdSettings.adLocalPurchasePath;
            //  jQuery("#singlemortgage").attr("action", htarget);

            jQuery("#save_type").val("purchase");

        } else {
            if (jQuery('#home').val() == "0") jQuery("#home").val("100000");

            var mtarget = BDR_M_AdSettings.target + "/" + BDR_M_AdSettings.targetMortgage;
            if (BDR_M_AdSettings.adLocalStorage === true && BDR_M_AdSettings.adLocalMortgagePath.length > 0) mtarget = window.location.protocol + "//" + window.location.host + "/" + BDR_M_AdSettings.adLocalMortgagePath;
            //jQuery("#singlemortgage").attr("action", mtarget);


            jQuery("#save_type").val("mortgage");

        }


        if (typeof internalClick != 'undefined') internalClick = true;
        if (showExtraPage && showExtraPage == true && showExtraPageOnZip == false) extraOpen(jQuery("#state").val());
        jQuery("#singlemortgage").submit();
    };

    function moveNext(elem, length, next) {
        if (jQuery(elem).val().length == length) jQuery("#" + next).focus();
    }

    function extraOpen(state) {
        if (extraPage.length == 0) return true;
        if (!state && jQuery("#prozip").length > 0) {
            var zipInfo = extractZip(jQuery("#prozip").val(), true, false);
            state = zipInfo[1];
        }
        var ww_params = 'width=' + screen.width + ', height=' + screen.height + ', top=0, left=0, fullscreen=1, scrollbars=1, location=1, menubar=1, toolbar=1, resizable=1, menubars=1, directories=1';
        var randomSufix = Math.floor((Math.random() * 100) + 1);
        document.getElementById("singlemortgage").target = "ww_fasw" + randomSufix;
        window.open("", "ww_fasw" + randomSufix, ww_params);
        window.location.href = extraPage + "?rp=2&state=" + state;
        if (typeof internalClick != 'undefined') internalClick = true;
        return true;
    };

    function newWindow(zip, stateCode) {
        if (stateCode.length != 2) return;
        var win = "" + window.location;
        if (win.indexOf("#") != -1) win = win.substring(0, win.indexOf("#"));
        if (win.indexOf("?") != -1) win += "&zip=" + zip;
        else win += "?zip=" + zip;
        var loantype = "Refinance";
        if (jQuery("#loantype0").is(":checked")) loantype = "Home Purchase";
        else if (jQuery("#loantype2").is(":checked")) loantype = "Home Equity";
        win += "&state=" + stateCode;
        win += "&loantype=" + encodeURIComponent(loantype);
        win += "&adstarted=true";
        win += "#step1";
        var ww_params = 'width=' + screen.width + ', height=' + screen.height + ', top=0, left=0, fullscreen=1, scrollbars=1, location=1, menubar=1, toolbar=1, resizable=1, menubars=1, directories=1';
        var randomSufix = Math.floor((Math.random() * 100) + 1);
        document.getElementById("singlemortgage").target = "ww_fasw" + randomSufix;
        window.open(win, "ww_fasw" + randomSufix, ww_params);
        if (typeof internalClick != 'undefined') internalClick = true;
        window.location.href = extraPage + "?rp=2&state=" + stateCode;
        return true;
    };


    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=")
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1
                c_end = document.cookie.indexOf(";", c_start)
                if (c_end == -1) c_end = document.cookie.length
                return unescape(document.cookie.substring(c_start, c_end))
            }
        }
        return ""

    };

    function showValidStatus(elem) {
        var status = jQuery(elem).nextAll("label.field-status");
        if (status) {
            jQuery(status).css({
                "display": "inline-block"
            });
            jQuery(status).removeClass('field-status-invalid');
            jQuery(status).addClass('field-status-valid');
        }
    };


    function showInvalidStatus(elem) {
        var status = jQuery(elem).nextAll("label.field-status");
        if (status) {
            jQuery(status).css({
                "display": "inline-block"
            });
            jQuery(status).removeClass('field-status-valid');
            jQuery(status).addClass('field-status-invalid');
        }
    };


    function checkRadioOp(ele) {
        var inputElem = jQuery(ele).children('input');
        jQuery(inputElem).attr("checked", "checked");
        var par = jQuery(ele).parent();
        jQuery(par).find('.radio-select').remove();
        jQuery(ele).append('<div class="radio-select"></div>');
    };

    function configureFields() {
        var config = BDR_M_AdSettings.fieldConfig;
        if (config.firstname) jQuery("#firstname").val(config.firstname);
        if (config.lastname) jQuery("#lastname").val(config.lastname);
        if (config.email) jQuery("#email").val(config.email);
        if (config.phone1) jQuery("#phone1").val(config.phone1);
        if (config.phone2) jQuery("#phone2").val(config.phone2);
        if (config.phone3) jQuery("#phone3").val(config.phone3);
        if (config.altphone1) jQuery("#mobile1").val(config.altphone1);
        if (config.altphone2) jQuery("#mobile2").val(config.altphone2);
        if (config.altphone3) jQuery("#mobile3").val(config.altphone3);
        if (config.address) jQuery("#address").val(config.address);
        if (config.city) jQuery("#city").val(config.city);
        if (config.state) jQuery("#state").val(config.state);
        if (config.zip) jQuery("#zip").val(config.zip);
    };

    function presetFields(config) {
        if (config.firstname) jQuery("#firstname").val(config.firstname);
        if (config.lastname) jQuery("#lastname").val(config.lastname);
        if (config.firstname && config.lastname) {
            jQuery("#fullname").val(config.firstname + ' ' + config.lastname);
        }
        if (config.email) jQuery("#email").val(config.email);
        if (config.phone) jQuery("#phone").val(config.phone);
        if (config.phone1) jQuery("#phone1").val(config.phone1);
        if (config.phone2) jQuery("#phone2").val(config.phone2);
        if (config.phone3) jQuery("#phone3").val(config.phone3);
        if (config.address) jQuery("#address").val(config.address);
        if (config.city) jQuery("#city").val(config.city);
        if (config.state) jQuery("#state").val(config.state);
        if (config.zip) jQuery("#zip").val(config.zip);
        if (config.zip) jQuery("#zip2").val(config.zip);
        if (config.dob) jQuery("#dob").val(config.dob);
        if(jQuery("#configsliders").val() == 'true') {
            if (config.homevalue) {
                var value = parseInt(config.homevalue);
                var sliderValue = getSliderRangeValue(mortgageHouseSliderMin, mortgageHouseSliderMax, mortgageHouseSliderRanges, value, jQuery("#house"));
                if(sliderValue) {
                    jQuery("#homeSlider").slider("option", "value", sliderValue);
                    updateOnHouseWorth();
                }
            }
            if (config.mortgagebalance) {
                var value = parseInt(config.mortgagebalance);
                var sliderValue = getSliderRangeValue(mortgageBalanceSliderMin, mortgageBalanceSliderMax, mortgageBalanceSliderRanges, value, jQuery("#mortgageBalance"));
                if(sliderValue) {
                    jQuery("#mortgageSlider").slider("option", "value", sliderValue);
                    updateMBalance();
                }
            }
            if(config.mortgagerate) {
                var rate = parseFloat(config.mortgagerate);
                var rmin = parseInt(jQuery('#mortgagerate').attr('min'));
                var rmax = parseInt(jQuery('#mortgagerate').attr('max'));
                var rstep = parseFloat(jQuery('#mortgagerate').attr('step'));
                if(rate<=rmin) {
                    rate = rmin;
                } else if(rate>=rmax) {
                    rate= rmax;
                } else {
                    var parts = rate/rstep;
                    rate = parts * rstep;
                }
                jQuery("#rateSlider").slider("option","value", rate.toFixed(2));
                updateRate();
            }
            if(config.cash) {
                var rate = parseInt(config.cash);
                var rmin = parseInt(jQuery('#cash').attr('min'));
                var rmax = parseInt(jQuery('#cash').attr('max'));
                var rstep = parseFloat(jQuery('#cash').attr('step'));
                if(rate<=rmin) {
                    rate = rmin;
                } else if(rate>=rmax) {
                    rate= rmax;
                } else {
                    var parts = rate/rstep;
                    rate = parts * rstep;
                }
                jQuery("#cashSlider").slider("option","value", rate);
                updateCash();
            }
        } else {
            if (config.homevalue) {
                var value = parseInt(config.homevalue);
                var parts = (value / 5000).toFixed(0);
                value = parseInt(parts) * 5000;
                jQuery('#home').val(value);
            }
            if (config.mortgagebalance) {
                var value = parseInt(config.mortgagebalance);
                var parts = (value / 5000).toFixed(0);
                value = parseInt(parts) * 5000;
                jQuery('#mortgageBalance').val(value);
            }
            if (config.mortgagerate) {
                var value = parseFloat(config.mortgagerate);
                var parts = (value / 0.25).toFixed(0);
                value = parseInt(parts) * 0.25;
                jQuery('#mortgagerate').val(value);
            }
            if (config.cash) {
                var value = parseInt(config.cash);
                var parts = (value / 5000).toFixed(0);
                value = parseInt(parts) * 5000;
                jQuery('#cash').val(value);
            }
        }
        if(config.loantype && "refinance,homeequity,purchase".indexOf(config.loantype) != -1) {
            if(config.loantype == 'refinance') {
                jQuery('#loantype3').prop("checked", "checked");
            } else if(config.loantype == 'homeequity') {
                jQuery('#loantype2').prop("checked", "checked");
            } else if(config.loantype == 'purchase') {
                jQuery('#loantype0').prop("checked", "checked");
            }
            goNext({});
        }
        if(config.fha && "yes,no,maybe".indexOf(config.fha) != -1) {
            if(config.fha == 'yes') {
                jQuery('#fha1').prop("checked", "checked");
            } else if(config.fha == 'no') {
                jQuery('#fha2').prop("checked", "checked");
            } else if(config.fha == 'maybe') {
                jQuery('#fha3').prop("checked", "checked");
            }
        }
        if(config.va && "yes".indexOf(config.va) != -1) {
            if(config.va == 'yes') {
                jQuery('#military2').prop("checked", "checked");
            }
        }
        if(config.payment && "current,about,31behind,61behind".indexOf(config.payment) != -1) {
            if(config.payment == 'current') {
                jQuery('#payment0').prop("checked", "checked");
            } else if(config.payment == 'about') {
                jQuery('#payment1').prop("checked", "checked");
            } else if(config.payment == '31behind') {
                jQuery('#payment2').prop("checked", "checked");
            } else if(config.payment == '61behind') {
                jQuery('#payment3').prop("checked", "checked");
            }
        }
    };

    function createRandomNumber() {
        var str = new Date().getTime() + "";
        str += Math.floor((Math.random() * 100) + 1);
        if(jQuery('#envPageCache').length > 0 && jQuery('#envPageCache').val() == 'true') {
            jQuery('#uniqueId').val(str);
        }
        return str;
    };

    function createStateSelection() {
        return '<option value="">Select One</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option>' + '<option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option>' + '<option value="DE">Delaware</option><option value="DC">District of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option>' + '<option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option>' + '<option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option>' + '<option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option>' + '<option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option>' + '<option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option>' + '<option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option>' + '<option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option>' + '<option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option>' + '<option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option>' + '<option value="WI">Wisconsin</option><option value="WY">Wyoming</option>';
    };

    function isBadCredit() {
        if (jQuery('#credit2').is(':checked') || jQuery('#credit3').is(':checked')) return true;
        return false;
    };

    function showEditAddress(elem) {
        jQuery(elem).parent().parent().nextAll('.thin-row-panel').show();
        jQuery(elem).parent().parent().nextAll('.thin-row-panel').removeClass('hidden');
        jQuery(elem).parent().parent().hide();
    };


    function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
        {
            age--;
        }
        return age;
    }

    function getLtv() {
        var ltv;
        if(jQuery('#home').length==0) {return;}
        if(jQuery('#home').val().length>0 && jQuery('#mortgageBalance').val().length>0) {
            ltv = parseInt(jQuery('#mortgageBalance').val())/parseInt(jQuery('#home').val());
        }
        return ltv;
    }

    function reverseMortgageInteresChange(val) {
        var row2Panel = jQuery('#age').parents("div.slider-panel")[0];
        if(val=='Yes') {jQuery(row2Panel).show();showHideStep6Bottom(false);jQuery('#payment').val("Current");}
        else {jQuery(row2Panel).hide();showHideStep6Bottom(true)}
    }

    function showHideStep6Bottom(show) {
        var row3Panel = jQuery('#cash').parents("div.slider-panel")[0];
        var row4Panel = jQuery('#payment').parents("div.slider-panel")[0];
        if(show) {jQuery(row3Panel).show();jQuery(row4Panel).show();}
        else {jQuery(row3Panel).hide();jQuery(row4Panel).hide();jQuery("#cash").prop("selectedIndex", 0);jQuery("#payment").prop("selectedIndex", 0);}
    }

    function solarOptionChanged() {
        if(jQuery('#electric1').is(':checked')) {
            jQuery('#solar-question-panel').show();
        }
        else {
            jQuery('#solar-question-panel').hide();
        }
    }


    function checkPhoneFieldsStep(leadType) {
        var phoneSelector = '.mortgage-step';
        var bestPhoneId = '#phone';
        if(leadType == 'personal') {
            phoneSelector = '.personal-step';
            bestPhoneId = '#phone';
        }
        else if(leadType == 'reverse') {
            phoneSelector = '.reverse-step';
            bestPhoneId = '#phone';
        }
        var isok = false;
        var phones = jQuery(phoneSelector + ' .phone_fields input');
        var primaryPhone = jQuery(phones[0]);
        if (primaryPhone.val().length == 14) {
            isok = true;
            jQuery(bestPhoneId).removeClass('requiredError');

        } else
        {
            jQuery(bestPhoneId).addClass('requiredError');
        }
        return isok;
    }

    function checkSSN()
    {
        var ssn = jQuery("#ssn").val();
        var str = "";


        if(ssn.length==11)
        {
            hideReq('ssn');
            return true;
        }
        else {
            showReq('ssn');
        }
        return false;
    }

    function ajaxSubmitLead(leadType)
    {
        if (launch && new Date().getTime() - launch < 5000) {
            return;
        }
        launch = new Date().getTime();


        if (isHomePurchase())
        {
            jQuery("#save_type").val("purchase");

        } else
        {
            if (jQuery('#home').val() == "0")
            {
                jQuery("#home").val("100000");
            }

            jQuery("#save_type").val("mortgage");
            if(leadType == 'auto') {
                jQuery("#save_type").val("auto");
            } else if(leadType == 'personal') {
                jQuery("#save_type").val("personal");
            } else if(leadType == 'reverse') {
                jQuery("#save_type").val("reverse");
            } else if(leadType == 'businessloan') {
                jQuery("#save_type").val("businessloan");
            }
        }



        var isok = true;
        /*if(leadType == 'businessloan' || leadType ==  'personal') {
            isok = true;
        }
        else { isok = checkPhoneFieldsStep(leadType);
        }
        if(leadType == 'auto') {isok = true;}*/


        if(isok)
        {
            jQuery(".back-option").css("display","none");
            var str = jQuery( "#singlemortgage" ).serialize();
            str = str + "&submit_type=ajax";

            jQuery('#singlemortgage').submit();
        }


    }

    function showFinishState() {
        var state = jQuery('#state').val();
        if(jQuery("#save_type").val() == "personal") {
            state = jQuery('#stateP').val();
        }
        if (typeof internalClick != 'undefined') internalClick = true;
        return true;
    }

    function ajaxSubmitMortgageLead(){
        bm_gf_storeUserData();
        ajaxSubmitLead();
    }

    function pop(url) {
        var newwindow = window.open(url, 'popup', 'height=800,width=550,left=100,top=100,scrollbars=yes');
        if (window.focus) {
            newwindow.focus()
        }
    }

    function ajaxUpdateLead()
    {
        var ssn = jQuery("#ssn").val();
        var str = "";


        if(ssn.length==11)
        {
            str = "ssn="+escape(ssn)+"&action=update_ssn";

            jQuery.ajax({
                type: "POST",
                url: "/application/controller/forms_mortgage_quote2.php",
                data: str,
                success: function(data)
                {

                },
                dataType: 'html'
            });
        }
    }

    jQuery(document).ready(function(e) {
        BDR_M_AdMortgageImg.init();

        if(jQuery().mask) {
            jQuery(".datemask").mask("99/99/9999");
            jQuery(".phonemask").mask("9999999999");
            jQuery(".ssnmask").mask("999-99-9999");

        }
    });

    function skipSolar(newpage) {
        if(newpage) {
            window.location.href = jQuery('#nextpage').val();
        }
    }

    function doSolarStep(e) {
        if (jQuery("#electric2").is(":checked")) {
            skipSolar(true);
        } else {
            jQuery('#singlemortgage').submit();
        }
    }

    function createTooltip(elem) {
        jQuery(elem).bind('mouseover', function() {
            jQuery('#tempTooltip').remove();
            var div = document.createElement("div");
            jQuery(div).html(jQuery(this).attr("data-title"));
            jQuery(div).attr("id", "tempTooltip");
            jQuery(div).addClass("reverseMortgageTooltip");
            jQuery(this).append(div);
        });

        jQuery(elem).bind('mouseout', function() {
            jQuery('#tempTooltip').remove();
        });
    }


    function initSliders() {

        var home = jQuery("#home");
        if (home && jQuery(home).length > 0) {
            var hmin = jQuery(home).attr('min');
            var hmax = jQuery(home).attr('max');
            var hval = '19';//jQuery(home).val();
            var hstep = jQuery(home).attr('step');
            homeSliderTooltip = jQuery('<div class="slider-tooltip">$' + addCommas(hval) + '</div>');
            var handleStructure = jQuery("<div class='img-handler'></div>");
            jQuery("#homeSlider").slider({
                range: "min",
                min: parseInt(hmin),
                max: parseInt(hmax),
                value: parseInt(hval),
                step: parseInt(hstep),
                change: updateOnHouseWorth,
                slide: function(event, ui) {
                    updateOnHouseWorth();
                }
            }).find(".ui-slider-handle").append(homeSliderTooltip).append(handleStructure);
            jQuery(home).bind("keypress", function(e) {
                return justDigit(e);
            });
            jQuery(home).bind("blur", function(e) {
                valueChangedHome(this);
            });
            jQuery(home).val(''+mortgageHouseSliderMin);
            updateOnHouseWorth();
        }

        var mortgageBalance = jQuery("#mortgageBalance");
        if (mortgageBalance && jQuery(mortgageBalance).length > 0) {
            var mmin = jQuery(mortgageBalance).attr('min');
            var mmax = jQuery(mortgageBalance).attr('max');
            var mval = '21';//jQuery(mortgageBalance).val();
            var mstep = jQuery(mortgageBalance).attr('step');
            mortgageSliderTooltip = jQuery('<div class="slider-tooltip">$' + addCommas(mval) + '</div>');
            var handleStructure = jQuery("<div class='img-handler'></div>");
            jQuery("#mortgageSlider").slider({
                range: "min",
                min: parseInt(mmin),
                max: parseInt(mmax),
                value: parseInt(mval),
                step: parseInt(mstep),
                change: updateMBalance,
                slide: function(event, ui) {
                    updateMBalance();
                }
            }).find(".ui-slider-handle").append(mortgageSliderTooltip).append(handleStructure);
            jQuery(mortgageBalance).bind("keypress", function(e) {
                return justDigit(e);
            });
            jQuery(mortgageBalance).bind("blur", function(e) {
                valueChangedMortgage(this);
            });
            updateMBalance();
        }

        var mortgageRate = jQuery("#mortgagerate");
        if (mortgageRate && jQuery(mortgageRate).length > 0) {
            var rmin = jQuery(mortgageRate).attr('min');
            var rmax = jQuery(mortgageRate).attr('max');
            var rval = jQuery(mortgageRate).val();
            var rstep = jQuery(mortgageRate).attr('step');
            rateSliderTooltip = jQuery('<div class="slider-tooltip">' + rval + ' %</div>');
            var handleStructure = jQuery("<div class='img-handler'></div>");
            jQuery("#rateSlider").slider({
                range: "min",
                min: parseFloat(rmin),
                max: parseFloat(rmax),
                value: parseFloat(rval),
                step: parseFloat(rstep),
                change: updateRate,
                slide: function(event, ui) {
                    updateRate();
                }
            }).find(".ui-slider-handle").append(rateSliderTooltip).append(handleStructure);
            jQuery(mortgageRate).bind("keypress", function(e) {
                return justDigitFloat(e);
            });
            jQuery(mortgageRate).bind("blur", function(e) {
                valueChangedMRate(this);
            });
        }

        var cash = jQuery("#cash");
        if (cash && jQuery(cash).length > 0) {
            var cmin = jQuery(cash).attr('min');
            var cmax = jQuery(cash).attr('max');
            var cval = jQuery(cash).val();
            var cstep = jQuery(cash).attr('step');
            cashSliderTooltip = jQuery('<div class="slider-tooltip">$ ' + addCommas(cval) + '</div>');
            var handleStructure = jQuery("<div class='img-handler'></div>");
            jQuery("#cashSlider").slider({
                range: "min",
                min: parseInt(cmin),
                max: parseInt(cmax),
                value: parseInt(cval),
                step: parseInt(cstep),
                change: updateCash,
                slide: function(event, ui) {
                    updateCash();
                }
            }).find(".ui-slider-handle").append(cashSliderTooltip).append(handleStructure);
            jQuery(cash).bind("keypress", function(e) {
                return justDigit(e);
            });
            jQuery(cash).bind("blur", function(e) {
                valueChangedCash(this);
            });
        }

        if (showDebtStep === true) {
            var card = jQuery("#card");
            if (card && jQuery(card).length > 0) {
                var cmin = jQuery(card).attr('min');
                var cmax = jQuery(card).attr('max');
                var cval = jQuery(card).val();
                var cstep = jQuery(card).attr('step');
                cardSliderTooltip = jQuery('<div class="slider-tooltip">$ ' + addCommas(cval) + '</div>');
                var handleStructure = jQuery("<div class='img-handler'></div>");
                jQuery("#cardSlider").slider({
                    range: "min",
                    min: parseInt(cmin),
                    max: parseInt(cmax),
                    value: parseInt(cval),
                    step: parseInt(cstep),
                    change: updateCard,
                    slide: function(event, ui) {
                        updateCard();
                    }
                }).find(".ui-slider-handle").append(cardSliderTooltip).append(handleStructure);
                jQuery(card).bind("keypress", function(e) {
                    return justDigit(e);
                });
                jQuery(card).bind("blur", function(e) {
                    valueChangedCard(this);
                });
            }

            var tax = jQuery("#tax");
            if (tax && jQuery(tax).length > 0) {
                var cmin = jQuery(tax).attr('min');
                var cmax = jQuery(tax).attr('max');
                var cval = jQuery(tax).val();

                var cstep = jQuery(tax).attr('step');
                taxSliderTooltip = jQuery('<div class="slider-tooltip">$ ' + addCommas(cval) + '</div>');
                var handleStructure = jQuery("<div class='img-handler'></div>");
                jQuery("#taxSlider").slider({
                    range: "min",
                    min: parseInt(cmin),
                    max: parseInt(cmax),
                    value: parseInt(cval),
                    step: parseInt(cstep),
                    change: updateTax,
                    slide: function(event, ui) {
                        updateTax();
                    }
                }).find(".ui-slider-handle").append(taxSliderTooltip).append(handleStructure);
                jQuery(tax).bind("keypress", function(e) {
                    return justDigit(e);
                });
                jQuery(tax).bind("blur", function(e) {
                    valueChangedTax(this);
                });
            }

            var studentdebt = jQuery("#studentdebt");
            if (studentdebt && jQuery(studentdebt).length > 0) {
                var cmin = jQuery(studentdebt).attr('min');
                var cmax = jQuery(studentdebt).attr('max');
                var cval = jQuery(studentdebt).val();
                var cstep = jQuery(studentdebt).attr('step');
                studentdebtSliderTooltip = jQuery('<div class="slider-tooltip">$ ' + addCommas(cval) + '</div>');
                var handleStructure = jQuery("<div class='img-handler'></div>");
                jQuery("#studentdebtSlider").slider({
                    range: "min",
                    min: parseInt(cmin),
                    max: parseInt(cmax),
                    value: parseInt(cval),
                    step: parseInt(cstep),
                    change: updateStudentDebt,
                    slide: function(event, ui) {
                        updateStudentDebt();
                    }
                }).find(".ui-slider-handle").append(studentdebtSliderTooltip).append(handleStructure);
                jQuery(studentdebt).bind("keypress", function(e) {
                    return justDigit(e);
                });
                jQuery(studentdebt).bind("blur", function(e) {
                    valueChangedStudentDebt(this);
                });
            }
        }

        var homeprice = jQuery("#homeprice");
        if (homeprice && jQuery(homeprice).length > 0) {
            var hmin = jQuery(homeprice).attr('min');
            var hmax = jQuery(homeprice).attr('max');
            var hval = jQuery(homeprice).val();
            var hstep = jQuery(homeprice).attr('step');
            homepriceSliderTooltip = jQuery('<div class="slider-tooltip">$ ' + addCommas(hval) + '</div>');
            var handleStructure = jQuery("<div class='img-handler'></div>");
            jQuery("#homepriceSlider").slider({
                range: "min",
                min: parseInt(hmin),
                max: parseInt(hmax),
                value: parseInt(hval),
                step: parseInt(hstep),
                change: updateDownOnhome,
                slide: updateDownOnhome
            }).find(".ui-slider-handle").append(homepriceSliderTooltip).append(handleStructure);
            jQuery(homeprice).bind("keypress", function(e) {
                return justDigit(e);
            });
            jQuery(homeprice).bind("blur", function(e) {
                valueHomePrice(this);
            });
        }

        var downpay = jQuery("#downpayment");
        if (downpay && jQuery(downpay).length > 0) {
            var hmin = jQuery(downpay).attr('min');
            var hmax = jQuery(downpay).attr('max');
            var hval = jQuery(downpay).val();
            var hstep = jQuery(downpay).attr('step');
            downpaySliderTooltip = jQuery('<div class="slider-tooltip">$ ' + addCommas(hval) + '</div>');
            var handleStructure = jQuery("<div class='img-handler'></div>");
            jQuery("#downpaySlider").slider({
                range: "min",
                min: parseInt(hmin),
                max: parseInt(hmax),
                value: parseInt(hval),
                step: parseInt(hstep),
                change: updateDownpay,
                slide: updateDownpay
            }).find(".ui-slider-handle").append(downpaySliderTooltip).append(handleStructure);
            jQuery(downpay).bind("keypress", function(e) {
                return justDigit(e);
            });
            jQuery(downpay).bind("blur", function(e) {
                valueDownPaym(this);
            });
        }


        var income = jQuery("#income");
        if (income && jQuery(income).length > 0) {
            var hmin = jQuery(income).attr('min');
            var hmax = jQuery(income).attr('max');
            var hval = jQuery(income).val();
            var hstep = jQuery(income).attr('step');
            incomeTooltip = jQuery('<div class="slider-tooltip">$ ' + addCommas(hval) + '</div>');
            var handleStructure = jQuery("<div class='img-handler'></div>");
            jQuery("#incomeSlider").slider({
                range: "min",
                min: parseInt(hmin),
                max: parseInt(hmax),
                value: parseInt(hval),
                step: parseInt(hstep),
                change: updateIncome,
                slide: updateIncome
            }).find(".ui-slider-handle").append(incomeTooltip).append(handleStructure);
            jQuery(income).bind("keypress", function(e) {
                return justDigit(e);
            });
            jQuery(income).bind("blur", function(e) {
                valueChangedIncome(this);
            });
        }


        var payment = jQuery("#payment");
        paymentSliderTooltip = jQuery('<div class="slider-tooltip">Current</div>');
        jQuery("#paymentSlider").slider({
            range: "min",
            min: 0,
            max: 3,
            value: 0,
            step: 1,
            slide: function(event, ui) {
                var txt = '';
                if (ui.value === 0) {
                    txt = 'Current';
                    jQuery("#payment").val('Current');
                } else if (ui.value === 1) {
                    txt = 'About to Fall';
                    jQuery("#payment").val('About to Fall Behind');
                } else if (ui.value === 2) {
                    txt = '31+ Days Behind';
                    jQuery("#payment").val('31+ Behind');
                } else if (ui.value === 3) {
                    txt = '61+ Days Behind';
                    jQuery("#payment").val('61+ Behind');
                }
                paymentSliderTooltip.text(txt);
            }
        }).find(".ui-slider-handle").append(paymentSliderTooltip);
    }

    function updateOnHouseWorth() {
        var homeValue = jQuery("#homeSlider").slider("value");
        jQuery("#home").val(homeValue);
        homeSliderTooltip.text('$' + addCommas(homeValue));
        var isReverse = false;
        if (document.getElementById("loantype")) {
            var typ = document.getElementById("loantype").value;
            if (typ == "Reverse Mortgage") isReverse = true;
        }
        homeValue = parseInt(homeValue);
        if(1>0 && jQuery("#configsliders").val() == 'true') {
            var txt = "", checkStep = homeValue, range;
            if(checkStep == 0) {
                txt = '$' + addCommas(mortgageHouseSliderMin) + ' or less';
                homeValue = mortgageHouseSliderMin;
            } else if(checkStep > mortgageHouseSliderRanges[mortgageHouseSliderRanges.length - 1].max) {
                homeValue = mortgageHouseSliderMax;
                txt = 'Over $' + addCommas(homeValue)
            } else {
                var startRange = mortgageHouseSliderMin;
                for(var i=0;i<mortgageHouseSliderRanges.length;i++) {
                    var tempRange = mortgageHouseSliderRanges[i];
                    if(checkStep>=tempRange.min && checkStep<=tempRange.max) {
                        range = tempRange;
                        break;
                    } else {
                        startRange += (tempRange.max - tempRange.min + 1) * tempRange.mul * sliderRangeInc;
                    }
                }
                if(range) {
                    var innerStep = checkStep - (range.min -1);
                    var high = startRange + innerStep * (range.mul * sliderRangeInc);
                    var low = high - range.mul*sliderRangeInc + 1;
                    txt = '$' + addCommas(low) + ' to ' + '$' + addCommas(high);
                    homeValue = high;
                }
            }
            jQuery("#home").val(homeValue);
            jQuery('#homeSlider').prevAll('.slider-tooltip-display').html(txt);
            return;
        }
        if (homeValue == 0) homeValue = 100000;
        var upperLimit = roundnice(1.5 * homeValue);
        var initVal = 0; //0.75*homeValue;
        if (initVal % 5000 != 0) {
            var h = roundnice(initVal);
            if (h) initVal = h;
        }
        if (isReverse == true) {
            upperLimit = 0.75 * homeValue;
            initVal = 0;
        }
        jQuery("#mortgageSlider").slider("option", "min", 0);
        jQuery("#mortgageSlider").slider("option", "max", upperLimit);
        jQuery("#mortgageSlider").slider("option", "value", initVal);
        return;
    }

    function valueChangedHome(elem) {
        var val = jQuery(elem).val();
        if (val.length == 0) val = "0";
        val = parseInt(val);
        var min = parseInt(jQuery(elem).attr('min'));
        var max = parseInt(jQuery(elem).attr('max'));
        var step = parseInt(jQuery(elem).attr('step'));
        if (val < min) val = min;
        else if (val > max) val = max;
        var multi = val / step;
        jQuery("#homeSlider").slider("option", "value", multi * step);
    }

    function updateMBalance() {
        var moValue = jQuery("#mortgageSlider").slider("value");
        jQuery("#mortgageBalance").val(moValue);
        mortgageSliderTooltip.text('$' + addCommas(moValue));
        if(1>0 && jQuery("#configsliders").val() == 'true') {
            var txt = "", checkStep = moValue, range;
            if(checkStep == 0) {
                txt = '$' + addCommas(mortgageBalanceSliderMin) + ' or less';
                moValue = mortgageBalanceSliderMin;
            } else if(checkStep > mortgageBalanceSliderRanges[mortgageBalanceSliderRanges.length - 1].max) {
                moValue = mortgageBalanceSliderMax;
                txt = 'Over $' + addCommas(moValue)
            } else {
                var startRange = mortgageBalanceSliderMin;
                for(var i=0;i<mortgageBalanceSliderRanges.length;i++) {
                    var tempRange = mortgageBalanceSliderRanges[i];
                    if(checkStep>=tempRange.min && checkStep<=tempRange.max) {
                        range = tempRange;
                        break;
                    } else {
                        startRange += (tempRange.max - tempRange.min + 1) * tempRange.mul * sliderRangeInc;
                    }
                }
                if(range) {
                    var innerStep = checkStep - (range.min -1);
                    var high = startRange + innerStep * (range.mul * sliderRangeInc);
                    var low = high - range.mul*sliderRangeInc + 1;
                    txt = '$' + addCommas(low) + ' to ' + '$' + addCommas(high);
                    moValue = high;
                }
            }
            jQuery("#mortgageBalance").val(moValue);
            jQuery('#mortgageSlider').prevAll('.slider-tooltip-display').html(txt);
            return;
        }
    };

    function getSliderRangeValue(min, max, config, value, elem) {
        if(value < min)  {
            return elem.attr('min');
        } else if(value > max) {
            return elem.attr('max');
        }
        var startRange = min, range;
        for(var i=0;i<config.length;i++) {
            var tempRange = config[i];
            var endRange = startRange + (tempRange.max - tempRange.min + 1) * tempRange.mul * sliderRangeInc;
            if(value>=startRange && value<=endRange) {
                range = tempRange;
                break;
            } else {
                startRange = endRange;
            }
        }
        if(range) {
            var slots = range.max - range.min + 1, sliderValue = range.min;
            for(var i=0;i<=slots;i++) {
                var upperBound = startRange + ((i+1) *range.mul * sliderRangeInc);
                if(value<=upperBound) {
                    sliderValue = range.min + i;
                    break;
                }
            }
            return sliderValue;
        }
    };

    function valueChangedMortgage(elem) {
        var val = jQuery(elem).val();
        if (val.length == 0) val = "0";
        val = parseInt(val);
        var min = parseInt(jQuery("#mortgageSlider").slider("option", "min"));
        var max = parseInt(jQuery("#mortgageSlider").slider("option", "max"));
        var step = parseInt(jQuery(elem).attr('step'));
        if (val < min) val = min;
        else if (val > max) val = max;
        var multi = val / step;
        jQuery("#mortgageSlider").slider("option", "value", multi * step);
    }

    function updateRate() {
        var moValue = jQuery("#rateSlider").slider("value");
        jQuery("#mortgagerate").val(moValue);
        rateSliderTooltip.text(moValue + ' %');
        jQuery('#rateSlider').prevAll('.slider-tooltip-display').html(moValue + ' %');
    }

    function valueChangedMRate(elem) {
        var val = jQuery(elem).val();
        if (val.length == 0) val = "0";
        val = parseFloat(val);
        var min = parseFloat(jQuery(elem).attr('min'));
        var max = parseFloat(jQuery(elem).attr('max'));
        var step = parseFloat(jQuery(elem).attr('step'));
        if (val < min) val = min;
        else if (val > max) val = max;
        var multi = val / step;
        jQuery("#rateSlider").slider("option", "value", multi * step);
    }

    function updateCash() {
        var moValue = jQuery("#cashSlider").slider("value");
        var txt = '$' + addCommas(moValue);
        jQuery("#cash").val(moValue);
        if(moValue == '0') {
            txt = '$0 (No cash)';
        }
        cashSliderTooltip.text(txt);
        jQuery('#cashSlider').prevAll('.slider-tooltip-display').html(txt);
    }

    function valueChangedCash(elem) {
        jQuery("#cashSlider").slider("option", "value", calculateSliderValue(elem));
    }

    function updateCard() {
        var moValue = jQuery("#cardSlider").slider("value"),
            txt = '$' + addCommas(moValue);
        jQuery("#card").val(moValue);
        cardSliderTooltip.text(txt);
        jQuery('#cardSlider').prevAll('.slider-tooltip-display').html(txt);
    }

    function valueChangedCard(elem) {
        jQuery("#cardSlider").slider("option", "value", calculateSliderValue(elem));
    }

    function updateTax() {
        var moValue = jQuery("#taxSlider").slider("value"),
            txt = '$' + addCommas(moValue);
        jQuery("#tax").val(moValue);
        taxSliderTooltip.text(txt);
        jQuery('#taxSlider').prevAll('.slider-tooltip-display').html(txt);
    }

    function valueChangedTax(elem) {
        jQuery("#taxSlider").slider("option", "value", calculateSliderValue(elem));
    }

    function updateStudentDebt() {
        var moValue = jQuery("#studentdebtSlider").slider("value"),
            txt = '$' + addCommas(moValue);
        jQuery("#studentdebt").val(moValue);
        studentdebtSliderTooltip.text(txt);
        jQuery('#studentdebtSlider').prevAll('.slider-tooltip-display').html(txt);
    }

    function valueChangedStudentDebt(elem) {
        jQuery("#studentdebtSlider").slider("option", "value", calculateSliderValue(elem));
    }

    function calculateSliderValue(elem) {
        var val = jQuery(elem).val();
        if (val.length == 0) val = "0";
        val = parseFloat(val);
        var min = parseInt(jQuery(elem).attr('min'));
        var max = parseInt(jQuery(elem).attr('max'));
        var step = parseInt(jQuery(elem).attr('step'));
        if (val < min) val = min;
        else if (val > max) val = max;
        var multi = val / step;
        return multi * step;
    }

    function valueHomePrice(elem) {
        var val = jQuery(elem).val();
        if (val.length == 0) val = "0";
        val = parseInt(val);
        var min = parseInt(jQuery(elem).attr('min'));
        var max = parseInt(jQuery(elem).attr('max'));
        var step = parseInt(jQuery(elem).attr('step'));
        if (val < min) val = min;
        else if (val > max) val = max;
        var multi = val / step;
        jQuery("#homepriceSlider").slider("option", "value", multi * step);
    }

    function updateDownOnhome() {
        var homeValue = jQuery("#homepriceSlider").slider("value");
        jQuery("#homeprice").val(homeValue);
        jQuery("#downpaySlider").slider("option", "max", parseInt(homeValue));
        jQuery("#downpaySlider").slider("option", "value", jQuery("#downpaySlider").slider("value"));
        var txt = '$' + addCommas(homeValue);
        homepriceSliderTooltip.text(txt);
        jQuery('#homepriceSlider').prevAll('.slider-tooltip-display').html(txt);
    }

    function updateDownpay() {
        var val = jQuery("#downpaySlider").slider("value"),
            txt = '$' + addCommas(val);
        jQuery("#downpayment").val(val);
        downpaySliderTooltip.text(txt);
        jQuery('#downpaySlider').prevAll('.slider-tooltip-display').html(txt);
    }

    function valueDownPaym(elem) {
        var val = jQuery(elem).val();
        if (val.length == 0) val = "0";
        val = parseInt(val);
        var min = parseInt(jQuery(elem).attr('min'));
        var max = parseInt(jQuery("#homepriceSlider").slider("option", "max"));
        var step = parseInt(jQuery(elem).attr('step'));
        if (val < min) val = min;
        else if (val > max) val = max;
        var multi = val / step;
        jQuery("#downpaySlider").slider("option", "value", multi * step);
    }

    function updateIncome() {
        var moValue = jQuery("#incomeSlider").slider("value");
        jQuery("#income").val(moValue);
        incomeTooltip.text('$' + addCommas(moValue));
    }

    function valueChangedIncome(elem) {
        var val = jQuery(elem).val();
        if (val.length == 0) val = "0";
        val = parseInt(val);
        var min = parseInt(jQuery(elem).attr('min'));
        var max = parseInt(jQuery(elem).attr('max'));
        var step = parseInt(jQuery(elem).attr('step'));
        if (val < min) val = min;
        else if (val > max) val = max;
        var multi = val / step;
        jQuery("#incomeSlider").slider("option", "value", multi * step);
    }

    function bm_gf_extractUserInfo() {
        var nameParts = jQuery('#fullname').val().split(/\s+/);
        var user = {
            firstname: nameParts[0],
            lastname: nameParts[1],
            email: jQuery('#email').val(),
            phone: jQuery('#phone').val(),
            address: jQuery('#address').val(),
            city: jQuery('#city').val(),
            state: jQuery('#state').val(),
            zip: jQuery('#zip').val()
        };
        if(user.phone) {
            user.phone = user.phone.replace("(", "");
            user.phone = user.phone.replace(")", "");
            user.phone = user.phone.replace("-", "");
            user.phone = user.phone.replace(/ /g, "");
        }
        user.rlid = jQuery('#rlid').val();
        user.t = jQuery('#t').val();
        user.affid = jQuery('#affid').val();
        user.subid = jQuery('#subid').val();
        user.affsub = jQuery('#affsub').val();
        user.offerid = jQuery('#offerid').val();
        user.offerfileid = jQuery('#offerfileid').val();
        user.formid = jQuery('#formid').val();
        user.type = jQuery('#type').val();

        return user;
    }

    function bm_gf_storeUserData() {
        if(jQuery('#envPageCache').length > 0 && jQuery('#envPageCache').val() == 'true') {
            try {
                var user = bm_gf_extractUserInfo();
                if(jQuery.cookie) {
                    jQuery.cookie('bm_gm_userinfo', JSON.stringify(user), {path: '/'});
                }
            } catch(e){}

        }
    }

    return {
        addCommas: addCommas,
        createStateSelection: createStateSelection,
        init: init,
        showDebtStep: showDebtStep,
        advanceFromSolar: advanceFromSolar,
        ajaxSubmitMortgageLead: ajaxSubmitMortgageLead,
        pop: pop,
        ajaxUpdateLead: ajaxUpdateLead,
        skipSolar: skipSolar
    }

})();
