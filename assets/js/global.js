/* global realresponseScreenReaderText */
(function( $ ) {

	$(document).ready(function() {
		if(window.location.pathname.includes('checkout/order-received')) {
			_price = $('.total .woocommerce-Price-amount').text().replace('$', '');
			console.log(_price);
			fbq('track', 'Purchase', { 'currency' : 'AUD' , 'value' : _price });
		}
	});

	$(document).on('click', '.book-now', function(e) {
		fbq('track', 'BookingClicked');
	});

	$(document).on('click', '.single_add_to_cart_button', function(e) {
		fbq('track', 'AddToCart');
	});

	$(document).on('click', '.ajax_add_to_cart', function(e) {
		fbq('track', 'AddToCart');
	});

	$(document).on('click', '.contact_us', function(e) {
		fbq('track', 'Contact');
	});

	// Variables and DOM Caching.
	var $body = $( 'body' ),
		$customHeader = $body.find( '.custom-header' ),
		$branding = $customHeader.find( '.site-branding' ),
		$navigation = $body.find( '.navigation-top' ),
		$navWrap = $navigation.find( '.wrap' ),
		$navMenuItem = $navigation.find( '.menu-item' ),
		$menuToggle = $navigation.find( '.menu-toggle' ),
		$menuScrollDown = $body.find( '.menu-scroll-down' ),
		$sidebar = $body.find( '#secondary' ),
		$entryContent = $body.find( '.entry-content' ),
		$formatQuote = $body.find( '.format-quote blockquote' ),
		isFrontPage = $body.hasClass( 'realresponse-front-page' ) || $body.hasClass( 'home blog' ),
		navigationFixedClass = 'site-navigation-fixed',
		navigationHeight,
		navigationOuterHeight,
		navPadding,
		navMenuItemHeight,
		idealNavHeight,
		navIsNotTooTall,
		headerOffset,
		menuTop = 0,
		resizeTimer;

	// Ensure the sticky navigation doesn't cover current focused links.
	$( 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex], [contenteditable]', '.site-content-contain' ).filter( ':visible' ).focus( function() {
		if ( $navigation.hasClass( 'site-navigation-fixed' ) ) {
			var windowScrollTop = $( window ).scrollTop(),
				fixedNavHeight = $navigation.height(),
				itemScrollTop = $( this ).offset().top,
				offsetDiff = itemScrollTop - windowScrollTop;

			// Account for Admin bar.
			if ( $( '#wpadminbar' ).length ) {
				offsetDiff -= $( '#wpadminbar' ).height();
			}

			if ( offsetDiff < fixedNavHeight ) {
				$( window ).scrollTo( itemScrollTop - ( fixedNavHeight + 50 ), 0 );
			}
		}
	});
	if ( isFrontPage && ( $body.hasClass( 'has-header-image' ) || $body.hasClass( 'has-header-video' ) ) ) {
		headerOffset = $customHeader.innerHeight() - navigationOuterHeight;
	} else {
		headerOffset = $customHeader.innerHeight();
	}
	/*var top_html = jQuery("#top-nav").html();
	jQuery(".menu-mobile-menu-container ul").append(top_html);
	jQuery(".navigation-mobile .main-navigation > .menu-social-links-menu-container").html('');*/
	// Set properties of navigation.
	function setNavProps() {
		navigationHeight      = $navigation.height();
		navigationOuterHeight = $navigation.outerHeight();
		navPadding            = parseFloat( $navWrap.css( 'padding-top' ) ) * 2;
		navMenuItemHeight     = $navMenuItem.outerHeight() * 2;
		idealNavHeight        = navPadding + navMenuItemHeight;
		navIsNotTooTall       = navigationHeight <= idealNavHeight;
	}

	// Make navigation 'stick'.
	function adjustScrollClass() {
		// Make sure we're not on a mobile screen.
		if ( 'none' === $menuToggle.css( 'display' ) ) {
			// Make sure the nav isn't taller than two rows.
			//if ( navIsNotTooTall ) {
				if ( jQuery( window ).scrollTop() >= headerOffset ) {
					//$navigation.addClass( navigationFixedClass );
					//console.log('add');
					jQuery('.site-branding').addClass( navigationFixedClass );
				} else {
					//$navigation.removeClass( navigationFixedClass );
					//console.log('remove');
					jQuery('.site-branding').removeClass( navigationFixedClass );
				}
			//} else {
				// Remove 'fixed' class if nav is taller than two rows.
			//	$navigation.removeClass( navigationFixedClass );
			//}
		}
	}

	// Set margins of branding in header.
	function adjustHeaderHeight() {
		if ( 'none' === $menuToggle.css( 'display' ) ) {

			// The margin should be applied to different elements on front-page or home vs interior pages.
			if ( isFrontPage ) {
				$branding.css( 'margin-bottom', navigationOuterHeight );
			} else {
				$customHeader.css( 'margin-bottom', navigationOuterHeight );
			}

		} else {
			$customHeader.css( 'margin-bottom', '0' );
			$branding.css( 'margin-bottom', '0' );
		}
	}

	// Set icon for quotes.
	function setQuotesIcon() {
		$( realresponseScreenReaderText.quote ).prependTo( $formatQuote );
	}

	// Add 'below-entry-meta' class to elements.
	function belowEntryMetaClass( param ) {
		var sidebarPos, sidebarPosBottom;

		if ( ! $body.hasClass( 'has-sidebar' ) || (
			$body.hasClass( 'search' ) ||
			$body.hasClass( 'single-attachment' ) ||
			$body.hasClass( 'error404' ) ||
			$body.hasClass( 'realresponse-front-page' )
		) ) {
			return;
		}

		sidebarPos       = $sidebar.offset();
		sidebarPosBottom = sidebarPos.top + ( $sidebar.height() + 28 );

		$entryContent.find( param ).each( function() {
			var $element = $( this ),
				elementPos = $element.offset(),
				elementPosTop = elementPos.top;

			// Add 'below-entry-meta' to elements below the entry meta.
			if ( elementPosTop > sidebarPosBottom ) {
				$element.addClass( 'below-entry-meta' );
			} else {
				$element.removeClass( 'below-entry-meta' );
			}
		});
	}

	/*
	 * Test if inline SVGs are supported.
	 * @link https://github.com/Modernizr/Modernizr/
	 */
	function supportsInlineSVG() {
		var div = document.createElement( 'div' );
		div.innerHTML = '<svg/>';
		return 'http://www.w3.org/2000/svg' === ( 'undefined' !== typeof SVGRect && div.firstChild && div.firstChild.namespaceURI );
	}

	/**
	 * Test if an iOS device.
	*/
	function checkiOS() {
		return /iPad|iPhone|iPod/.test(navigator.userAgent) && ! window.MSStream;
	}

	/*
	 * Test if background-attachment: fixed is supported.
	 * @link http://stackoverflow.com/questions/14115080/detect-support-for-background-attachment-fixed
	 */
	function supportsFixedBackground() {
		var el = document.createElement('div'),
			isSupported;

		try {
			if ( ! ( 'backgroundAttachment' in el.style ) || checkiOS() ) {
				return false;
			}
			el.style.backgroundAttachment = 'fixed';
			isSupported = ( 'fixed' === el.style.backgroundAttachment );
			return isSupported;
		}
		catch (e) {
			return false;
		}
	}

	// Add header video class after the video is loaded.
	$( document ).on( 'wp-custom-header-video-loaded', function() {
		$body.addClass( 'has-header-video' );
	});

    $('.show-menu').click(function(e) {
        e.preventDefault();
        $('.slider-menu').css({ 'height' : '100vh' });
        $('body').addClass('no-scroll');
    });

    $('#close-menu').click(function(e) {
        e.preventDefault();
        $('.slider-menu').css({ 'height' : '0' });
        $('body').removeClass('no-scroll');
    });

    $('#side-nav a i.menu-open').click(function(e) {
    	e.preventDefault();
    	e.stopPropagation();

    	var sub = $(this).parents('li').children('.sub-menu');
    	$(sub).addClass('expanded');
    	$('#side-nav').addClass('sub-out');
        /*var diff = ($(sub).offset().top - $('#side-nav').offset().top) * -1;
        if($(sub).attr('id') !== 'side-nav') {
            $(sub).css({'top': diff + 'px'});
        }*/
	});

    $('#side-nav a i.menu-close').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        var sub = $(this).parents('ul');
        $(sub).removeClass('expanded');
        $('#side-nav').removeClass('sub-out');
    });

    $(window).scroll(function(e) {
    	headerScroll();
	});

    $(document).ready(function() {
    	headerScroll();

        $.each($('.slider-menu .menu-item-has-children'), function(k,v) {
    		//Update the first item to have the same text as the parent
        	var _text = '';
    		$.each($(v).children('a'), function(l,w) {
    			$.each($(w).children('span'), function(m,x) {
                    _text = $(x).text();
				});
			});
    		var _children = $(v).find('.sub-menu');
			$.each(_children, function(l,w) {
        		$.each($(w).find('.menu-title span'), function(m,x) {
					$(x).text(_text);
				});
			});
		});
	});

    function headerScroll() {
    	if(!$('body').hasClass('cq')) {
			var _amt = $(window).scrollTop();
			if (_amt > 100) {
				$('#masthead').addClass('pinned');
				$('#covid').addClass('pinned');
				$('#first-aid-header').addClass('pinned');
				$('#geelong').addClass('pinned');
				$('.slider-menu').addClass('pinned');
			} else {
				$('#masthead').removeClass('pinned');
				$('#covid').removeClass('pinned');
				$('#first-aid-header').removeClass('pinned');
				$('#geelong').removeClass('pinned');
				$('.slider-menu').removeClass('pinned');
			}
		}
	}

	$('#videoModal').on('hide.bs.modal', function(e) {
		if($('#youtube_player').length > 0) {
			$('#youtube_player').attr('src', '');
		}

		if($('#myVideo').length > 0) {
			document.getElementById("myVideo").pause();
		}
	});

	$('#videoModal').on('show.bs.modal', function(e) {
		if($('#youtube_player').length > 0) {
			$('#youtube_player').attr('src', $('#youtube_player').data('src'));
		}

		if($('#myVideo').length > 0) {
			document.getElementById("myVideo").play();
		}
	});

	$('header#geelong a').click(function(e) {
		e.preventDefault();
		$(window).scrollTop($('#footer').position().top - 100, 500, 'ease-in-out');
	});

	$('#intro-blocks .rr-next').on('click', function(e) {
    	introNext(e);
	});

	$('#intro-blocks .rr-prev').on('click', function(e) {
		introPrev(e);
	});

	$('#trainer-blocks .rr-next').on('click', function(e) {
		trainerNext(e);
	});

	$('#trainer-blocks .rr-prev').on('click', function(e) {
		trainerPrev(e);
	});

	$('#intro-blocks').swipe({
		swipeLeft:function(event, direction, distance, duration, fingerCount) {
			introNext(event);
		},
		swipeRight:function(event, direction, distance, duration, fingerCount) {
			introPrev(event);
		}
	});

	$('#trainer-blocks').swipe({
		swipeLeft:function(event, direction, distance, duration, fingerCount) {
			trainerNext(event);
		},
		swipeRight:function(event, direction, distance, duration, fingerCount) {
			trainerPrev(event);
		}
	});

	/*$('.dot.city').click(function() {
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$('.dot.city').removeClass('active');
			$this = $(this);
			$this.addClass('active');
		}
		filterJobs();
	});*/

	$('.job-skill').click(function() {
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$('.job-skill').removeClass('active');
			$this = $(this);
			$this.addClass('active');
		}
		filterJobs();
	});

	$('.filter-location').click(function(e) {
		filterJobs();
	});

	$('.filter-type').click(function(e) {
		filterJobs();
	});

	function filterJobs() {
		_location = [];
		$.each($('.filter-location input:checked'), function(k,v) {
			_location.push($(v).data('id'));
		});

		_type = [];
		$.each($('.filter-type input:checked'), function(k,v) {
			_type.push($(v).data('type'));
		});

		$.each($('.job'), function (k, v) {
			$(v).parent().addClass('visible').removeClass('hidden');
			//console.log($(v).data('locations'));
			if(_location.length > 0) {
				_contained = $(v).data('locations').some(function (v) {
					return _location.indexOf(v) >= 0;
				});

				console.log(_contained);
				if (_contained) {
					$(v).parent().addClass('visible');
				} else {
					$(v).parent().removeClass('visible').addClass('hidden');
				}
			} else {
				$(v).parent().removeClass('hidden').addClass('visible');
			}

			if(_type.length > 0) {
				//check type
				if (_type.includes($(v).data('type'))) {
					if (!$(v).parent().hasClass('hidden')) {
						$(v).parent().addClass('visible');
					}
				} else {
					$(v).parent().addClass('hidden');
				}
			}
		});
	}

	function introNext(e) {
		e.preventDefault();
		_parent = $(this).parent();
		//bump item 0 off and put it at the end
		$('#intro-blocks .rr-item:first').insertAfter($('#intro-blocks .rr-item:last'));
		$('#intro-blocks .rr-item').removeClass('active').removeClass('before').removeClass('after');
		$('#intro-blocks .rr-item:nth-child(1)').addClass('before');
		$('#intro-blocks .rr-item:nth-child(2)').addClass('active');
		$('#intro-blocks .rr-item:nth-child(3)').addClass('after');
	}

	function introPrev(e) {
		e.preventDefault();
		//bump item end off and put it at 0
		$('#intro-blocks .rr-item:last').insertBefore($('#intro-blocks .rr-item:first'));
		$('#intro-blocks .rr-item').removeClass('active').removeClass('before').removeClass('after');
		$('#intro-blocks .rr-item:nth-child(1)').addClass('before');
		$('#intro-blocks .rr-item:nth-child(2)').addClass('active');
		$('#intro-blocks .rr-item:nth-child(3)').addClass('after');
	}

	function trainerNext(e) {
		e.preventDefault();
		_parent = $(this).parent();
		$('#trainer-blocks .rr-item:first').insertAfter($('#trainer-blocks .rr-item:last'));
		$('#trainer-blocks .rr-item').removeClass('active').removeClass('before').removeClass('after');
		$('#trainer-blocks .rr-item:nth-child(1)').addClass('active');
	}

	function trainerPrev(e) {
		e.preventDefault();
		$('#trainer-blocks .rr-item:last').insertBefore($('#trainer-blocks .rr-item:first'));
		$('#trainer-blocks .rr-item').removeClass('active').removeClass('before').removeClass('after');
		$('#trainer-blocks .rr-item:nth-child(1)').addClass('active');
	}

	$(document).on('change', '#seeCourses', function(e) {
		e.preventDefault();
		//$('#axcel-courses').html('<span><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i><br>Loading available courses</span>');
		$this = $(this);

		window.location.href = $this.val();

		/*$.ajax({
			'url' : '/booking/axcel.php',
			'dataType' : 'html',
			'data' : {
				'aid' : $('#seeCourses option:selected').data('axid'),
				'cid' : $this.val(),
				'number' : 5,
				'more_button': true,
				'type' : 'w'
			},
			'success' : function(data) {
				$('#axcel-courses').html(data);
				$('[data-toggle="popover"]').popover();
				$this.children('i').removeClass('fa-spinner fa-pulse').addClass('fa-search');
			}
		});*/
	});

	$(document).on('change', '#location_id', function(e) {
		e.preventDefault();
		$('#axcel-courses').html('<span><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i><br>Loading available courses</span>');
		$this = $(this);

		$.ajax({
			'url' : '/booking/axcel.php',
			'dataType' : 'html',
			'data' : {
				'aid' : ($this.val() == 'Virtual' ? $('#v_axcel_id').val() : $('#axcel_id').val()),
				'cid' : ($this.val() == 'Virtual' ? $('#v_course_id').val() : $('#course_id').val()),
				'lid' : ($this.val() == 'Virtual' ? '' : $this.val()),
				'lpid' : ($this.val() == 'Virtual' ? '' : $('#location_id option:selected').data('id')),
				'widget' : 1,
				'number' : 20,
				'more_button': true,
				'type' : 'w',
				'showTimezone' : ($this.val() == 'Virtual' ? 1 : 0),
			},
			'success' : function(data) {
				$('#axcel-courses').html(data);
				$('[data-toggle="popover"]').popover();
				$this.children('i').removeClass('fa-spinner fa-pulse').addClass('fa-search');
			}
		});
	});

	$(document).on('change', '#see_virtual', function(e) {
		e.preventDefault();
		$('#axcel-courses').html('<span><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i><br>Loading available courses</span>');
		$this = $(this);
		$option = $('#see_virtual option:selected');

		$.ajax({
			'url' : '/booking/axcel.php',
			'dataType' : 'html',
			'data' : {
				'tid' : 2,
				'aid' : $option.data('axid'),
				'cid' : $this.val(),
				'number' : 20,
				'more_button': true,
				'type' : 'w',
				'showTimezone' : 1
			},
			'success' : function(data) {
				$('#axcel-courses').html(data);
				$('[data-toggle="popover"]').popover();
				$this.children('i').removeClass('fa-spinner fa-pulse').addClass('fa-search');
			}
		});
	});

	$(document).on('change', '#see_geelong', function(e) {
		e.preventDefault();
		$('#axcel-courses').html('<span><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i><br>Loading available courses</span>');
		$this = $(this);
		$option = $('#see_geelong option:selected');

		$.ajax({
			'url' : '/booking/axcel.php',
			'dataType' : 'html',
			'data' : {
				'aid' : $option.data('axid'),
				'cid' : $this.val(),
				'number' : 20,
				'more_button': true,
				'lid' : 'VIC',
				'geelong' : 1,
				'type' : 'w',
				'showTimezone' : 0
			},
			'success' : function(data) {
				$('#axcel-courses').html(data);
				$('[data-toggle="popover"]').popover();
				$this.children('i').removeClass('fa-spinner fa-pulse').addClass('fa-search');
			}
		});
	});

	$(document).on('click', '.training-location', function(e) {
		$this = $(this);
		//hide all other
		$('.training-location').removeClass('active');
		$this.addClass('active');
		//expand the list of courses at this location.
		$('.training-location-courses:not([data-id=' + $this.data('id') + '])').slideUp();
		$('.training-location-courses[data-id=' + $this.data('id') + ']').slideDown();
	});

	$(document).on('click', '.public-instance-button', function(e) {
		e.preventDefault();
		var _toencode;
		if($('body').hasClass('page-template-page_virtualfirstaid')) {
			_toencode = '{"iid" : ' + $(this).data('id') + ', "amt" : ' + $(this).data('amt') + ', "date" : "' + $(this).data('date') + '", "tid" : 2, "cid" : "' + $(this).data('cid') + '", "lid" : "' + $(this).data('lid') + '"}';
		} else {
			_toencode = '{"iid" : ' + $(this).data('id') + ', "amt" : ' + $(this).data('amt') + ', "date" : "' + $(this).data('date') + '", "tid" : ' + ($('#location_id').val() == 'Virtual' ? 2 : 1) + ', "cid" : "' + $(this).data('cid') + '", "lid" : "' + $(this).data('lid') + '"}';
		}

		var _enc = $.base64.encode(_toencode);

		window.location.href = '/booking/' + _enc;
	});
})( jQuery );

/*jQuery(document).ready(function() {
	setTimeout(function() {
		jQuery('.page-student-details form').areYouSure({
			'message' : 'You have not completed your enrolment. You will not be able to receive a certificate without first completing your enrolment.'
		});
	}, 3000);
});*/

jQuery(document).on('click', '.superior-faq-title a', function(e) {
	//alert('poop');
	e.preventDefault();
	e.stopPropagation();
});

jQuery(document).ready(function($) {
	$(document).on('click', '.mega-menu-item-has-children.sub-menu-header > a', function(e) {
		if($(window).width() < 769) {
			e.preventDefault();
			e.stopPropagation();
			let _parent = $(this).parent();
			_parent.toggleClass('expanded');
			_parent.children('.mega-sub-menu').slideToggle();
		}
	});

	//Fix up the top level ones
	$(document).on('click', '#mega-menu-mega > li > a', function(e) {
		if($(window).width() < 769) {
			e.preventDefault();
			e.stopPropagation();

			if ($(this).hasClass('mega-toggle-on')) {
				$(this).parent().removeClass('mega-toggle-on');
			}

			$(this).toggleClass('mega-toggle-on');
		}
	});

	$(document).on('click', '.mega-toggle-block', function(e) {
		$('#megamenu2').toggleClass('menu-open');
	});
});