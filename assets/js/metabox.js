jQuery(function($) {
    $('div[data-wpt-id="wpcf-where_train"] input[type="radio"]').each(function() {
        if ($(this).val() == 'Onsite' && $(this).is(":checked")) {
            console.log('onsite');
            $('#onsite-booking-box').show();
            $('#public-booking-box').hide();
        } else if($(this).val() == 'Public' && $(this).is(":checked")) {
            console.log('public');
            $('#public-booking-box').show();
            $('#onsite-booking-box').hide(); 
        }
    });
    $('div[data-wpt-id="wpcf-where_train"] input[type="radio"]').change(function() {
        if ($(this).val() == 'Onsite') {
            $('#public-booking-box').hide();
            $('#onsite-booking-box').show();
        } else {
            $('#public-booking-box').show();
            $('#onsite-booking-box').hide(); 
        }
    });
});