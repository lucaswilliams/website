'use strict';

angular.module('formApp', ['ngAnimate','revolunet.stepper'])
.controller('formCtrl', ['$scope', '$http', function($scope, $http) {
	$scope.formParams = {};
	$scope.stage = "";
	$scope.formValidation = false;
	$scope.toggleJSONView = false;
  	$scope.show_error = false;
	$scope.toggleFormErrorsView = false;
  	$scope.formParams.other_firstname = {};
  	$scope.formParams.other_lastname = {};
  	$scope.formParams.other_email = {};

	$scope.formParams.site_participants = 1;
	$scope.formParams.minsite_participants = 1;
	$scope.formParams.maxsite_participants = 30;
	// Navigation functions
	$scope.next = function (stage) {
		//$scope.direction = 1;
		//$scope.stage = stage;
		$scope.formValidation = true;

		if ($scope.multiStepForm.$valid) {
			$scope.direction = 1;
			$scope.stage = stage;
			$scope.setProgressBar(stage);
			$scope.formValidation = false;
			$scope.show_error = false;
		}else{
			$scope.show_error = true;
			$scope.validate_stage(stage);
		}
	};

	$scope.back = function (stage) {
		$scope.direction = 0;
		$scope.stage = stage;
		$scope.setProgressBar(stage);
	};
	$scope.validate_stage = function(stage){
		console.log(stage);
	}
	
  
  	$scope.change_participants = function(){
  		if($scope.formParams.public_participants > 1 || formParams.one_of == 'No'){
  			$scope.formParams.nextParticipantSlide = 'public_stage4';
  		}else{
  			$scope.formParams.nextParticipantSlide = 'payment';
  		}
  	}
	// Post to desired exposed web service.
	$scope.submitForm = function () {
		// multiStepForm.submit();
		var siteUrl = MyAjax.siteurl;
		var filter_ajaxurl = MyAjax.ajaxurl;

		// Check form validity and submit data using $http
		if ($scope.multiStepForm.$valid) {
			$scope.formValidation = false;
			jQuery.ajax({
		        url: filter_ajaxurl,
		        type: 'post',
		        dataType: 'json',
		        data: {
		            action: 'process_formdata',
		            form_data: $scope.formParams
		        },
		        success: function (response) {
		            console.log('response');
		            window.location.href = siteUrl+"/booking_payment";
		        }
		    });
		}
	};
  
	$scope.reset = function() {
		// Clean up scope before destorying
		$scope.formParams = {};
		$scope.stage = "";
	}
	$scope.participants_range = function(count){
		if($scope.formParams.one_of == 'Yes')
			count = count - 1;
		var ratings = []; 
		for (var i = 1; i <= count; i++) { 
			ratings.push(i) 
		} 
		return ratings;
	}
	$scope.setProgressBar = function(curStep){
		if('stage' == curStep) curStep = 1;
		if('site_stage1' == curStep) curStep = 2;
		if('site_stage2' == curStep) curStep = 3;
		if('site_stage3' == curStep) curStep = 4;
		if('site_stage4' == curStep) curStep = 5;
		if('site_stage5' == curStep) curStep = 6;
		if('public_stage1' == curStep) curStep = 2;
		if('public_stage2' == curStep) curStep = 3;
		if('public_stage3' == curStep) curStep = 4;
		if('public_stage4' == curStep) curStep = 5;
		if('contact_to_admin' == curStep) curStep = 6;
		if('summary' == curStep) curStep = 7;
		if('payment' == curStep) curStep = 8;
		if('finish' == curStep) curStep = 9;
	    var steps = 10;
	    var percent = parseFloat(100 / steps) * curStep;
	    //percent = percent.toFixed();
	    jQuery("#myBar")
	      .css("width",percent+"%");
	  }
}]);