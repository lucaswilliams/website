<?php
/**
 * Template Name:Booking Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Dev_Theme
 * @since Dev Theme 1.0
 */

get_header('booking'); ?>

<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                	
                    
                    <header class="entry-header">
						<div class="title-bg">
                        	<div class="auto-area">
                        		<h1 class="entry-title text-center"><?php the_title(); ?></h1>
                            </div>    
						</div>
					</header><!-- .entry-header -->
				<div class="auto-area">
					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'jext' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->
					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'jext' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
			
            	</div>
				</article><!-- #post -->
				<?php comments_template(); ?>
			<?php endwhile; ?>
		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_footer('booking'); ?>
