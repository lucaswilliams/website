<?php
/**
 * Template Name:New Booking
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Dev_Theme
 * @since Dev Theme 1.0
 */

if($_SERVER['HTTP_HOST'] == 'students.cqfirstaid.com.au') {
    get_header('cq');
} else {
    get_header('mega-menu');
}
?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<div id="section1" class="header-overlap booking-sec">
    <div class="banner-content booking">
        <div class="wrap">
        	<?php if($smalltitle){ ?>
            <h6><?php echo $smalltitle; ?></h6>
        	<?php } ?>
        	<?php if($largetitle){ ?>
            <h1><?php echo $largetitle; ?></h1>
            <?php } ?>
        </div>
    </div>
</div>

<?php
while ( have_posts() ) {
    the_post();
    
    $id = get_the_ID();
    $course = $_GET['course_id'] ?? 0;
    
    $intros = get_field('course_introductions', $id);
    if($intros) {
        echo '<div class="container">
            <div class="row">
                <div class="col">';

                    foreach($intros as $intro) {
                        if($intro['course_id'] == $course) {
                            echo '<h1>'.$intro['course_title'].'</h1>';
                            if(strlen($intro['course_description']) > 0) {
                                echo $intro['course_description'];
                            }
                        }
                    }
        echo        '</div>
                </div>
            </div>';
            
    }
?>
        <div class="container">
            <div class="row">
                <div class="col">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
<?php
}

if($_SERVER['HTTP_HOST'] == 'students.cqfirstaid.com.au') {
    get_footer('cq');
} else {
    get_footer();
}