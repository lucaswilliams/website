<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.2
 */

?>
</div>
<!-- #content -->
<?php 
?>
<div class="footer">
  <a href="#" class="topbutton bb" style="display: block;">Back to top</a>
  <div class="auto-area">
    <div class="top-footer">
	  <div class="footer-left">
      <div id="text-5" class="">
        
        </div>
	  
	  </div>
	   <div class="clear"></div>
    </div>
  </div>
</div>

<!-- #colophon -->
</div>
<!-- .site-content-contain -->
</div>
<!-- #page -->
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.min.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		
		//console.log(jQuery(window).width());
		if(jQuery(window).width() > 1280){
			jQuery('#how_slider').owlCarousel({
				//loop: true,
				margin:25,
			    nav:true,
			    dots:true,
			    items: 3,
			    stagePadding: 100
		  	});
	  	} else {
			jQuery('#how_slider').owlCarousel({
			    stagePadding: 50,
			    loop:true,
			    margin:25,
			    dots:true,
			    nav:true,
			    responsive:{
			    	0:{items:1},
			        320:{items:2},
					375:{items:2},
			        480:{items:2},
			        540:{items:2},
			        1100:{items:3}
			    }
		  	});
		}
	});
</script>
<?php wp_footer(); ?>

<script type="text/javascript"> _linkedin_partner_id = "293300"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=293300&fmt=gif" /> </noscript>
</body></html>