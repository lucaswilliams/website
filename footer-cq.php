<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.2
 */

?>
</div>
<!-- #content -->
<div class="footer">
    <div class="container">
        <div class="row" style="height: 150px">
            <div class="col-12 col-lg-6">
                <p>114b Hanson Road, Gladstone QLD 4680</p>
                <p>training@cqfirstaid.com.au | (07) 4978 1112</p>
                <p>&nbsp;</p>
                <p>RTO Code: 32398</p>
            </div>
            <div class="col-12 col-lg-6 text-right">
                <a data-testid="linkElement" href="https://www.instagram.com/cqfirstaid/" target="_blank" rel="noopener" class="_26AQd"><wix-image id="img_0_comp-ilkp6s7s" class="_1-6YJ uWpzU" data-image-info="{&quot;containerId&quot;:&quot;dataItem-k79ueuih-comp-ilkp6s7s&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:200,&quot;height&quot;:200,&quot;uri&quot;:&quot;e1aa082f7c0747168d9cf43e77046142.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-is-svg="false" data-is-svg-mask="false" data-image-zoomed="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/e1aa082f7c0747168d9cf43e77046142.png/v1/fill/w_39,h_39,al_c,q_85,usm_0.66_1.00_0.01/e1aa082f7c0747168d9cf43e77046142.webp"><img alt="Instagram" src="https://static.wixstatic.com/media/e1aa082f7c0747168d9cf43e77046142.png/v1/fill/w_39,h_39,al_c,q_85,usm_0.66_1.00_0.01/e1aa082f7c0747168d9cf43e77046142.webp" style="width: 39px; height: 39px; object-fit: cover;"></wix-image></a>
                <a data-testid="linkElement" href="https://www.facebook.com/CQFirstAid/" target="_blank" rel="noopener" class="_26AQd"><wix-image id="img_1_comp-ilkp6s7s" class="_1-6YJ uWpzU" data-image-info="{&quot;containerId&quot;:&quot;dataItem-img3yung-comp-ilkp6s7s&quot;,&quot;displayMode&quot;:&quot;fill&quot;,&quot;imageData&quot;:{&quot;width&quot;:200,&quot;height&quot;:200,&quot;uri&quot;:&quot;4057345bcf57474b96976284050c00df.png&quot;,&quot;displayMode&quot;:&quot;fill&quot;}}" data-has-bg-scroll-effect="" data-bg-effect-name="" data-is-svg="false" data-is-svg-mask="false" data-image-zoomed="" data-has-ssr-src="" data-src="https://static.wixstatic.com/media/4057345bcf57474b96976284050c00df.png/v1/fill/w_39,h_39,al_c,q_85,usm_0.66_1.00_0.01/4057345bcf57474b96976284050c00df.webp"><img alt="Facebook - Black Circle" src="https://static.wixstatic.com/media/4057345bcf57474b96976284050c00df.png/v1/fill/w_39,h_39,al_c,q_85,usm_0.66_1.00_0.01/4057345bcf57474b96976284050c00df.webp" style="width: 39px; height: 39px; object-fit: cover;"></wix-image></a>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-12">
                <p class="copyright">&copy; 2021 by CQ First Aid & Safety Pty Ltd All Rights Reserved</p>
            </div>
        </div>
    </div>
</div>

<!-- #colophon -->
</div>
<!-- .site-content-contain -->
</div>
<!-- #page -->
<?php wp_footer(); ?>


<script type="text/javascript"> _linkedin_partner_id = "293300"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=293300&fmt=gif" /> </noscript>
</body></html>