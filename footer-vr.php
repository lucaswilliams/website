<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.2
 */

?>
<!-- #content -->
<?php 
$show_author = get_field('show_page_author');
if($show_author) {
?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="page-author">
                    <?php
                        $author_id = $post->post_author;
                        $author = get_userdata($author_id);
                        $avatar = get_avatar($author);
                        
                        $original_date = get_the_time( 'U' );
                        $modified_date = get_the_modified_time( 'U' );
                        $date_format = get_option( 'date_format' );
                        if ( $modified_date >= $original_date + 86400 ) {
                            $post_date = 'Last updated: '.date($date_format, $modified_date);
                        } else {
                            $post_date = 'Posted on: '.date($date_format, $original_date);
                        }
                    ?>
                    <div class="row align-items-center">
                        <div class="col-4">
                            <?php echo $avatar; ?>
                        </div>
                        <div class="col-8">
                            <h2><?php echo $author->display_name; ?></h2>
                            <p><?php echo $post_date; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>
<div class="footer">
	<a href="#" class="topbutton bb">Back to top</a>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 footer-logo justify-content-center">
                <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='https://www.realresponse.com.au/wp-content/uploads/2018/04/footer-logo.png' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
                <?php
                if ( has_nav_menu( 'social' ) ) : ?>
                    <nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'realresponse' ); ?>">
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'social',
                            'menu_class'     => 'social-links-menu',
                            'depth'          => 1,
                            'link_before'    => '<span class="screen-reader-text">',
                            'link_after'     => '</span>' . realresponse_get_svg( array( 'icon' => 'chain' ) ),
                        ) );
                        ?>
                    </nav><!-- .social-navigation -->
                <?php endif; ?>
            </div>
            <div class="col-12 col-lg-6 footer-links">
                <div class="row">
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav1',
                            'menu_id'        => 'footer-nav1',
                            'walker' => new CSS_Menu_Maker_Walker()
                        ) ); ?>
                    </div>
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav2',
                            'menu_id'        => 'footer-nav2',
                        ) ); ?>
                    </div>
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav3',
                            'menu_id'        => 'footer-nav3',
                        ) ); ?>
                    </div>
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav4',
                            'menu_id'        => 'footer-nav4',
                        ) ); ?>
                    </div>
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav5',
                            'menu_id'        => 'footer-nav5',
                        ) ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col">
            <div id="feefo-service-review-carousel-widgetId" class="feefo-review-carousel-widget-service"></div>
        </div>
    </div>
</div>

<div class="copyright">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center text-md-left copy-text">
                <p>&copy; Copyright <?php echo date('Y'); ?> Real Response. All Rights Reserved.</p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
                <p><a href="<?php echo site_url(); ?>/privacy-policy/">Privacy Policy</a> | <a href="<?php echo site_url(); ?>/terms-conditions/">Terms &amp; Conditions</a></p>
            </div>
        </div>
    </div>
</div>

<!-- #colophon -->
</div>
<!-- .site-content-contain -->
</div>
<!-- #page -->
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.min.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		
		//console.log(jQuery(window).width());
		if(jQuery(window).width() > 1280){
			jQuery('#how_slider').owlCarousel({
				//loop: true,
				margin:25,
			    nav:true,
			    dots:true,
			    items: 3,
			    stagePadding: 100
		  	});
	  	} else {
			jQuery('#how_slider').owlCarousel({
			    stagePadding: 50,
			    loop:true,
			    margin:25,
			    dots:true,
			    nav:true,
			    responsive:{
			    	0:{items:1},
			        320:{items:2},
					375:{items:2},
			        480:{items:2},
			        540:{items:2},
			        1100:{items:3}
			    }
		  	});
		}
	});
</script>
<?php wp_footer(); ?>

<script>
    var _original;

    jQuery(document).ready(function($) {
        _original = parseInt($('#serious-games').css('background-size'));
        //console.log(_original);
        scrollLogo();
        scrollBlurb();
        scrollVR();

        $(window).scroll(function(e) {
            scrollLogo();
            scrollBlurb();
            scrollVR();
        });

        $(document).on('click', '.discover-expand', function(e) {
            e.preventDefault();
            $(this).prev().slideToggle();

            if($(this).text() == 'Discover') {
                $(this).text('See Less');
            } else {
                $(this).text('Discover');
            }
        })

        function scrollLogo() {
            if(window.innerWidth > 767) {
                headerbottom = document.getElementById('covid').getBoundingClientRect().bottom;
                zoom = document.getElementById('serious-games-zoom').getBoundingClientRect();
                bottom = document.getElementById('vr-services').getBoundingClientRect().top;

                if (zoom.top < headerbottom && zoom.bottom > headerbottom) {
                    //We might just be in the zoom area
                    //how far through it all are we?

                    //how many pixels left to the top.
                    toTop = Math.max(zoom.bottom - headerbottom);
                    //As a percentage
                    percent = 1 - (toTop / zoom.height);

                    //console.log(toTop, window.innerHeight, headerbottom);

                    if ((toTop - 1) <= (window.innerHeight - headerbottom)) {
                        $('html').addClass('locked');
                        percent = 1;
                        //fade in the rest of the page
                        $('#vr-pop-in').fadeIn();
                        $('#serious-games img').fadeOut();
                        $('#serious-games p').fadeOut(400, function () {
                            $('html').removeClass('locked');
                        });
                    } else {
                        $('#vr-pop-in').fadeOut();
                        $('#serious-games img').fadeIn();
                        $('#serious-games p').fadeIn();
                    }

                    scale = 1 + (percent * 25);
                }

                if (zoom.top > headerbottom) {
                    scale = 1;
                }

                //$('#serious-games').css({
                //    'background-size' : imgwidth
                //});

                $('#serious-games img').css({
                    'transform': 'scale(' + scale + ')'
                });
            }
        }

        function scrollBlurb() {
            if(window.innerWidth < 768) {
                headerbottom = document.getElementById('covid').getBoundingClientRect().bottom;
                zoom = document.getElementById('vr-blurb').getBoundingClientRect();
                windowSpace = window.innerHeight - headerbottom;

                if(zoom.top < windowSpace) {
                    //It's in view...
                    $('#vr-blurb p.fixed').css({ 'display' : 'flex'});
                }

                if(zoom.top > windowSpace) {
                    //It's way down the page
                    $('#vr-blurb p.fixed').hide();
                }

                if((zoom.height + zoom.top) < 0) {
                    //It's above where we are
                    $('#vr-blurb p.fixed').hide();
                }
            }
        }

        function scrollVR() {
            if(window.innerWidth > 767) {
                headerbottom = document.getElementById('covid').getBoundingClientRect().bottom;
                screenHeight = window.innerHeight - headerbottom;
                $.each($('.vr-story'), function (k, v) {
                    elem = document.getElementById($(v).attr('id')).getBoundingClientRect();
                    toppos = elem.top - headerbottom;
                    botpos = elem.bottom - headerbottom;
                    if (toppos < screenHeight && botpos > 0) {
                        //this element is visible
                        //how far scrolled is it?
                        _percent = (botpos / screenHeight) * 2;

                        $(v).find('.bg-image').css({'top': (_percent * 64) - 64 + 'px'});
                    }
                });
            }
        }
    });
</script>

<script type="text/javascript"> _linkedin_partner_id = "293300"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=293300&fmt=gif" /> </noscript>
</body></html>