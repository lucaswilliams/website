<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.2
 */

?>
</div> 
<!-- #content -->
<?php 
$show_author = get_field('show_page_author');
if($show_author) {
?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="page-author">
                    <?php
                        $author_id = $post->post_author;
                        $author = get_userdata($author_id);
                        $avatar = get_avatar($author);
                        
                        $original_date = get_the_time( 'U' );
                        $modified_date = get_the_modified_time( 'U' );
                        $date_format = get_option( 'date_format' );
                        if ( $modified_date >= $original_date + 86400 ) {
                            $post_date = 'Last updated: '.date($date_format, $modified_date);
                        } else {
                            $post_date = 'Posted on: '.date($date_format, $original_date);
                        }
                    ?>
                    <div class="row align-items-center">
                        <div class="col-4">
                            <?php echo $avatar; ?>
                        </div>
                        <div class="col-8">
                            <h2><?php echo $author->display_name; ?></h2>
                            <p><?php echo $post_date; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>
<div class="footer">
	<a href="#" class="topbutton bb">Back to top</a>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 footer-logo justify-content-center">
                <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='https://www.realresponse.com.au/wp-content/uploads/2018/04/footer-logo.png' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
                <?php
                if ( has_nav_menu( 'social' ) ) : ?>
                    <nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'realresponse' ); ?>">
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'social',
                            'menu_class'     => 'social-links-menu',
                            'depth'          => 1,
                            'link_before'    => '<span class="screen-reader-text">',
                            'link_after'     => '</span>' . realresponse_get_svg( array( 'icon' => 'chain' ) ),
                        ) );
                        ?>
                    </nav><!-- .social-navigation -->
                <?php endif; ?>
            </div>
            <div class="col-12 col-lg-6 footer-links">
                <div class="row">
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav1',
                            'menu_id'        => 'footer-nav1',
                            'walker' => new CSS_Menu_Maker_Walker()
                        ) ); ?>
                    </div>
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav2',
                            'menu_id'        => 'footer-nav2',
                        ) ); ?>
                    </div>
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav3',
                            'menu_id'        => 'footer-nav3',
                        ) ); ?>
                    </div>
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav4',
                            'menu_id'        => 'footer-nav4',
                        ) ); ?>
                    </div>
                    <div class="col-6 col-md-4">
                        <?php wp_nav_menu( array(
                            'theme_location' => 'footer-nav5',
                            'menu_id'        => 'footer-nav5',
                        ) ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyright">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center text-md-left copy-text">
                <p>&copy; Copyright <?php echo date('Y'); ?> Real Response. All Rights Reserved.</p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
                <p><a href="<?php echo site_url(); ?>/privacy-policy/">Privacy Policy</a> | <a href="<?php echo site_url(); ?>/terms-conditions/">Terms &amp; Conditions</a></p>
            </div>
        </div>
    </div>
</div>

<!-- #colophon -->
</div>
<!-- .site-content-contain -->
</div>
<!-- #page -->
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.min.js'></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		
		//console.log(jQuery(window).width());
		if(jQuery(window).width() > 1280){
			jQuery('#how_slider').owlCarousel({
				//loop: true,
				margin:25,
			    nav:true,
			    dots:true,
			    items: 3,
			    stagePadding: 100
		  	});
	  	} else {
			jQuery('#how_slider').owlCarousel({
			    stagePadding: 50,
			    loop:true,
			    margin:25,
			    dots:true,
			    nav:true,
			    responsive:{
			    	0:{items:1},
			        320:{items:2},
					375:{items:2},
			        480:{items:2},
			        540:{items:2},
			        1100:{items:3}
			    }
		  	});
		}
	});
</script>
<?php wp_footer(); ?>


<script type="text/javascript"> _linkedin_partner_id = "293300"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=293300&fmt=gif" /> </noscript>
</body></html>