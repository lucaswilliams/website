<?php
/**
 * Real Response functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 */

/**
 * Real Response only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

function wpse_69549_admin_debug( $user_login, $user )
{
    if($user->ID == 11) {
        setcookie( 'wp_debug', 'on', time() + 86400, '/', get_site_option( 'siteurl' ) );
    }
}
add_action( 'wp_login', 'wpse_69549_admin_debug', 10, 2 );

add_action('save_post', 'save_post_callback', 10, 3);
function save_post_callback($post_ID, $post, $update) {
    if($update) {
        return;
    }

    $dev = false;
    global $wpdb;
    if(DB_NAME == LIVE_DATABASE) {
        //We are in live, so copy to dev
        $find = ['www.realresponse', 'realresponse.local'];
        $replace = ['dev.realresponse', 'realresponse.dev'];
        $wpdb->select(DEV_DATABASE);
    } else {
        //We are in dev, so copy to live
        $dev = true;
        $find = ['dev.realresponse', 'realresponse.dev'];
        $replace = ['www.realresponse', 'realresponse.local'];
        $wpdb->select(LIVE_DATABASE);
    }

    //echo '<pre>'; var_dump($dev); echo '</pre>';
    $wpdb->show_errors(true);
    $params = [
        'ID' => $post_ID,
        'post_author' => $post->post_author,
        'post_date' => $post->post_date,
        'post_date_gmt' => $post->post_date_gmt,
        'post_content' => $post->post_content,
        'post_title' => $post->post_title,
        'post_excerpt' => $post->post_excerpt,
        'post_status' => ($dev ? 'draft' : $post->post_status),
        'comment_status' => $post->comment_status,
        'ping_status' => $post->ping_status,
        'post_password' => $post->post_password,
        'post_name' => $post->post_name,
        'to_ping' => $post->to_ping,
        'pinged' => $post->pinged,
        'post_modified' => $post->post_modified,
        'post_modified_gmt' => $post->post_modified_gmt,
        'post_content_filtered' => $post->post_content_filtered,
        'post_parent' => $post->post_parent,
        'guid' => str_replace($find, $replace,$post->guid),
        'menu_order' => $post->menu_order,
        'post_type' => $post->post_type,
        'post_mime_type' => $post->post_mime_type,
        'comment_count' => $post->comment_count,
    ];
    //echo '<pre>'; var_dump($params); echo '</pre>';
    //echo '<pre>'; var_dump($wpdb); echo '</pre>';
    $table_prefix = $wpdb->prefix;
    $wpdb->query($wpdb->prepare("REPLACE INTO ".$table_prefix."posts(ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES (%d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", $params));

    //reset environment.
    $wpdb->select(DB_NAME);
    //die();
}

add_action('edit_attachment', 'save_attachment_mb_data', 10, 1);
add_action('add_attachment', 'save_attachment_mb_data', 10, 1);
function save_attachment_mb_data( $post_id ) {
    global $wpdb, $table_prefix;

    $select = "SELECT * FROM ".$table_prefix."posts WHERE id = " . $post_id;
    $data = $wpdb->get_results($select)[0];

    $metaselect = "SELECT * FROM ".$table_prefix."postmeta WHERE post_id = " . $post_id;
    $meta = $wpdb->get_results($metaselect, ARRAY_A);
    //echo '<pre>'; var_dump($data); echo '</pre>';
    if(DB_NAME == LIVE_DATABASE) {
        //We are in live, so copy to dev
        //echo '<p>in live</p>';
        //$find = ['www.realresponse', 'realresponse.local'];
        //$replace = ['dev.realresponse', 'realresponse.dev'];
        $find = [];
        $replace = [];
        $wpdb->select(DEV_DATABASE);
    } else {
        //We are in dev, so copy to live
        //echo '<p>in dev</p>';
        $find = ['dev.realresponse', 'realresponse.dev'];
        $replace = ['www.realresponse', 'realresponse.local'];
        $wpdb->select(LIVE_DATABASE);
    }
    $wpdb->show_errors(true);
    $params = [
        'ID' => $post_id,
        'post_author' => $data->post_author,
        'post_date' => $data->post_date,
        'post_date_gmt' => $data->post_date_gmt,
        'post_content' => $data->post_content,
        'post_title' => $data->post_title,
        'post_excerpt' => $data->post_excerpt,
        'post_status' => $data->post_status,
        'comment_status' => $data->comment_status,
        'ping_status' => $data->ping_status,
        'post_password' => $data->post_password,
        'post_name' => $data->post_name,
        'to_ping' => $data->to_ping,
        'pinged' => $data->pinged,
        'post_modified' => $data->post_modified,
        'post_modified_gmt' => $data->post_modified_gmt,
        'post_content_filtered' => $data->post_content_filtered,
        'post_parent' => $data->post_parent,
        'guid' => str_replace($find, $replace,$data->guid),
        'menu_order' => $data->menu_order,
        'post_type' => $data->post_type,
        'post_mime_type' => $data->post_mime_type,
        'comment_count' => $data->comment_count,
    ];
    //echo '<pre>'; var_dump($params); echo '</pre>';
    //echo '<pre>'; var_dump($wpdb); echo '</pre>';
    $wpdb->query($wpdb->prepare("REPLACE INTO ".$table_prefix."posts(ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count) VALUES (%d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", $params));

    //Now drop all postmeta for this post
    $wpdb->query($wpdb->prepare("DELETE FROM ".$table_prefix."postmeta WHERE post_id = %d", [$post_id]));

    //Copy postmeta from one DB to other
    foreach($meta as $metarow) {
        //echo '<pre>'; var_dump($metarow); echo '</pre>';
        unset($metarow['meta_id']);
        $wpdb->insert($table_prefix.'postmeta', $metarow);
    }

    //reset environment.
    $wpdb->select(DB_NAME);
    //die();
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function realresponse_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/realresponse
	 * If you're building a theme based on Real Response, use a find and replace
	 * to change 'realresponse' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'realresponse' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'realresponse-featured-image', 2000, 1200, true );

	add_image_size( 'realresponse-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'         => __( 'Top Menu', 'realresponse' ),
		'sub'         => __( 'Sub Menu', 'realresponse' ),
		'social'      => __( 'Social Links Menu', 'realresponse' ),
        'mob-nav'     => __( 'Mobile Menu', 'realresponse' ),
        'footer-nav1' => __( 'Footer Menu 1', 'realresponse' ),
        'footer-nav2' => __( 'Footer Menu 2', 'realresponse' ),
        'footer-nav3' => __( 'Footer Menu 3', 'realresponse' ),
        'footer-nav4' => __( 'Footer Menu 4', 'realresponse' ),
        'footer-nav5' => __( 'Footer Menu 5', 'realresponse' ),
        'mega' => __( 'Mega Menu', 'realresponse')
	) );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', realresponse_fonts_url() ) );
}
add_action( 'after_setup_theme', 'realresponse_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function realresponse_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( realresponse_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Real Response content width of the theme.
	 *
	 * @since Real Response 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'realresponse_content_width', $content_width );
}
add_action( 'template_redirect', 'realresponse_content_width', 0 );

/**
 * Register custom fonts.
 */
function realresponse_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'realresponse' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Real Response 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function realresponse_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'realresponse-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'realresponse_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function realresponse_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'realresponse' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'realresponse' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'realresponse' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'realresponse' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'realresponse' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'realresponse' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Header Cart info', 'realresponse' ),
		'id'            => 'cart-info',
		'description'   => __( 'Appears on header.', 'realresponse' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Shop page header button', 'realresponse' ),
		'id'            => 'shop-header-button',
		'description'   => __( 'Appears on header.', 'realresponse' ),
		'before_widget' => '<div id="%1$s" class="enquire_btn_top %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'realresponse_widgets_init' );

function custom_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'featured_image' => 'Image',
        'title' => 'Title',
        'comments' => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
        'date' => 'Date'
    );
    return $columns;
}
add_filter('manage_rfa_client_posts_columns' , 'custom_columns');
add_filter('manage_rfa_people_posts_columns' , 'custom_columns');

function custom_columns_data( $column, $post_id ) {
    switch ( $column ) {
        case 'featured_image':
            the_post_thumbnail( [64, 64] );
            break;
    }
}
add_action( 'manage_rfa_client_posts_custom_column' , 'custom_columns_data', 10, 2 );
add_action( 'manage_rfa_people_posts_custom_column' , 'custom_columns_data', 10, 2 );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Real Response 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function realresponse_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'realresponse' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'realresponse_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Real Response 1.0
 */
function realresponse_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'realresponse_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function realresponse_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'realresponse_pingback_header' );

/**
 * Display custom color CSS.
 */
function realresponse_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo realresponse_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'realresponse_colors_css_wrap' );

/**
 * Enqueue scripts and styles.
 */
function realresponse_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'realresponse-fonts', realresponse_fonts_url(), array(), null );

	// Theme stylesheet.
	wp_enqueue_style( 'realresponse-style', get_stylesheet_uri() );

	// Load the dark colorscheme.
	if ( 'dark' === get_theme_mod( 'colorscheme', 'light' ) || is_customize_preview() ) {
		wp_enqueue_style( 'realresponse-colors-dark', get_theme_file_uri( '/assets/css/colors-dark.css' ), array( 'realresponse-style' ), '1.0' );
	}

	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'realresponse-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'realresponse-style' ), '1.0' );
		wp_style_add_data( 'realresponse-ie9', 'conditional', 'IE 9' );
	}

	wp_enqueue_style( 'lightgalery', get_theme_file_uri( '/assets/css/lightgallery.css' ), array( 'realresponse-style' ), '1.0' );

	/*wp_enqueue_style( 'colorbox-css', get_template_directory_uri() . '/assets/css/colorbox.css', array( 'realresponse-style' ), '1.0');*/

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'realresponse-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'realresponse-style' ), '1.0' );
	wp_enqueue_style( 'realresponse-team', get_theme_file_uri( '/assets/css/team.css' ), array(  ), '1.0' );
	wp_enqueue_script( 'realresponse-team', get_theme_file_uri( '/assets/js/ats.css' ), array(  ), '1.0' );

	wp_style_add_data( 'realresponse-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'realresponse-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$realresponse_l10n = array(
		'quote'          => realresponse_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	if ( has_nav_menu( 'top' ) ) {
		wp_enqueue_script( 'realresponse-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
		$realresponse_l10n['expand']         = __( 'Expand child menu', 'realresponse' );
		$realresponse_l10n['collapse']       = __( 'Collapse child menu', 'realresponse' );
		$realresponse_l10n['icon']           = realresponse_get_svg( array( 'icon' => 'angle-down' ) );
	}
	

	wp_enqueue_script( 'realresponse-global', get_theme_file_uri( '/assets/js/global.js' ), array( 'jquery' ), '1.0', true );

	wp_enqueue_script( 'jquery-scrollto', get_theme_file_uri( '/assets/js/jquery.scrollTo.js' ), array( 'jquery' ), '2.1.2', true );

	wp_localize_script( 'realresponse-skip-link-focus-fix', 'realresponseScreenReaderText', $realresponse_l10n );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
  
    //Bootstrap
    wp_enqueue_style( 'bootstrap_4_base-bootstrap-css', get_theme_file_uri('css/bootstrap.css') );
    wp_enqueue_style( 'bootstrap_4_base-style', get_theme_file_uri('css/custom.css'), [], time() );
    wp_enqueue_script( 'jquery-bootstrap-popper', get_theme_file_uri( '/js/bootstrap.popper.min.js' ), array( 'jquery' ), '4.0.0', true );
    wp_enqueue_script( 'jquery-bootstrap', get_theme_file_uri( '/js/bootstrap.min.js' ), array( 'jquery-bootstrap-popper' ), '4.0.0', true );
    //wp_enqueue_script( 'jquery-areyousure', get_theme_file_uri( '/js/jquery.are-you-sure.js' ), array( 'jquery' ), '4.0.0', true );
    wp_enqueue_script( 'jquery-swipe', get_theme_file_uri( '/assets/js/jquery.swipe.js' ), array( 'jquery' ), '4.0.0', false );
    wp_enqueue_script( 'jquery-base64', get_theme_file_uri( '/assets/js/jquery.base64.min.js' ), array( 'jquery' ), '4.0.0', false );
}
add_action( 'wp_enqueue_scripts', 'realresponse_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Real Response 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function realresponse_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'realresponse_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Real Response 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function realresponse_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'realresponse_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Real Response 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function realresponse_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'realresponse_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Real Response 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function realresponse_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'realresponse_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Real Response 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function realresponse_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'realresponse_widget_tag_cloud_args' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );
/**
 *  Theme option for admin
 */
require get_parent_theme_file_path( '/inc/realresponse-theme-functions.php' );

function wpdocs_theme_name_scripts() {
	
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/css/font-awesome.css');
	//wp_enqueue_style( 'responsive', get_template_directory_uri() . '/assets/css/responsive.css');
	
	// validation
	wp_enqueue_script( 'review', 'https://api.feefo.com/api/javascript/real-first-aid', array( 'jquery' ), '1.0', TRUE );
	wp_enqueue_script( 'cookies-js', get_template_directory_uri() . '/assets/js/jquery.cookie.js', array( 'jquery' ), '1.0', TRUE );
	wp_enqueue_script( 'min-js', get_template_directory_uri() . '/assets/js/jquery-ui.min.js', array( 'jquery' ), '1.0', TRUE );

	wp_enqueue_script( 'lightgallery', get_template_directory_uri() . '/assets/js/lightgallery.js', array( 'jquery' ), '1.0', TRUE );

	wp_enqueue_script( 'lg-video', get_template_directory_uri() . '/assets/js/lg-video.js', array( 'jquery' ), '1.0', TRUE );

	wp_enqueue_script( 'angular-js', get_template_directory_uri() . '/assets/js/angular.min.js', array( 'jquery' ) );

	wp_enqueue_script( 'animate-js', get_template_directory_uri() . '/assets/js/angular-animate.min.js', array( 'jquery' ), '1.0', TRUE );

	wp_enqueue_script( 'custom_form_js-js', get_template_directory_uri() . '/assets/js/custom_form_js.js', array( 'jquery' ), '1.0', TRUE );

	/*wp_enqueue_script( 'jquery.colorbox-js', get_template_directory_uri() . '/assets/js/jquery.colorbox.js', array( 'jquery' ), '1.0', TRUE );*/

	wp_enqueue_script( 'ngformapp', get_template_directory_uri() . '/assets/js/ngformapp.js', array( 'jquery' ) );

	wp_enqueue_script( 'custom_js', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), '1.0', TRUE );
	wp_localize_script( 'custom_js', 'realresponse', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	if ( has_nav_menu( 'mob-nav' ) ) {
		//wp_enqueue_script( 'realresponse-navigation-mob', get_theme_file_uri( '/assets/js/navigation-mob.js' ), array( 'jquery' ), '1.0', true );
		//$realresponse_l10n['expand']         = __( 'Expand child menu', 'realresponse' );
		//$realresponse_l10n['collapse']       = __( 'Collapse child menu', 'realresponse' );
		//$realresponse_l10n['icon']           = realresponse_get_svg( array( 'icon' => 'angle-left') );
	}
	
	

}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

function realresponse_theme_customizer( $wp_customize ) {
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo', array(
		'label'    => __( 'Logo', 'realresponse' ),
		'section'  => 'footer_logo_section',
		'settings' => 'footer_logo',
	) ) );
	$wp_customize->add_section( 'footer_logo_section' , array(
    'title'       => __( 'Footer Logo', 'realresponse' ),
    'priority'    => 30,
    'description' => 'Upload a logo  for footer',
	) );
	$wp_customize->add_setting( 'footer_logo' );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo', array(
    'label'    => __( 'Footer Logo', 'realresponse' ),
    'section'  => 'footer_logo_section',
    'settings' => 'footer_logo',
) ) );
	return $wp_customize;
}
add_action( 'customize_register', 'realresponse_theme_customizer' );
function wpc_custom_front_sections( $num_sections )
{
	return 7; //Change this number to change the number of the sections.
}
add_filter( 'realresponse_front_page_sections', 'wpc_custom_front_sections' );
class My_Walker_Nav_Menu extends Walker_Nav_Menu {
  function start_lvl(&$output, $depth = 0, $args = Array()) {
	$indent = str_repeat("\t", $depth);
	$output .= "\n$indent<ul class=\"sub-menu\">\n<div class=\"wrap\">";
  }
  function end_lvl(&$output, $depth = 0, $args = Array()) {
	$output .= '</div></ul>';
	}
}

class Side_Nav_Menu_Walker extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = Array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-menu\">\n";
        if($depth == 0) {
            $output .= '<li><a href="#" class="menu-title"><span>First Aid Courses</span><i class="fa fa-chevron-left menu-close"></i></a>';
        }
    }
    function end_lvl(&$output, $depth = 0, $args = Array()) {
        $output .= '</ul>';
    }
    
    /**
     * Start the element output.
     *
     * @see Walker::start_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;
        /**
         * Filter the CSS class(es) applied to a menu item's list item element.
         *
         * @since 3.0.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
         * @param object $item    The current menu item.
         * @param array  $args    An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth   Depth of menu item. Used for padding.
         */
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        // New
        $class_names .= ' nav-item';
        
        if (in_array('menu-item-has-children', $classes)) {
            $class_names .= ' dropdown';
        }
        if (in_array('current-menu-item', $classes)) {
            $class_names .= ' active';
        }
        //
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
        // print_r($class_names);
        /**
         * Filter the ID applied to a menu item's list item element.
         *
         * @since 3.0.1
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
         * @param object $item    The current menu item.
         * @param array  $args    An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth   Depth of menu item. Used for padding.
         */
        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
        // New
        if ($depth === 0) {
            $output .= $indent . '<li' . $id . $class_names .'>';
        }
        //
        // $output .= $indent . '<li' . $id . $class_names .'>';
        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';
        // New
        /*if ($depth === 0) {
            $atts['class'] = 'nav-link';
        }
        if ($depth === 0 && in_array('menu-item-has-children', $classes)) {
            $atts['class']       .= ' dropdown-toggle';
            $atts['data-toggle']  = 'dropdown';
        }
        if ($depth > 0) {
            $atts['class'] = 'dropdown-item';
        }
        if (in_array('current-menu-item', $item->classes)) {
            $atts['class'] .= ' active';
        }*/
        // print_r($item);
        //
        /**
         * Filter the HTML attributes applied to a menu item's anchor element.
         *
         * @since 3.6.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array $atts {
         *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
         *
         *     @type string $title  Title attribute.
         *     @type string $target Target attribute.
         *     @type string $rel    The rel attribute.
         *     @type string $href   The href attribute.
         * }
         * @param object $item  The current menu item.
         * @param array  $args  An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth Depth of menu item. Used for padding.
         */
        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }
        $item_output = $args->before;
        // New
        /*
        if ($depth === 0 && in_array('menu-item-has-children', $classes)) {
            $item_output .= '<a class="nav-link dropdown-toggle"' . $attributes .'data-toggle="dropdown">';
        } elseif ($depth === 0) {
            $item_output .= '<a class="nav-link"' . $attributes .'>';
        } else {
            $item_output .= '<a class="dropdown-item"' . $attributes .'>';
        }
        */
        //
        $item_output .= '<a'. $attributes .'><span>';
        /** This filter is documented in wp-includes/post-template.php */
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        if ($depth === 0 && in_array('menu-item-has-children', $classes)) {
            $item_output .= '</span><i class="fa fa-chevron-right menu-open"></i>';
        }
        $item_output .= '</a>';
        $item_output .= $args->after;
        /**
         * Filter a menu item's starting output.
         *
         * The menu item's starting output only includes `$args->before`, the opening `<a>`,
         * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
         * no filter for modifying the opening and closing `<li>` for a menu item.
         *
         * @since 3.0.0
         *
         * @param string $item_output The menu item's starting HTML output.
         * @param object $item        Menu item data object.
         * @param int    $depth       Depth of menu item. Used for padding.
         * @param array  $args        An array of {@see wp_nav_menu()} arguments.
         */
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
    /**
     * Ends the element output, if needed.
     *
     * @see Walker::end_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Page data object. Not used.
     * @param int    $depth  Depth of page. Not Used.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if (isset($args->has_children) && $depth === 0) {
            $output .= "</li>\n";
        }
    }
}

class My_Walker_Nav_Menu_mobile extends Walker_Nav_Menu {

}
class CSS_Menu_Maker_Walker extends Walker {

  var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );
  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class='sub-menu'>\n";
  }

  function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }
  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $class_names = $value = '';
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    /*/* Add active class
    if(in_array('current-menu-item', $classes)) {
      $classes[] = 'active';
      unset($classes['current-menu-item']);
    }
*/
    /* Check for children */
	$html = "";
    $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
    if (!empty($children)) {
      $classes[] = 'has-sub';
	  $html = realresponse_get_svg( array( 'icon' => 'angle-down') );
    }

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= '</a>'. $html ;
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  function end_el( &$output, $item, $depth = 0, $args = array() ) {
    $output .= "</li>\n";
  }
}

/*remove_filter('the_content', 'wpautop');

function get_page_and_add_autop( $content ) {
    global $post;
    //echo $post->post_type.' '.$post->post_name;
    if($post->post_type == 'page' && $post->post_name == 'home-2') {
        add_filter('the_content', 'wpautop', 9999);
    }
    return $content;
}
add_filter( 'the_content', 'get_page_and_add_autop' );*/

function custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function rr_gallery_listing() {
?>
<div class="gallery">
	<ul>
	<?php
    global $post;
    $args = array( 'posts_per_page' => 5, 'post_type' => 'ygallery');
    $myposts = get_posts( $args );
    foreach ( $myposts as $post ){
      setup_postdata( $post ); ?>
        <?php
			$v_url = get_the_post_video_url($post->ID);
			$v_url2 = substr(strrchr($v_url, '/'), 1);
			 ?>
			<li class="box" data-responsive="<?php echo "<div class='vid'>". get_the_post_video_url($post->ID)."</div>";?>" data-src="<?php echo "<div class='vid'>". get_the_post_video_url($post->ID)."</div>";?>" data-sub-html='<h3 class="post-title"><?php echo the_title(); ?></h3>
				<?php echo '<div class="video-content">'. the_content().'</div>'; ?>
				<a class="enq_btn" href="/contact-us/">ENQUIRE ABOUT a course like this </a>'>
				<?php echo do_shortcode('[wp_colorbox_media url="'.$v_url.'" type="youtube" hyperlink="click here to pop up youtube video"]');?>
				<h3><?php echo the_title(); ?></h3>
			</li>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
    <?php }
    wp_reset_postdata(); ?>
    </ul>
</div>
<?php }
add_shortcode( 'rr_gallery', 'rr_gallery_listing' );?>
<?php
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
	$title= $form['button']['text'];
	$id = $form['id'];
    return "<button class='btn btn-primary' id='gform_submit_button_{$id}'><span class='inner'>$title</span></button>";
}

// Woocommerece custimization start by vaishali 
/*remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10);
remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_price',10);
remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating',5);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart',10);
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail',10);
add_action('woocommerce_shop_loop_item_title' , 'custom_product_order',10); */
function custom_product_order(){
	add_action('woocommerce_after_shop_loop_item_title','woocommerce_start_product_details_div',1);
			add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_title',5);
			add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price',10);
			add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating',15);
		add_action('woocommerce_after_shop_loop_item_title', 'div_ends',15);
		add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_get_product_short_description',16);
	
		add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_get_product_content_left',16);
			add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_thumbnail_div_start',16);
			add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail',16);
			add_action('woocommerce_after_shop_loop_item_title', 'div_ends',16);
			add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_get_related_product',17);
		add_action('woocommerce_after_shop_loop_item_title', 'div_ends',17);
		
		add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_get_product_content_right', 20);
			add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_get_product_content', 20);
		add_action('woocommerce_after_shop_loop_item_title', 'div_ends',20);
	add_action('woocommerce_after_shop_loop_item_title', 'div_ends',22);
	
	add_action('woocommerce_after_shop_loop_item_title','woocommerce_start_links_div',25);
		add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_read_more_link', 25);
		add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart',25);
	add_action('woocommerce_after_shop_loop_item_title','woocommerce_end_links_div',25);
}
function div_ends(){
	echo '</div>';
}
function woocommerce_thumbnail_div_start(){
	echo '<div class="product_image">';
}
function woocommerce_get_product_content_left(){
	echo '<div class="left">';
}
function woocommerce_get_product_content_right(){
	/*global $product;
	echo strlen($product->post->post_content);*/
	echo '<div class="right product_content">';
}
function woocommerce_get_product_content(){
	global $post;
	echo $post->post_content;
}
function woocommerce_get_product_short_description(){
	echo '<div class="short_desc">';
	wc_get_template( 'single-product/short-description.php' );
	echo '</div>';
}

add_action( 'after_setup_theme', 'wc_support' );
function wc_support() {
add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

//Product Cat Create page
function wh_taxonomy_add_new_meta_field() {
    ?>
    <div class="form-field">
        <label for="wh_meta_bottom_desc"><?php _e('Category Bottom Description', 'wh');?></label>
        <?php wp_editor( $wh_meta_bottom_desc, 'wh_meta_bottom_desc' );?>
        <p class="description"><?php _e('Enter a meta description, <= 160 character', 'wh'); ?></p>
    </div>
    <?php
}
//Product Cat Edit page
function wh_taxonomy_edit_meta_field($term) {
    $term_id = $term->term_id;
    $wh_meta_bottom_desc = get_term_meta($term_id, 'wh_meta_bottom_desc', true);
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="wh_meta_bottom_desc"><?php _e('Category Bottom  Description', 'wh'); ?></label></th>
        <td>
        <?php wp_editor( $wh_meta_bottom_desc, 'wh_meta_bottom_desc' );?>
            <p class="description"><?php _e('Enter a category bottom description', 'wh'); ?></p>
        </td>
    </tr>
    <?php
}
add_action('product_cat_add_form_fields', 'wh_taxonomy_add_new_meta_field', 10, 1);
add_action('product_cat_edit_form_fields', 'wh_taxonomy_edit_meta_field', 10, 1);
remove_filter("wh_meta_bottom_desc", "wptexturize");
remove_filter("wh_meta_bottom_desc", "convert_chars");
// Save extra taxonomy fields callback function.
function wh_save_taxonomy_custom_meta($term_id) {
    $wh_meta_bottom_desc = filter_input(INPUT_POST, 'wh_meta_bottom_desc');
    
    update_term_meta($term_id, 'wh_meta_bottom_desc', $wh_meta_bottom_desc);
}
add_action('edited_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
add_action('create_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
/*========== Remove Category Column on Wordpress Category Listing Page ==========*/
function cap_remove_category_desc_cat_listing($columns){
	// only edit the columns on the current taxonomy
	if ( !isset($_GET['taxonomy']) || ($_GET['taxonomy'] != 'category' && $_GET['taxonomy'] != 'product_cat') )
	    return $columns;
	// unset the description columns
	if ( $posts = $columns['description'] ){ unset($columns['description']); }
	return $columns;
}
add_filter('manage_edit-category_columns','cap_remove_category_desc_cat_listing');
add_filter('manage_edit-product_cat_columns','cap_remove_category_desc_cat_listing');
function product_filter(){
	dynamic_sidebar('product_filter');
}
function checkout_steps_html(){
	?><div class="steps">
		<ul>
			<li class="cart <?php echo (is_cart())?'active':'';?>"><span>1</span>Shopping Cart</li>
			<li class="chekout <?php echo (is_checkout() && !is_order_received_page())?'active':'';?>"><span>2</span>Payment &amp; Shipping Details</li>
			<li class="thankyou <?php echo (is_order_received_page())?'active':'';?>"><span>3</span>Thank You</li>
		</ul>
		<div class="clear"></div>
	</div>
	<?php
}
add_action('woocommerce_before_thank_page','checkout_steps_html',20);
add_action('woocommerce_before_cart','checkout_steps_html',20);
add_action('woocommerce_before_checkout_form','checkout_steps_html',20);
function gb_change_cart_string($translated_text, $text, $domain) {
	$translated_text = str_replace("basket", "cart", $translated_text);
	$translated_text = str_replace("Basket", "Cart", $translated_text);
	return $translated_text;
}
add_filter("gettext", "gb_change_cart_string", 100, 3);
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = -1;
  return $cols;
}
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
add_action( 'woocommerce_custom_checkout_cart_summary', 'woocommerce_order_review', 10 );
add_filter( 'woocommerce_checkout_fields' , 'alter_woocommerce_checkout_fields' );
function alter_woocommerce_checkout_fields( $fields ) {
     unset($fields['order']['order_comments']);
     return $fields;
}
add_action('woocommerce_checkout_buttons','back_to_cart_link',5);
function back_to_cart_link(){
    //get the cart link
    global $woocommerce;
    $cartUrl = $woocommerce->cart->get_cart_url();
    //the HTML markup to add
    $backToCartLink="<p class='checkout_buttons'><a class='alt' href='".$cartUrl."'> <i class='fa fa-angle-left' aria-hidden='true'></i>
 Back to shopping Cart</a>";
    echo $backToCartLink;
	do_action( 'woocommerce_review_order_before_submit' ); ?>
	<?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="btn btn-primary" name="woocommerce_checkout_place_order" id="place_order" value="Confirm Order" data-value="Confirm Order" />' ); ?>
	<?php do_action( 'woocommerce_review_order_after_submit' );
	echo "</p>";
}
add_action('woocommerce_after_thank_page','back_to_cart_link_thankyoupage',5);
function back_to_cart_link_thankyoupage(){
    //get the cart link
    global $woocommerce;
    $shopUrl = get_permalink( woocommerce_get_page_id( 'shop' ) );
    //the HTML markup to add
    echo "<p class='checkout_buttons'><a class='alt' href='".$shopUrl."'> &lt; Back to Shop</a>";
	echo "</p>";
}
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
function bk_title_order_received( $title, $id ) {
	if ( is_order_received_page() && get_the_ID() === $id ) {
		$title = "Thank you";
	}
	if( is_cart() && get_the_ID() == $id){
		$title = "Shopping Cart";
	}
	if( is_checkout() && !is_order_received_page() && get_the_ID() == $id){
		$title = "Payment and Shipping Details";
	}
	return $title;
}
add_filter( 'the_title', 'bk_title_order_received', 10, 2 );
//add_filter('wp_nav_menu_items','sk_wcmenucart', 10, 2);
add_filter('add_to_cart_fragments', 'header_add_to_cart_fragment');
function header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
 
    ob_start();
	?>
    <a class="wcmenucart-contents cart-button" href="<?php echo $woocommerce->cart->get_cart_url();?>" title="View your shopping cart"><i class="fa fa-shopping-cart"></i> <span class="count"><?php echo $woocommerce->cart->cart_contents_count;?></span></a>
	<?php
    $fragments['a.cart-button'] = ob_get_clean();
     return $fragments;
}
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
add_action( 'woocommerce_after_cart', 'woo_add_continue_shopping_button_to_cart' ,15);
function woo_add_continue_shopping_button_to_cart() {
	 $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
	 echo '<div class="cart_bottom clear">';
	 echo ' <a href="'.$shop_page_url.'" class="button linkbutton"> <i class="fa fa-angle-left" aria-hidden="true"></i> Continue Shopping </a>';
	 do_action('woocommerce_proceed_to_checkout');
	 echo '</div>';
}
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11);
function dequeue_woocommerce_cart_fragments() { if( ! is_woocommerce()) wp_dequeue_script('wc-cart-fragments'); }

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    tr[data-plugin="plugin-activation-date/plugin-activation-date.php"], 
    #menu-posts-rfa_locations, #menu-posts-rfa_courses, 
    #menu-posts-rfa_coupons, #menu-posts-course_booking, 
    #toplevel_page_real-coupon-usage, #toplevel_page_real-coupon-bulk,
    #toplevel_page_real-booking-link, #toplevel_page_real-discounts,
	#toplevel_page_real-timeframe, .mce-notification-error {
	    display: none;
	}
	
	.validation-error {
        color: #721c24;
        background-color: #f8d7da;
        border: 1px solid #f5c6cb;
        padding: 8px;
        border-bottom-left-radius: .25rem;
        border-bottom-right-radius: .25rem;
    }
    
    .btn {
        display: inline-block;
        font-weight: 400;
        color: #212529;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-color: transparent;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    
    .btn-success {
        color: #fff;
        background-color: #28a745;
        border-color: #28a745;
    }
    
    .btn-danger:hover, .btn-danger:active, .btn-danger:focus {
        color: #fff;
        background-color: #218838;
        border-color: #1e7e34;
    }
    
    .btn-danger {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
    }
    
    .btn-danger:hover, .btn-danger:active, .btn-danger:focus {
        color: #fff;
        background-color: #c82333;
        border-color: #bd2130;
    }
  </style>';
}

add_action( 'pre_get_posts', 'myFilter1', 10, 2 );
function myFilter1( $q ) {
  // example of $q properties: https://pastebin.com/raw/YK1uaE0M
  if(isset($_POST['action']) && $_POST['action']=="menu-quick-search" && isset($_POST['menu-settings-column-nonce'])){
    // other parameters for more refinement: https://pastebin.com/raw/kZ7hwpyx
    if( is_a($q->query_vars['walker'], 'Walker_Nav_Menu_Checklist') ){
      $q->query_vars['posts_per_page'] =  300;
      $q->query_vars['order_by'] = 'post_title';
    }
  }
  return $q;
}

add_action('admin_enqueue_scripts', 'admin_script', 1);
function admin_script() {
    wp_enqueue_script('admin_error_handler', get_template_directory_uri().'/js/admin-error.js');
    wp_enqueue_script(
        'admin_scripts_rr', //unique handle
        get_template_directory_uri().'/js/admin-scripts.js', //location
        array('jquery')  //dependencies
    );
}

//Page Slug Body Class
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

function bookingform_activate() {
    register_post_type('rfa_podcast',
        array(
            'labels' => array(
                'name' => 'Podcasts',
                'singular_name' => 'Podcast'
            ),
            'public' => true,
            'supports' => array('title', 'editor', 'thumbnail'),
            'menu_icon' => 'dashicons-microphone',
            'show_in_menu' => true,
            'menu_position' => 5,
            'rewrite' => [ 'slug' => 'podcast' ]
        )
    );

    register_taxonomy('rfa_podcast_categories',['rfa_podcast'], [
        'hierarchical' => false,
        'labels' => [
            'name' => _x( 'Podcast Categories', 'taxonomy general name' ),
            'singular_name' => _x( 'Podcast Category', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Podcast Categories' ),
            'all_items' => __( 'All Podcast Categories' ),
            'parent_item' => __( 'Parent Podcast Category' ),
            'parent_item_colon' => __( 'Parent Podcast Category:' ),
            'edit_item' => __( 'Edit Podcast Category' ),
            'update_item' => __( 'Update Podcast Category' ),
            'add_new_item' => __( 'Add New Podcast Category' ),
            'new_item_name' => __( 'New Type Podcast Category' ),
            'menu_name' => __( 'Podcast Categories' ),
        ],
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => [ 'slug' => 'podcast-categories' ],
    ]);

	register_post_type('rfa_coupons',
		array(
			'labels' => array(
				'name' => 'Coupons',
				'singular_name' => 'Coupons'
			),
			'public' => true,
			'supports' => array('title', 'editor'),
			'menu_icon' => 'dashicons-tickets'
		)
	);
    
    register_taxonomy('rfa_coupon_types',['rfa_coupons'], [
        'hierarchical' => true,
        'labels' => [
            'name' => _x( 'Coupon Types', 'taxonomy general name' ),
            'singular_name' => _x( 'Coupon Type', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Types' ),
            'all_items' => __( 'All Types' ),
            'parent_item' => __( 'Parent Type' ),
            'parent_item_colon' => __( 'Parent Type:' ),
            'edit_item' => __( 'Edit Type' ),
            'update_item' => __( 'Update Type' ),
            'add_new_item' => __( 'Add New Type' ),
            'new_item_name' => __( 'New Type Name' ),
            'menu_name' => __( 'Types' ),
        ],
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => [ 'slug' => 'coupon-type' ],
    ]);

	register_post_type('rfa_locations',
        array(
            'labels' => array(
                'name' => 'Locations',
                'singular_name' => 'Locations'
            ),
            'public' => true,
            'show_in_rest' => true,
            'supports' => array('title', 'editor'),
            'menu_icon' => 'dashicons-location-alt'
        )
    );

    register_taxonomy('rfa_states',['rfa_locations'], [
        'hierarchical' => false,
        'labels' => [
            'name' => _x( 'States', 'taxonomy general name' ),
            'singular_name' => _x( 'State', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search States' ),
            'all_items' => __( 'All States' ),
            'parent_item' => __( 'Parent State' ),
            'parent_item_colon' => __( 'Parent State:' ),
            'edit_item' => __( 'Edit State' ),
            'update_item' => __( 'Update State' ),
            'add_new_item' => __( 'Add New State' ),
            'new_item_name' => __( 'New State' ),
            'menu_name' => __( 'States' ),
        ],
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => [ 'slug' => 'state' ],
    ]);
    
    register_post_type('rfa_courses',
        array(
            'labels' => array(
                'name' => 'Courses',
                'singular_name' => 'Course'
            ),
            'public' => true,
            'show_in_rest' => true,
            'supports' => array('title', 'editor'),
            'menu_icon' => 'dashicons-media-document',
        )
    );

	register_taxonomy('rfa_course_types',['rfa_courses'], [
		'hierarchical' => true,
		'labels' => [
			'name' => _x( 'Course Types', 'taxonomy general name' ),
			'singular_name' => _x( 'Type', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Types' ),
			'all_items' => __( 'All Types' ),
			'parent_item' => __( 'Parent Type' ),
			'parent_item_colon' => __( 'Parent Type:' ),
			'edit_item' => __( 'Edit Type' ),
			'update_item' => __( 'Update Type' ),
			'add_new_item' => __( 'Add New Type' ),
			'new_item_name' => __( 'New Type Name' ),
			'menu_name' => __( 'Types' ),
		],
		'show_ui' => true,
		'show_admin_column' => true,
        'show_in_rest' => true,
		'query_var' => true,
		'rewrite' => [ 'slug' => 'course-type' ],
	]);
    
    register_post_type('rfa_jobs',
        array(
            'labels' => array(
                'name' => 'Jobs',
                'singular_name' => 'Job'
            ),
            'public' => true,
            'supports' => array('title', 'editor'),
            'menu_icon' => 'dashicons-tickets',
            'rewrite' => [ 'slug' => 'jobs' ]
        )
    );
    
    register_post_type('rfa_skills',
        array(
            'labels' => array(
                'name' => 'Job Skills',
                'singular_name' => 'Job Skill'
            ),
            'public' => true,
            'supports' => array('title', 'editor'),
            'menu_icon' => 'dashicons-tickets'
        )
    );

    register_post_type('rfa_client',
        [
            'labels' => [
                'name' => 'Clients',
                'singular_name' => 'Client'
            ],
            'public' => true,
            'supports' => ['title', 'editor', 'thumbnail'],
            'menu_icon' => 'dashicons-building'
        ]
    );

    register_taxonomy('rfa_client_groups',['rfa_client'], [
        'hierarchical' => true,
        'labels' => [
            'name' => _x( 'Groups', 'taxonomy general name' ),
            'singular_name' => _x( 'Group', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Groups' ),
            'all_items' => __( 'All Groups' ),
            'edit_item' => __( 'Edit Group' ),
            'update_item' => __( 'Update Group' ),
            'add_new_item' => __( 'Add New Group' ),
            'new_item_name' => __( 'New Team Group' ),
            'menu_name' => __( 'Groups' ),
        ],
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => [ 'slug' => 'client-groups' ],
    ]);

    register_post_type('rfa_people',
    [
            'labels' => [
                'name' => 'People',
                'singular_name' => 'Person'
            ],
            'public' => true,
            'supports' => ['title', 'editor', 'thumbnail'],
            'menu_icon' => 'dashicons-networking'
        ]
    );

    register_taxonomy('rfa_people_teams',['rfa_people'], [
        'hierarchical' => true,
        'labels' => [
            'name' => _x( 'Teams', 'taxonomy general name' ),
            'singular_name' => _x( 'Team', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Teams' ),
            'all_items' => __( 'All Teams' ),
            'edit_item' => __( 'Edit Team' ),
            'update_item' => __( 'Update Team' ),
            'add_new_item' => __( 'Add New Team' ),
            'new_item_name' => __( 'New Team Name' ),
            'menu_name' => __( 'Teams' ),
        ],
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_rest' => true,
        'query_var' => true,
        'rewrite' => [ 'slug' => 'people-teams' ],
    ]);
    
    add_menu_page('Course Bookings', 'Course Bookings', 'manage_options', 'post.php?post=5059&action=edit', '', '', 21);
    add_submenu_page( 'post.php?post=5059&action=edit', 'Booking List', 'Booking List', 'manage_options', 'admin.php?page=real-coupon-usage', 'coupon_usage_page');
    add_submenu_page( 'post.php?post=5059&action=edit', 'Booking Settings', 'Booking Settings', 'manage_options', 'post.php?post=5059&action=edit');
    add_submenu_page( 'post.php?post=5059&action=edit', 'States', 'States', 'manage_options', 'edit-tags.php?taxonomy=rfa_states&post_type=rfa_locations');
    add_submenu_page( 'post.php?post=5059&action=edit', 'Locations', 'Locations', 'manage_options', 'edit.php?post_type=rfa_locations');
	add_submenu_page( 'post.php?post=5059&action=edit', 'Course Types', 'Course Types', 'manage_options', 'edit-tags.php?taxonomy=rfa_course_types&post_type=rfa_courses');
	add_submenu_page( 'post.php?post=5059&action=edit', 'Courses', 'Courses', 'manage_options', 'edit.php?post_type=rfa_courses');
	add_submenu_page( 'post.php?post=5059&action=edit', 'Coupons', 'Coupons', 'manage_options', 'edit.php?post_type=rfa_coupons');
    add_submenu_page( 'post.php?post=5059&action=edit', 'Coupon Types', 'Coupon Types', 'manage_options', 'edit-tags.php?taxonomy=rfa_coupon_types&post_type=rfa_coupons');
    add_submenu_page(  'post.php?post=5059&action=edit', 'Coupon Usage', 'Coupon Usage', 'manage_options', 'admin.php?page=real-timeframe', 'coupon_timeframe' );
	add_submenu_page( 'post.php?post=5059&action=edit', 'Bulk Create Coupons', 'Bulk Create Coupons', 'manage_options', 'admin.php?page=real-coupon-bulk', 'bulk_coupons');
	add_submenu_page( 'post.php?post=5059&action=edit', 'Booking Links', 'Booking Links', 'manage_options', 'admin.php?page=real-booking-link', 'generate_links');
	add_submenu_page( 'post.php?post=5059&action=edit', 'Course Discounts', 'Course Discounts', 'manage_options', 'admin.php?page=real-discounts', 'discount_codes');
	add_submenu_page( 'post.php?post=5059&action=edit', 'Xero Settings', 'Xero Settings', 'manage_options', 'admin.php?page=xero-settings', 'xero_settings');
}
add_action( 'init', 'bookingform_activate' );

add_action( 'rest_api_init', function () {
    register_rest_route( 'booking', '/locations', array(
        'methods'  => 'GET',
        'callback' => 'wp_api_get_locations',
    ) );
    
    register_rest_route( 'booking', '/states', array(
        'methods'  => 'GET',
        'callback' => 'wp_api_get_states',
    ) );
    
    register_rest_route( 'booking', '/courses', array(
        'methods'  => 'GET',
        'callback' => 'wp_api_get_courses',
    ) );

    register_rest_route( 'booking', '/equipfee', array(
        'methods'  => 'GET',
        'callback' => 'wp_api_get_equipment_fee',
    ) );
    
    register_rest_route( 'booking', '/discounts', array(
        'methods'  => 'GET',
        'callback' => 'wp_api_get_discounts',
    ) );
    
    register_rest_route( 'booking', '/coursetype/(?P<id>\d+)', array(
        'methods'  => 'GET',
        'callback' => 'wp_api_get_course_types',
    ) );
    
    register_rest_route( 'booking', '/public', array(
        'methods'  => 'POST',
        'callback' => 'wp_api_add_public_booking',
    ) );
} );

function wp_api_get_locations() {
    $locations = [];
    $posts = get_posts([
        'post_type' => 'rfa_locations',
        'numberposts' => -1
    ]);
    foreach($posts as $post) {
        $post->public = (int)get_field('available_for_public_courses', $post->ID);
        $post->onsite = (int)get_field('available_for_onsite_courses', $post->ID);
        $post->axcelerate_id = get_field('axcelerate_id', $post->ID);
        $post->image = get_field('image', $post->ID);
        $locations[] = $post;
    }
    
    return $locations;
}

function wp_api_get_states() {
    $states = [];
    $posts = get_terms( array(
        'taxonomy' => 'rfa_states',
        'hide_empty' => false,
    ) );
    foreach($posts as $post) {
        $post->axcelerate_id = strtoupper($post->slug);
        $post->image = get_field('image', $post->ID);
        $states[] = $post;
    }
    
    return $states;
}

function wp_api_add_public_booking() {
    $posts = new WP_Query([
        'title' => $_POST['coupon'],
        'post_type' => 'rfa_coupons'
    ]);
    //var_dump($_POST['participants']);
    //echo '<br>';
    $decoded = urldecode($_POST['participants']);
    //var_dump($decoded);
    //echo '<br>';
    parse_str($decoded, $public_participants);
    //var_dump($public_participants);
    //die();
    
    $pcount = 0;
    if ( $posts->have_posts() ) {
        while ($posts->have_posts()) {
            $posts->the_post();
            $post = get_post();
            //remove one use from the coupon
            $uses = get_field('uses_remaining', $post->ID);
            $pcount = get_field('usage_type', $post->ID) == 2 ? count($public_participants) : 1;
        }
    }
    
    $db_params = [
        'course_id' => $_POST['course_id'],
        'location_id' => $_POST['location_id'],
        'booking_contact' => $_POST['booking_contact'],
        'booking_content' => $_POST['booking_content'],
        'coupon' => $_POST['coupon'],
        'booking_date' => $_POST['booking_date'],
        'course_date' => $_POST['course_date'],
        'price' => $_POST['price'],
        'places' => $pcount,
        'participants' => count($public_participants),
        'clientid' => 'Portal Booking',
        'gclid' => 'Portal Booking',
        'trafficsource' => 'Portal Booking'
    ];
    
    global $wpdb;
    $wpdb->insert('wp_rr_bookings', $db_params);
    //var_dump($wpdb->last_query);
    //var_dump($wpdb->last_error);
}

function wp_api_get_equipment_fee() {
    $fee = (float)get_field('online_equipment_delivery_fee', 5059);
    return $fee;
}

function wp_api_get_courses() {
    $courses = [];
    $posts = get_posts([
        'post_type' => 'rfa_courses',
        'numberposts' => -1
    ]);
    foreach($posts as $post) {
        $pricedata = get_field('pricing_information', $post->ID);
        $prices = array();
        foreach ($pricedata as $i => $price) {
            if (is_array($pricedata) && $i == (count($pricedata) - 1)) {
                $price['maximum_participants'] = 1000;
            }
        
            $prices[] = $price;
        }
        $pricing = json_encode($prices, JSON_NUMERIC_CHECK);
        $post->pricing = str_replace("'", "\'", $pricing);

        $post->public = (int)get_field('available_as_public_course', $post->ID);
        $post->onsite = (int)get_field('available_as_onsite_course', $post->ID);
        $post->online = (int)get_field('available_as_online_course', $post->ID);
        $post->duration = (int)get_field('duration', $post->ID);
        $post->hours = (float)get_field('duration_hours', $post->ID);
        $ax_details = get_field('axcelerate_options', $post->ID);
        $axids = [];
        foreach($ax_details as $detail) {
            $axids[] = $detail['course_id'];
        }
        $post->axcelerate_id = implode(',', $axids);
        $post->ctype = get_field('axcelerate_type', $post->ID) ?? 'w';
        $post->ctemp = get_field('axcelerate_template_id', $post->ID) ?? '46979';
        $clocations = get_field('available_at', $post->ID);
        $post->for_rr = get_field('real_response', $post->ID);
        $post->for_cq = get_field('cq_first_aid', $post->ID);

        $cls = [];
        if(is_array($clocations) && count($clocations) > 0) {
            foreach($clocations as $cloc) {
                //get the terms
                $terms = get_the_terms($cloc, 'rfa_states');
                $tm = strtoupper($terms[0]->slug);
                $tm = get_field('axcelerate_id', $cloc);
                //$tm = $cloc;
                if(!in_array($tm, $cls)) {
                    $cls[] = $tm;
                }
            }
        }
        $clocation = implode(',', $cls);
        $post->clocation = $clocation;
        $post->category = '';
        $catID = get_the_terms($post->ID, 'rfa_course_types');
        if (is_array($catID) && count($catID) > 0) {
            $post->category = $catID[0]->name;
        }
        $courses[] = $post;
    }
    
    return $courses;
}

function wp_api_get_course_types($request) {
    $course = new \stdClass();
    $course_id = $request['id'];
    $terms = get_the_terms($course_id, 'rfa_course_types');
    $account_code = '100';
    if(count($terms) > 0) {
        $term_id = $terms[0]->term_id;
        $account_code = get_field('xero_account_code', 'rfa_course_types_'.$term_id);
        if(strlen($account_code) == 0) {
            $account_code = '100';
        }
        
        $taxcode = 'EXEMPTOUTPUT';
        $taxrate = 0;
        $gst_status = get_field('gst_status', 'rfa_course_types_'.$term_id);
        if($gst_status == 1) {
            $taxcode = 'OUTPUT';
            $taxrate = 10;
        }
    }
    
    $course->account = $account_code;
    $course->taxcode = $taxcode;
    $course->taxrate = $taxrate;
    
    return $course;
}

function my_site_rest_index( $response ){
    return array();
}
add_filter('rest_index', 'my_site_rest_index');

function wp_api_get_discounts() {
    global $wpdb;
    
    $discountprices = $wpdb->get_results('SELECT * FROM wp_axcel_courses WHERE coursedate >= NOW() AND newprice IS NOT null ORDER BY coursedate');
    $discounts = [];
    foreach($discountprices as $discount) {
        $discounts[$discount->id] = $discount->newprice;
    }
    
    return $discounts;
}

function my_project_updated_send_email( $post_id, $post, $update ) {
    
    // If this is a revision, don't send the email.
    if ( wp_is_post_revision( $post_id ) )
        return;
    
    //var_dump($post); die();
    if($post->post_type == 'rfa_jobs') {
        if (strpos($post->post_name, $post->ID) === false) {
            global $wpdb;
            $wpdb->update('wp_posts', ['post_name' => $post->ID.'-'.strtolower(str_replace([' ', '(', ')'], ['-', '', ''], $post->post_title))], ['id' => $post->ID]);
        }
    }
}
add_action( 'wp_insert_post', 'my_project_updated_send_email', 10, 3 );

function rr_breadcrumbs($post) {
    $parents = get_post_ancestors( $post->ID );
    echo '<a href="'.site_url().'">Home</a>';
    
    if($parents) {
        for($i = count($parents) - 1; $i >= 0; $i--) {
            $parent = $parents[$i];
            $parentpage = get_post($parent);
            echo ' / <a href="'.get_the_permalink($parent).'">'.$parentpage->post_title.'</a>';
        }
    }
    
    $thispage = get_post($post);
    echo ' / <a href="'.get_the_permalink($post).'">'.$thispage->post_title.'</a>';
}

function add_coupon_columns ( $columns ) {
	$columns = [
		'cb' => '<input type="checkbox" />',
		'title' => 'Coupon Code'
	];

	return array_merge ( $columns, array (
		'uses_remaining' => __ ( 'Uses Remaining' ),
        'coupon_type' => __ ( 'Coupon Type' ),
        'coupon_course' => __('Course'),
        'coupon_location' => __('Location')
	) );
}
add_filter ( 'manage_rfa_coupons_posts_columns', 'add_coupon_columns' );

function coupon_custom_column ( $column, $post_id ) {
	$post = get_post($post_id);
	switch ( $column ) {
		case 'uses_remaining':
            global $wpdb;
            if($post->post_type == 'rfa_coupons') {
                $uses = get_field('uses', $post_id);
                $sql = "SELECT sum(b.places) as used
				FROM wp_rr_bookings b
				WHERE b.coupon = '".$post->post_title."'";
                $uses_data = $wpdb->get_results($sql);
                foreach($uses_data as $use) {
                    $remaining = $uses - $use->used;
                }
            }
			echo '<a href="'.site_url().'/wp-admin/admin.php?page=real-coupon-usage&code='.$post->post_title.'">';
			if($uses == 0) {
				echo 'Unlimited';
			} else {
				echo $remaining.'/'.$uses;
			}
			echo '</a>';
			break;
        
        case 'coupon_type':
            echo get_the_term_list($post->ID, 'rfa_coupon_types', '', ', ', '');
            break;
        
        case 'coupon_course':
            $courses = get_field('course', $post->ID);
            if(is_array($courses)) {
                $titles = [];
                foreach($courses as $course) {
                    $titles[] = get_the_title($course);
                }
                
                echo implode(', ', $titles);
            }
            break;
        
        case 'coupon_location':
            $locations = get_field('location', $post->ID);
            if(is_array($locations)) {
                $titles = [];
                foreach($locations as $location) {
                    $titles[] = get_the_title($location);
                }
        
                echo implode(', ', $titles);
            }
            break;
	}
}
add_action ( 'manage_rfa_coupons_posts_custom_column', 'coupon_custom_column', 10, 3 );

function add_job_columns ( $columns ) {
    $columns = [
        'cb' => '<input type="checkbox" />',
        'title' => 'Job Title'
    ];
    
    $after = [
        'date' => 'Date'
    ];
    
    return array_merge ( $columns, array (
        'location' => __ ( 'Location' ),
        'skills' => __ ( 'Skills' ),
        'type' => __('Type')
    ), $after );
}
add_filter ( 'manage_rfa_jobs_posts_columns', 'add_job_columns' );

function job_custom_column ( $column, $post_id ) {
    $post = get_post($post_id);
    switch ( $column ) {
        case 'location':
            $courses = get_field('location', $post->ID);
            if(is_array($courses)) {
                $titles = [];
                foreach($courses as $course) {
                    $titles[] = get_the_title($course);
                }
                
                echo implode(', ', $titles);
            }
            break;
    
        case 'skills':
            $courses = get_field('skills', $post->ID);
            if(is_array($courses)) {
                $titles = [];
                foreach($courses as $course) {
                    $titles[] = get_the_title($course);
                }
            
                echo implode(', ', $titles);
            }
            break;
    
        case 'type':
            $courses = get_field('type', $post->ID);
            echo ($courses == 0 ? 'Permanent' : 'Casual');
            break;
    }
}
add_action ( 'manage_rfa_jobs_posts_custom_column', 'job_custom_column', 10, 3 );

function mfields_set_default_object_terms( $post_id, $post ) {
	if ( 'publish' === $post->post_status && $post->post_type === 'rfa_coupons' ) {
		$defaults = array(
			'rfa_coupon_types' => array( 'uncategorised' )
		);
		$taxonomies = get_object_taxonomies( $post->post_type );

		foreach ( (array) $taxonomies as $taxonomy ) {
			$terms = wp_get_post_terms( $post_id, $taxonomy );
			if ( empty( $terms ) && array_key_exists( $taxonomy, $defaults ) ) {
				wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
			}
		}
	}
}
add_action( 'save_post', 'mfields_set_default_object_terms', 100, 2 );

//Force coupon uniqueness
add_action('admin_head', 'hide_publish_button');
function hide_publish_button() {
	global $post;

	if($post->post_type == 'rfa_coupons') {
		?>
		<script type="text/javascript">
			$(document).ready(function() {
				$('input[name="post_title"]').on('blur', function(e) {
					//Does this name exist?
					$.ajax({
						url: '<?php echo site_url(); ?>/wp-admin/admin-ajax.php',
						type: 'post',
						dataType: 'json',
						data: {
							action: 'check_coupon',
							post_title: $('input[name="post_title"]').val()
						},
						success: function( response ) {
							//console.log(response);
							//console.log(response.coupons);
							if(response.coupons == 0) {
								$('#publish').removeAttr('disabled');
							} else {
								$('#publish').attr('disabled', 'disabled');
							}
						}
					});
				});
			});
		</script>
		<?php
	}
}

add_action( 'wp_ajax_check_coupon', 'check_coupon_title' );
add_action( 'wp_ajax_nopriv_check_coupon', 'check_coupon_title' );

function check_coupon_title() {
	global $wpdb;
	$sql = "select ID from $wpdb->posts where post_title = '".$_POST['post_title']."' AND post_type = 'rfa_coupons' AND post_status = 'publish' ";
	$mypostids = $wpdb->get_col($sql);
	$ret = new stdClass();
	$ret->coupons = count($mypostids);
	echo json_encode($ret);
	die();
}

function filter_coupons_by_taxonomies( $post_type, $which ) {
    
    // Apply this only on a specific post type
    if ( 'rfa_coupons' !== $post_type )
        return;
    
    // A list of taxonomy slugs to filter by
    $taxonomies = array( 'rfa_coupon_types' );
    
    foreach ( $taxonomies as $taxonomy_slug ) {
        
        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy( $taxonomy_slug );
        $taxonomy_name = $taxonomy_obj->labels->name;
        
        // Retrieve taxonomy terms
        $terms = get_terms( $taxonomy_slug );
        
        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
        foreach ( $terms as $term ) {
            printf(
                '<option value="%1$s" %2$s>%3$s</option>',
                $term->slug,
                ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
                $term->name
            );
        }
        echo '</select>';
    }
    
}
add_action( 'restrict_manage_posts', 'filter_coupons_by_taxonomies' , 10, 2);

function show_course_list( $atts ) {
    $course_id = 0;

    $masterLocations = ['VIC' => 'Melbourne', 'NSW' => 'Sydney', 'WA' => 'Perth'];

    if(isset($atts['location'])) {
        $locations = [$masterLocations[$atts['location']]];
    } else {
        $locations = $masterLocations;
    }

    if(isset($_GET['course_id'])) {
        $course_id = $_GET['course_id'];
    } elseif(isset($atts['course_id'])) {
        $course_id = $atts['course_id'];
    }
    
    /*if(isset($_GET['location_id'])) {
        $location_id = $_GET['location_id'];
    } elseif(isset($atts['location_id'])) {
        $location_id = $atts['location_id'];
    }*/

    ob_start();

    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            function getCourses() {
                <?php foreach($locations as $key => $location) { ?>
                    $.ajax({
                        'url': '/booking/axcel.php',
                        'dataType': 'html',
                        'data': {
                            'type' : 'w',
                            'cid' : <?php echo $_GET['course_id']; ?>,
                            <?php
                            if ($course_id > 0) {
                                echo '  \'aid\' : ' . $course_id . ',';
                            }

                            echo ' \'state\' : \''.$key.'\',';

                            if (isset($atts['number'])) {
                                echo ' \'number\' : ' . $atts['number'] . ',';
                            }
                            ?>

                        },
                        'success': function (data) {
                            $('#axcel-courses-<?php echo $key; ?>').html(data);
                            $('[data-toggle="popover"]').popover();
                        },
                        'error': function(jqXHR, textStatus, errorThrown) {
                            $('#axcel-courses-<?php echo $key; ?>').html(jqXHR.responseText);
                            $('[data-toggle="popover"]').popover();
                        }
                    });
                <?php } ?>
            }

            $(document).on('click', '.public-instance-button', function(e) {
                e.preventDefault();
                var _toencode = '{"iid" : ' + $(this).data('id') + ', "amt" : ' + $(this).data('amt') + ', "date" : "' + $(this).data('date') + '", "tid" : 1, "cid" : "<?php echo $_GET['course_id']; ?>", "lid" : "' + $(this).data('lid') + '"}';

                var _enc = $.base64.encode(_toencode);
                window.location.href = '<?php echo get_site_url(); ?>/booking/' + _enc;
            });

            getCourses();
        });
    </script>
<?php
    $ret = '';
    $ret = ob_get_clean();
    foreach($locations as $key => $location) {
        $ret .= '<h3>'.$location.'</h3><div id="axcel-courses-'.$key.'" class="axcel-courses">
                <span><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i><br>Loading available courses</span>
            </div>';
    }
    return $ret;
}

add_shortcode( 'course_list', 'show_course_list' );

add_shortcode('rr-team-showcase', function ($atts = [], $content = null, $tag = '') {
    //get the employers
    $out = '';
    $id = $atts['id'];
    $emplist = get_post_meta($id, 'employers');
    $scroll = $atts['scroll'] ?? false;
    $infobox = $atts['infobox'] ?? false;
    $title = $atts['title'] ?? false;
    $cols = $atts['cols'] ?? 3;
    $employers = maybe_unserialize($emplist)[0];
    shuffle($employers);
    if($scroll) {
        $out .= '<div class="ats-layout-grid ats-layout">
        <ul team-id="' . $id . '" class="employers-box filter-container" style="width: 100%; text-align: center; padding-left: 0;">';
        foreach ($employers as $employer) {
            $post = get_post($employer);
            $out .= '<li class="a-grid-3  filter-item" style="" data-employer-id="' . $post->ID . '" data-title="' . $post->post_title . '">
           <div ats-profile="" data-profile="" class="sortable-box " title="" style="padding:  10px; margin: 0 5px 10px;">
              <div class="employer_photo grid-container sortable" data-block-name="photo" data-tooltip-name="photo" style="margin-top: 10px; font-size: 14px;">
                 <div class="photo-shape ats-round" style="width: 140px;">
                    <div class="photo-wrapper">
                       <div class="photo-container" style="background-image:url(' . get_the_post_thumbnail_url($post->ID) . ');
                          "></div>
                    </div>
                 </div>
              </div>
              <div class="employer_name grid-container sortable" style="margin-top: 10px;" data-block-name="name" data-tooltip-name="name"><span class="team-field-content" style="
                 font-size: 22px;
                 color: rgba(221,15,44,1);
                 font-weight: bold;
                 font-style: normal;
                 text-align: center;
                 text-transform: none;
                 ">' . ($title ? $post->post_title : '') . '</span></div>
              <div class="employer_position grid-container sortable" style="margin-top: 0px;" data-block-name="position" data-tooltip-name="position"><span class="team-field-content" style="
                 font-size: 14px;
                 color: rgba(110,110,110,1);
                 font-weight: normal;
                 font-style: normal;
                 text-align: center;
                 text-transform: none;
                 ">' . get_post_meta($post->ID, 'position')[0] . '</span></div>
              <div class="sortable"></div>
              <div class="sortable"></div>
           </div>
           <div class="ats-employer-panel ats-employer-panel-clean a-slide-panel-content-wrapper" id="ats_employer_panel_' . $id . '_' . $post->ID . '">
              <div class="a-scroll">
                 <div class="a-title">
                    <ul class="actions">
                       <li>
                          <button class="btn btn-transparent close-panel" title="Close">
                             <svg viewBox="0 0 56 56">
                                <use xlink:href="#icon-delete"></use>
                             </svg>
                          </button>
                       </li>
                    </ul>
                 </div>
                 <div class="ats-employer-panel-container">
                    <div class="ats-employer-panel-head">
                       <div class="photo" style="border-color: #E6E6E6">
                          <div class="photo_wrapper">
                             <div style="background-image: url(' . get_the_post_thumbnail_url($post->ID) . ')"></div>
                          </div>
                       </div>
                       <div class="info">
                          <div class="name">
                             ' . $post->post_title . '
                          </div>
                          <div class="position">
                             ' . get_post_meta($post->ID, 'position')[0] . '
                          </div>
                       </div>
                    </div>
                    <div class="sortable"></div>
                    <div class="ats-employer-panel-body">
                       <div class="ats-employer-panel-body-wrapper entry">
                          ' . wpautop($post->post_content) . '
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </li>';
        }
        $out .= '</ul></div>';
    } else {
        $blocks = [];

        foreach($employers as $employer) {
            $post = get_post($employer);
            $blocks[] = [
                'title' => $post->post_title,
                'content' => get_post_meta($post->ID, 'position')[0],
                'picture' => get_the_post_thumbnail_url($post->ID, [300, 300])
            ];
        }

        if($blocks) {
            echo '<div class="real_trainers_area">
                <div class="container">
                    <div class="real_trainers row justify-content-center">
                        <div class="col-12 col-md-9">';
            the_field('trainers_introduction');
            echo '</div>
                <div class="col-12 '.($cols == 3 ? 'col-md-9' : '').'">';
            $carousel = 'trainer-blocks';
            $blocksize = 'col-sm-'.(12/$cols);
            include('template-parts/carousel/image-carousel.php');

            echo '    </div>
                    </div>
                </div>
            </div>';
        }
    }

    return $out;
});

/**
 * Rename "home" in breadcrumb
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_home_text' );
function wcc_change_breadcrumb_home_text( $defaults ) {
    // Change the breadcrumb home text from 'Home' to 'Apartment'
    $defaults['home'] = 'Shop';
    return $defaults;
}

add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );
function woo_custom_breadrumb_home_url() {
    return '/shop';
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb');

function fontawesome_dashboard() {
    wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', '', '4.7.0', 'all');
}

add_action('admin_init', 'fontawesome_dashboard');

function filter_wc_add_to_cart_message_html( $message, $products ) {
    $pos = strpos($message, '</a>');
    $message = substr($message, $pos + 4);
    /*$message = str_replace('View cart', '<i class="fa fa-2x fa-shopping-cart"></i>', $message);
    echo $message.'<br>';*/
    return $message;
};
add_filter( 'wc_add_to_cart_message_html', 'filter_wc_add_to_cart_message_html', 10, 2 );

add_filter( 'wc_stripe_show_payment_request_on_checkout', '__return_true' );