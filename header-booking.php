<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */
if($_SERVER['HTTP_HOST'] == 'students.cqfirstaid.com.au' && $_SERVER['REQUEST_URI'] != '/student-details/') {
    header('Location: https://students.cqfirstaid.com.au/student-details/');
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
<link rel="manifest" href="/favicon/site.webmanifest">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<?php wp_head(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/owl.carousel.min.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/responsive.css" />

</head>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004058892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004058892/?guid=ON&amp;script=0"/>
</div>
</noscript>
<body <?php body_class(); ?>>
<div id="page" class="site">
<a class="skip-link screen-reader-text" href="#content">
<?php _e( 'Skip to content', 'realresponse' ); ?>
</a>
    <?php if($_SERVER['HTTP_HOST'] == 'dev.realresponse.com.au') { ?>
        <header id="covid" style="background-color: #DA1E32">
            DEVELOPMENT SITE!
        </header>
    <?php } else { ?>
        <header id="covid">
            <?php if(time() < strtotime('2022-01-05')) {
                echo 'Our offices will be closed until the 5th of Jan 2022  - Wishing Everyone a Happy and Safe Holidays';
            } else { ?>
            <a href="https://www.realresponse.com.au/covid-19/">
                Important information regarding COVID-19
            </a>
            <?php } ?>
        </header>
    <?php } ?>
    <header id="masthead">
        <div class="container">
            <div class="row">
                <div class="col-2 rr-logo align-items-center">
                    <a href="<?php echo get_site_url(); ?>">
                        <img src="https://www.realresponse.com.au/wp-content/uploads/2018/04/cropped-logo.png">
                    </a>
                    <a href="#" id="show-menu-desk" class="show-menu"><i class="fa fa-angle-down"></i></a>

                </div>
                <div class="col-8 text-center">
                    <div class="navigation-top">
                        <div class="wrap">
                            <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                        </div>
                        <!-- .wrap -->
                    </div>
                    <div class="pinned-top">
                        Got Questions? We’ve got answers, call 1300 744 980
                    </div>
                </div>
                <div class="col-2 rr-phone align-items-center">
                    <a href="tel:1300 744 980">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span class="gphone">1300 744 980</span>
                    </a>
                </div>
                <a href="#" id="show-menu-mobile" class="show-menu"><i class="fa fa-bars"></i></a>
            </div>
            <div class="sub-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-2">
                            <a class="btn btn-default book-now" style="text-transform: none" href="/booking">Book Now</a>
                        </div>
                        <div class="col-8">
                            <?php get_template_part( 'template-parts/navigation/navigation', 'sub' ); ?>
                        </div>
                        <div class="col-2 text-right">
                            <a class="btn btn-default" href="/contact-us">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
    </header>

    <div class="slider-menu">
        <div class="container">
            <div class="row">
                <div class="col-2">
                    <div class="content">
                        <a href="#" id="close-menu">
                            <i class="fa fa-times"></i>
                        </a>
                        <?php
                        wp_nav_menu( array(
                            'theme_location'  => 'top',
                            'menu_id'         => 'side-nav',
                            'walker'          => new Side_Nav_Menu_Walker(),
                            'container_class' => 'side-menu-container'
                        ));
                        ?>
                        <div class="social">
                            <?php
                            if ( has_nav_menu( 'social' ) ) : ?>
                                <nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'realresponse' ); ?>">
                                    <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'social',
                                        'menu_class'     => 'social-links-menu',
                                        'depth'          => 1,
                                        'link_before'    => '<span class="screen-reader-text">',
                                        'link_after'     => '</span>' . realresponse_get_svg( array( 'icon' => 'chain' ) ),
                                    ) );
                                    ?>
                                </nav><!-- .social-navigation -->
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #masthead -->


<?php
	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( ! is_page() && ! realresponse_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'realresponse-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>
<div class="site-content-contain">
<div id="content" class="site-content">
