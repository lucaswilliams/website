<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <title>Student Details | CQ First Aid</title>
    <meta name="google-site-verification" content="gUXCJmVa2EAIIX4PvkEjAt1kSSHQ2Q2xT0zditzkXjk" />
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" type="image/png" sizes="32x32" href="https://booking.cqfirstaid.com.au/img/favicon.jpg">

    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/owl.carousel.min.css" />
    <script type='text/javascript' src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.colorbox.js"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/colorbox.css" />
    <script type='text/javascript'>
        jQuery(document).ready(function(){
            jQuery(".iframe2").colorbox({inline:true, buttonClose: false, innerWidth:400, innerHeight:550,scrolling:true,
                onComplete:function(){
                    //jQuery('#cboxClose').hide();
                    jQuery('#colorbox').addClass("contact_popup");
                }
            });
            jQuery(".iframe4").colorbox({inline:true, buttonClose: false, innerWidth:400, innerHeight:520,scrolling:false,
                onComplete:function(){
                    //jQuery('#cboxClose').hide();
                    jQuery('#colorbox').addClass("shop_contact_popup");
                }
            });
            jQuery(".iframe").colorbox({inline:true, innerWidth:980, innerHeight:550,scrolling:false,
                onComplete:function(){
                    //jQuery('#cboxClose').hide();
                    jQuery('#colorbox').removeClass("youtube_video").addClass("book_inquery");
                }
            });
            jQuery(".youtube").colorbox({iframe:true, innerWidth:980, innerHeight:550,
                onComplete:function(){
                    //jQuery('#cboxClose').show();
                }
            });
        });
    </script>
    <style>
        html iframe.cboxIframe > html {
            margin-top: 0px !important;
        }
    </style>
    <script type='text/javascript'>
        jQuery(document).ready(function() {
            jQuery("form p span input").each(function() {
                jQuery(this).attr("placeholder", jQuery(this).attr("title"));
            });
            jQuery("form p span textarea").each(function() {
                jQuery(this).attr("placeholder", jQuery(this).text());
                jQuery(this).text("");
            });
        });</script>
    <?php wp_enqueue_style( 'responsive', get_template_directory_uri() . '/assets/css/responsive.css'); ?>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/cq.css" />
</head>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1004058892;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004058892/?guid=ON&amp;script=0"/>
    </div>
</noscript>
<!-- End Google Code for Remarketing Tag -->
<body <?php body_class('cq'); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content">
        <?php _e( 'Skip to content', 'realresponse' ); ?>
    </a>

    <header id="masthead">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <a href="https://www.cqfirstaid.com.au">
                        <img src="https://static.wixstatic.com/media/554b3d_56714158863048cd8166f38009765163~mv2.png/v1/fill/w_441,h_100,al_c,q_85,usm_0.66_1.00_0.01/CQFA%20Logo%202021%20Long-02.webp" alt="CQFA Logo 2021 Long-02.png">
                    </a>
                </div>
                <div class="col-4 text-right">
                    <p style="font-size: 14px;">RTO Code: 32398</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <ul>
                        <li><a href="https://www.cqfirstaid.com.au/shop">Shop</a></li>
                        <li><a href="https://www.cqfirstaid.com.au">Contact</a></li>
                        <li><a href="https://booking.cqfirstaid.com.au">Calendar</a></li>
                        <li><a href="https://www.cqfirstaid.com.au/learner-info">Learner Info</a></li>
                        <li><a href="https://www.cqfirstaid.com.au/courses">Courses</a></li>
                        <li><a href="https://www.cqfirstaid.com.au/about-us">About</a></li>
                        <li><a href="https://www.cqfirstaid.com.au">Home</a></li>
                    </ul>
                </div>
            </div>
    </header>
    <!-- #masthead -->

    <div class="site-content-contain">
        <div id="content" class="site-content">
