<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */
if($_SERVER['HTTP_HOST'] == 'students.cqfirstaid.com.au' && $_SERVER['REQUEST_URI'] != '/student-details/') {
    header('Location: https://students.cqfirstaid.com.au/student-details/');
}
?>
<!DOCTYPE html> 
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta name="google-site-verification" content="gUXCJmVa2EAIIX4PvkEjAt1kSSHQ2Q2xT0zditzkXjk" />
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
<link rel="manifest" href="/favicon/site.webmanifest">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<?php wp_head(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/owl.carousel.min.css" />
<script type='text/javascript' src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.colorbox.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/colorbox.css" />
<script type='text/javascript'>   
    jQuery(document).ready(function(){
      jQuery(".iframe2").colorbox({inline:true, buttonClose: false, innerWidth:400, innerHeight:550,scrolling:true,
         onComplete:function(){
            //jQuery('#cboxClose').hide();
            jQuery('#colorbox').addClass("contact_popup");
          }
      });
      jQuery(".iframe4").colorbox({inline:true, buttonClose: false, innerWidth:400, innerHeight:520,scrolling:false,
         onComplete:function(){
            //jQuery('#cboxClose').hide();
            jQuery('#colorbox').addClass("shop_contact_popup");
          }
      });
      jQuery(".iframe").colorbox({inline:true, innerWidth:980, innerHeight:550,scrolling:false,
        onComplete:function(){
          //jQuery('#cboxClose').hide();
          jQuery('#colorbox').removeClass("youtube_video").addClass("book_inquery");
        }
      });
      jQuery(".youtube").colorbox({iframe:true, innerWidth:980, innerHeight:550,
        onComplete:function(){
          //jQuery('#cboxClose').show();
        }
      });
    });
</script>
<style>
html iframe.cboxIframe > html {
  margin-top: 0px !important;
}
</style>
<script type='text/javascript'>
  jQuery(document).ready(function() {
    jQuery("form p span input").each(function() {
        jQuery(this).attr("placeholder", jQuery(this).attr("title"));       
    });      
    jQuery("form p span textarea").each(function() {
        jQuery(this).attr("placeholder", jQuery(this).text());
        jQuery(this).text("");
    });
});</script>
<?php wp_enqueue_style( 'responsive', get_template_directory_uri() . '/assets/css/responsive.css'); ?>
</head>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004058892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004058892/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- End Google Code for Remarketing Tag -->
<body <?php body_class(); ?>>
<div id="page" class="site">
<a class="skip-link screen-reader-text" href="#content">
<?php _e( 'Skip to content', 'realresponse' ); ?>
</a>

<!--contact and shop contact popups -->
<div style="display:none">
    <div id="contact_popup">
        <div class="main-bg">
            <div class="top-area">
                <h3>Contact US</h3>
                <a href="javascript:void(0)" class="btnclose"></a>
                <div class="clear"></div>
            </div>
            <?php //echo do_shortcode('[gravityform id="1" description="false"]'); ?>
            <!--[if lte IE 8]>
            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
            <![endif]-->
            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
            <script>
                hbspt.forms.create({
                    region: "na1",
                    portalId: "20865841",
                    formId: "5c83e7b9-4634-4327-9936-f56d2d986e18"
                });
            </script>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- end popups -->

<header id="prehead" class="nomobile">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <div class="social">
                    <a href="https://www.facebook.com/RealResponseAus/" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                    <a href="https://www.instagram.com/real_response" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCjvptGsu5_bU_wGfLi1Nbgw?view_as=subscriber" target="_blank">
                        <i class="fa fa-youtube-play"></i>
                    </a>
                </div>
            </div>

            <div class="col-8 text-right">
                <a href="tel:1300744980" class="call_us">1300 744 980</a>
                <a href="/contact-us">Contact Us</a>
                <a href="/booking" class="booking_link">BOOK NOW</a>
            </div>
        </div>
    </div>
</header>
<header id="megamenu2">
    <div class="container">
        <div class="row">
            <a id="header-logo" href="/"><img src="https://www.realresponse.com.au/wp-content/uploads/2018/04/cropped-logo.png"></a>
            <div class="col text-right">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'mega',
                    'menu_id'        => 'mega-menu',
                    'walker' => new My_Walker_Nav_Menu()
                ) );
                ?>
            </div>
        </div>
    </div>
</header>

<div class="site-content-contain">
<div id="content" class="site-content">
