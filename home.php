<?php
//This is the blog page thing
get_header('mega-menu');
$post = get_page_by_path( 'education', OBJECT, 'page' );
if(has_post_video()) {
    $thumbnail = '<video autoplay muted loop id="myVideo">
            <source src="'.get_the_post_video_url().'" type="video/mp4">
        </video>';
} elseif ( has_post_thumbnail() ) {
    $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    $thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
}
$smalltitle = get_field('small_title', $post->ID);
$largetitle = get_field('large_title', $post->ID);
?>

<div id="section1" class="header-overlap">
    <div class="featured-banner"><?php echo $thumbnail; ?></div>
    <div class="banner-content">
        <div class="wrap ">
            <h6><?php echo $smalltitle; ?></h6>
            <h1><?php echo $largetitle; ?></h1>
        </div>
    </div>
</div>

<div id="blog-posts">
    <div class="container">
        <!--<div class="row alm-listing">-->
            <?php
            //Get the posts
            /*$posts = get_posts([
                'post_type' => 'post',
                'numberposts' => '12'
            ]);
            
            foreach($posts as $p) {
                $post = get_post($p->ID, OBJECT);
                echo '<div class="col-12 col-md-6 col-lg-4">';
                echo '<div class="post">';
                $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
                echo '<div style="background-image: url('.esc_url($thumb_img[0]).');" class="post-thumbnail"></div>';
                echo '<h2><a href="'.get_the_permalink().'">'.$post->post_title.'</a></h2>';
                echo '<h3>Posted on '.date('d/m/Y', strtotime($post->post_date)).' by '.get_the_author($post->ID).'</h3>';
                echo '<p class="excerpt">'.get_the_excerpt($post->ID).'</p>';
                echo '<p class="text-center"><a class="btn btn-primary" href="'.get_the_permalink().'">Read more</a></p>';
                echo '</div>';
                echo '</div>';
            }*/
            echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="12" button_label="More posts" transition_container_classes="row"]');
            ?>

        <!--</div>-->
    </div>
</div>

<?php
    get_footer();