<?php
/**
 * realresponse: realresponse Theme Settings
 *
 * @package WordPress
 * @subpackage Grabner
 * @since 1.0
 */

add_action("admin_menu", "add_realresponse_theme_menu_item");
function add_realresponse_theme_menu_item() {
	add_theme_page("Realresponse Theme Settings", "Realresponse Theme Settings", "manage_options", "theme-options", "realresponse_option_page", null, 99);
}
function realresponse_option_page() { ?>
	<div class="wrap">
		<h1>Realresponse Theme Options Page</h1>
		<?php settings_errors(); ?>
		<form method="post" action="options.php">
			<table class="form-table">
				<tr valign="top">
					<th scope="row">Common Page Options</th>
					<?php
					 $site_contact = get_option( 'site_contact' ); 
					 $copyright_text = get_option( 'copyright_text' );
					 $header_bottom_right = get_option( 'header_bottom_right' );
					 $header_bottom_link = get_option( 'header_bottom_link' );
					 $header_bottom_left = get_option( 'header_bottom_left' );
					  $contact_link = get_option( 'contact_link' );
					   $contact_text = get_option( 'contact_text' );
					   $stricky_text = get_option( 'stricky_text' );
					 
					?>
					<td>
						<div>
						<div><label>Contact No</label><input type="text" name="site_contact" id="site_contact"  value="<?php echo $site_contact; ?>"/></div>
						<div><label>Copyright Text </label><input type="text" name="copyright_text" id="copyright_text"  value="<?php echo $copyright_text; ?>"/></div>
						<div><label>Header Bottom  Text </label><input type="text" name="header_bottom_left" id="header_bottom_left"  value="<?php echo $header_bottom_left; ?>"/></div>
						<div><label>Header Bottom  Text</label><input type="text" name="header_bottom_right" id="header_bottom_right"  value="<?php echo $header_bottom_right; ?>"/></div>
						<div><label>Header Bottom  url</label><input type="text" name="header_bottom_link" id="header_bottom_link"  value="<?php echo $header_bottom_link; ?>"/></div>
						<div><label>Contact Text</label><input type="text" name="contact_text" id="contact_text"  value="<?php echo $contact_text; ?>"/></div>
						<div><label>Contact link</label><input type="text" name="contact_link" id="contact_link"  value="<?php echo $contact_link; ?>"/></div>
						<div><label>Stricky  link</label><textarea type="text" name="stricky_text" id="stricky_text" > <?php echo $stricky_text; ?></textarea></div>
						</div>
						<style>
						label {
							width: 250px;
							display: inline-block;
						}
						</style>
					</td>
				</tr>
				
			</table>
			<?php settings_fields("theme-options-grp"); ?>
			<?php do_settings_sections( 'theme-options' ); ?>
			<?php submit_button(); ?>
		</form>
	</div>
<?php 
}
add_action('admin_init','realresponse_theme_settings');
function realresponse_theme_settings(){
	add_option('first_field_option',1);// add theme option to database
	
		register_setting( 'theme-options-grp', 'site_contact');
		register_setting( 'theme-options-grp', 'copyright_text');
		register_setting( 'theme-options-grp', 'header_bottom_link');
		register_setting( 'theme-options-grp', 'header_bottom_right');
		register_setting( 'theme-options-grp', 'header_bottom_left');
		register_setting( 'theme-options-grp', 'contact_link');
		register_setting( 'theme-options-grp', 'contact_text');
		register_setting( 'theme-options-grp', 'stricky_text');
		
}
?>