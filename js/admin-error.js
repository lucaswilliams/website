window.onerror = function(message, url, lineNumber) {
    console.log('ERROR CAUGHT: ' + message + ' in ' + url + ' (' + lineNumber + ')');
    return true;
};