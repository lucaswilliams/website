<?php
/**
 * Template Name:Course Booking Form
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Dev_Theme
 * @since Dev Theme 1.0
 */

get_header('booking'); /*?>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.10/angular-ui-router.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-animate.min.js"></script>

<script src="<?php echo get_template_directory_uri() ?>/js/angular.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/angular-ui-router.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/angular-animate.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/jquery.spinner.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/ngformapp.js"></script><?php */?>
<div id="primary" class="content-area full-width">
	<div class="page-heading">
		<div class="auto-area">
			<?php dynamic_sidebar('home-page-heading');?>
		</div>
	</div>
	<div class="sub-heading">
		<div class="auto-area">
			<?php dynamic_sidebar('home-page-sub-heading');?>
			<div class="clear"></div>
		</div>
	</div>
	<main id="main" class="site-main" role="main">
		<div ng-app="formApp" class="booking-form brokermatch-multitype-form rfa_booking_form" ng-controller="formCtrl" ng-cloak>
			<div class="container refinance-slide">
				<div class="row">
					<div align="center" class="prog_top" ng-show="stage!='stage'">
						<div class="progress-panel">
							<div id="myProgress">
								<div id="myBar"></div>
							</div>
							<span>progress</span>
						</div>
					</div>
					<div class="col-md-12">&nbsp;</div>
				</div>
				<form action="" name="multiStepForm" class="form-validation" role="form" method="post" novalidate>
					<input type="hidden" name="action" value="submit">
					<div class="animate-switch-container" ng-switch on="stage" ng-class="{forward: direction, backward:!direction}">
						<div class="animate-switch" ng-switch-default class="formstep slide visible" data-stage="1" >
							<div class="slide-wrapper mortgage-step form2-slide-wrapper">
								<div class="legend-text custom">Where do you want to train?</div>
								<fieldset class="radio-item-group">
									<div class="loan-group-wrapper loantype-1">
										<div class="loan-radio-item loan_form2_item loantype-2">
									        <input id="where_train2" ng-model="formParams.where_train" name="where_train" class="btn-radio" value="Onsite" type="radio" checked="checked" required ng-class="{'input-error': formValidation && formParams.where_train.$error.required}">
									        <label class="label-bottom" for="where_train2">Onsite</label>
									    </div>
									    <div class="loan-radio-item loan_form2_item ">
									        <input id="where_train1" ng-model="formParams.where_train" name="where_train" class="btn-radio" value="Public"  type="radio" required ng-class="{'input-error': formValidation && formParams.where_train.$error.required}">
									        <label class="label-bottom" for="where_train1">Public</label>
									    	<div class="radio-select"></div>
									    </div>
										<p ng-show="show_error && formValidation" class="my-error">Field is Required</p>										

									    <button type="button" class="btn next cross-button btn-red" ng-click="next(formParams.where_train=='Public' ? 'public_stage1' : 'site_stage1')">Continue</button>
									</div>
								</fieldset>
							</div>
						</div>


						<div ng-switch-when="site_stage1" id="site_stage1" class="animate-switch formstep slide visible" data-stage="1">
							<div class="slide-wrapper">
								<div class="legend-text input_title">Choose Onsite Location <span class="required">*</span></div>
								<fieldset class="radio-item-group">
									<div class="protype-group-wrapper location_site_main">
										<div class="radio-item loan_form2_item protype-0">
											<input id="location_site1" ng-model="formParams.onsite_location" name="location_site" class="btn-radio" value="Melbourne" type="radio" required>
											<label class="label-bottom" for="location_site1">Melbourne</label>
											<div class="radio-select"></div>
										</div>
										<div class="radio-item loan_form2_item protype-1">
											<input id="location_site2" ng-model="formParams.onsite_location" name="location_site" class="btn-radio" value="Sydney" type="radio" required>
											<label class="label-bottom" for="location_site2">Sydney</label>
										</div>
										<div class="radio-item loan_form2_item protype-1">
											<input id="location_site3" ng-model="formParams.onsite_location" name="location_site" class="btn-radio" value="Other location" type="radio" required>
											<label class="label-bottom" for="location_site3">Other Location</label>
										</div>
										<div class="radio-item loan_form2_item protype-1">
											<input id="other_location" ng-model="formParams.other_onsite_location" name="other_location_site" class="btn-radio" value="" type="text" placeholder="Type other location" ng-required="formParams.onsite_location == 'Other location'">
											<p class="other_location my-error" ng-show="(show_error && formValidation && formParams.onsite_location == 'Other location' && !formParams.other_onsite_location)">Type location</p>
										</div>
										<p ng-show="(show_error && formValidation && !formParams.onsite_location)" class="my-error">Choose location</p>
										<button type="button" class="btn prev cross-button btn-red" ng-click="back('stage')">Back</button>
										<button type="button" class="btn next cross-button btn-red" ng-click="next('site_stage2')">Continue</button>
									</div>
								</fieldset>
							</div>
						</div>
						<div ng-switch-when="site_stage2" id="site_stage2" class="animate-switch formstep slide visible" data-stage="2">
							<div class="slide-wrapper">
								<h2>Onsite Course Details: </h2>
								<div class="legend-text input_title">Choose your course  <span class="required">*</span></div>
								<fieldset class="radio-item-group choose_course_section">
									<div class="credit-group-wrapper site_course_main">
										<div class="radio-item loan_form2_item">
											<input id="site_course1" ng-model="formParams.site_course" name="site_course" class="btn-radio" value="Provide Cardiopulmonary Resuscitation (CPR) - 3.5 hrs" type="radio" required>
											<label class="label-bottom" for="site_course1">Provide Cardiopulmonary Resuscitation (CPR) - 3.5 hrs <i class="pull-right info-icon fa fa-info-circle"></i></label>
											<div class="course_information">
												<h3>KEY INFORMATION</h3>
												<p>HLTAID003</p>
												<p>On site training available</p>
												<p>Course length: 1 or 2 day options</p>
												<p>Nationally Accredited</p>
												<p>Simulation training included</p>
												<p>Certification lenght: 3 years</p>
												<p>Anaphylaxis Training included</p>
											</div>
										</div>
										<div class="radio-item loan_form2_item ">
											<input id="site_course2" ng-model="formParams.site_course" name="site_course" class="btn-radio" value="Provide Basic Emergency Life Support (Level 1) - 6 hrs" type="radio" required>
											<label class="label-bottom" for="site_course2">Provide Basic Emergency Life Support (Level 1) - 6 hrs <i class="pull-right info-icon fa fa-info-circle"></i></label>
											<div class="course_information">
												<h3>KEY INFORMATION</h3>
											</div>
										</div>
										<div class="radio-item loan_form2_item">
											<input id="site_course3" ng-model="formParams.site_course" name="site_course" class="btn-radio" value="Provide First Aid - (Level 2 / Senior) - 7.5 hrs" type="radio" required>
											<label class="label-bottom" for="site_course3">Provide First Aid - (Level 2 / Senior) - 7.5 hrs <i class="pull-right info-icon fa fa-info-circle"></i></label>
											<div class="course_information">
												<h3>KEY INFORMATION</h3>
											</div>
										</div>
										<div class="radio-item loan_form2_item">
											<input id="site_course4" ng-model="formParams.site_course" name="site_course" class="btn-radio" value="Provide First Aid - (Level 3 / Advanced) - 15 hrs" type="radio" required>
											<label class="label-bottom" for="site_course4">Provide First Aid - (Level 3 / Advanced) - 15 hrs <i class="pull-right info-icon fa fa-info-circle"></i></label>
											<div class="course_information">
												<h3>KEY INFORMATION</h3>
											</div>
										</div>
										<div class="radio-item loan_form2_item multiple_course">
											<input id="site_course_multiple" ng-model="formParams.site_course" name="site_course" class="btn-radio" value="Multiple course" type="radio" required>
											<label class="label-bottom" for="site_course_multiple">Multiple course</label>
										</div>
										<a target="_blank" href="#">Not sure what course you need, request a call back</a>
										<p ng-show="(show_error && formValidation && !formParams.site_course)" class="my-error">Choose course</p>
									</div>
								</fieldset>
								<div class="legend-text input_title">How many participants do you want us to train?  <span class="required">*</span></div>
								<fieldset class="radio-item-group">
									
									<div class="participant-group-wrapper ">
										<div class="radio-item loan_form2_item site_participants_main participants_spinner" >
											<div class="input-group spinner" data-trigger="spinner">
            									<div rn-stepper min="formParams.minsite_participants" max="formParams.maxsite_participants" ng-model="formParams.site_participants"></div>
										        <?php /*?><input ng-model="formParams.site_participants" name="site_participants" id="site_participants" type="text" data-max="18" data-min="0" data-step="1" class="form-control text-center" value="1" data-rule="quantity">
												<div class="input-group-addon">
													<a href="javascript:;" class="spin-up" data-spin="up"><i class="fa fa-caret-up"></i></a>
													<a href="javascript:;" class="spin-down" data-spin="down"><i class="fa fa-caret-down"></i></a>
												</div><?php */?>
											</div>
											<div class="clear"></div>
											<p ng-show="(show_error && formValidation && !formParams.site_participants)" class="my-error">Choose participants</p>
										</div>
									</div>
								</fieldset>
								<div class="legend-text input_title">Please Choose Date  <span class="required">*</span></div>
								<fieldset class="radio-item-group">
									<div class="credit-group-wrapper site_date_main">
										<div class="booking_date_site">
											<?php //echo do_shortcode('[booking]');  ?>
										</div>
										<p class="my-error" style="display:none">Choose Date</p>
									</div>
								</fieldset>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('site_stage1')">Back</button>
								<button type="button" class="btn next cross-button btn-red" ng-click="next(formParams.site_participants < 18 ? 'site_stage3' : 'contact_to_admin')">Continue</button>
							</div>
						</div>
						<div ng-switch-when="site_stage3" id="site_stage3" class="animate-switch formstep slide visible" data-stage="3">
							<div class="slide-wrapper">
								<h2>Onsite Booking Contact Details: </h2>
								<div class="legend-text input_title">First  Name  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item firstname_main">
										<input ng-model="formParams.site_firstname" class="txtfield fullname" name="site_firstname" id="site_firstname" placeholder="First  Name" value="" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.site_firstname)" class="my-error">Enter First  Name</div>
									</div>
								</fieldset>
								<div class="legend-text input_title">Last Name  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item lastname_main">
										<input ng-model="formParams.site_lastname" class="txtfield fullname" name="site_lastname" id="public_lastname" placeholder=" Last Name" value="" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.site_lastname)" class="my-error">Enter  Last Name</div>
									</div>
								</fieldset>
								<div class="legend-text input_title"> E-mail  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item email_main">
										<input ng-model="formParams.site_email" class="txtfield email" name="site_email" id="email" placeholder="Your E-mail" value="" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.site_email)" class="my-error">Enter Valid Email</div>
									</div>
								</fieldset>
								<div class="legend-text input_title">Contact No</div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item contact_main">
										<input ng-model="formParams.site_contact" class="txtfield phonemask" size="10" name="site_contact" id="public_contact" placeholder="Your Contact No" value="" type="text">
									</div>
								</fieldset>
								<div class="legend-text input_title">Company Name</div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item company_main">
										<input ng-model="formParams.site_company" class="txtfield" name="site_company" id="company" placeholder="Your Company Name" value="" type="text">
									</div>
								</fieldset>
								<div class="legend-text input_title">Position / Job Title</div>
								<fieldset class="radio-item-group">
									<div class="radio-item loan_form2_item job_title_main">
										<input ng-model="formParams.job_title" class="txtfield" name="job_title" id="job_title" placeholder="Position / Job Title" value="" type="text">
									</div>
								</fieldset>
								<div class="clear"></div>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('site_stage2')">Back</button>
								<button type="button" class="btn next cross-button btn-red" ng-click="next('site_stage4')">Continue</button>
							</div>
						</div>
						<div ng-switch-when="site_stage4" id="site_stage4" class="animate-switch formstep slide visible" data-stage="4">
							<div class="slide-wrapper">
								<h2>Address Details For Onsite Course: </h2>
								<div class="legend-text input_title">Building Number & Street name  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item street_main">
										<input ng-model="formParams.street" value="" class="txtfield" id="street" name="street"  placeholder="Building Number & Street name" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.street)" class="my-error">Enter  Street</div>
									</div>
								</fieldset>
								<div class="legend-text input_title">City, Suburb or Town  <span class="required">*</span></div>
								<fieldset class="radio-item-group">
									<div class="radio-item loan_form2_item city_main">
										<input ng-model="formParams.city" value="" class="txtfield" id="city" name="city"  placeholder="City, Suburb or Town" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.city)" class="my-error" >Enter  City </div>
									</div>
								</fieldset>
								<div class="legend-text input_title">State / Territory  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item state_main">
										<input ng-model="formParams.state" value="" class="txtfield" id="state" name="state"  placeholder="State / Territory" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.state)" class="my-error" >Enter  State</div>
									</div>
								</fieldset>
								<div class="legend-text input_title">Postcode  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item postcode_main">
										<input ng-model="formParams.postcode" value="" class="txtfield" id="postcode" name="postcode"  placeholder="Postcode" type="text" maxlength="5" required>
										<div ng-show="(show_error && formValidation && !formParams.postcode)" class="my-error">Enter  Postcode</div>
									</div>
								</fieldset>
								<div class="legend-text input_title">Comments, questions or other information</div>
								<fieldset class="radio-item-group">
									<div class="radio-item loan_form2_item comment_main">
										<textarea ng-model="formParams.comment" name="comment"></textarea>
									</div>
								</fieldset>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('site_stage3')">Back</button>
								<button type="button" class="btn next cross-button btn-red" ng-click="next('site_stage5')">Continue</button>
							</div>
						</div>
						<div ng-switch-when="site_stage5" id="site_stage5" class="animate-switch formstep slide visible" data-stage="5">
							<div class="slide-wrapper">
								<div class="legend-text input_title">Real First Aid provides thousands of organisation following services </div>
								<fieldset class="radio-item-group">
									<div class="radio-item loan_form2_item ">
										<input ng-model="formParams.services.service1" type="checkbox" name="services[]" ng-true-value='Professional Servicing and Maintainance of First Aid Kits' value="Professional Servicing and Maintainance of First Aid Kits" class="services" id="service1" />
										<label class="label-bottom" for="service1">Professional Servicing and Maintainance of First Aid Kits</label>
									</div>
									<div class="radio-item loan_form2_item ">
										<input ng-model="formParams.services.service2" ng-true-value='Defibrillator Sales Maintainance and Training' type="checkbox" name="services[]" value="Defibrillator Sales Maintainance and Training" class="services" id="service2"/>
										<label class="label-bottom" for="service2">Defibrillator Sales Maintainance and Training</label>
									</div>
									<div class="radio-item loan_form2_item">
										<input ng-model="formParams.services.service3" ng-true-value='Fire Warden Training" class="services' type="checkbox" name="services[]" value="Fire Warden Training" class="services" id="service3"/>
										<label class="label-bottom" for="service3">Fire Warden Training</label>
									</div>
									<div class="radio-item loan_form2_item ">
										<input ng-model="formParams.services.service4" ng-true-value='Office Evacuation Training' type="checkbox" name="services[]" value="Office Evacuation Training" class="services" id="service4"/>
										<label class="label-bottom" for="service4">Office Evacuation Training</label>
									</div>
								</fieldset>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('site_stage4')">Back</button>
								<button type="button" class="btn next cross-button btn-red" ng-click="next('summary')">Continue</button>
							</div>
						</div>
						



						<div ng-switch-when="public_stage1" id="public_stage1" class="animate-switch formstep slide visible" data-stage="1">
							<div class="slide-wrapper">
								<div class="legend-text input_title">Choose Public Location <span class="required">*</span></div>
								<fieldset class="radio-item-group">
									<div class="protype-group-wrapper location_public_main">
										<div class="radio-item loan_form2_item protype-0">
											<input id="location_public1" ng-model="formParams.public_location" name="location_public" class="btn-radio" value="Melbourne" type="radio" required >
											<label class="label-bottom" for="location_public1">Melbourne</label>
											<div class="radio-select"></div>
										</div>
										<div class="radio-item loan_form2_item protype-1">
											<input id="location_public2" ng-model="formParams.public_location" name="location_public" class="btn-radio" value="Sydney" type="radio" required>
											<label class="label-bottom" for="location_public2">Sydney</label>
										</div>
									   	<div class="radio-item loan_form2_item protype-1">
											<input id="location_public3" ng-model="formParams.public_location" name="location_public" class="btn-radio" value="Other location" type="radio" required>
											<label class="label-bottom" for="location_public3">Other Location</label>
										  </div>
										<div class="radio-item loan_form2_item protype-1">
											<input id="other_location" ng-model="formParams.other_public_location" name="other_location_public" class="btn-radio" value="" type="text" placeholder="Type other location" ng-required="formParams.public_location == 'Other location'">
											<p ng-show="(show_error && formValidation && formParams.public_location == 'Other location' && !formParams.other_public_location)" class="my-error other_location">Type location</p>
										</div>
										<p ng-show="(show_error && formValidation && !formParams.public_location)" class="my-error">Choose location</p>
									</div>
									<button type="button" class="btn prev cross-button btn-red" ng-click="back('stage')">Back</button>
									<button type="button" class="btn next cross-button btn-red" ng-click="next('public_stage2')">Continue</button>
								</fieldset>
							</div>
						</div>
						<div ng-switch-when="public_stage2" id="public_stage2" class="animate-switch formstep slide visible" data-stage="2">
							<div class="slide-wrapper">
								<h2>Course Details: </h2>
								<div class="legend-text input_title">Choose your course  <span class="required">*</span></div>
								<div class="public_default_div">
									<fieldset class="radio-item-group">
										<div class="credit-group-wrapper public_course_main">
											<div class="radio-item loan_form2_item">
												<input id="public_course1" ng-model="formParams.public_course" name="public_course" class="btn-radio" value="Provide Cardiopulmonary Resuscitation (CPR) - 3.5 hrs" type="radio" required>
												<label class="label-bottom" id="public_course1">Provide Cardiopulmonary Resuscitation (CPR) - 3.5 hrs</label>
											</div>
											<div class="radio-item loan_form2_item ">
												<input id="public_course2" ng-model="formParams.public_course" name="public_course" class="btn-radio" value="Provide Basic Emergency Life Support (Level 1) - 6 hrs" type="radio" required>
												<label class="label-bottom" for="public_course2">Provide Basic Emergency Life Support (Level 1) - 6 hrs</label>
											</div>
											<div class="radio-item loan_form2_item">
												<input id="public_course3" ng-model="formParams.public_course" name="public_course" class="btn-radio" value="Provide First Aid - (Level 2 / Senior) - 7.5 hrs" type="radio" required>
												<label class="label-bottom" for="public_course3">Provide First Aid - (Level 2 / Senior) - 7.5 hrs</label>
											</div>
											<div class="radio-item loan_form2_item">
												<input id="public_course4" ng-model="formParams.public_course" name="public_course" class="btn-radio" value="Provide First Aid - (Level 3 / Advanced) - 15 hrs" type="radio" required>
												<label class="label-bottom" for="public_course4">Provide First Aid - (Level 3 / Advanced) - 15 hrs</label>
											</div>
											<div class="radio-item loan_form2_item">
												<input id="public_course_multiple" ng-model="formParams.public_course" name="public_course" class="btn-radio" value="Multiple course" type="radio" required>
												<label class="label-bottom" for="public_course_multiple">Multiple course</label>
											</div>
											<a target="_blank" href="#">Not sure what course you need, request a call back</a>
											<p ng-show="(show_error && formValidation && !formParams.public_course)" class="my-error">Choose course</p>
										</div>
									</fieldset>
									<div class="legend-text input_title">How many participants do you want us to train?  <span class="required">*</span></div>
									<fieldset class="radio-item-group">
										<div class="participant-group-wrapper ">
											<div class="radio-item loan_form2_item public_participants_main">
												<select ng-model="formParams.public_participants" name="public_participants" class="select_arrow" id="public_participants" required>
													<option class="init" value="">Select</option>
														<?php 
															for($i=1;$i<6;$i++){
																echo "<option value='".$i."'>".$i."</option>";
															}
														?>
													<option value="more than 5">more than 5</option>
												</select>
												<p ng-show="(show_error && formValidation && !formParams.public_participants)" class="my-error">Choose participants</p>
											</div>
										</div>
									</fieldset>
                                </div>
								<div class="legend-text input_title">Please Choose Date  <span class="required">*</span></div>
								<fieldset class="radio-item-group">
									<div class="credit-group-wrapper site_date_main">
										<div class="booking_date_site">
											<?php //echo do_shortcode('[booking]');  ?>
										</div>
										<p class="my-error" style="display:none">Choose Date</p>
									</div>									
								</fieldset>
								<div class="legend-text input_title">Please Select Time  <span class="required">*</span></div>
								<fieldset class="radio-item-group">
									<div class="credit-group-wrapper site_time_main">
									    <div class="half_fieldset">
											<select ng-model="formParams.public_time" name="public_time" class="select_arrow" id="site_time" required>
												<option class="init" value="">Select</option>
												<?php 
													for($i=1;$i<13;$i++){
														echo "<option value='".$i."'>".$i."</option>";
													}
												?>
											</select>
										</div>
										<div class="half_fieldset half_fieldset_right">
											<select ng-model="formParams.public_am" name="public_am" class="select_arrow" id="site_am" required>
												<option class="init" value="">Select</option>
												<option value="AM">AM</option>
												<option value="PM">PM</option>	
											</select>
										</div>
										<div class="clear"></div>
										<p ng-show="(show_error && formValidation && !formParams.public_time && !formParams.public_am )" class="my-error">Choose Time</p>
									</div>
								</fieldset>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('public_stage1')">Back</button>
								<button type="button" class="btn next cross-button btn-red" ng-click="next(formParams.public_participants == 'more than 5' ? 'contact_to_admin' : 'public_stage3')">Continue</button>
							</div>
						</div>
						<div ng-switch-when="public_stage3" id="public_stage3" class="animate-switch formstep slide visible" data-stage="3">
							<div class="slide-wrapper">
								<h2>Contact Details: </h2>
								<div class="legend-text input_title">First  Name  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item firstname_main">
										<input ng-model="formParams.public_firstname" class="txtfield fullname" name="public_firstname" id="public_firstname" placeholder="First  Name" value="" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.public_firstname)" class="my-error">Enter First  Name</div>
									</div>
								</fieldset>
								<div class="legend-text input_title">Last Name  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item lastname_main">
										<input ng-model="formParams.public_lastname" class="txtfield fullname" name="public_lastname" id="public_lastname" placeholder=" Last Name" value="" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.public_lastname)" class="my-error">Enter  Last Name</div>
									</div>
								</fieldset>
								<div class="legend-text input_title"> E-mail  <span class="required">*</span></div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item email_main">
										<input ng-model="formParams.public_email" class="txtfield email" name="public_email" id="email" placeholder="Your E-mail" value="" type="text" required>
										<div ng-show="(show_error && formValidation && !formParams.public_email)" class="my-error" >Enter Valid Email</div>
									</div>
								</fieldset>
								<div class="legend-text input_title">Contact No</div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item contact_main">
										<input ng-model="formParams.public_contact" class="txtfield phonemask" size="10" name="public_contact" id="public_contact" placeholder="Your Contact No" value="" type="text">
									</div>
								</fieldset>
								<div class="legend-text input_title">Company Name</div>
								<fieldset class="radio-item-group ">
									<div class="radio-item loan_form2_item company_main">
										<input ng-model="formParams.company" class="txtfield" name="company" id="company" placeholder="Your Company Name" value="" type="text">
									</div>
								</fieldset>
								<div class="legend-text input_title">Position / Job Title</div>
								<fieldset class="radio-item-group">
									<div class="radio-item loan_form2_item job_title_main">
										<input ng-model="formParams.job_title" class="txtfield" name="job_title" id="job_title" placeholder="Position / Job Title" value="" type="text">
									</div>
								</fieldset>
								<div class="clear"></div>
								<div id="are_you"></div>
								<div class="legend-text input_title are_you">Are you one of the {{formParams.public_participants}} participants?</div>
									<fieldset class="radio-item-group one_of_main">
										<div class="loan-radio-item loan_form2_item ">
											<input id="one_of1" ng-model="formParams.one_of" name="one_of" class="btn-radio" value="Yes"  type="radio" required>
											<label class="label-bottom" for="one_of1">Yes</label>
											<div class="radio-select"></div>
										</div>
										<div class="loan-radio-item loan_form2_item loantype-2">
											<input id="one_of2" ng-model="formParams.one_of" name="one_of" class="btn-radio" value="No" type="radio" required>
											<label class="label-bottom" for="one_of2">No</label>
										</div>
										<div ng-show="(show_error && formValidation && !formParams.one_of)" class="my-error" >Choose member</div>
								</fieldset>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('public_stage2')">Back</button>
								<button type="button" class="btn next cross-button btn-red" ng-click="next((formParams.one_of == 'No' || formParams.public_participants > 1) ? 'public_stage4' : 'summary')">Continue</button>
							</div>
						</div>
						<div ng-switch-when="public_stage4" id="public_stage4" class="animate-switch formstep slide visible" data-stage="4">
							<div class="slide-wrapper">
								<h2>Participant details: </h2>
								
								<div ng-repeat="n in participants_range(formParams.public_participants)">

									<div class="formParams.public_participants n">
										<div class="legend-text input_title">Participant {{n}} First  Name</div>

										<fieldset class="radio-item-group ">
											<div class="radio-item loan_form2_item firstname_main">
												<input type="text" ng-attr-name="other_firstname{{$index + 1}}" ng-model="formParams.other_firstname[$index+1]" required>

												<div ng-show="(show_error && formValidation && !formParams.other_firstname[$index+1])" class="my-error">Enter First  Name</div>
											</div>
										</fieldset>
										<div class="legend-text input_title">Participant {{n}} Last Name</div>
										<fieldset class="radio-item-group ">
											<div class="radio-item loan_form2_item lastname_main">
												<input class="txtfield fullname" ng-model="formParams.other_lastname[$index+1]" ng-attr-name="other_lastname{{$index + 1}}" placeholder=" Last Name" value="" type="text" required>
												<div ng-show="(show_error && formValidation && !formParams.other_lastname[$index+1])" class="my-error">Enter  Last Name</div>
											</div>
										</fieldset>
										<div class="legend-text input_title"> Participant {{n}} E-mail</div>
										<fieldset class="radio-item-group ">
											<div class="radio-item loan_form2_item email_main">
												<input class="txtfield email" ng-model="formParams.other_email[$index+1]" ng-attr-name="other_email{{$index + 1}}" placeholder="Your E-mail" value="" type="text" required>
												<div ng-show="(show_error && formValidation && !formParams.other_email[$index+1])" class="my-error">Enter Valid Email</div>
											</div>
										</fieldset>
									</div>
								</div>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('public_stage3')">Back</button>
								<button type="button" class="btn next cross-button btn-red" ng-click="next('summary')">Continue</button>
							</div>
						</div>


						<div ng-switch-when="contact_to_admin" id="contact_to_admin" class="animate-switch formstep slide visible" data-stage="5">
							<div class="slide-wrapper">
								<h2>Contact To Admin: </h2>
								<div class="legend-text custom"> 
									Please call 1300 241 715
								</div>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('stage')">Back</button>
								<?php /*?><button type="button" class="btn next cross-button btn-red" ng-click="next('finish')">Continue</button><?php */?>
							</div>
						</div>
						<div ng-switch-when="summary" id="summary" class="animate-switch formstep slide visible" data-stage="6">
							<div class="slide-wrapper summery_section">
								<h3>Summary</h3>
								<div ng-if="formParams.where_train == 'Onsite'">
									<p><strong>Where Train :</strong> {{formParams.where_train}}</p>
									<p><strong>Location : </strong>{{formParams.onsite_location}}</p>
									<p><strong>Course :</strong> {{formParams.site_course}}</p>
									<p><strong>Participants :</strong> {{formParams.site_participants}}</p>
									<p><strong>Firstname :</strong> {{formParams.site_firstname}}</p>
									<p><strong>Lastname :</strong> {{formParams.site_lastname}}</p>
									<p><strong>Email :</strong> {{formParams.site_email}}</p>
									<p><strong>Contact :</strong> {{formParams.site_contact}}</p>
									<p><strong>Company :</strong> {{formParams.site_company}}</p>
									<p><strong>Job Title :</strong> {{formParams.job_title}}</p>
									
									<p><strong>Street :</strong> {{formParams.street}}</p>
									<p><strong>City :</strong> {{formParams.city}}</p>
									<p><strong>State :</strong> {{formParams.state}}</p>
									<p><strong>Postcode :</strong> {{formParams.postcode}}</p>
									<p><strong>Comment :</strong> {{formParams.comment}}</p>
									<p><strong>Services :</strong> 
										<p ng-repeat="val in formParams.services">
											{{val}}
										</p>
									</p>
								</div>
								<div ng-if="formParams.where_train == 'Public'">
									<p><strong>Where Train : </strong>{{formParams.where_train}}</p>
									<p><strong>Location : </strong>{{formParams.public_location}}</p>
									<p><strong>Course : </strong>{{formParams.public_course}}</p>
									<p><strong>Participants : </strong>{{formParams.public_participants}}</p>
									<p><strong>Booking Date : </strong>{{formParams.public_booking_date}}</p>
									<p><strong>Time : </strong>{{formParams.public_time}} {{formParams.public_am}}</p>
									<p><strong>Firstname : </strong>{{formParams.public_firstname}}</p>
									<p><strong>Lastname : </strong>{{formParams.public_lastname}}</p>
									<p><strong>Email : </strong>{{formParams.public_email}}</p>
									<p><strong>Contact : </strong>{{formParams.public_contact}}</p>
									<p><strong>Company : </strong>{{formParams.company}}</p>
									<p><strong>Job Title : </strong>{{formParams.job_title}}</p>
								</div>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('stage')">Edit</button>
								<button type="button" class="btn next cross-button btn-red" ng-click="submitForm()">Continue</button>
								<?php /*?><button type="button" class="btn next cross-button btn-red" ng-click="next('payment')">Continue</button><?php */?>
								
							</div>
						</div>
						<div ng-switch-when="payment" id="payment" class="animate-switch formstep slide visible" data-stage="7">
							<div class="slide-wrapper">
								<h2>Payment</h2>
								
								<div class="legend-text input_title">Pay with following payment options Now</div>
								<div class="payment_inner">
									<ul class="course_payment_container">
										<li>
											<label for="stripe_payment_method"><input id="stripe_payment_method" type="radio" name="payment_method" value="stripe" checked="checked" /> Stripe Payment</label>
											<div class="payment_content stripe_content">
												<?php echo do_shortcode('[accept_stripe_payment name="aa" price="100" class="stripe_payment_btn btn btn-primary" button_text="Pay $'.$price.' now"]');?></div>
											</li>
										</li>
									</ul>
									<div class="visa_card_img"><img src="<?php echo get_template_directory_uri().'/images/visa-mastercard-discover.png'; ?>" /></div>
								</div>
								<fieldset class="radio-item-group">
									<div class="radio-item loan_form2_item terms_main">
										<input type="checkbox" name="terms" value="1" id="terms"/>
										<label class="label-bottom" for="terms">Accept Terms</label>
										<div class="my-error" style="display: none;">Accept Terms</div>
									</div>							
								</fieldset>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('public_stage3')">Back</button>
								
								<button type="button" class="btn next cross-button btn-red" ng-click="submitForm()">Continue</button>
							</div>
						</div>
						<div ng-switch-when="finish" id="finish" class="animate-switch formstep slide visible" data-stage="8">
							<div class="slide-wrapper">
								<h2>Finish</h2>
			                	<div class="legend-text custom">Thank you .</div>
								<button type="button" class="btn prev cross-button btn-red" ng-click="back('payment')">Back</button>
								<?php /*?><button type="button" class="btn next cross-button btn-red" ng-click="next('finish')">Continue</button><?php */?>
							</div>
						</div>


						
						
						<div class="animate-switch" ng-switch-when="success">
							<div class="success-wrap">
								<h2 class="confirmation-text">Thank you</h2>
								<p>Your message has been sent.<br>You should receive a confirmation email.</p>
								<div><button type="button" class="btn btn-success" ng-click="reset()" >Send Another</button></div>
							</div>
						</div>
						<div class="animate-switch" ng-switch-when="error" >
							<div class="error-wrap">
								<h2 class="confirmation-text">Error</h2>
								<p>There was an error when attempting to submit your request.<br>Please try again later.</p>
								<p><strong>*This will always error until a web service URL is set.*</strong></p>
								<div><button type="button" class="btn btn-danger" ng-click="reset()" >Try again</button></div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="booking_right col-sm-5 hidden-xs">
        	
        	<div id="feefo-service-review-carousel-widgetId" class="feefo-review-carousel-widget-service"></div>
        	<?php /*?><img src="<?php echo get_template_directory_uri().'/images/booking_right.png'; <?php */?>
        </div>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();
			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
			// End of the loop.
		endwhile; ?>
	</main><!-- .site-main -->
	<?php get_sidebar( 'content-bottom' ); ?>
	<div class="clear"></div>
</div><!-- .content-area -->
<?php //get_sidebar(); ?>
<?php get_footer('booking'); ?>
<script type="text/javascript"> jQuery(".chosen-select").chosen({width: "100%"}); </script>
