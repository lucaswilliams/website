<?php
/**
 * Template name: Gallery
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since jnext 1.0
 */
get_header('mega-menu'); ?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?> 
<div id="section1" class="header-overlap align-main">
	<div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
    <div class="banner-content align-banner">	
        <div class="wrap">
            <h1><?php echo $largetitle; ?></h1>
        </div>
    </div>
</div>
  <div id="maincontent" class="general-template gallery-page">
  <div class="wrap">
       <?php /* The loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  		  <div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'jext' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	  	<div class="gallery">
            <ul id="lightgalleryvedio" class="list-unstyled posts">					
				<?php $additional_loop = new WP_Query("post_type='ygallery'&paged=$paged&posts_per_page=9"); ?>	
				<?php $i = 1; while ($additional_loop->have_posts()) : $additional_loop->the_post(); ?>
					<?php 
					$v_url = get_the_post_video_url($post->ID);
					$v_url = substr(strrchr($v_url, '/'), 1);
					 ?>
				 	<li class="box" data-responsive="<?php echo "<div class='vid'>". get_the_post_video_url($post->ID)."</div>";?>" data-src="<?php echo "<div class='vid'>". get_the_post_video_url($post->ID)."</div>";?>" data-sub-html='<h3 class="post-title"><?php echo the_title(); ?></h3>
					 	<?php echo '<div class="video-content">'. the_content().'</div>'; ?>
						<a class="enq_btn" href="/contact-us/">ENQUIRE ABOUT a course like this </a>'>
						 <a href="#">
						 	<img class="yt-img" alt="First Aid Video <?php echo $i++; ?>" src="https://img.youtube.com/vi/<?php echo $v_url ; ?>/hqdefault.jpg"  />
						 	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/play-icon.png" alt="play" class="play-me" /></a>
					 	<h3><?php echo the_title(); ?></h3>
					</li>
				<?php endwhile; ?>
			</ul>
			<div class="clear"></div>
				<!--pagination-->
				<?php load_more_button(); ?>				
				<?php wp_reset_postdata(); ?>
          	</div>
	  	</div><!-- .entry-content -->
        <script type="text/javascript">
	        jQuery(document).ready(function(){
	            jQuery('#lightgalleryvedio').lightGallery();
	        });
        </script>
	<?php endwhile; ?>
	
	</article>
  </div>
  <!-- #content -->   
</div>
<!-- #primary -->
<script>
	jQuery('.see-less').click(function() {
			jQuery(this).parent().parent().parent().addClass('less');
			jQuery(this).parent().parent().parent().removeClass('fullc');
	});
	jQuery('.see-more').click(function() {
			jQuery('.reason .right').removeClass('fullc');
			jQuery(this).parent().parent().addClass('fullc');
			jQuery(this).parent().parent().removeClass('less');
	});
</script>
<?php get_footer();
