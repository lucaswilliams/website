<?php
/**
 * Template name: Our People
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');
?>
<?php
    if ( has_post_thumbnail() ) {
        $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    }
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<div class="container">
    <div class="row">
        <div class="col-12">

<?php
while ( have_posts() ) {
    the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="row">
            <div class="col-8 col-md-12">
                <h1>Our people</h1>
            </div>
            <div class="col-4 col-md-12">
                <?php
                $people = get_posts([
                    'post_type' => 'rfa_people',
                    'posts_per_page' => -1,
                    'orderby' => 'rand'
                ]);

                $pteams = get_terms([
                    'taxonomy' => 'rfa_people_teams',
                    'hide_empty' => false,
                    'slug' => $post->post_name
                ]);

                if(count($pteams) > 0) {
                    $pteam = $pteams[0];
                }

                $teams = get_terms([
                    'taxonomy' => 'rfa_people_teams',
                    'hide_empty' => false,
                    'parent' => $pteam->term_id
                ]);

                echo '<div class="row justify-content-center d-none d-md-flex" id="team-options">';
                echo '<div class="col-12 col-md-2 person-team-container">';
                echo '<a href="#" class="person-team active" data-id="0">All</a>';
                echo '</div>';
                foreach($teams as $team) {
                    echo '<div class="col-12 col-md-2 person-team-container">';
                    echo '<a href="#" class="person-team" data-id="'.$team->term_id.'">'.$team->name.'</a>';
                    echo '</div>';
                }
                echo '</div>';

                echo '<select name="team" id="team" class="d-md-none">';
                echo '<option value="0">All</option>';
                foreach($teams as $team) {
                    echo '<option value="'.$team->term_id.'">'.$team->name.'</option>';
                }
                echo '</select>'
                ?>
            </div>
        </div>

        <?php
        if(count($people) > 0) {
            echo '<div class="row">';
            foreach($people as $i => $person) {
                $my_teams = wp_get_post_terms($person->ID, 'rfa_people_teams');
                $teamlist = [];
                foreach($my_teams as $team) {
                    $teamlist[] = $team->term_id;
                }
                echo '<div class="col-6 col-lg-4">';
                echo '<div class="person" data-id="'.$i.'" data-teams="'.json_encode($teamlist).'">';
                $personThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($person->ID), 'medium');
                echo '<a href="#" class="person-modal" data-id="'.$i.'">';
                echo '<div class="image" style="background-image: url('.$personThumbnail[0].');"></div>';
                echo '<h2>'.$person->post_title.'</h2>';
                echo '<p>'.get_field('position', $person->ID).'</p>';
                echo '<div class="person-content">'.wpautop($person->post_content).'</div>';
                echo '</a>';
                echo '</div>';
                echo '</div>';
            }
            echo '</div>';
        }

        $questions = get_field('accordion_content');
        if(count($questions) > 0) {
            echo '<div class="accordion" id="peopleAccordion">';

            foreach ($questions as $i => $question) {
                echo '<div class="card">
                    <div class="card-header" id="heading'.$i.'">
                      <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse'.$i.'" aria-expanded="true" aria-controls="collapse'.$i.'">
                                '.$question['title'].'
                            </button>
                      </h2>
                    </div>
                
                    <div id="collapse'.$i.'" class="collapse" aria-labelledby="heading'.$i.'" data-parent="#peopleAccordion">
                      <div class="card-body">
                            '.$question['content'].'
                      </div>
                    </div>
                  </div>';
            }

            echo '</div>';
        }
        ?>
        <div class="modal fade" id="peopleModal" tabindex="-1" aria-labelledby="peopleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="person-content-body">

                    </div>
                </div>
            </div>
        </div>
    </article><!-- #post-## -->
<?php
    } // End of the loop.
?>
        </div>
    </div>
</div> <!-- .container -->

    </div>

<script>
    jQuery(document).ready(function($) {
        $(document).on('click', '.person-modal', function(e) {
            e.preventDefault();
            $('#person-content-body').html($('.person[data-id="' + $(this).data('id') + '"]').html());
            $('#person-content-body h2').unwrap();
            $('#person-content-body .person-content').removeClass('person-content').addClass('popup-content');
            $('#peopleModal').modal('show');
        });

        $(document).on('click', '.person-team', function(e) {
            e.preventDefault();
            _team = $(this).data('id');

            $('.person-team.active').removeClass('active');
            $(this).addClass('active');

            $.each($('.person'), function(k,v) {
                vdata = $(v).data('teams');
                if(vdata.includes(_team) || _team == 0) {
                    $(v).parent().show();
                } else {
                    $(v).parent().hide();
                }
            });
        });

        $(document).on('change', '#team', function(e) {
            e.preventDefault();
            _team = $(this).val();

            console.log(_team);

            $('.person-team.active').removeClass('active');
            $(this).addClass('active');

            $.each($('.person'), function(k,v) {
                vdata = $(v).data('teams');
                console.log(vdata);
                if(vdata.includes(_team) || _team == 0) {
                    $(v).parent().show();
                } else {
                    $(v).parent().hide();
                }
            });
        });
    });
</script>

<?php get_footer();
