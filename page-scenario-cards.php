<?php
/**
 * Template name: Scenario Cards
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

//get_header('mega-menu');
get_header('mega-menu');
?>
<?php
    $smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
    if ( has_post_thumbnail() ) {
        $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    } else {
        //We actually don't have an image, so...
        echo '<div class="container">
                <div class="row">
                    <div class="col">
                        <h1>'.$largetitle.'</h1>
						'.wpautop(get_field('introductory_content', $post->ID)).'
                    </div>
                </div>
            </div>';
}
?>

<div class="container">
    <div class="row">
        <div class="col-12">

<?php
while ( have_posts() ) {
    the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php
        $categories = get_field('categories');
        if(count($categories) > 0) {
            echo '<div class="accordion" id="categoryAccordion">';

            foreach ($categories as $i => $category) {
                echo '<div class="card">
                    <div class="card-header" id="heading'.$i.'">
                      <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse'.$i.'" aria-expanded="true" aria-controls="collapse'.$i.'">
                                '.$category['category_title'].'
                            </button>
                      </h2>
                    </div>
                    <div id="collapse'.$i.'" class="collapse" aria-labelledby="heading'.$i.'" data-parent="#categoryAccordion">
                        <div class="card-body">';
                    foreach($category['scenarios'] as $scenario) {
                        echo '<div class="card">
                                <div class="card-body inner">
                                    <div class="row">
                                        <div class="col">
                                            <img src="https://www.realresponse.com.au/wp-content/uploads/2020/07/icon-document.png" width="64px">
                                            <h2><a href="'.$scenario['scenario_file'].'" target="_blank">'.$scenario['scenario_title'].'</a></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                    }
                echo '</div></div></div>';
            }

            echo '</div>';
        }
        ?>
    </article><!-- #post-## -->
<?php
    } // End of the loop.
?>
        </div>
    </div>
</div> <!-- .container -->

    </div>

<script>
    jQuery(document).ready(function($) {
        
    });
</script>

<?php get_footer();
