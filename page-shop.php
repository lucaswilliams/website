<?php
/**
 * Template Name: Shop Pages 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<div class="margin-up"></div>
<div id="main" class="site-main wooco-main">
  <header class="entry-header">
    <div class="cart-shop">
        <div class="auto-area">
          <?php dynamic_sidebar('shop-header-button');?>
        </div>
    </div>
    <div class="title-bg">
      <div class="auto-area">
        <div class="title-area">
          <h2 class="entry-title"><?php the_title(); ?></h2>
          <div class="clear"></div>
        </div>
      </div>    
    </div>
  </header>
<div id="maincontent" class="general-template">
	<div class="container">
      <?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/page/content', 'custompage' );

		endwhile; // End of the loop.
	  ?>
    </div>
</div>
</div>
<?php get_footer();



