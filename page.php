<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<?php
if(has_post_video()) {
    $thumbnail = '<video autoplay muted loop id="myVideo">
            <source src="'.get_the_post_video_url().'" type="video/mp4">
        </video>';
} elseif ( has_post_thumbnail() ) {
    $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    $thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
}
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

    <div id="section1" class="header-overlap">
        <div class="featured-banner"><?php echo $thumbnail; ?></div>
        <div class="banner-content">
            <div class="wrap ">
                <h6><?php echo $smalltitle; ?></h6>
                <h1><?php echo $largetitle; ?></h1>
                <?php if($post->post_name !== 'covid-19-real-response') { ?>
                <div class="entry-content">
                    <div class="rating_box">
                        <a id="popup-page" class="iframe2 btn btn-primary cboxElement" href="#contact_popup">Enquire Now</a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>

<div id="maincontent" class="general-template">
	<div class="wrap">
      <?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/page/content', 'custompage' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
	  ?>
    </div>
</div>

<?php get_footer();



