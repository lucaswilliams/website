<?php
/**
 * Template name:About Us
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');
//get_header('mega-menu');
?>
<?php if ( has_post_thumbnail() ) :
    $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
endif;
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<header class="page-head">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="tile">
                    <div class="row">
                        <div class="col-12 col-md-7">
                            <h1><?php echo $largetitle; ?></h1>
                            <?php
                                echo wpautop(get_field('introductory_content'));
                            ?>
                        </div>

                        <div class="col-12 col-md-5">
                            <div class="head-card-image" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);">
                                <!--<img src="<?php echo esc_url( $thumbnail[0] ); ?>">-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<?php
while(have_posts()){
    the_post();
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div id="our-story">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1>Our Story</h1>
                        <?php
                        $content = wpautop(get_field('our_story', $post->ID));
                        echo $content;
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <?php $story = get_field('our_story_elements', $post->ID); ?>
        <div id="our-story-timeline" class="d-none d-md-block">
            <div class="container" style="position:relative;">
                <div class="row">
                    <div class="col-12" style="overflow-x: scroll; cursor: grab;" id="entry-container">
                        <div class="timeline-entries" id="timeline-entries" style="width: <?php echo (count($story) + 1) * 185; ?>px">
                        <?php
                        function desc_block($elem) {
                            echo '<div class="filled">';
                            echo '<h2>'.$elem['year'].'</h2>';
                            echo wpautop('<strong>'.$elem['description'].'</strong><br>'.$elem['story']);
                            echo '</div>';
                        }
                        foreach($story as $key => $elem) {
                            echo '<div class="timeline-entry">';
                            echo '<div class="timeline-desc">';
                            if($key % 2 == 1) {
                                desc_block($elem);
                            }
                            echo '</div>';
                            echo '<div class="timeline-desc">';
                            if($elem['image']) {
                                echo '<div class="timeline-img" style="background-image: url('.$elem['image']['url'].');"></div>';
                            }
                            echo '</div>';
                            echo '<div class="timeline-desc">';

                            if($elem['image_2']) {
                                echo '<div class="timeline-img" style="background-image: url('.$elem['image_2']['url'].');"></div>';
                            } else {
                                if($key % 2 == 0) {
                                    echo '<div class="filled">';
                                    echo '</div>';
                                }
                            }
                            echo '</div>';
                            echo '<div class="timeline-desc">';
                            if($key % 2 == 0) {
                                desc_block($elem);
                            }
                            echo '</div>';
                            echo '</div>';
                        }
                        ?>
                        </div>
                    </div>
                    <div id="timeline-indicators" class="col-12 justify-content-center"></div>
                </div>
                <div id="indicator-before"></div>
                <div id="indicator-after"></div>
            </div>
        </div>
        <div id="our-story-timeline-mobile" class="d-md-none">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php
                    $active = 'active';
                    foreach($story as $key => $elem) {
                        echo '<div class="carousel-item '.$active.'">';
                        if($elem['image']) {
                            echo '<div class="timeline-img" style="background-image: url('.$elem['image']['url'].');"></div>';
                        }
                        if($elem['image_2']) {
                            echo '<div class="timeline-img" style="background-image: url('.$elem['image_2']['url'].');"></div>';
                        } else {
                            echo '<div class="timeline-img"></div>';
                        }
                        echo '<div class="timeline-desc">';
                        desc_block($elem);
                        echo '</div>';
                        echo '</div>';
                        $active = '';
                    }
                    ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div id="our-story-2">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <?php
                        $content = wpautop(get_field('our_story_lower', $post->ID));
                        $parts = explode('</p>', $content);
                        $start = array_shift($parts);
                        echo $start.'</p>';
                        echo '<div class="d-block d-md-none"><a class="readmore" href="#">Read more</a></div>';
                        echo '<div class="d-none d-md-block">'.implode('</p>', $parts).'</div>';
                        echo '<div class="d-none"><a class="readless" href="#">Read less</a></div>';
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="our-team">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <h1>Our team</h1>
                        <?php echo wpautop(get_field('our_team_blurb', $post->ID)); ?>
                        <p class="d-none d-md-inline-block text-center"><a href="/our-people" class="btn btn-default">MEET WHOLE TEAM</a></p>
                    </div>
                    <div class="col-12 col-md-8">
                        <?php
                            $people = get_posts([
                                'post_type' => 'rfa_people',
                                'posts_per_page' => -1,
                                'orderby' => 'rand'
                            ]);

                            $personnumber = 1;

                            if(count($people) > 0) {
                                echo '<div class="row" id="personContainer">';
                                foreach($people as $i => $person) {
                                    echo '<div class="col-6 rrperson'.($personnumber > 4 ? ' hidden' : '').'" data-pos="'.$personnumber.'">';
                                    echo '<div class="person" data-id="'.$i.'">';
                                    $personThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($person->ID), 'medium');
                                    echo '<a href="#" class="person-modal" data-id="'.$i.'">';
                                    echo '<div class="image" style="background-image: url('.$personThumbnail[0].');"></div>';
                                    echo '<h2>'.$person->post_title.'</h2>';
                                    echo '<p>'.get_field('position', $person->ID).'</p>';
                                    echo '</a>';
                                    echo '</div>';
                                    echo '</div>';
                                    $personnumber++;
                                }
                                echo '</div>';
                            }

                            echo '<div id="people-indicators">';
                            for($i = 0; $i <= (count($people) / 4); $i++) {
                                echo '<div class="people-indicator'.($i == 0 ? ' active' : '').'"></div>';
                            }
                            echo '</div>';
                        ?>
                    </div>
                    <div class="col-12">
                        <p class="d-md-none text-center" style="margin-top: 64px;"><a href="/our-people" class="btn btn-default">MEET WHOLE TEAM</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div id="our-mission">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="mission-summary">
                            <h1>Our mission</h1>
                            <?php
                            $content = wpautop(get_field('our_mission', $post->ID));
                            $parts = explode('</p>', $content);
                            $start = array_shift($parts);
                            echo $start.'</p>';
                            echo '<div class="d-block d-md-none"><a class="readmore" href="#">Read more</a></div>';
                            echo '<div class="d-none d-md-block">'.implode('</p>', $parts).'</div>';
                            echo '<div class="d-none"><a class="readless" href="#">Read less</a></div>';
                            ?>
                        </div>
                        <div class="mission-highlight">
                            <?php echo wpautop(get_field('our_mission_highlight', $post->ID)); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="our-values">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1>Our values</h1>
                    </div>
                </div>
                <div class="row">
                    <?php
                        $values = get_field('our_values', $post->ID);
                        foreach($values as $value) {
                            echo '<div class="col-12 col-md-6 col-lg-4">';
                            echo '<h2>'.$value['title'].'</h2>';
                            echo wpautop($value['text']);
                            echo '</div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </article><!-- #post-## -->
<?php
} // End of the loop.
?>
    </div>
<script>
    const ele = document.getElementById('entry-container');
    ele.style.cursor = 'grab';

    let pos = { top: 0, left: 0, x: 0, y: 0 };
    let maxwidth = document.getElementById('timeline-entries').offsetWidth - document.getElementById('entry-container').offsetWidth;
    //console.log(maxwidth);

    const calculateIndicators = function() {
        //console.log(pos);
        if(pos.left == 0) {
            document.getElementById('indicator-before').style.display = "none";
        } else {
            document.getElementById('indicator-before').style.display = "block";
        }

        if(pos.left > maxwidth) {
            document.getElementById('indicator-after').style.display = "none";
        } else {
            document.getElementById('indicator-after').style.display = "block";
        }

        let myMax = maxwidth + 30;
        let scroll = Math.abs(jQuery('#timeline-entries').position().left);
        let percentage = Math.floor(scroll / myMax * 100);
        let blocks = Math.ceil(maxwidth / jQuery('.timeline-entry').width()) + 1;

        let _blocksContent = '';
        for(i = 0; i < blocks; i++) {
            _blocksContent += '<div class="timeline-indicator';
            let _percLow = i * (100/blocks);
            let _percHigh = (i + 1) * (100/blocks);
            if(percentage > _percLow && percentage <= _percHigh) {
                _blocksContent += ' active';
            }
            _blocksContent += '"></div>';
        }

        jQuery('#timeline-indicators').html(_blocksContent);
    };

    const mouseDownHandler = function(e) {
        ele.style.cursor = 'grabbing';
        ele.style.userSelect = 'none';

        pos = {
            left: ele.scrollLeft,
            top: ele.scrollTop,
            // Get the current mouse position
            x: e.clientX,
            y: e.clientY,
        };

        document.addEventListener('mousemove', mouseMoveHandler);
        document.addEventListener('mouseup', mouseUpHandler);
    };

    const mouseMoveHandler = function(e) {
        // How far the mouse has been moved
        const dx = e.clientX - pos.x;
        const dy = e.clientY - pos.y;

        // Scroll the element
        ele.scrollTop = pos.top - dy;
        ele.scrollLeft = pos.left - dx;
    };

    const mouseUpHandler = function() {
        ele.style.cursor = 'grab';
        ele.style.removeProperty('user-select');

        pos.left = ele.scrollLeft;
        pos.top = ele.scrollTop;

        calculateIndicators();

        document.removeEventListener('mousemove', mouseMoveHandler);
        document.removeEventListener('mouseup', mouseUpHandler);
    };

    // Attach the handler
    ele.addEventListener('mousedown', mouseDownHandler);
    calculateIndicators();

    document.getElementById('indicator-before').addEventListener('click', function(e) {
        if(pos.left > 186) {
            pos.left -= 187;
            ele.scrollLeft = pos.left;
        } else {
            pos.left = 0;
            ele.scrollLeft = 0;
        }
        calculateIndicators();
    });

    document.getElementById('indicator-after').addEventListener('click', function(e) {
        if(pos.left < maxwidth) {
            pos.left += 187;
            ele.scrollLeft = pos.left;
        } else {
            pos.left = maxwidth;
            ele.scrollLeft = maxwidth;
        }
        calculateIndicators();
    });

    jQuery(document).ready(function($) {
        _first = $('.rrperson:not(.hidden):first').data('pos');
        _last = $('.rrperson:not(.hidden):last').data('pos');

        $('.person-modal').click(function(e) {
            e.preventDefault();
        });

        function rotatePeople() {
            $('#personContainer').fadeOut(400, function(e) {
                _total = $('.rrperson').length;
                //get the pos of the first and last unhidden ones

                _newfirst = _first + 4;
                if(_newfirst > _total) {
                    _newfirst-= _total;
                }
                _newlast = _last + 4;
                if(_newlast > _total) {
                    _newlast-= _total;
                }

                $.each($('.rrperson'), function(k,v) {
                    _pos = $(v).data('pos');
                    if(_newfirst > _newlast) {
                        //a bunch in the middle are hidden
                        if(_pos >= _newfirst || _pos <= _newlast) {
                            $(v).removeClass('hidden');
                        } else {
                            $(v).addClass('hidden');
                        }
                    } else {
                        if(_pos >= _newfirst && _pos <= _newlast) {
                            $(v).removeClass('hidden');
                        } else {
                            $(v).addClass('hidden');
                        }
                    }
                });

                _first = _newfirst;
                _last = _newlast;

                $next = $('.people-indicator.active').removeClass('active').next();
                if($next.length) {
                    $next.addClass('active');
                } else {
                    $('.people-indicator:first').addClass('active');
                }

                $('#personContainer').fadeIn(400, function(e) {
                    setTimeout(function() {
                        rotatePeople();
                    }, 5000);
                });
            });
        };

        setTimeout(function() {
            rotatePeople();
        }, 5000);

        $(document).on('click', '.readmore', function(e) {
            e.preventDefault();
            //read more
            var _parent = $(this).parent();
            _parent.removeClass('d-block').removeClass('d-md-none').addClass('d-none');
            //content
            var _next = _parent.next();
            _next.removeClass('d-md-block').removeClass('d-none');
            //read less...
            _next.next().removeClass('d-none').addClass('d-block').addClass('d-md-none');
        });

        $(document).on('click', '.readless', function(e) {
            e.preventDefault();
            //read less
            var _parent = $(this).parent();
            _parent.removeClass('d-block').removeClass('d-md-none').addClass('d-none');
            //content
            var _next = _parent.prev();
            _next.addClass('d-md-block').addClass('d-none');
            //read more...
            _next.prev().removeClass('d-none').addClass('d-block').addClass('d-md-none');
        });
    });
</script>
<?php get_footer();