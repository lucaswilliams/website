<?php

/**
 * Template name: 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Real Response
 */

get_header('mega-menu'); ?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<div id="section1" class="header-overlap">
    <div class="featured-banner">
	    <img src="<?php echo esc_url( $thumbnail[0] ); ?>" alt="Banner image" class="img-responsive"/>
    </div>
    <div class="banner-content">	
        <div class="wrap">
            <h6><?php echo $smalltitle; ?></h6>
            <h1><?php echo $largetitle; ?></h1>
        </div>
    </div>
</div>

<div id="maincontent" class="general-template case_study">
	<div class="wrap"> 
       <?php /* The loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<!-- .entry-header -->	
		
		  <div class="entry-content">
		     	<div class="gallery">
            <ul id="lightgallery" class="list-unstyled posts">
						  <?php
							$all_case_posts=get_posts(array('post_type'=>'case-study','posts_per_page'=>40));
							$i=0;
							foreach($all_case_posts as $case_post){
							$i=$i+1;
							$case_post=(array)$case_post;
							$image = wp_get_attachment_image_src( get_post_thumbnail_id($case_post['ID']), 'single-post-thumbnail' );
							?>
							 <li class="box" data-responsive="<?php echo $image[0]; ?>" data-src="<?php echo $image[0]; ?>" data-sub-html='<h3 class="post-title"><?php echo $case_post['post_title']; ?></h3>
							 	<?php
								echo $case_post['post_content'];
								?>
								<a class="enq_btn" href="/contact-us/">ENQUIRE ABOUT a course like this </a>'>
								 <a href="#">
									<?php if($image){ ?><img  alt="" src="<?php echo $image[0]; ?>"/><?php } ?>
								 </a>
								 <h3>
								<?php
								echo $case_post['post_title'];
								?>
								</h3>
							</li>
							<?php
							} ?>
            </ul>
				<div class="clear"></div>
          </div>
				<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery('#lightgallery').lightGallery();
				});
				</script>
				  <?php comments_template(); ?>
				  <?php endwhile; ?>
		  </div>		  
	</article>
  </div>
  <!-- #content --> 

</div>
<!-- #primary -->
<script>
	jQuery('.see-less').click(function() {
	jQuery(this).parent().parent().parent().addClass('less');
			jQuery(this).parent().parent().parent().removeClass('fullc');
	});
	jQuery('.see-more').click(function() {
			jQuery('.reason .right').removeClass('fullc');
			jQuery(this).parent().parent().addClass('fullc');
			jQuery(this).parent().parent().removeClass('less');
	});

</script>
<?php get_footer(); ?>



