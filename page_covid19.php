<?php
/**
 * Template name: COVID-19 Course
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<header class="page-head">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="tile">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <h1><?php echo $largetitle; ?></h1>
                            <p id="breadcrumbs"><?php rr_breadcrumbs($post->ID); ?></p>
                            <?php echo wpautop(get_field('introductory_content')); ?>
                            <p><a href="<?php echo site_url(); ?>/booking/eyJjaWQiOiIyNDUxNiIsInRpZCI6IjIifQ==" class="btn btn-primary">Enrol Now</a></p>
                        </div>
                        
                        <div class="col-12 col-md-7">
                            <div class="head-card-image" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);">
                                <img src="<?php echo esc_url( $thumbnail[0] ); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="maincontent" class="course-pages courses">
<?php
while ( have_posts() ) : the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <article id="services" class="realresponse-panel post-70 page type-page status-publish hentry">
            <div class="panel-content ">
                <div class="wrap">
                    <header class="entry-header">
                        <h2 class="entry-title">Includes 3 Units</h2>
                    </header>
                    <div class="entry-content container">
                        <div class="row row-eq-height">
                            <?php foreach(get_field('how_it_works') as $box) {
                                echo '<div class="col-12 col-md-4">
                                    <div class="how_it_works">
                                        <div class="simage"><img src="'.$box['icon']['url'].'" /></div>
                                        <div class="scontent">
                                            <h2>'.$box['title'].'</h2>
                                            <div class="desc">'.$box['content'].'</div>
                                        </div>
                                    </div>
                                </div>';
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <header class="page-head">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-11" id="findCourse">
                        <div class="tile">
                            <div class="row align-items-center">
                                <div class="col-12 text-center">
                                    <?php the_field('corporate_notice'); ?>
                                    <a href="https://www.realresponse.com.au/booking/eyJjaWQiOiIyNDUxNiIsInRpZCI6IjIifQ==" class="btn btn-primary">Enrol Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="most_popular_courses" id="courses">
            <div class="container">
                <div class="row row-eq-height justify-content-center">
                    <?php
                    $courses = get_field('product_boxes');
                    foreach($courses as $course) {
                        //$course = get_field('product_boxes', $cse->ID)[0];
                        $cid = '';
                        if($course['booking_course'] != null) {
                            $cid = base64_encode('{"cid" : '.$course['virtual_booking_course'][0].', "tid" : 2}');
                        }
                        ?>
                        <div class="col-12 col-lg-8">
                            <div class="course">
                                <div class="row d-none d-md-flex">
                                    <div class="col-7">
                                        <h3 class="header2"><?php echo $course['header']; ?></h3>
                                        <h4><?php echo $course['subheader']; ?></h4>
                                        <p class="code"><?php echo $course['unit_code']; ?></p>
                                        <a class="btn btn-primary" title="Enrol Now" href="<?php echo site_url(); ?>/booking/<?php echo $cid; ?>">Enrol Now</a>
                                        <div class="group_b_icon">
                                            Group Bookings &amp;<br />
                                            Public Course Available
                                            <img src="https://www.realresponse.com.au/wp-content/uploads/2018/01/group-booking-public-course_info-03.png" alt="icon">
                                            <div class="gcontent" style="display: none;">
                                                This course is available as an onsite course where we come out to your site or as a public course where you come to one of our training facilities
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <?php
                                        if(strlen($course['course_length']) > 0) {
                                            echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
                                        }
                                        
                                        if(strlen($course['certificate_length']) > 0) {
                                            echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
                                        }
                                        
                                        if($course['nationally_accredited']) {
                                            echo '<p><img src="'.get_theme_file_uri('assets/images/nrt.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
                                        }
                                        ?>
                                        <p>Course details:</p>
                                        <?php echo $course['description']; ?>
                                    </div>

                                    <div class="accordion d-md-none">
                                        <div class="accordion-control">More Details</div>

                                        <div class="accordion-content">
                                            <?php
                                            if(strlen($course['course_length']) > 0) {
                                                echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
                                            }
                                            
                                            if(strlen($course['certificate_length']) > 0) {
                                                echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
                                            }
                                            
                                            if($course['nationally_accredited']) {
                                                echo '<p><img src="'.get_theme_file_uri('assets/images/nrt.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
                                            }
                                            ?>
                                            <p>Course details:</p>
                                            <?php echo $course['description']; ?>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="col-9 col-md-12 d-md-none"><?php echo $course['header']; ?></h3>
                                <p class="col-3 d-md-none text-right"><?php echo $course['unit_code']; ?></p>
                                <h4 class="col-9 col-md-12 d-md-none"><?php echo $course['subheader']; ?></h4>
                                <span class="col-3 d-md-none">
                                    <a class="btn btn-primary mob-enq" title="Enrol Now" href="https://www.realresponse.com.au/booking/">Book</a>
                                </span>

                                <div class="accordion d-md-none">
                                    <div class="accordion-content">
                                        <?php
                                        if(strlen($course['course_length']) > 0) {
                                            echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
                                        }
                                        
                                        if(strlen($course['certificate_length']) > 0) {
                                            echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
                                        }
                                        
                                        if($course['nationally_accredited']) {
                                            echo '<p><img src="'.get_theme_file_uri('assets/images/nrt.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
                                        }
                                        ?>
                                        <p>Course details:</p>
                                        <?php echo $course['description']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </article><!-- #post-## -->
<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



