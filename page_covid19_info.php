<?php
/**
 * Template name: COVID-19 Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');
//$post = get_page_by_path( 'blog', OBJECT, 'page' );
if ( has_post_thumbnail() ) {
    $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    //$thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
}

$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

    <header class="page-head">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="tile">
                        <div class="row">
                            <div class="col-12 col-md-5">
                                <h1><?php echo $largetitle; ?></h1>
                                <p id="breadcrumbs"><?php rr_breadcrumbs($post->ID); ?></p>
                                <?php echo wpautop(get_field('introductory_content')); ?>
                                <p><a href="<?php echo site_url(); ?>/booking" class="btn btn-primary book-now">Book Now</a></p>
                            </div>

                            <div class="col-12 col-md-7">
                                <div class="head-card-image" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);">
                                    <img src="<?php echo esc_url( $thumbnail[0] ); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="maincontent" class="course-pages courses">
<?php
while ( have_posts() ) : the_post();
    ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <article id="services" class="realresponse-panel post-70 page type-page status-publish hentry">
            <div class="panel-content ">
                <div class="wrap">
                    <header class="entry-header">
                        <h2 class="entry-title">COVID-19 Services</h2>
                    </header>
                    <div class="entry-content container">
                        <div class="row row-eq-height justify-content-center">
                            <?php foreach(get_field('how_it_works') as $box) {
                            echo '<div class="col-12 col-md-6 col-lg-3">
                                    <div class="how_it_works">
                                        <div class="simage"><img src="'.$box['icon']['url'].'" /></div>
                                        <div class="scontent">
                                            <h2>'.$box['title'].'</h2>
                                            <div class="desc">'.$box['content'].'</div>
                                        </div>';

                            if($box['has_link'] != 0) {
                                $link = '#contact_popup';
                                $classes = ' mob-enq iframe2';
                                if($box['has_link'] == 2) {
                                    $link = $box['link_address'].'" target="_blank';
                                    $classes = '';
                                }?>
                                <div class="link">
                                    <a class="btn btn-primary<?php echo $classes; ?>" href="<?php echo $link; ?>"><?php echo $box['link_text']; ?></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </article>

        <div class="container">
            <div class="row">
                <div class="col">
                    <?php the_content(); ?>

                    <?php
                    $vr_stories = get_field('content_block', $post->ID);
                    $i = 0;
                    foreach ($vr_stories as $story) {
                        $i++;
                    ?>
                        <a id="section-<?php echo $i; ?>" class="internal-anchor"></a>
                        <h2><?php echo $story['title']; ?></h2>
                        <?php echo wpautop($story['content']); ?>
                        <br>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="container" id="covid-faqs">
            <div class="row">
                <div class="col-12">
                    <h2 class="entry-title">Frequently Asked Questions</h2>
                    <?php
                    $faq_cats = get_field('faq_category');
                    echo do_shortcode('[superior_faq layout="boxed" open_icon="plus" close_icon="minus" headline_tag="h4" permalink="no" deeplinking="yes" animate_content="yes" excerpt="no" categories="'.$faq_cats.'"]');
                    ?>
                </div>
            </div>
        </div>
    </article>
<?php
endwhile; // End of the loop.
?>
    </div>
<?php get_footer();