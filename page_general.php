<?php
/**
 * Template name:General
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<div id="section1" class="header-overlap align-main">
    <?php if(!empty($thumbnail)){ ?>
		<div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
    <?php } ?>
    <div class="banner-content align-banner">	
        <div class="wrap">
        	<?php if($smalltitle){ ?>
            	<h6><?php echo $smalltitle; ?></h6>
        	<?php } ?>
            <h1><?php echo $largetitle; ?></h1>
        </div>
    </div>
</div>

<div id="maincontent" class="general-template">
	<div class="wrap">
      <?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/page/content', 'custompage' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
	  ?>
    </div>
</div>

<?php get_footer();



