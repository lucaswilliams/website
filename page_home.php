<?php
/**
 * Template name: Home Page 2021
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

//get_header('mega-menu');
get_header('mega-menu');
?>
<?php if ( has_post_thumbnail() ) :
    $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
endif;
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<header class="page-head" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);"></header>

<?php
while(have_posts()){
    the_post();
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div id="our-story">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-7">
                        <div class="head-card">
                            <?php
                            $content = wpautop(get_field('introductory_content', $post->ID));
                            echo $content;
                            ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="head-highlight">
                            <?php echo $largetitle; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="our-clients">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-4">
                        <h1>Our clients</h1>
                        <?php echo wpautop(get_field('clients_blurb', $post->ID)); ?>
                        <!--<p class="d-none d-md-inline-block text-center"><a href="/our-people" class="btn btn-default">Learn More</a></p>-->
                    </div>
                    <div class="col-12 col-md-8">
                        <?php
                            $pteams = get_terms([
                                'taxonomy' => 'rfa_client_groups',
                                'hide_empty' => false,
                                'slug' => 'home'
                            ]);

                            $people = get_posts([
                                'post_type' => 'rfa_client',
                                'posts_per_page' => -1,
                                'orderby' => 'rand',
                                'tax_query' => [
                                    [
                                        'taxonomy' => 'rfa_client_groups',
                                        'field' => 'term_id',
                                        'terms' => $pteams[0]->term_id,
                                        'include_children' => false
                                    ]
                                ]
                            ]);

                            $personnumber = 1;

                            if(count($people) > 0) {
                                echo '<div class="row" id="personContainer">';
                                foreach($people as $i => $person) {
                                    $personThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($person->ID), 'medium');
                                    if(is_array($personThumbnail) && count($personThumbnail) > 0) {
                                        echo '<div class="col-6 rrperson' . ($personnumber > 4 ? ' hidden' : '') . '" data-pos="' . $personnumber . '">';
                                        echo '<div class="person" data-id="' . $i . '">';
                                        echo '<div class="image" style="background-image: url(' . $personThumbnail[0] . ');"></div>';
                                        echo '</div>';
                                        echo '</div>';
                                        $personnumber++;
                                    }
                                }
                                echo '</div>';
                            }

                            echo '<div id="people-indicators">';
                            for($i = 0; $i <= ($personnumber / 4); $i++) {
                                echo '<div class="people-indicator'.($i == 0 ? ' active' : '').'"></div>';
                            }
                            echo '</div>';
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="our-services">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1>Our Services</h1>
                    </div>
                </div>
                <div class="row mx-auto my-auto">
                    <div id="serviceCarousel" class="carousel slide w-100" data-ride="carousel">
                        <div class="carousel-inner w-100" role="listbox">
                                <?php
                                    $i = 0;
                                    $values = get_field('our_services', $post->ID);
                                    foreach($values as $value) {
                                        echo '<div class="carousel-item '.($i == 0 ? 'active' : '').'">';
                                        echo '<div class="col-6 col-md-4 col-lg-2">';
                                        echo '<a class="service" href="'.$value['link'].'">';
                                        echo '<img src="'.$value['image']['url'].'">';
                                        echo '<p>'.$value['title'].'</p>';
                                        echo '</a>';
                                        echo '</div>';
                                        echo '</div>';
                                        $i++;
                                    }
                                ?>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#serviceCarousel" role="button" data-slide="prev">
                    <i class="fa fa-chevron-left fa-lg text-muted"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next text-faded" href="#serviceCarousel" role="button" data-slide="next">
                    <i class="fa fa-chevron-right fa-lg text-muted"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div id="logos">
            <div class="container">
                <div class="row">
                    <?php foreach(get_field('logos', $post->ID) as $logo) {
                        echo '<div class="col-6 col-md-3"><div class="logo" style="background-image: url(\''.$logo['image']['sizes']['medium'].'\');"></div></div>';
                    } ?>
                </div>
            </div>
        </div>

        <div id="our-values">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1>What makes us different</h1>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <?php
                        $values = get_field('what_makes_us_different', $post->ID);
                        foreach($values as $value) {
                            echo '<div class="col-12 col-md-6 col-lg-4 difference">';
                            echo '<h2>'.$value['title'].'</h2>';
                            echo wpautop($value['text']);
                            echo '<p class="text-center"><img src="'.get_template_directory_uri().'/assets/images/pink-tick.svg"></p>';
                            echo '</div>';
                        }
                    ?>
                </div>
            </div>
        </div>

        <div id="about-rr">
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <h1>About Real Response</h1>
                        <?php echo wpautop(get_field('about_real_response', $post->ID)); ?>
                    </div>
                </div>
            </div>
        </div>
    </article><!-- #post-## -->
<?php
} // End of the loop.
?>
    </div>
<script>
    jQuery(document).ready(function($) {
        _first = $('.rrperson:not(.hidden):first').data('pos');
        _last = $('.rrperson:not(.hidden):last').data('pos');

        _sfirst = $('.servicectr:not(.hidden):first').data('pos');
        _slast = $('.servicectr:not(.hidden):last').data('pos');

        console.log(_sfirst, _slast);

        $('.person-modal').click(function(e) {
            e.preventDefault();
        });

        function rotatePeople() {
            $('#personContainer').fadeOut(400, function(e) {
                _total = $('.rrperson').length;
                //get the pos of the first and last unhidden ones

                _newfirst = _first + 4;
                if(_newfirst > _total) {
                    _newfirst-= _total;
                }
                _newlast = _last + 4;
                if(_newlast > _total) {
                    _newlast-= _total;
                }

                $.each($('.rrperson'), function(k,v) {
                    _pos = $(v).data('pos');
                    if(_newfirst > _newlast) {
                        //a bunch in the middle are hidden
                        if(_pos >= _newfirst || _pos <= _newlast) {
                            $(v).removeClass('hidden');
                        } else {
                            $(v).addClass('hidden');
                        }
                    } else {
                        if(_pos >= _newfirst && _pos <= _newlast) {
                            $(v).removeClass('hidden');
                        } else {
                            $(v).addClass('hidden');
                        }
                    }
                });

                _first = _newfirst;
                _last = _newlast;

                $next = $('.people-indicator.active').removeClass('active').next();
                if($next.length) {
                    $next.addClass('active');
                } else {
                    $('.people-indicator:first').addClass('active');
                }

                $('#personContainer').fadeIn(400, function(e) {
                    setTimeout(function() {
                        rotatePeople();
                    }, 5000);
                });
            });
        };

        setTimeout(function() {
            rotatePeople();
        }, 5000);

        $(document).on('click', '.readmore', function(e) {
            e.preventDefault();
            //read more
            var _parent = $(this).parent();
            _parent.removeClass('d-block').removeClass('d-md-none').addClass('d-none');
            //content
            var _next = _parent.next();
            _next.removeClass('d-md-block').removeClass('d-none');
            //read less...
            _next.next().removeClass('d-none').addClass('d-block').addClass('d-md-none');
        });

        $(document).on('click', '.readless', function(e) {
            e.preventDefault();
            //read less
            var _parent = $(this).parent();
            _parent.removeClass('d-block').removeClass('d-md-none').addClass('d-none');
            //content
            var _next = _parent.prev();
            _next.addClass('d-md-block').addClass('d-none');
            //read more...
            _next.prev().removeClass('d-none').addClass('d-block').addClass('d-md-none');
        });

        $('#serviceCarousel').carousel({
            interval: 5000
        })

        $('#serviceCarousel .carousel-item').each(function(){
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            //this line controls 6 in the slider.
            for (var i=0;i<5;i++) {
                next=next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
            }
        });


        $('#serviceCarousel').on('slid.bs.carousel', function () {
            $("#serviceCarousel .carousel-item.active div:first-child").addClass('selected');
        });
    });
</script>
<?php get_footer();