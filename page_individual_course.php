<?php
/**
 * Template name:Individual Course
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<?php
if(has_post_video()) {
    $thumbnail = '<video autoplay muted loop id="myVideo">
            <source src="'.get_the_post_video_url().'" type="video/mp4">
        </video>';
} elseif ( has_post_thumbnail() ) {
    $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    $thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
}
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);

$bookLink = '';

if($_SERVER['REQUEST_URI'] == '/first-aid-for-parents/') {
    $bookLink = '/eyJjaWQiOiIyNTMzOCIsInRpZCI6IjIifQ==';
}
?>

<?php
while (have_posts()) : the_post();
    $blocks = get_field('product_boxes');
?>

<header class="page-head">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="tile">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <h1><?php echo $largetitle; ?></h1>
                            <p id="breadcrumbs"><?php rr_breadcrumbs($post->ID); ?></p>
                            <?php echo wpautop(get_field('introductory_content')); ?>
                            <p><a href="<?php echo site_url(); ?>/booking<?php echo $bookLink; ?>" class="btn btn-primary book-now">Book Now</a></p>
                        </div>

                        <div class="col-12 col-md-7">
                            <div class="head-card-image" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);">
                                <img src="<?php echo esc_url( $thumb_img[0] ); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="maincontent">
    <article id="services" class="realresponse-panel post-70 page type-page status-publish hentry">
        <div class="panel-content ">
            <div class="wrap">
                <header class="entry-header">
                    <h2 class="entry-title">How it works</h2>
                </header>
                <div class="entry-content container">
                    <div class="row row-eq-height">
                        <?php foreach(get_field('how_it_works') as $box) {
                            echo '<div class="col-12 col-md-6 col-lg-3">
                                    <div class="how_it_works">
                                        <div class="simage"><img src="'.$box['icon']['url'].'" /></div>
                                        <div class="scontent">
                                            <h2>'.$box['title'].'</h2>
                                            <div class="desc">'.$box['content'].'</div>
                                        </div>
                                    </div>
                                </div>';
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </article>
<?php
    $virtual = false;
    if(isset($blocks[0]['virtual_booking_course'][0])) {
        $virtual = $blocks[0]['virtual_booking_course'][0];
    }
    if(isset($blocks[0]['booking_course'][0])) {
        $cid = $blocks[0]['booking_course'][0];
        $public = get_field('available_as_public_course', $cid);
        if($public || $virtual) {
            ?>
            <header class="page-head">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-11" id="findCourse">
                            <div class="tile">
                                <div class="row align-items-center">
                                    <div class="col-12 col-md-6">
                                        <h2>View upcoming courses</h2>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-12">
                                                <?php
                                                $courses = get_field('courses');
                                                //echo '<pre>'; var_dump($courses); echo '</pre>';
                                                ?>
                                                <select id="see_virtual">
                                                    <option value="#">select course type</option>
                                                    <?php
                                                    foreach($courses as $cse) {
                                                        $course = get_field('product_boxes', $cse)[0];
                                                        $ax_details = get_field('axcelerate_options', $course['booking_course'][0]);
                                                        $axids = [];
                                                        foreach($ax_details as $detail) {
                                                            $axids[] = $detail['course_id'];
                                                        }

                                                        echo '<option value="'.$course['booking_course'][0].'" data-axid="'.implode(',', $axids).'">'.$course['header'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div id="axcel-courses" class="axcel-courses"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <?php
        }
    }

    echo '<div class="cstm_content">
        <div class="bg-red banner">';

    foreach($blocks as $block) {
        $book_text = 'Book Now';
        if(strlen($block['booking_information_text']) > 0) {
            $book_text = $block['booking_information_text'];
        }
        echo '<div class="rating_overlap">
            <div class="auto-area">
                <div class="primary_course clear">
                    <div class="pbox cpr most_popular_inner product_box">';
                        if($virtual) {
                            echo '<div class="online_ribbon">';
                            $vtype = get_field('virtual_course_ribbon_type', $post->ID);
                            switch($vtype) {
                                case 1:
                                    echo '<div class="ribbon_content">This course is delivered virtually over Zoom</div>';
                                    break;

                                default:
                                    echo '<div class="ribbon_content"><a href="/online-first-aid-courses">This course is available for Virtual Delivery</a></div>';
                                    break;
                            }
                            echo '</div>';
                        }
                        echo '<div class="item">
                            <div class="mpc_50">
                                <h3 class="header2">'.$block['header'].'</h3>
                                <h4>'.$block['subheader'].'</h4>
                                <p>'.$block['unit_code'].'</p>';
        if(strlen($block['course_length']) > 0) {
            echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $block['course_length'] . '</strong> Course Length</p>';
        }

        if(strlen($block['certificate_length']) > 0) {
            echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $block['certificate_length'] . '</strong> Certificate Length</p>';
        }

        if($block['nationally_accredited']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/cert.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
        }
    
        if($block['statement_of_attendance']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/cert.svg').'" class="icon"><strong>Statement of Attendance</strong></p>';
        }
    
        if($block['take_home_mask']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/bag.svg').'" class="icon"><strong>Optional take home mask</strong></p>';
        }

        if($block['requires_pre-work']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/pc.svg').'" class="icon"><strong>Requires pre-course work</strong></p>';
        }

        if($block['price']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/usd_USD.svg').'" class="icon"><strong>'.$block['price'].'</strong></p>';
        }

        echo '              </div>
                            <div class="mpc_30">
                                <p><strong>Description:</strong></p>
                                '.$block['description'].'
                            </div>
                            <div class="mpc_20">
                                <a id="popup-page" class="btn btn-primary book-now" title="Book Now" href="/booking'.$block['book_now_link'].'">'.$book_text.'</a>';
        switch($block['booking_type']) {
            case 0 :
                echo '<div class="group_b_icon">Group Bookings Only<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as an onsite course where we come out to your site</div>';
                break;

            case 1:
                echo '<div class="group_b_icon">Public Bookings Only<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as a public course where you come to one of our training facilities</div>';
                break;

            case 2:
                echo '<div class="group_b_icon">Group or Public Bookings<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as an onsite course where we come out to your site or as a public course where you come to one of our training facilities</div>';
                break;
        }
        echo '                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    }

    echo '</div>
    </div>';
?>

<?php
$faq_cats = get_field('faq_category');
if(strlen($faq_cats) > 0) {
?>
    <div class="container" id="faq-section">
        <div class="row">
            <div class="col-12">
                <h2 class="entry-title">Frequently Asked Questions</h2>
                    <?php
                    echo do_shortcode('[superior_faq layout="boxed" open_icon="plus" close_icon="minus" headline_tag="h4" permalink="no" deeplinking="yes" animate_content="yes" excerpt="no" categories="' . $faq_cats . '"]');
                    ?>
            </div>
        </div>
    </div>
<?php
}

    get_template_part( 'template-parts/page/content', 'custompage' );

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



