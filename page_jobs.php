<?php
/**
 * Template name: Work at Real Response
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>


<div id="section1" class="header-overlap">
    <div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
    <div class="banner-content">
        <div class="wrap ">
            <h6><?php echo $smalltitle; ?></h6>
            <h1><?php echo $largetitle; ?></h1>
        </div>
    </div>
</div>

<div id="maincontent" class="course-pages courses">
<?php
while ( have_posts() ) : the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="how_does_it_wrk_area" id="theContent">
					    <?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
        
        <!--<div id="jobs-location">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8 col-lg-6">
                        <h1>Jobs by Location</h1>
                        <div class="dotmap">
                            <div class="dot" data-row="0" data-col="7"></div>
                            <div class="dot" data-row="0" data-col="8"></div>
                            <div class="dot" data-row="0" data-col="9"></div>
                            <div class="dot" data-row="0" data-col="12"></div>
                            <div class="dot" data-row="1" data-col="6"></div>
                            <div class="dot" data-row="1" data-col="7"></div>
                            <div class="dot" data-row="1" data-col="8"></div>
                            <div class="dot" data-row="1" data-col="12"></div>
                            <div class="dot" data-row="2" data-col="4"></div>
                            <div class="dot" data-row="2" data-col="5"></div>
                            <div class="dot" data-row="2" data-col="6"></div>
                            <div class="dot" data-row="2" data-col="7"></div>
                            <div class="dot" data-row="2" data-col="8"></div>
                            <div class="dot" data-row="2" data-col="9"></div>
                            <div class="dot" data-row="2" data-col="12"></div>
                            <div class="dot" data-row="2" data-col="13"></div>
                            <div class="dot" data-row="3" data-col="3"></div>
                            <div class="dot" data-row="3" data-col="4"></div>
                            <div class="dot" data-row="3" data-col="5"></div>
                            <div class="dot" data-row="3" data-col="6"></div>
                            <div class="dot" data-row="3" data-col="7"></div>
                            <div class="dot" data-row="3" data-col="8"></div>
                            <div class="dot" data-row="3" data-col="9"></div>
                            <div class="dot" data-row="3" data-col="10"></div>
                            <div class="dot" data-row="3" data-col="11"></div>
                            <div class="dot" data-row="3" data-col="12"></div>
                            <div class="dot" data-row="3" data-col="13"></div>
                            <div class="dot" data-row="4" data-col="2"></div>
                            <div class="dot" data-row="4" data-col="3"></div>
                            <div class="dot" data-row="4" data-col="4"></div>
                            <div class="dot" data-row="4" data-col="5"></div>
                            <div class="dot" data-row="4" data-col="6"></div>
                            <div class="dot" data-row="4" data-col="7"></div>
                            <div class="dot" data-row="4" data-col="8"></div>
                            <div class="dot" data-row="4" data-col="9"></div>
                            <div class="dot" data-row="4" data-col="10"></div>
                            <div class="dot" data-row="4" data-col="11"></div>
                            <div class="dot" data-row="4" data-col="12"></div>
                            <div class="dot" data-row="4" data-col="13"></div>
                            <div class="dot" data-row="4" data-col="14"></div>
                            <div class="dot" data-row="5" data-col="1"></div>
                            <div class="dot" data-row="5" data-col="2"></div>
                            <div class="dot" data-row="5" data-col="3"></div>
                            <div class="dot" data-row="5" data-col="4"></div>
                            <div class="dot" data-row="5" data-col="5"></div>
                            <div class="dot" data-row="5" data-col="6"></div>
                            <div class="dot" data-row="5" data-col="7"></div>
                            <div class="dot" data-row="5" data-col="8"></div>
                            <div class="dot" data-row="5" data-col="9"></div>
                            <div class="dot" data-row="5" data-col="10"></div>
                            <div class="dot" data-row="5" data-col="11"></div>
                            <div class="dot" data-row="5" data-col="12"></div>
                            <div class="dot" data-row="5" data-col="13"></div>
                            <div class="dot" data-row="5" data-col="14"></div>
                            <div class="dot" data-row="5" data-col="15"></div>
                            <div class="dot" data-row="6" data-col="0"></div>
                            <div class="dot" data-row="6" data-col="1"></div>
                            <div class="dot" data-row="6" data-col="2"></div>
                            <div class="dot" data-row="6" data-col="3"></div>
                            <div class="dot" data-row="6" data-col="4"></div>
                            <div class="dot" data-row="6" data-col="5"></div>
                            <div class="dot" data-row="6" data-col="6"></div>
                            <div class="dot" data-row="6" data-col="7"></div>
                            <div class="dot" data-row="6" data-col="8"></div>
                            <div class="dot" data-row="6" data-col="9"></div>
                            <div class="dot" data-row="6" data-col="10"></div>
                            <div class="dot" data-row="6" data-col="11"></div>
                            <div class="dot" data-row="6" data-col="12"></div>
                            <div class="dot" data-row="6" data-col="13"></div>
                            <div class="dot" data-row="6" data-col="14"></div>
                            <div class="dot" data-row="6" data-col="15"></div>
                            <div class="dot" data-row="6" data-col="16"></div>
                            <div class="dot" data-row="7" data-col="0"></div>
                            <div class="dot" data-row="7" data-col="1"></div>
                            <div class="dot" data-row="7" data-col="2"></div>
                            <div class="dot" data-row="7" data-col="3"></div>
                            <div class="dot" data-row="7" data-col="4"></div>
                            <div class="dot" data-row="7" data-col="5"></div>
                            <div class="dot" data-row="7" data-col="6"></div>
                            <div class="dot" data-row="7" data-col="7"></div>
                            <div class="dot" data-row="7" data-col="8"></div>
                            <div class="dot" data-row="7" data-col="9"></div>
                            <div class="dot" data-row="7" data-col="10"></div>
                            <div class="dot" data-row="7" data-col="11"></div>
                            <div class="dot" data-row="7" data-col="12"></div>
                            <div class="dot" data-row="7" data-col="13"></div>
                            <div class="dot" data-row="7" data-col="14"></div>
                            <div class="dot" data-row="7" data-col="15"></div>
                            <div class="dot" data-row="7" data-col="16"></div>
                            <div class="dot" data-row="8" data-col="0"></div>
                            <div class="dot" data-row="8" data-col="1"></div>
                            <div class="dot" data-row="8" data-col="2"></div>
                            <div class="dot" data-row="8" data-col="3"></div>
                            <div class="dot" data-row="8" data-col="4"></div>
                            <div class="dot" data-row="8" data-col="5"></div>
                            <div class="dot" data-row="8" data-col="6"></div>
                            <div class="dot" data-row="8" data-col="7"></div>
                            <div class="dot" data-row="8" data-col="8"></div>
                            <div class="dot" data-row="8" data-col="9"></div>
                            <div class="dot" data-row="8" data-col="10"></div>
                            <div class="dot" data-row="8" data-col="11"></div>
                            <div class="dot" data-row="8" data-col="12"></div>
                            <div class="dot" data-row="8" data-col="13"></div>
                            <div class="dot" data-row="8" data-col="14"></div>
                            <div class="dot" data-row="8" data-col="15"></div>
                            <div class="dot" data-row="8" data-col="16"></div>
                            <div class="dot" data-row="9" data-col="1"></div>
                            <div class="dot" data-row="9" data-col="2"></div>
                            <div class="dot" data-row="9" data-col="3"></div>
                            <div class="dot" data-row="9" data-col="4"></div>
                            <div class="dot" data-row="9" data-col="5"></div>
                            <div class="dot" data-row="9" data-col="6"></div>
                            <div class="dot" data-row="9" data-col="7"></div>
                            <div class="dot" data-row="9" data-col="8"></div>
                            <div class="dot" data-row="9" data-col="9"></div>
                            <div class="dot" data-row="9" data-col="10"></div>
                            <div class="dot" data-row="9" data-col="11"></div>
                            <div class="dot" data-row="9" data-col="12"></div>
                            <div class="dot" data-row="9" data-col="13"></div>
                            <div class="dot" data-row="9" data-col="14"></div>
                            <div class="dot" data-row="9" data-col="15"></div>
                            <div class="dot" data-row="9" data-col="16"></div>
                            <div class="dot" data-row="10" data-col="1"></div>
                            <div class="dot" data-row="10" data-col="2"></div>
                            <div class="dot" data-row="10" data-col="3"></div>
                            <div class="dot" data-row="10" data-col="4"></div>
                            <div class="dot" data-row="10" data-col="9"></div>
                            <div class="dot" data-row="10" data-col="10"></div>
                            <div class="dot" data-row="10" data-col="11"></div>
                            <div class="dot" data-row="10" data-col="12"></div>
                            <div class="dot" data-row="10" data-col="13"></div>
                            <div class="dot" data-row="10" data-col="14"></div>
                            <div class="dot" data-row="10" data-col="15"></div>
                            <div class="dot" data-row="10" data-col="21"></div>
                            <div class="dot" data-row="10" data-col="22"></div>
                            <div class="dot" data-row="11" data-col="1"></div>
                            <div class="dot" data-row="11" data-col="2"></div>
                            <div class="dot" data-row="11" data-col="10"></div>
                            <div class="dot" data-row="11" data-col="11"></div>
                            <div class="dot" data-row="11" data-col="12"></div>
                            <div class="dot" data-row="11" data-col="13"></div>
                            <div class="dot" data-row="11" data-col="14"></div>
                            <div class="dot" data-row="11" data-col="15"></div>
                            <div class="dot" data-row="11" data-col="22"></div>
                            <div class="dot" data-row="11" data-col="23"></div>
                            <div class="dot" data-row="12" data-col="11"></div>
                            <div class="dot" data-row="12" data-col="12"></div>
                            <div class="dot" data-row="12" data-col="13"></div>
                            <div class="dot" data-row="12" data-col="14"></div>
                            <div class="dot" data-row="12" data-col="22"></div>
                            <div class="dot" data-row="12" data-col="23"></div>
                            <div class="dot" data-row="13" data-col="21"></div>
                            <div class="dot" data-row="13" data-col="22"></div>
                            <div class="dot" data-row="14" data-col="12"></div>
                            <div class="dot" data-row="14" data-col="13"></div>
                            <div class="dot" data-row="14" data-col="20"></div>
                            <div class="dot" data-row="14" data-col="21"></div>
                            <div class="dot" data-row="14" data-col="22"></div>
                            <div class="dot" data-row="15" data-col="12"></div>
                            <div class="dot" data-row="15" data-col="13"></div>
                            <div class="dot" data-row="15" data-col="20"></div>
                            <div class="dot" data-row="15" data-col="21"></div>

                            <?php
                                $locations = get_posts([
                                    'post_type' => 'rfa_locations',
                                    'numberposts' => -1
                                ]);
                                
                                foreach($locations as $location) {
                                    $position = get_field('city_location', $location->ID);
                                    $pos = explode(',', $position);
                                    if(count($pos) > 1) {
                                        echo '<div class="dot city" data-row="' . $pos[1] . '" data-col="' . $pos[0] . '" data-location="'.$location->ID.'"></div>';
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
	<div id="jobs-skill">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 class="section-head">Jobs by Skill</h1>
                    <div class="row">
                        <?php
                        $skills = get_posts([
                            'post_type' => 'rfa_skills',
                            'numberposts' => -1
                        ]);
                        
                        foreach($skills as $skill) {
                            $image = get_field('tile_image', $skill->ID);
                            $img = '';
                            if($image != null) {
                                $img = $image['sizes']['medium'] ?? $image['url'];
                            }
                            echo '<div class="col-12 col-md-6 col-lg-4">
                                <div class="job-skill align-items-center justify-content-center d-flex" data-skill="'.$skill->ID.'">
                                    <div class="job-skill-background" style="background-image: url(\''.$img.'\')"></div>
                                    <h2>'.$skill->post_title.'</h2>
                                </div>
                            </div>';
                        }
                        ?>
                    </div>
				</div>
			</div>
		</div>
	</div>-->
        
    <div id="jobs-reasons">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>Why work with us?</h1>
                </div>
            </div>
            <div class="row row-eq-height">
                <?php
                    $reasons = get_field('why_work_with_us');
                    if(is_array($reasons)) {
                        foreach ($reasons as $reason) {
                            echo '<div class="col-12 col-md-4">
                            <div class="reason">
                                <h2>' . $reason['header'] . '</h2>
                                <p>' . $reason['content'] . '</p>
                            </div>
                        </div>';
                        }
                    }
                ?>
            </div>
        </div>
    </div>

    <div id="jobs-list">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3 filters">
                    <h1 class="section-head">Filters</h1>
                    <h3>Region</h3>
                    <ul>
                        <?php
                            $regions = get_posts([
                                'post_type' => 'rfa_locations',
                                'numberposts' => -1,
                                'meta_key' => 'available_for_jobs',
                                'meta_value' => 1
                            ]);
                            foreach($regions as $region) {
                                echo '<li><label class="filter-location" onclick=""><input type="checkbox" data-id="'.$region->ID.'"> '.$region->post_title.'</label></li>';
                            }
                        ?>
                    </ul>
                    <h3>Job Type</h3>
                    <ul>
                        <li><label class="filter-type" onclick=""><input type="checkbox" data-type="0"> Full Time</label></li>
                        <li><label class="filter-type" onclick=""><input type="checkbox" data-type="2"> Part Time</label></li>
                        <li><label class="filter-type" onclick=""><input type="checkbox" data-type="1"> Casual</label></li>
                    </ul>
                </div>
                <div class="col-12 col-md-9">
                    <h1 class="section-head">Jobs Listing</h1>
                    <div class="row justify-content-center permanent-positions">
                        <?php
                            $jobs = get_posts([
                                'post_type' => 'rfa_jobs',
                                'numberposts' => -1
                            ]);
                            
                            foreach($jobs as $job) {
                                $location = get_field('location', $job->ID);
                                $skill = get_field('skills', $job->ID);
                                $type = get_field('position_type', $job->ID);
                                $locations = [];
                                $skills = [];
    
                                $data_locations = [];
                                $data_skills = [];
    
                                if(is_array($location)) {
                                    foreach ($location as $l) {
                                        $data_locations[] = $l->ID;
                                        $locations[] = $l->post_title;
                                    }
                                }
    
                                if(is_array($skill)) {
                                    foreach ($skill as $s) {
                                        $data_skills[] = $s->ID;
                                        $skills[] = $s->post_title;
                                    }
                                }
                                
                                echo '<div class="col-12">
                                    <div class="job" data-locations="'.json_encode($data_locations).'" data-skills="'.json_encode($data_skills).'" data-type="'.$type.'">
                                        <h2><a href="'.get_permalink($job->ID).'">'.$job->post_title.'</a></h2>
                                        <h3>'.implode(', ', $locations).' | '.implode(', ', $skills).' | '.($type == 1 ? 'Casual' : ($type == 0 ? 'Full Time' : 'Part Time')).'</h3>
                                        '.get_field('summary', $job->ID).'
                                    </div>
                                </div>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php
        $apply = get_field('to_apply', $post->ID);
        if(strlen($apply) > 0) {
    ?>
    
    <div class="container" id="to-apply">
        <div class="row">
            <div class="col">
                <?php echo $apply; ?>
            </div>
        </div>
    </div>
    
    <?php
        }
    ?>
        <div id="jobs-location">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8 col-lg-6">
                        <div class="dotmap">
                            <div class="dot" data-row="0" data-col="7"></div>
                            <div class="dot" data-row="0" data-col="8"></div>
                            <div class="dot" data-row="0" data-col="9"></div>
                            <div class="dot" data-row="0" data-col="12"></div>
                            <div class="dot" data-row="1" data-col="6"></div>
                            <div class="dot" data-row="1" data-col="7"></div>
                            <div class="dot" data-row="1" data-col="8"></div>
                            <div class="dot" data-row="1" data-col="12"></div>
                            <div class="dot" data-row="2" data-col="4"></div>
                            <div class="dot" data-row="2" data-col="5"></div>
                            <div class="dot" data-row="2" data-col="6"></div>
                            <div class="dot" data-row="2" data-col="7"></div>
                            <div class="dot" data-row="2" data-col="8"></div>
                            <div class="dot" data-row="2" data-col="9"></div>
                            <div class="dot" data-row="2" data-col="12"></div>
                            <div class="dot" data-row="2" data-col="13"></div>
                            <div class="dot" data-row="3" data-col="3"></div>
                            <div class="dot" data-row="3" data-col="4"></div>
                            <div class="dot" data-row="3" data-col="5"></div>
                            <div class="dot" data-row="3" data-col="6"></div>
                            <div class="dot" data-row="3" data-col="7"></div>
                            <div class="dot" data-row="3" data-col="8"></div>
                            <div class="dot" data-row="3" data-col="9"></div>
                            <div class="dot" data-row="3" data-col="10"></div>
                            <div class="dot" data-row="3" data-col="11"></div>
                            <div class="dot" data-row="3" data-col="12"></div>
                            <div class="dot" data-row="3" data-col="13"></div>
                            <div class="dot" data-row="4" data-col="2"></div>
                            <div class="dot" data-row="4" data-col="3"></div>
                            <div class="dot" data-row="4" data-col="4"></div>
                            <div class="dot" data-row="4" data-col="5"></div>
                            <div class="dot" data-row="4" data-col="6"></div>
                            <div class="dot" data-row="4" data-col="7"></div>
                            <div class="dot" data-row="4" data-col="8"></div>
                            <div class="dot" data-row="4" data-col="9"></div>
                            <div class="dot" data-row="4" data-col="10"></div>
                            <div class="dot" data-row="4" data-col="11"></div>
                            <div class="dot" data-row="4" data-col="12"></div>
                            <div class="dot" data-row="4" data-col="13"></div>
                            <div class="dot" data-row="4" data-col="14"></div>
                            <div class="dot" data-row="5" data-col="1"></div>
                            <div class="dot" data-row="5" data-col="2"></div>
                            <div class="dot" data-row="5" data-col="3"></div>
                            <div class="dot" data-row="5" data-col="4"></div>
                            <div class="dot" data-row="5" data-col="5"></div>
                            <div class="dot" data-row="5" data-col="6"></div>
                            <div class="dot" data-row="5" data-col="7"></div>
                            <div class="dot" data-row="5" data-col="8"></div>
                            <div class="dot" data-row="5" data-col="9"></div>
                            <div class="dot" data-row="5" data-col="10"></div>
                            <div class="dot" data-row="5" data-col="11"></div>
                            <div class="dot" data-row="5" data-col="12"></div>
                            <div class="dot" data-row="5" data-col="13"></div>
                            <div class="dot" data-row="5" data-col="14"></div>
                            <div class="dot" data-row="5" data-col="15"></div>
                            <div class="dot" data-row="6" data-col="0"></div>
                            <div class="dot" data-row="6" data-col="1"></div>
                            <div class="dot" data-row="6" data-col="2"></div>
                            <div class="dot" data-row="6" data-col="3"></div>
                            <div class="dot" data-row="6" data-col="4"></div>
                            <div class="dot" data-row="6" data-col="5"></div>
                            <div class="dot" data-row="6" data-col="6"></div>
                            <div class="dot" data-row="6" data-col="7"></div>
                            <div class="dot" data-row="6" data-col="8"></div>
                            <div class="dot" data-row="6" data-col="9"></div>
                            <div class="dot" data-row="6" data-col="10"></div>
                            <div class="dot" data-row="6" data-col="11"></div>
                            <div class="dot" data-row="6" data-col="12"></div>
                            <div class="dot" data-row="6" data-col="13"></div>
                            <div class="dot" data-row="6" data-col="14"></div>
                            <div class="dot" data-row="6" data-col="15"></div>
                            <div class="dot" data-row="6" data-col="16"></div>
                            <div class="dot" data-row="7" data-col="0"></div>
                            <div class="dot" data-row="7" data-col="1"></div>
                            <div class="dot" data-row="7" data-col="2"></div>
                            <div class="dot" data-row="7" data-col="3"></div>
                            <div class="dot" data-row="7" data-col="4"></div>
                            <div class="dot" data-row="7" data-col="5"></div>
                            <div class="dot" data-row="7" data-col="6"></div>
                            <div class="dot" data-row="7" data-col="7"></div>
                            <div class="dot" data-row="7" data-col="8"></div>
                            <div class="dot" data-row="7" data-col="9"></div>
                            <div class="dot" data-row="7" data-col="10"></div>
                            <div class="dot" data-row="7" data-col="11"></div>
                            <div class="dot" data-row="7" data-col="12"></div>
                            <div class="dot" data-row="7" data-col="13"></div>
                            <div class="dot" data-row="7" data-col="14"></div>
                            <div class="dot" data-row="7" data-col="15"></div>
                            <div class="dot" data-row="7" data-col="16"></div>
                            <div class="dot" data-row="8" data-col="0"></div>
                            <div class="dot" data-row="8" data-col="1"></div>
                            <div class="dot" data-row="8" data-col="2"></div>
                            <div class="dot" data-row="8" data-col="3"></div>
                            <div class="dot" data-row="8" data-col="4"></div>
                            <div class="dot" data-row="8" data-col="5"></div>
                            <div class="dot" data-row="8" data-col="6"></div>
                            <div class="dot" data-row="8" data-col="7"></div>
                            <div class="dot" data-row="8" data-col="8"></div>
                            <div class="dot" data-row="8" data-col="9"></div>
                            <div class="dot" data-row="8" data-col="10"></div>
                            <div class="dot" data-row="8" data-col="11"></div>
                            <div class="dot" data-row="8" data-col="12"></div>
                            <div class="dot" data-row="8" data-col="13"></div>
                            <div class="dot" data-row="8" data-col="14"></div>
                            <div class="dot" data-row="8" data-col="15"></div>
                            <div class="dot" data-row="8" data-col="16"></div>
                            <div class="dot" data-row="9" data-col="1"></div>
                            <div class="dot" data-row="9" data-col="2"></div>
                            <div class="dot" data-row="9" data-col="3"></div>
                            <div class="dot" data-row="9" data-col="4"></div>
                            <div class="dot" data-row="9" data-col="5"></div>
                            <div class="dot" data-row="9" data-col="6"></div>
                            <div class="dot" data-row="9" data-col="7"></div>
                            <div class="dot" data-row="9" data-col="8"></div>
                            <div class="dot" data-row="9" data-col="9"></div>
                            <div class="dot" data-row="9" data-col="10"></div>
                            <div class="dot" data-row="9" data-col="11"></div>
                            <div class="dot" data-row="9" data-col="12"></div>
                            <div class="dot" data-row="9" data-col="13"></div>
                            <div class="dot" data-row="9" data-col="14"></div>
                            <div class="dot" data-row="9" data-col="15"></div>
                            <div class="dot" data-row="9" data-col="16"></div>
                            <div class="dot" data-row="10" data-col="1"></div>
                            <div class="dot" data-row="10" data-col="2"></div>
                            <div class="dot" data-row="10" data-col="3"></div>
                            <div class="dot" data-row="10" data-col="4"></div>
                            <div class="dot" data-row="10" data-col="9"></div>
                            <div class="dot" data-row="10" data-col="10"></div>
                            <div class="dot" data-row="10" data-col="11"></div>
                            <div class="dot" data-row="10" data-col="12"></div>
                            <div class="dot" data-row="10" data-col="13"></div>
                            <div class="dot" data-row="10" data-col="14"></div>
                            <div class="dot" data-row="10" data-col="15"></div>
                            <div class="dot" data-row="11" data-col="1"></div>
                            <div class="dot" data-row="11" data-col="2"></div>
                            <div class="dot" data-row="11" data-col="10"></div>
                            <div class="dot" data-row="11" data-col="11"></div>
                            <div class="dot" data-row="11" data-col="12"></div>
                            <div class="dot" data-row="11" data-col="13"></div>
                            <div class="dot" data-row="11" data-col="14"></div>
                            <div class="dot" data-row="11" data-col="15"></div>
                            <div class="dot" data-row="12" data-col="11"></div>
                            <div class="dot" data-row="12" data-col="12"></div>
                            <div class="dot" data-row="12" data-col="13"></div>
                            <div class="dot" data-row="12" data-col="14"></div>
                            <div class="dot" data-row="14" data-col="12"></div>
                            <div class="dot" data-row="14" data-col="13"></div>
                            <div class="dot" data-row="15" data-col="12"></div>
                            <div class="dot" data-row="15" data-col="13"></div>
                            
                            <?php
                            $locations = get_posts([
                                'post_type' => 'rfa_locations',
                                'numberposts' => -1
                            ]);
                            
                            foreach($locations as $location) {
                                $position = get_field('city_location', $location->ID);
                                $pos = explode(',', $position);
                                if(count($pos) > 1) {
                                    echo '<div class="dot city" data-row="' . $pos[1] . '" data-col="' . $pos[0] . '" data-location="'.$location->ID.'"></div>';
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</article><!-- #post-## -->
<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



