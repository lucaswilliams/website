<?php
/**
 * Template name: Locations
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<?php if ( has_post_thumbnail() ) :
    $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
endif;
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

    <div id="section1" class="header-overlap">
        <!-- course-banner-->
        <div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
        <div class="banner-content">
            <div class="wrap">
                <h1><?php echo $largetitle; ?></h1>
            </div>
        </div>
    </div>

<div id="maincontent">
<?php
while ( have_posts() ) : the_post();

    $blurb = get_the_content();
    if(strlen($blurb) > 0) {
        echo '<div id="section2" class="section section2">
                <div class="wrap">
                  <div class="entry-content">
                    '.$blurb.'
                  </div>
                </div>
              </div>';
    }

    $bg = '';
    $im = get_field('onsite_background');
    if($im) {
        $bg = ' style="background-image: url('.$im['url'].');"';
    }

    echo '<div class="on-site" id="onsitetraining">';
    echo '<div class="wrap"'.$bg.'>';
    echo '<div class="w-50">&nbsp;</div>';
    echo '<div class="w-50">';
    echo '<h1>On-site Training</h1>';
    $onsite = get_field('onsite_courses');
    echo wpautop($onsite);
    echo '</div>';
    echo '</div>';
    echo '</div>';

    echo '<div class="wrap" id="publictraining">';
        echo '<h1 style="text-align: center">Public Training</h1>';
        $public = get_field('public_locations');
        //echo '<pre>'; var_dump($public); echo '</pre>';
        if($public) {
            $cities = $public[0]['cities'];
            foreach($cities as $city) {
                echo '<div class="city">';
                echo '<h2>'.$city['city_name'].'</h2>';
                foreach($city['city_locations'] as $location) {
                    echo '<div class="w-50">';
                    echo '<div class="location">';
                    $image = $location['image'];
                    if($image) {
                        echo '<img src="'.$image['url'].'">';
                    }
                    echo '<div class="text">';
                    if(strlen($location['venue_name']) > 0) {
                        echo '<h3>'.$location['venue_name'].'</h3>';
                    }
                    echo '<p>'.$location['address'].'</p>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                }
                echo '<div style="clear: both"></div>';
                echo '</div>';
            }
        }

    echo '</div>';

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



