<?php
/**
 * Template name: Mental Health Courses
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<header class="page-head">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="tile">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <h1><?php echo $largetitle; ?></h1>
                            <p id="breadcrumbs"><?php rr_breadcrumbs($post->ID); ?></p>
                            <?php echo wpautop(get_field('introductory_content')); ?>
                            <p><a href="<?php echo site_url(); ?>/booking" class="btn btn-primary book-now">Book Now</a></p>
                        </div>
                        
                        <div class="col-12 col-md-7">
                            <div class="head-card-image" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);">
                                <img src="<?php echo esc_url( $thumbnail[0] ); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-12 col-md-11" id="findCourse">
                <div class="tile">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-6">
                            <h2>Find a mental health course near you</h2>
                        </div>
                        <div class="col-12 col-md-6">
							<div class="row">
								<div class="col-12">
									<select id="seeCourses">
										<option value="#">See our First Aid Courses</option>
										<?php
										$courses_a = get_field('most_popular_course_repeater');
										$courses_b = get_field('specialised_course_repeater');
										$courses_c = get_field('additional_course_repeater');

										$courses = array_merge($courses_a, $courses_b, $courses_c);
										usort($courses, 'cmp');

										function cmp($a, $b)
										{
											if ($a->post_title == $b->post_title) {
												return 0;
											}
											return ($a->post_title < $b->post_title) ? -1 : 1;
										}

										foreach($courses as $course) {
											echo '<option value="'.get_the_permalink($course->ID).'">'.$course->post_title.'</option>';
										}
										?>
									</select>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="maincontent" class="course-pages courses">
<?php
while ( have_posts() ) : the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="container">
			<div class="row">
				<div class="col">
					<div class="introduction-text">
					
					</div>
				</div>
			</div>
		</div>

	<div class="most_popular_courses" id="courses">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="section-head">Most Popular Courses</h2>
					<h3 class="section-head">Group Bookings &amp; Public Course Available
                    	<img src="https://www.realresponse.com.au/wp-content/uploads/2018/01/group-booking-public-course_info-03_white.png" alt="icon" class="group_icon">
                    	<span class="gcontent">These courses are available as onsite courses where we come out to your site or as public courses where you come to one of our training facilities</span>
					</h3>
				</div>
			</div>
			<div class="row row-eq-height">
				<?php $courses = get_field('most_popular_course_repeater');
					foreach($courses as $cse) {
						$course = get_field('product_boxes', $cse->ID)[0];
						$virtual = $course['virtual_booking_course'];
						$cid = '';
						if($course['booking_course'] != null) {
						    $cid = base64_encode('{"cid" : '.$course['booking_course'][0].'}');
                        }
				?>
				<div class="col-12 col-lg-6">
                    <div class="course">
                        <?php
                        if($virtual) {
                            echo '<div class="online_ribbon">';
                            $vtype = get_field('virtual_course_ribbon_type', $post->ID);
                            switch($vtype) {
                                case 1:
                                    echo '<div class="ribbon_content">This course is delivered virtually over Zoom</div>';
                                    break;

                                default:
                                    echo '<div class="ribbon_content"><a href="/online-first-aid-courses">This course is available for Virtual Delivery</a></div>';
                                    break;
                            }
                            echo '</div>';
                        }
                        ?>
					    <div class="row d-none d-md-flex">
							<div class="col-7">
								<h3 class="header2"><a href="<?php echo the_permalink($cse->ID); ?>"><?php echo $course['header']; ?></a></h3>
								<h4><?php echo $course['subheader']; ?></h4>
								<p class="code"><?php echo $course['unit_code']; ?></p>
								<a class="btn btn-primary book-now" title="Book Now" href="<?php echo site_url(); ?>/booking/<?php echo $cid; ?>">Book Now</a>
								<div class="group_b_icon">
									Group Bookings &amp;<br />
									Public Course Available
									<img src="https://www.realresponse.com.au/wp-content/uploads/2018/01/group-booking-public-course_info-03.png" alt="icon">
									<div class="gcontent" style="display: none;">
										This course is available as an onsite course where we come out to your site or as a public course where you come to one of our training facilities
									</div>
								</div>
							</div>
							<div class="col-5">
								<?php
								if(strlen($course['course_length']) > 0) {
									echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
								}

								if(strlen($course['certificate_length']) > 0) {
									echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
								}

								if($course['nationally_accredited']) {
									echo '<p><img src="'.get_theme_file_uri('assets/images/mhfa.svg').'" class="icon"><strong>Accredited through Mental Health First Aid Australia</strong></p>';
								}
								?>
								<p>Course details:</p>
								<?php echo $course['description']; ?>
								<p class="mt-0">
									<a class="lnk_more_info text-right" href="<?php echo the_permalink($cse->ID); ?>">More Info</a>
								</p>
							</div>

							<div class="accordion d-md-none">
								<div class="accordion-control">More Details</div>

								<div class="accordion-content">
									<?php
									if(strlen($course['course_length']) > 0) {
										echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
									}

									if(strlen($course['certificate_length']) > 0) {
										echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
									}

									if($course['nationally_accredited']) {
										echo '<p><img src="'.get_theme_file_uri('assets/images/mhfa.svg').'" class="icon"><strong>Accredited through Mental Health First Aid Australia</strong></p>';
									}
									?>
									<p>Course details:</p>
									<?php echo $course['description']; ?>
									<a class="lnk_more_info" href="<?php echo the_permalink($cse->ID); ?>">More Info</a>
								</div>
							</div>
						</div>
						<h3 class="col-9 col-md-12 d-md-none"><a href="<?php echo the_permalink($cse->ID); ?>"><?php echo $course['header']; ?></a></h3>
						<p class="col-3 d-md-none text-right"><?php echo $course['unit_code']; ?></p>
						<h4 class="col-9 col-md-12 d-md-none"><?php echo $course['subheader']; ?></h4>
						<span class="col-3 d-md-none">
							<a class="btn btn-primary mob-enq book-now" title="Book Now" href="https://www.realresponse.com.au/booking/">Book</a>
						</span>

						<div class="accordion d-md-none">
							<div class="accordion-control">More Details</div>

							<div class="accordion-content">
								<?php
								if(strlen($course['course_length']) > 0) {
									echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
								}

								if(strlen($course['certificate_length']) > 0) {
									echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
								}

								if($course['nationally_accredited']) {
									echo '<p><img src="'.get_theme_file_uri('assets/images/mhfa.svg').'" class="icon"><strong>Accredited through Mental Health First Aid Australia</strong></p>';
								}
								?>
								<p>Course details:</p>
								<?php echo $course['description']; ?>
								<a class="lnk_more_info" href="<?php echo the_permalink($cse->ID); ?>">More Info</a>
							</div>
						</div>
					</div>
				</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>

	<div class="specialised_courses" id="specialised" style="position: relative;">
        <a id="specialisedCourses" style="position: absolute; top: -50px"></a>
   		<div class="container">
      		<div class="row">
				<div class="col-12">
					<h2 class="section-head">Specialised Courses</h2>
					<h3 class="section-head">Group Bookings Only
						<img src="http://www.realresponse.com.au/wp-content/uploads/2018/01/group-booking-public-course_info-03_white.png" alt="icon" class="group_icon" />
						<span class="gcontent" style="display: none;">These courses are only offered as onsite courses where we come out to your site and run a private course for your team</span>
					</h3>
				</div>
			</div>

			<div class="row justify-content-center">
			<?php $courses = get_field('specialised_course_repeater');
				foreach($courses as $cse) {
					//var_dump($cse);
					$course = get_field('product_boxes', $cse->ID)[0];
					//var_dump($course);
			?>
				<div class="col-12 col-md-6 col-xl-3">
					<div class="course">
						<h3 class="col-9 col-md-12"><a href="<?php echo the_permalink($cse->ID); ?>"><?php echo $course['header']; ?></a></h3>
						<h4 class="col-9 col-md-12"><?php echo $course['description']; ?></h4>
						<a class="btn btn-primary iframe2" title="Enquire Now" href="#contact_popup">Enquire Now</a>
					</div>
				</div>
			<?php
				}
			?>
			</div>
		</div>
      </div>
   </div>
	
	
	<div id="section9" class="section section9 rrpb-0">
		<div class="wrap">
			<div class="entry-header rrpb-0 rrmb-0">
				<h2 class="entry-title">Customer Reviews</h2>
			</div>
			<div class="">
				<div class="rating_dbox_area">
					<div class="rating_overlap">
						<div class="rating_dbox">
							<div id="feefo-service-review-carousel-widgetId" class="feefo-review-carousel-widget-service"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <?php
        $trainers = get_field('trainers');
        if($trainers) {
            echo '<div class="real_trainers_area">
                <div class="container">
                    <div class="real_trainers row justify-content-center">
                        <div class="col-12 col-md-9">';
            the_field('trainers_introduction');
            echo '</div>
                <div class="col-12 col-md-9">';
            $blocks = [];
            foreach($trainers as $trainer) {
                $blocks[] = [
                    'title' => $trainer['name'],
                    'content' => $trainer['position'],
                    'picture' => $trainer['picture']
                ];
            }
    
            $carousel = 'trainer-blocks';
            $blocksize = 'col-sm-4';
            include('template-parts/carousel/image-carousel.php');
            
            echo '    </div>
                    </div>
                </div>
            </div>';
        }
    ?>

    <div class="container" id="faq-section">
        <div class="row">
            <div class="col-12">
                <h2 class="entry-title">Frequently Asked Questions</h2>
                <?php
                $faq_cats = get_field('faq_category');
                echo do_shortcode('[superior_faq layout="boxed" open_icon="plus" close_icon="minus" headline_tag="h4" permalink="no" deeplinking="yes" animate_content="yes" excerpt="no" categories="'.$faq_cats.'"]');
                ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-9" id="theContent">
                <?php
                    the_content();
                ?>
            </div>
        </div>
    </div>
</article><!-- #post-## -->
<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



