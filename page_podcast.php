<?php
/**
 * Template name: Podcast
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');
if(has_post_video()) {
    $thumbnail = '<video autoplay muted loop id="myVideo">
            <source src="'.get_the_post_video_url().'" type="video/mp4">
        </video>';
} elseif ( has_post_thumbnail() ) {
    $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    $thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
}
$smalltitle = get_field('small_title', $post->ID);
$largetitle = get_field('large_title', $post->ID);
?>

<div id="section1" class="header-overlap">
    <div class="featured-banner"><?php echo $thumbnail; ?></div>
    <div class="banner-content">
        <div class="wrap ">
            <h6><?php echo $smalltitle; ?></h6>
            <h1><?php echo $largetitle; ?></h1>
        </div>
    </div>
</div>

<div class="container" id="podcast_intro">
    <div class="row">
        <div class="col">
            <?php echo wpautop(get_field('introductory_content')); ?>
        </div>
    </div>
</div>

<div id="blog-posts">
    <div class="container">
        <?php
        echo do_shortcode('[ajax_load_more post_type="rfa_podcast" repeater="template_1" posts_per_page="12" button_label="More podcasts" transition_container_classes="row"]');
        ?>
    </div>
</div>

<div id="guests">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Our Guests</h1>
                <div class="row">
                    <?php
                    $guests = get_field('our_guests');
                    foreach($guests as $guest) {
                    ?>
                    <div class="guest col-12 col-md-6 col-lg-4">
                        <img class="d-block w-100" src="<?php echo $guest['guest_image'] ?>" alt="First slide">
                        <h2><?php echo $guest['guest_name']; ?></h2>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="section9" class="section section9 rrpb-0">
    <div class="wrap">
        <div class="entry-header rrpb-0 rrmb-0">
            <h2 class="entry-title">Customer Reviews</h2>
        </div>
        <div class="">
            <div class="rating_dbox_area">
                <div class="rating_overlap">
                    <div class="rating_dbox">
                        <div id="feefo-service-review-carousel-widgetId" class="feefo-review-carousel-widget-service"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();