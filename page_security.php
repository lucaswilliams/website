<?php
/**
 * Template name:Security template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<?php
if(has_post_video()) {
    $thumbnail = '<video autoplay muted loop id="myVideo">
            <source src="'.get_the_post_video_url().'" type="video/mp4">
        </video>';
} elseif ( has_post_thumbnail() ) {
    $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
    $thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
}
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

    <div id="section1" class="header-overlap">
        <div class="featured-banner"><?php echo $thumbnail; ?></div>
        <div class="banner-content">
            <div class="wrap ">
                <h6><?php echo $smalltitle; ?></h6>
                <h1><?php echo $largetitle; ?></h1>
                <div class="entry-content">
                    <div class="rating_box">
                        <a id="popup-page" class="iframe2 btn btn-primary cboxElement" href="#contact_popup">Enquire Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div id="maincontent">
<?php
while ( have_posts() ) : the_post();

    $blurb = get_field('blurb');
    if(strlen($blurb) > 0) {
        echo '<div id="section2" class="section section2">
                <div class="wrap">
                  <div class="entry-content">
                    '.$blurb.'
                  </div>
                </div>
              </div>';
    }

    $boxes = get_field('product_boxes');
    if($boxes) {
        echo '<div id="section3" class="section section3 most_popular_courses rrsecurity">
	            <div class="wrap">
		          <div class="most_popular_inner">
			        <div class="items">';

        foreach($boxes as $box) {
            if($box['info_button_type'] == 0) {
                $link = '<a id="popup-page" class="btn btn-primary" title="'.$box['info_button_text'].'" href="'.$box['info_button_link'].'">'.$box['info_button_text'].'</a>';
            } else {
                $link = '<a id="popup-page" class="iframe2 cboxElement btn btn-primary" title="'.$box['info_button_text'].'" href="#contact_popup">'.$box['info_button_text'].'</a>';
            }

            echo '<div class="item">
                    <div class="item-inner row">
                      <div class="col-12 col-md-6 col-lg-4">
                        <h3 class="header2">'.$box['title'].'</h3>
						<h4>'.$box['subtitle'].'</h4>';
            if(strlen($box['course_length']) > 0) {
                echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $box['course_length'] . '</strong> Course Length</p>';
            }
            echo '    </div>
                      <div class="col-12 col-md-6 contents">
						'.$box['content'].'
					  </div>
					  <div class="col-12 col-md-6 col-lg-2">';
					    echo $link;
						echo '<div class="group_b_icon">';
                        switch($box['group_bookings']) {
                            case 1 :
                                echo '<div class = "gb_l">
                                        Group Bookings Only
                                      </div>
                                      <div class = "gb_r">
                                        <div class = "gcontent">
                                          This course is available as an onsite course where we come out to your site
                                        </div>
                                      </div>';
                                break;
            
                            case 2 :
                                echo '<div class = "gb_l">
                                        Public Course Available
                                      </div>
                                      <div class = "gb_r">
                                        <div class = "gcontent">
                                          This course is available as a public course where you come to one of our training facilities
                                        </div>
                                      </div>';
                                break;
            
                            case 3 :
                                echo '<div class = "gb_l">
                                        Group Bookings &amp; Public Course Available
                                      </div>
                                      <div class = "gb_r">
                                        <div class = "gcontent">
                                          This course is available as an onsite course where we come out to your site or as a public course where you come to one of our training facilities
                                        </div>
                                      </div>';
                                break;
                        }
                        echo '</div>
					  </div>
                    </div>
                  </div>';
        }

        echo '      </div>
                  </div>
                </div>
              </div>';
    }

	get_template_part( 'template-parts/page/content', 'custompage' );

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



