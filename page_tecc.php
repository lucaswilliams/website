<?php
/**
 * Template name:TECC template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<?php
if(has_post_video()) {
    $thumbnail = '<video autoplay muted loop id="myVideo">
            <source src="'.get_the_post_video_url().'" type="video/mp4">
        </video>';
} elseif ( has_post_thumbnail() ) {
    $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    $thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
}
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<?php
while (have_posts()) : the_post();
    $blocks = get_field('product_boxes');
?>

<div id="section1" class="header-overlap">
    <div class="featured-banner"><?php echo $thumbnail; ?></div>
    <div class="banner-content">	
        <div class="wrap ">
            <h6><?php echo $smalltitle; ?></h6>
            <h1><?php echo $largetitle; ?></h1>
            <div class="entry-content">
            	<div class="rating_box">
                    <?php if(isset($blocks[0]['booking_course'][0])) { ?>
            		    <a id="popup-page" class="btn btn-primary book-now" href="/booking">Book Now</a>
                    <?php } else { ?>
                        <a id="popup-page" class="iframe2 btn btn-primary cboxElement" href="#contact_popup">Enquire Now</a>
                    <?php } ?>
            	</div>
            </div>
        </div>
    </div>
</div>

<div id="maincontent">
<?php
    the_field('introduction_content');
    $virtual = false;
    if(isset($blocks[0]['virtual_booking_course'][0])) {
        $virtual = $blocks[0]['virtual_booking_course'][0];
    }
    if(isset($blocks[0]['booking_course'][0])) {
        $cid = $blocks[0]['booking_course'][0];
        $public = get_field('available_as_public_course', $cid);
        if($public || $virtual) {
            ?>
            <header class="page-head">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-11" id="findCourse">
                            <div class="tile">
                                <div class="row align-items-center">
                                    <div class="col-12 col-md-6">
                                        <h2>Find a course near you</h2>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="hidden" id="course_id" value="<?php echo $cid; ?>">
                                                <input type="hidden" id="axcel_id" value="<?php the_field('axcelerate_id', $cid); ?>">

                                                <input type="hidden" id="v_course_id" value="<?php echo $virtual; ?>">
                                                <input type="hidden" id="v_axcel_id" value="<?php the_field('axcelerate_id', $virtual); ?>">
                                                
                                                <select id="location_id">
                                                    <option value="#">Choose your location</option>
                                                    <option value="VIC">Victoria</option>
                                                    <option value="NSW">New South Wales</option>
                                                    <option value="SA">South Australia</option>
                                                    <option value="WA">Western Australia</option>
                                                    <?php echo ($virtual ? '<option value="Virtual">Virtual Training</option>' : ''); ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="axcel-courses" class="axcel-courses"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <?php
        }
    }

    echo '<div class="cstm_content">
        <div class="bg-red banner">';

    foreach($blocks as $block) {
        $book_text = 'Book Now';
        if(strlen($block['booking_information_text']) > 0) {
            $book_text = $block['booking_information_text'];
        }
        if(intval($block['book_now_link']) === 0) {
            $booking_link = $block['book_now_link'];
        } else {
            $booking_link = 'https://realresponse.com.au/booking';
        }
        echo '<div class="rating_overlap">
            <div class="auto-area">
                <div class="primary_course clear">
                    <div class="pbox cpr most_popular_inner product_box">';
                        if($virtual) {
                            echo '<div class="online_ribbon">';
                            $vtype = get_field('virtual_course_ribbon_type', $post->ID);
                            switch($vtype) {
                                case 1:
                                    echo '<div class="ribbon_content">This course is delivered virtually over Zoom</div>';
                                    break;

                                default:
                                    echo '<div class="ribbon_content"><a href="/online-first-aid-courses">This course is available for Virtual Delivery</a></div>';
                                    break;
                            }
                            echo '</div>';
                        }
                        echo '<div class="item">
                            <div class="mpc_50">
                                <h3 class="header2">'.$block['header'].'</h3>
                                <h4>'.$block['subheader'].'</h4>
                                <p>'.$block['unit_code'].'</p>';
        if(strlen($block['course_length']) > 0) {
            echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $block['course_length'] . '</strong> Course Length</p>';
        }

        if(strlen($block['certificate_length']) > 0) {
            echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $block['certificate_length'] . '</strong> Certificate Length</p>';
        }

        if($block['nationally_accredited']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/cert.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
        }
    
        if($block['statement_of_attendance']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/cert.svg').'" class="icon"><strong>Statement of Attendance</strong></p>';
        }
    
        if($block['take_home_mask']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/bag.svg').'" class="icon"><strong>Optional take home mask</strong></p>';
        }

        if($block['requires_pre-work']) {
            echo '<p><img src="'.get_theme_file_uri('assets/images/pc.svg').'" class="icon"><strong>Requires pre-course work</strong></p>';
        }
        echo '              </div>
                            <div class="mpc_30">
                                <p><strong>Description:</strong></p>
                                '.$block['description'].'
                            </div>
                            <div class="mpc_20">
                                <a id="popup-page" class="btn btn-primary book-now" title="Book Now" href="'.$booking_link.'">'.$book_text.'</a>';
        switch($block['booking_type']) {
            case 0 :
                echo '<div class="group_b_icon">Group Bookings Only<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as an onsite course where we come out to your site</div>';
                break;

            case 1:
                echo '<div class="group_b_icon">Public Bookings Only<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as a public course where you come to one of our training facilities</div>';
                break;

            case 2:
                echo '<div class="group_b_icon">Group or Public Bookings<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as an onsite course where we come out to your site or as a public course where you come to one of our training facilities</div>';
                break;
        }
        echo '                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    }

    echo '</div>
    </div>';

	get_template_part( 'template-parts/page/content', 'custompage' );

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



