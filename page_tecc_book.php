<?php
/**
 * Template name:TECC template Level 2
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<?php if ( has_post_thumbnail() ) :
    $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
endif;
$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

    <div id="section1" class="header-overlap">
        <div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
        <div class="banner-content">
            <div class="wrap ">
                <h6><?php echo $smalltitle; ?></h6>
                <h1><?php echo $largetitle; ?></h1>
                <div class="entry-content">
                    <div class="rating_box">
                        <a id="popup-page" class="iframe2 btn btn-primary cboxElement" href="#contact_popup">Enquire Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="maincontent">
        <?php
        while ( have_posts() ) : the_post();

            the_field('introduction_content');

            $blocks = get_field('product_boxes');
            echo '<div class="cstm_content">
        <div class="bg-red banner">';

            foreach($blocks as $block) {
                $book_text = 'Book Now';
                if(strlen($block['booking_information_text']) > 0) {
                    $book_text = $block['booking_information_text'];
                }
                echo '<div class="rating_overlap">
            <div class="auto-area">
                <div class="primary_course clear">
                    <div class="pbox cpr most_popular_inner product_box">
                        <div class="item">
                            <div class="mpc_80">
                                <h3 class="header2">'.$block['header'].'</h3>
                                <h4>'.$block['subheader'].'</h4>
                                <p>'.$block['unit_code'].'</p>';
                if(strlen($block['course_length']) > 0) {
                    echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $block['course_length'] . '</strong> Course Length</p>';
                }

                if(strlen($block['certificate_length']) > 0) {
                    echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $block['certificate_length'] . '</strong> Certificate Length</p>';
                }

                if($block['nationally_accredited']) {
                    echo '<p><img src="'.get_theme_file_uri('assets/images/cert.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
                }
                echo '          <p><strong>Description:</strong></p>
                                '.$block['description'].'
                            </div>
                            <div class="mpc_20">
                                <a id="popup-page" class="btn btn-primary book-now" title="Book Now" href="/booking'.$block['book_now_link'].'>'.$book_text.'</a>';
                switch($block['booking_type']) {
                    case 0 :
                        echo '<div class="group_b_icon">Group Bookings Only<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as an onsite course where we come out to your site</div>';
                        break;

                    case 1:
                        echo '<div class="group_b_icon">Public Bookings Only<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as a public course where you come to one of our training facilities</div>';
                        break;

                    case 2:
                        echo '<div class="group_b_icon">Group or Public Bookings<img src="'.get_site_url().'/wp-content/uploads/2018/04/group-booking-public-course_info-03.png" alt="icon">
                                <div class="gcontent" style="display: none;">This course is available as an onsite course where we come out to your site or as a public course where you come to one of our training facilities</div>';
                        break;
                }
                echo '                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
            }

            echo '</div>
    </div>';

            get_template_part( 'template-parts/page/content', 'custompage' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;

        endwhile; // End of the loop.
        ?>
    </div>
<?php get_footer();



