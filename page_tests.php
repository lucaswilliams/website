<?php
/**
 * Template name:Online Tests
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu'); ?>
<?php
    if ( has_post_thumbnail() ) {
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>
    <div id="section1" class="header-overlap align-main">
        <?php if(!empty($thumbnail)){ ?>
            <div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
        <?php } ?>
        <div class="banner-content align-banner">
            <div class="wrap">
                <?php if($smalltitle){ ?>
                    <h6><?php echo $smalltitle; ?></h6>
                <?php } ?>
                <h1><?php echo $largetitle; ?></h1>
            </div>
        </div>
    </div>
<?php
    }
?>

<div id="maincontent" class="general-template">
	<div class="container">
        <?php
		    while ( have_posts() ) {
		        the_post();
        ?>
            <div class="row">
                <div class="col">
                    <article id="post-<?php echo $id; ?>" <?php post_class(); ?>>
                        <?php the_content(); ?>
                    </article><!-- #post-## -->
                </div>
            </div>
        <?php
                $links = get_field('tests');
                foreach($links as $link) {
                    echo '<div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <img src="https://www.realresponse.com.au/wp-content/uploads/2020/07/icon-document.png" width="64px">
                                            <h2><a href="'.$link['link'].'" target="_blank">'.$link['title'].'</a></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                }
            }
	    ?>
    </div>
</div>

<?php get_footer();



