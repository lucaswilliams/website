<?php
/**
 * Template name: Training Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<header class="page-head">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="tile">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <h1><?php echo $largetitle; ?></h1>
                            <p id="breadcrumbs"><?php rr_breadcrumbs($post->ID); ?></p>
                            <?php echo wpautop(get_field('introductory_content')); ?>
                        </div>
                        
                        <div class="col-12 col-md-7">
                            <div class="head-card-image" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);">
                                <img src="<?php echo esc_url( $thumbnail[0] ); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="maincontent" class="course-pages courses">
<?php
while ( have_posts() ) : the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="container" id="introBlocks">
			<div class="row">
				<div class="col">
					<h2>Our Services</h2>
				</div>
			</div>
			<div class="row">
				<div class="col">
                    <?php
                        $blocks = get_field('introduction_blocks', $post->ID);
                        $carousel = 'intro-blocks';
                        $blocksize = 'col-sm-4';
                        $title = true;
                        include('template-parts/carousel/image-carousel.php');
                    ?>
				</div>
			</div>
		</div>

        <div class="container" id="training-faqs">
            <div class="row">
                <div class="col">
                    <h2>Frequently Asked Questions</h2>
                    <?php
                    $faq_cats = get_field('faq_category');
                    echo do_shortcode('[superior_faq layout="boxed" open_icon="plus" close_icon="minus" headline_tag="h4" permalink="no" deeplinking="yes" animate_content="yes" excerpt="no" categories="'.$faq_cats.'"]');
                    ?>
                </div>
            </div>
        </div>

    </article><!-- #post-## -->
<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



