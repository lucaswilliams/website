<?php
/**
 * Template name: Virtual First Aid
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');?>
<?php if ( has_post_thumbnail() ) :
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'realresponse-featured-image' );
	endif; 
	$smalltitle = $numberofsecs = get_field('small_title', $post->ID);
	$largetitle = $numberofsecs = get_field('large_title', $post->ID);
?>

<header class="page-head">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="tile">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <h1><?php echo $largetitle; ?></h1>
                            <p id="breadcrumbs"><?php rr_breadcrumbs($post->ID); ?></p>
                            <?php echo wpautop(get_field('introductory_content')); ?>
                            <p><a href="<?php echo site_url(); ?>/booking" class="btn btn-primary book-now">Book Now</a></p>
                        </div>
                        
                        <div class="col-12 col-md-7">
                            <div id="head-card-image" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);">
                                <img src="<?php echo esc_url( $thumbnail[0] ); ?>">
                                <?php $uriparts = explode('?', $_SERVER['REQUEST_URI']); if($uriparts[0] == '/online-first-aid-courses/') { ?>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#videoModal" id="rr-play">
                                        <i class="fa fa-play"></i>
                                    </button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<?php if($uriparts[0] == '/online-first-aid-courses/') { ?>
    <!-- Modal -->
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="video-container">
                        <iframe id="youtube_player" src="https://www.youtube.com/embed/dTdJ9EK4oG8?enablejsapi=1" data-src="https://www.youtube.com/embed/dTdJ9EK4oG8?enablejsapi=1" frameborder="0" allowfullscreen class="video"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div id="vr-sub-banner">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="wrap">
                    <h1>Complete a first aid course without leaving the comfort of your home</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="maincontent" class="course-pages courses">
<?php
while ( have_posts() ) : the_post();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <article id="services" class="realresponse-panel post-70 page type-page status-publish hentry">
            <div class="panel-content ">
                <div class="wrap">
                    <header class="entry-header">
                        <h2 class="entry-title">How it works</h2>
                    </header>
                    <div class="entry-content container">
                        <div class="row row-eq-height">
                            <?php
                            $works = get_field('how_it_works', $post->ID);
                            //var_dump($works);
                            foreach($works as $box) {
                                echo '<div class="col-12 col-md-6 col-lg-3">
                                    <div class="how_it_works">
                                        <div class="simage"><img src="'.$box['icon']['url'].'" /></div>
                                        <div class="scontent">
                                            <h2>'.$box['title'].'</h2>
                                            <div class="desc">'.$box['content'].'</div>
                                        </div>
                                    </div>
                                </div>';
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <header class="page-head">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-11" id="findCourse">
                        <div class="tile">
                            <div class="row align-items-center">
                                <div class="col-12 col-md-6">
                                    <h2>View upcoming courses</h2>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <?php
                                            $courses = get_field('courses');
                                            //echo '<pre>'; var_dump($courses); echo '</pre>';
                                            ?>
                                            <select id="see_virtual">
                                                <option value="#">select course type</option>
                                                <?php
                                                foreach($courses as $cse) {
                                                    $course = get_field('product_boxes', $cse)[0];
                                                    $ax_details = get_field('axcelerate_options', $course['virtual_booking_course'][0]);
                                                    $axids = [];
                                                    foreach($ax_details as $detail) {
                                                        $axids[] = $detail['course_id'];
                                                    }

                                                    echo '<option value="'.$course['booking_course'][0].'" data-axid="'.implode(',', $axids).'">'.$course['header'].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div id="axcel-courses" class="axcel-courses"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="most_popular_courses" id="courses">
            <div class="container">
                <div class="row row-eq-height">
                    <?php
                    $courses = get_field('product_boxes');
                    foreach($courses as $course) {
                        //$course = get_field('product_boxes', $cse->ID)[0];
                        $cid = '';
                        if($course['booking_course'] != null) {
                            $cid = base64_encode('{"cid" : '.$course['virtual_booking_course'][0].', "tid" : 2}');
                        }
                        ?>
                        <div class="col-12 col-lg-4">
                            <div class="course">
                                <div class="row d-none d-md-flex">
                                    <div class="col-7">
                                        <h3 class="header2"><?php echo $course['header']; ?></h3>
                                        <h4><?php echo $course['subheader']; ?></h4>
                                        <p class="code"><?php echo $course['unit_code']; ?></p>
                                        <a class="btn btn-primary book-now" title="Book Now" href="<?php echo site_url(); ?>/booking/<?php echo $cid; ?>">Book Now</a>
                                        <div class="group_b_icon">
                                            Group Bookings &amp;<br />
                                            Public Course Available
                                            <img src="https://www.realresponse.com.au/wp-content/uploads/2018/01/group-booking-public-course_info-03.png" alt="icon">
                                            <div class="gcontent" style="display: none;">
                                                This course is available as an onsite course where we come out to your site or as a public course where you come to one of our training facilities
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <?php
                                        if(strlen($course['course_length']) > 0) {
                                            echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
                                        }
                                        
                                        if(strlen($course['certificate_length']) > 0) {
                                            echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
                                        }
                                        
                                        if($course['nationally_accredited']) {
                                            echo '<p><img src="'.get_theme_file_uri('assets/images/nrt.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
                                        }
                                        ?>
                                        <p>Course details:</p>
                                        <?php echo $course['description']; ?>
                                    </div>

                                    <div class="accordion d-md-none">
                                        <div class="accordion-control">More Details</div>

                                        <div class="accordion-content">
                                            <?php
                                            if(strlen($course['course_length']) > 0) {
                                                echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
                                            }
                                            
                                            if(strlen($course['certificate_length']) > 0) {
                                                echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
                                            }
                                            
                                            if($course['nationally_accredited']) {
                                                echo '<p><img src="'.get_theme_file_uri('assets/images/nrt.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
                                            }
                                            ?>
                                            <p>Course details:</p>
                                            <?php echo $course['description']; ?>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="col-9 col-md-12 d-md-none"><?php echo $course['header']; ?></h3>
                                <p class="col-3 d-md-none text-right"><?php echo $course['unit_code']; ?></p>
                                <h4 class="col-9 col-md-12 d-md-none"><?php echo $course['subheader']; ?></h4>
                                <span class="col-3 d-md-none">
                                    <a class="btn btn-primary mob-enq book-now" title="Book Now" href="https://www.realresponse.com.au/booking/">Book</a>
                                </span>

                                <div class="accordion d-md-none">
                                    <div class="accordion-content">
                                        <?php
                                        if(strlen($course['course_length']) > 0) {
                                            echo '<p><img src="' . get_theme_file_uri('assets/images/duration.svg') . '" class="icon"><strong>' . $course['course_length'] . '</strong> Course Length</p>';
                                        }
                                        
                                        if(strlen($course['certificate_length']) > 0) {
                                            echo '<p><img src="' . get_theme_file_uri('assets/images/cert.svg') . '" class="icon"><strong>' . $course['certificate_length'] . '</strong> Certificate Length</p>';
                                        }
                                        
                                        if($course['nationally_accredited']) {
                                            echo '<p><img src="'.get_theme_file_uri('assets/images/nrt.svg').'" class="icon"><strong>Nationally Accredited</strong></p>';
                                        }
                                        ?>
                                        <p>Course details:</p>
                                        <?php echo $course['description']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="container" id="faq-section">
            <div class="row">
                <div class="col-12">
                    <h2 class="entry-title">Frequently Asked Questions</h2>
                    <?php
                    $faq_cats = get_field('faq_category');
                    echo do_shortcode('[superior_faq layout="boxed" open_icon="plus" close_icon="minus" headline_tag="h4" permalink="no" deeplinking="yes" animate_content="yes" excerpt="no" categories="'.$faq_cats.'"]');
                    ?>
                </div>
            </div>
        </div>

        <?php
            $trainers = get_field('trainers');
            if($trainers) {
                echo '<div class="real_trainers_area">
                    <div class="container">
                        <div class="real_trainers row justify-content-center">
                            <div class="col-12 col-md-9">';
                the_field('trainers_introduction');
                echo '</div>
                    <div class="col-12 col-md-9">';
                $blocks = [];
                foreach($trainers as $trainer) {
                    $blocks[] = [
                        'title' => $trainer['name'],
                        'content' => $trainer['position'],
                        'picture' => $trainer['picture']
                    ];
                }
        
                $carousel = 'trainer-blocks';
                $blocksize = 'col-sm-4';
                include('template-parts/carousel/image-carousel.php');
                
                echo '    </div>
                        </div>
                    </div>
                </div>';
            }
        ?>
    </article><!-- #post-## -->
<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile; // End of the loop.
?>
</div>
<?php get_footer();



