<?php
/**
 * Template name: VR2020
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');
if(has_post_video()) {
    //die('video');
    $video = '<video id="myVideo">
            <source src="'.get_the_post_video_url().'" type="video/mp4">
        </video>';
}

if ( has_post_thumbnail() ) {
    //die('thumb');
    $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
    //$thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
    $thumbnail = '<a href="#" data-toggle="modal" data-target="#videoModal">
                    <img src="'.esc_url($thumb_img[0]).'">
                  </a>';
}
$smalltitle = get_field('small_title', $post->ID);
$largetitle = get_field('large_title', $post->ID);
?>

    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $video; ?>
                </div>
            </div>
        </div>
    </div>

    <div id="section1" class="header-overlap vr-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 ml-auto">
                    <?php echo $video; ?>
                </div>
                <div class="col-12 col-md-6">
                    <h6><?php echo $smalltitle; ?></h6>
                    <h1><?php echo $largetitle; ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div id="serious-games" class="parallax-bg vr-page">
        <div class="container-fluid h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col">
                    <img src="<?php echo get_theme_file_uri('assets/images/serious-games.svg'); ?>">
                    <div class="container" style="margin-top: 24px;">
                        <div class="row">
                            <div class="col">
                                <p><?php the_field('serious_games_tagline', $post->ID); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="serious-games-zoom" class="vr-page">

    </div>
    <div id="vr-pop-in">
        <div id="vr-popped">
            <div id="vr-services">
                <div class="container">
                    <div class="row vr-services">
                        <?php
                        $services = get_field('services', $post->ID);

                        foreach($services as $service) {
                        ?>
                            <div class="col-12 col-md-4">
                                <div class="vr-service">
                                    <h2><?php echo $service['title']; ?></h2>
                                    <p><?php echo $service['tagline']; ?></p>
                                    <img src="<?php echo $service['image']; ?>">
                                    <div class="discover-more">
                                        <?php echo wpautop($service['description']); ?>
                                    </div>
                                    <a href="#" class="discover-expand">Discover</a>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div id="vr-clients" class="row">
                <div class="col">
                    <h2>Clients</h2>
                    <?php echo do_shortcode('[rr-team-showcase id="1483" cols="4"]'); ?>
                </div>
            </div>
        </div>

        <div id="vr-blurb">
            <p class="fixed"><?php the_field('vr_blurb', $post->ID); ?></p>
        </div>

        <div id="vr-popped-2">
            <div id="vr-stories">
                <div class="container">
                    <?php
                        $vr_stories = get_field('vr_stories', $post->ID);
                        $i = 0;
                        foreach ($vr_stories as $story) {
                            $i++;
                    ?>
                    <div class="row vr-story" id="vr-<?php echo $i; ?>">
                        <div class="col-12 col-md-6 col-img <?php echo ($story['layout'] == 1 ? 'd-lg-none' : ''); ?>">
                            <img src="<?php echo get_theme_file_uri('assets/images/rr-slice.svg'); ?>" class="bg-image">
                            <img src="<?php echo $story['image']; ?>">
                        </div>
                        <div class="col-12 col-md-6 col-text">
                            <h2><?php echo $story['header']; ?></h2>
                            <?php echo wpautop($story['text']); ?>
                        </div>
                        <?php if($story['layout'] == 1) { ?>
                            <div class="col-12 col-md-6 col-img d-none d-lg-block">
                                <img src="<?php echo get_theme_file_uri('assets/images/rr-slice.svg'); ?>" class="bg-image">
                                <img src="<?php echo $story['image']; ?>">
                            </div>
                        <?php } ?>
                        </div>
                    <?php
                        }
                    ?>
                </div>
            </div>

            <div id="vr-systems">
                <div class="container">
                    <div class="row vr-systems">
                        <?php
                        $services = get_field('systems', $post->ID);

                        foreach($services as $service) {
                            ?>
                            <div class="col-12 col-md-3">
                                <div class="vr-system">
                                    <h2><?php echo $service['title']; ?></h2>
                                    <p><?php echo $service['tagline']; ?></p>
                                    <img src="<?php echo $service['image']; ?>">
                                    <div class="discover-more">
                                        <?php echo wpautop($service['description']); ?>
                                    </div>
                                    <a class="discover-expand" href="#">Discover</a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div id="uni-adelaide">
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-lg-none ua-grey">
                            <img src="<?php the_field('university_of_adelaide_icon'); ?>">
                        </div>
                        <div class="col-12 col-lg-8 ua-white">
                            <?php the_field('university_of_adelaide_text'); ?>
                        </div>
                        <div class="d-none d-lg-block col-lg-4 ua-grey">
                            <img src="<?php the_field('university_of_adelaide_icon'); ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="vr-page container">
                <div class="row">
                    <div class="col">
                        <?php
                        $faq_cats = get_field('faq_category');
                        echo do_shortcode('[superior_faq layout="boxed" open_icon="plus" close_icon="minus" headline_tag="h4" permalink="no" deeplinking="yes" animate_content="yes" excerpt="no" categories="'.$faq_cats.'"]');
                        ?>
                    </div>
                </div>
            </div>
        </div>

<?php get_footer('vr');