<?php
/**
  template name:Popup Page
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since jnext 1.0
 */

get_header('popup'); ?>

<div class="main-bg">

<div class="top-area">

	<a href="javascript:void(0)" class="btnclose"></a>

	<div class="clear"></div>

</div>


<?php echo do_shortcode('[contact-form-7 id="171" title="Book now"]'); ?>


<div class="clear"></div>

</div>

<script type="text/javascript">

	jQuery(document).ready(function(){		

		/*jQuery(document).bind('cbox_open', function() {

			jQuery('html').css({ margin: '0' });

		});*/

	

		jQuery('.btnclose').click(function(){

			parent.jQuery.fn.colorbox.close();

		});

		jQuery(".industry input[type='radio']").click(function(){

			jQuery(".main-bg").css({"left":"-1960px"});

		});
		
		
		
		jQuery(".booknow-text").click(function(){

			jQuery(".main-bg").css({"left":"-980px"});

		});
		
		jQuery(".inquiry-text").click(function(){

			jQuery(".main-bg").css({"left":"-3920px"});

		});

		jQuery("#goto_step1").click(function(){

			jQuery(".main-bg").css({"left":"0px"});

		});

		jQuery("#s_form2").click(function(){

			jQuery(".main-bg").css({"left":"-1960px"});

		});

		jQuery("#goto_step2").click(function(){

			jQuery(".main-bg").css({"left":"-980px"});

		});

		jQuery(".level-choice input[type='radio']").click(function(){

			jQuery(".main-bg").css({"left":"-2940px"});

		});
		
		jQuery("#s_form3").click(function(){

			jQuery(".main-bg").css({"left":"-2940px"});

		});

		jQuery("#goto_step3").click(function(){

			jQuery(".main-bg").css({"left":"-1960px"});

		});
		
		
		jQuery(".course-size input[type='radio']").click(function(){

			jQuery(".main-bg").css({"left":"-3920px"});

		});
		
		jQuery("#s_form4").click(function(){

			jQuery(".main-bg").css({"left":"-3920px"});

		});

		jQuery("#goto_step4").click(function(){

			jQuery(".main-bg").css({"left":"-2940px"});

		});

	});

</script>