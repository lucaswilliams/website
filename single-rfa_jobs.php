<?php
/**
 * Template name: Work at Real Response (Individual Job)
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');

$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( 12624 ), 'realresponse-featured-image' );
//var_dump($thumbnail);
?>
    
    
    <div id="section1" class="header-overlap">
        <div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
        <div class="banner-content">
            <div class="wrap ">
                <h1><?php echo get_the_title(); ?></h1>
            </div>
        </div>
    </div>
    
    <div id="maincontent" class="course-pages courses">
        <?php
        while ( have_posts() ) : the_post();
            $job_id = get_the_ID();
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <div class="container" id="to-apply">
                    <div class="row">
                        <div class="col">
                            <div class="how_does_it_wrk_area">
                                <h1>About the Role</h1>
                                <?php the_field('about_the_role'); ?>
                            </div>
                            <div class="how_does_it_wrk_area">
                                <h1>The role will involve</h1>
                                <?php the_field('the_role_will_involve'); ?>
                            </div>
                            <div class="how_does_it_wrk_area">
                                <h1>Requirements</h1>
                                <?php the_field('requirements'); ?>
                            </div>
                            <div class="how_does_it_wrk_area">
                                <h1>How to Apply</h1>
                                <p>Complete contact form below with your resume, cover letter and any other supporting documentation</p>
                                <?php echo do_shortcode('[gravityform id="7" title="false" description="false" ajax="true field_values=’desiredrole='.$job_id.'′"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </article><!-- #post-## -->
            <?php
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
        
        endwhile; // End of the loop.
        ?>
    </div>
<?php get_footer();



