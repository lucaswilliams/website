<?php
/**
 * Template name: Podcast (Individual Entry)
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

get_header('mega-menu');

$the_slug = 'jobs';
$args = array(
    'post_name' => $the_slug,
    'post_type' => 'page',
    'post_status' => 'publish',
    'numberposts' => 1
);
$my_posts = get_posts($args);
if( $my_posts ) :
    $post_id = $my_posts[0]->ID;
endif;
$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'realresponse-featured-image' );
?>
    
    
    <div id="section1" class="header-overlap">
        <div class="featured-banner" style="background-image:url(<?php echo esc_url( $thumbnail[0] ); ?>)"></div>
        <div class="banner-content">
            <div class="wrap ">
                <h1>Podcast</h1>
            </div>
        </div>
    </div>
    
    <div class="container" id="podcast-post">
        <div class="row">
            <div class="col-12 col-lg-8">
                <?php
                while ( have_posts() ) {
                    the_post();
                    $job_id = get_the_ID();
                    echo '<h1>'.get_the_title().'</h1>';
                    echo '<h3 class="post-meta">By <a href="#">'.get_the_author($post->ID).'</a> '.date('j M Y', strtotime($post->post_date)).'</h3>';

                    $platforms = get_field('podcast_availability');
                    foreach($platforms as $platform) {
                        echo '<a class="btn btn-primary" href="'.$platform['location'].'">Subscribe on '.$platform['provider'].'</a>';
                    }

                    $podfile = get_field('podcast_file');
                    echo '<audio controls><source src="'.$podfile['url'].'"></audio>';

                    echo wpautop(get_the_content());
                }
                ?>
            </div>
            <div class="col-12 col-lg-4">
                <h2>You may like:</h2>
                <?php
                $posts = get_posts([
                    'post_type' => 'rfa_podcast',
                    'numberposts' => 4,
                    'exclude' => $job_id
                ]);

                echo '<div class="row">';
                foreach($posts as $post) {
                    echo '<div class="col-12 col-md-4 col-lg-12">';
                    echo '<div class="podcast">';
                    $thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
                    echo '<div style="background-image: url('.esc_url($thumb_img[0]).');" class="post-thumbnail"></div>';
                    echo '<h2><a href="'.get_the_permalink().'">'.$post->post_title.'</a></h2>';
                    echo '</div>';
                    echo '</div>';
                }
                echo '</div>';
                ?>
            </div>
        </div>
    </div>
<?php get_footer();



