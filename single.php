<?php
get_header('mega-menu');
//$post = get_page_by_path( 'blog', OBJECT, 'page' );
if(has_post_video()) {
$thumbnail = '<video autoplay muted loop id="myVideo">
    <source src="'.get_the_post_video_url().'" type="video/mp4">
</video>';
} elseif ( has_post_thumbnail() ) {
$thumb_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'realresponse-featured-image');
$thumbnail = '<div style="background-image: url('.esc_url($thumb_img[0]).'); width: 100%; height: 100%; background-size: cover; background-position: center;"></div>';
}
?>

<div id="section1" class="header-overlap">
    <div class="featured-banner"><?php echo $thumbnail; ?></div>
    <div class="banner-content">
        <div class="wrap ">
            <h1><?php echo $post->post_title; ?></h1>
        </div>
    </div>
</div>

<div class="wrap">
	<div class="container">
        <div class="row">
            <div class="col">
			<?php
                /* Start the Loop */
                while ( have_posts() ) : the_post();
    
                    get_template_part( 'template-parts/post/content', get_post_format() );
    
                    // If comments are open or we have at least one comment, load up the comment template.
                    /*if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;*/
    
                    the_post_navigation( array(
                        'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'realresponse' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'realresponse' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . realresponse_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
                        'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'realresponse' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'realresponse' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . realresponse_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
                    ) );
    
                endwhile; // End of the loop.
                ?>
            </div><!-- .col -->
		</div><!-- .row -->
	</div><!-- .container -->
	<?php //get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
