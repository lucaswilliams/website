<?php

if($blocks) {
    echo '<div id="'.$carousel.'" class="rr-carousel">
            <div class="rr-inner row">';
    foreach($blocks as $key => $block) {
        $class = '';
        switch($key) {
            case 0 :
                $class = 'before';
                break;
            case 1 :
                $class = 'active';
                break;
            case 2 :
                $class = 'after';
                break;
        }

        if(is_array($block['picture'])) {
            $image = $block['picture']['sizes']['medium'];
        } else {
            $image = $block['picture'];
        }

        echo '<div class="rr-item '.$class.' col-12 '.$blocksize.'">
                <div class="image">
                    <img src="'.$image.'">
                </div>';
        if($title) {
            echo '<h3>'.($title ? $block['title'] : '').'</h3>
                  <p>'.($title ? $block['content'] : '').'</p>';
        }
        echo '</div>';
    }
    
    /*foreach($blocks as $key => $block) {
        echo '<div class="carousel-indica'
    }*/
    
    echo '</div>
            <a class="rr-control rr-prev" href="#'.$carousel.'" role="button" data-slide="prev">
                <i class="fa fa-chevron-left"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="rr-control rr-next" href="#'.$carousel.'" role="button" data-slide="next">
                <i class="fa fa-chevron-right"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>';

    echo '<style>
        .real_trainers_area #trainer-blocks .rr-inner .rr-item:nth-of-type(1n+'.($cols + 1).') {
            display: none;
        }
    </style>';
}
            
