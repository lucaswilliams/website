<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'realresponse' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'realresponse' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
