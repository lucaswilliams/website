<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-branding">
	<div class="wrap">

		<div class="logo-sec">
		<?php the_custom_logo(); ?>

		<div class="site-branding-text">
			<?php if ( is_front_page() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php endif; ?>

			
		</div><!-- .site-branding-text -->

		<?php if ( ( realresponse_is_frontpage() || ( is_home() && is_front_page() ) ) && ! has_nav_menu( 'top' ) ) : ?>
		<a href="#content" class="menu-scroll-down"><?php echo realresponse_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'realresponse' ); ?></span></a>
	<?php endif; ?>
	 </div>
	 
	 	<div class="header-right">
		  <div class="sticky-right">
			<?php echo get_option( 'stricky_text' ); ?>
		  </div>
	      <?php if ( has_nav_menu( 'top' ) ) : ?>
		  <div class="navigation-top">
			<div class="wrap">
			  <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
			</div>
			<!-- .wrap -->
		  </div>
		  <?php endif; ?>
		  
		   <?php if ( has_nav_menu( 'top' ) ) : 
		   
		    $contact_link = get_option( 'contact_link' );
			$contact_text = get_option( 'contact_text' );
		   ?>
		  <div class="navigation-mobile">
		  	  <p class="contact-us"><a href="<?php echo $contact_link; ?>"><?php echo $contact_text; ?></a></p>
			  <?php get_template_part( 'template-parts/navigation/navigation', 'mobile' ); ?>
		  </div>
		  <?php endif; ?>
		  </div>
		  <!-- .navigation-top -->
	</div><!-- .wrap -->
</div><!-- .site-branding -->
