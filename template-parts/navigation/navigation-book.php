<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-branding">
	<div class="wrap">

		<div class="logo-sec">
		<?php the_custom_logo(); ?>

		<div class="site-branding-text">
			<?php if ( is_front_page() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php endif; ?>

			<?php
			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) :
			?>
				<p class="site-description"><?php echo $description; ?></p>
			<?php endif; ?>
		</div><!-- .site-branding-text -->

		<?php if ( ( realresponse_is_frontpage() || ( is_home() && is_front_page() ) ) && ! has_nav_menu( 'top' ) ) : ?>
		<a href="#content" class="menu-scroll-down"><?php echo realresponse_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'realresponse' ); ?></span></a>
	<?php endif; ?>
	 </div>
	 <div class="header-right">
		  <ul id="menu-mainmenu" class="nav-menu">
			  <li id="menu-item-25" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-25"><a href="http://www.realresponse.com.au" class=" "><span class="menu-image-title">EXIT BOOKING FORM</span></a></li>
			  
		  </ul>
			
	</div>
		  
	 	  <!-- .navigation-top -->
	</div><!-- .wrap -->
</div><!-- .site-branding -->
