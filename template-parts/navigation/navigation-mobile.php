<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.2
 */

?>
<div id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Mobile Menu', 'realresponse' ); ?>">
	<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
		<?php
		echo realresponse_get_svg( array( 'icon' => 'bars' ) );
		echo realresponse_get_svg( array( 'icon' => 'close' ) );
		_e( '', 'realresponse' );
		?>
	</button>
	<?php 
		
		?>
	<?php /*wp_nav_menu( array(
		'theme_location' => 'mob-nav',
		'menu_id'        => 'mob-nav',
		'items_wrap' =>  realresponse_get_svg( array( 'icon' => 'close' ) ). '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'walker' => new My_Walker_Nav_Menu_mobile()
	) ); */
	 /*wp_nav_menu( array(
		'theme_location' => 'mob-nav',
		'menu_id'        => 'mob-nav',
		'items_wrap' =>  realresponse_get_svg( array( 'icon' => 'close' ) ). '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'walker' => new My_Walker_Nav_Menu_mobile()
	) ); */
	wp_nav_menu( array(
		'theme_location' => 'mob-nav',
		'menu_id'        => 'mob-nav',
		'items_wrap' =>  realresponse_get_svg( array( 'icon' => 'close' ) ). '<ul id="%1$s" class="%2$s">%3$s</ul>',
		//'walker' => new My_Walker_Nav_Menu()
	) );
	wp_nav_menu( array(
		'theme_location' => 'social',
		'menu_class'     => 'social-links-menu',
		'depth'          => 1,
		'link_before'    => '<span class="screen-reader-text sas">',
		'link_after'     => '</span>' . realresponse_get_svg( array( 'icon' => 'chain' ) ),
	) );
	?>

	<?php if ( ( realresponse_is_frontpage() || ( is_home() && is_front_page() ) ) && has_custom_header() ) : ?>
		<a href="#content" class="menu-scroll-down"><?php echo realresponse_get_svg( array( 'icon' => 'arrow-right' ) ); ?><span class="screen-reader-text"><?php _e( 'Scroll down to content', 'realresponse' ); ?></span></a>
	<?php endif; ?>
</div><!-- #site-navigation -->
