<?php
/**
 * Displays sub navigation for home page
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.2
 */

wp_nav_menu(
    array(
        'theme_location' => 'sub',
        'menu_id'        => 'sub-nav',
        'walker' => new My_Walker_Nav_Menu()
    )
);
