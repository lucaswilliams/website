<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.2
 */

?>
<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'realresponse' ); ?>">
	<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
		<?php
		echo realresponse_get_svg( array( 'icon' => 'bars' ) );
		echo realresponse_get_svg( array( 'icon' => 'close' ) );
		_e( '', 'realresponse' );
		?>
	</button>
	<?php 
		
		?>
	<?php 
	 wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => 'top-nav',
		'walker' => new My_Walker_Nav_Menu()
	) ); 
	 ?>
</nav><!-- #site-navigation -->
