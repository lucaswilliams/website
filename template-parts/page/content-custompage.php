<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Real_Response
 * @since 1.0
 * @version 1.0
 */

$id = get_the_ID();
$course = $_GET['course_id'] ?? 0;

$intros = get_field('course_introductions', $id);
if($intros) {
    foreach($intros as $intro) {
        if($intro['course_id'] == $course) {
            echo '<h1>'.$intro['course_title'].'</h1>';
            if(strlen($intro['course_description']) > 0) {
                echo $intro['course_description'];
            }
        }
    }
}
?>
<article id="post-<?php echo $id; ?>" <?php post_class(); ?>>
	<?php
        the_content();
    ?>
</article><!-- #post-## -->
