<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

<?php do_action( 'woocommerce_before_cart_table' ); ?>

<div class="shop_table shop_table_responsive cart" cellspacing="0">
    <div class="cart_header row">
        <div class="col-12 col-lg-6 product-name"><?php _e( 'Description', 'woocommerce' ); ?></div>
        <div class="col-4 col-lg-2 product-price text-right d-none d-lg-block"><?php _e( 'Unit Price', 'woocommerce' ); ?></div>
        <div class="col-3 col-lg-2 product-quantity d-none d-lg-block"><?php _e( 'Qty', 'woocommerce' ); ?></div>
        <div class="col-4 col-lg-2 product-subtotal text-right d-none d-lg-block"><?php _e( 'Sub-Total', 'woocommerce' ); ?></div>
    </div>

    <?php do_action( 'woocommerce_before_cart_contents' ); ?>

    <?php
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
            $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
            ?>
            <div class="row align-items-center <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

                <div class="d-none d-md-block col-md-2 col-lg-1 product-thumbnail">
                    <?php
                        $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                        if ( ! $product_permalink ) {
                            echo $thumbnail;
                        } else {
                            printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
                        }
                    ?>
                </div>

                <div class="col-8 col-md-6 col-lg-5 product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
                    <?php
                        if ( ! $product_permalink ) {
                            echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                        } else {
                            echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() ), $cart_item, $cart_item_key );
                        }

                        // Meta data
                        echo WC()->cart->get_item_data( $cart_item );

                        // Backorder notification
                        if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                            echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
                        }
                    ?>
                </div>

                <div class="col-4 col-lg-2 product-price text-right" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
                    <?php
                        echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                    ?>
                    <div class="d-lg-none">
                        <?php
                        if ( $_product->is_sold_individually() ) {
                            $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                        } else {
                            $product_quantity = woocommerce_quantity_input( array(
                                'input_name'  => "cart[{$cart_item_key}][qty]",
                                'input_value' => $cart_item['quantity'],
                                'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                'min_value'   => '0'
                            ), $_product, false );
                        }

                        echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );

                        echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                            '<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-times" aria-hidden="true"></i></a>',
                            esc_url(WC()->cart->get_remove_url($cart_item_key)),
                            __('Remove this item', 'woocommerce'),
                            esc_attr($product_id),
                            esc_attr($_product->get_sku())
                        ), $cart_item_key);

                        ?>
                    </div>
                </div>

                <div class="col-3 col-lg-2 product-quantity d-none d-lg-flex align-items-center" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
                    <?php
                        if ( $_product->is_sold_individually() ) {
                            $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                        } else {
                            $product_quantity = woocommerce_quantity_input( array(
                                'input_name'  => "cart[{$cart_item_key}][qty]",
                                'input_value' => $cart_item['quantity'],
                                'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                'min_value'   => '0'
                            ), $_product, false );
                        }

                        echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                    echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                        '<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-times" aria-hidden="true"></i></a>',
                        esc_url(WC()->cart->get_remove_url($cart_item_key)),
                        __('Remove this item', 'woocommerce'),
                        esc_attr($product_id),
                        esc_attr($_product->get_sku())
                    ), $cart_item_key);
                    ?>
                </div>

                <div class="col-4 col-lg-2 product-subtotal d-none d-lg-block text-right" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
                    <?php
                        echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                    ?>
                </div>
            </div> <!-- .row -->
            <?php
        }
    }

    do_action( 'woocommerce_cart_contents' );
    ?>
    <div class="row">
        <div class="actions col-12 col-md-6">

            <?php if ( wc_coupons_enabled() ) { ?>
                <div class="coupon row">
                    <div class="col-12 col-md-8">
                        <label for="coupon_code"><?php _e( 'Coupon Code:', 'woocommerce' ); ?></label>
                        <input type="text" name="coupon_code" class="form-control" id="coupon_code" value="" placeholder="<?php //esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" />
                        <button type="submit" class="btn btn-primary" name="apply_coupon" style="float: right; margin-top: 12px;">Apply</button>

                        <?php do_action( 'woocommerce_cart_coupon' ); ?>
                    </div>
                </div>
            <?php } ?>

            <?php /*?><input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" /><?php */?>

            <?php do_action( 'woocommerce_cart_actions' ); ?>

            <?php wp_nonce_field( 'woocommerce-cart' ); ?>
        </div>
        <div class="cart-total-td col-12 col-md-6">
            <div class="cart-collaterals">

                <?php do_action( 'woocommerce_cart_collaterals' ); ?>

            </div>
        </div>
    </div>

    <?php do_action( 'woocommerce_after_cart_contents' ); ?>
</div>

<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>



<?php do_action( 'woocommerce_after_cart' ); ?>
