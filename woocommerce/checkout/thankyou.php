<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $order ) { ?>
    <?php do_action('woocommerce_before_thank_page'); ?>
    <?php if ($order->has_status('failed')) { ?>

        <p class="woocommerce-thankyou-order-failed"><?php _e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce'); ?></p>

        <p class="woocommerce-thankyou-order-failed-actions">
            <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>"
               class="button pay"><?php _e('Pay', 'woocommerce') ?></a>
            <?php if (is_user_logged_in()) { ?>
                <a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>"
                   class="button pay"><?php _e('My Account', 'woocommerce'); ?></a>
            <?php } ?>
        </p>
    
    <?php } else { ?>

        <p class="woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', __('Thank you for your recent purchase. Your goods will be dispatched within 48 hours.Please dont hesitate to contact us with any questions you may have.', 'woocommerce'), $order); ?></p>
        <div class="row">
            <div class="col-12">
                <header class="title">
                    <h3><?php _e('Purchase summary', 'woocommerce'); ?></h3>
                </header>
                <table class="table table-striped">
                <tr>
                    <th>Qty</th>
                    <th>Item</th>
                    <th class="text-right">Total</th>
                </tr>
                <?php
                if (sizeof($order->get_items()) > 0) {
                    foreach ($order->get_items() as $item) {
                        $_product = apply_filters('woocommerce_order_item_product', $order->get_product_from_item($item), $item);
                        $item_meta = new WC_Order_Item_Meta($item['item_meta'], $_product);
                        ?>
                        <tr>
                            <td>
                                <?php echo apply_filters('woocommerce_order_item_quantity_html', ' <span class="product-quantity">' . sprintf('%s <i class="fa fa-times"></i> ', $item['qty']) . '</span>', $item); ?>
                            </td>
                            <td>
                                <?php
                                if ($_product && !$_product->is_visible())
                                    echo apply_filters('woocommerce_order_item_name', $item['name'], $item);
                                else
                                    echo apply_filters('woocommerce_order_item_name', sprintf('<a href="%s">%s</a>', get_permalink($item['product_id']), $item['name']), $item);
                                
                                $item_meta->display();
                                if ($_product && $_product->exists() && $_product->is_downloadable() && $order->is_download_permitted()) {
                                    $download_files = $order->get_item_downloads($item);
                                    $i = 0;
                                    $links = array();
                                    foreach ($download_files as $download_id => $file) {
                                        $i++;
                                        $links[] = '<small><a href="' . esc_url($file['download_url']) . '">' . sprintf(__('Download file%s', 'woocommerce'), (count($download_files) > 1 ? ' ' . $i . ': ' : ': ')) . esc_html($file['name']) . '</a></small>';
                                    }
                                    echo '<br/>' . implode('<br/>', $links);
                                }
                                ?>
                                <?php
                                if ($order->has_status(array('completed', 'processing')) && ($purchase_note = get_post_meta($_product->id, '_purchase_note', true))) {
                                    ?>
                                    <p class="small"><?php echo wpautop(do_shortcode(wp_kses_post($purchase_note))); ?></p>
                                    <?php
                                }
                                ?>
                            </td>
                            <td class="text-right">
                                <?php echo $order->get_formatted_line_subtotal($item); ?>
                            </td>
                        </tr>
                        
                        <?php
                    }
                }
                if ($totals = $order->get_order_item_totals()) foreach ($totals as $total) {
                    if ($total['label'] == 'Total:') {
                ?>
                    <tr>
                        <td colspan="2"><strong><?php echo $total['label']; ?></strong></td>
                        <td class="product-total text-right"><?php echo $total['value']; ?></td>
                    </tr>
                <?php
                    }
                }
                ?>
                </table>
            </div>
        </div>
        <?php do_action('woocommerce_order_details_after_order_table', $order); ?>
        <div class="row">
            <div class="col-12 col-md-8">
                <?php
                if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address() && get_option('woocommerce_calc_shipping') !== 'no') { ?>
                <div class="col2-set addresses">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <?php } ?>
                            <header class="title">
                                <h3><?php _e('Billing/Shipping to', 'woocommerce'); ?></h3>
                            </header>
                            <address>
                                <?php
                                if (!$order->get_formatted_billing_address()) _e('N/A', 'woocommerce'); else echo $order->get_formatted_billing_address();
                                ?>
                            </address>
                            <?php if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address() && get_option('woocommerce_calc_shipping') !== 'no') { ?>
                        </div><!-- /.col-1 -->
                        <div class="col-12 col-md-6">
                            <header class="title">
                                <h3><?php _e('Shipping to', 'woocommerce'); ?></h3>
                            </header>
                            <address>
                                <?php
                                if (!$order->get_formatted_shipping_address()) _e('N/A', 'woocommerce'); else echo $order->get_formatted_shipping_address();
                                ?>
                            </address>
                        </div>
                    </div><!-- /.col-2 -->
                </div><!-- /.col2-set -->
            
            <?php } ?>
            </div>
            <div class="col-12 col-md-4">
                <?php
                if ($available_gateways = WC()->payment_gateways->get_available_payment_gateways()) {
                    foreach ($available_gateways as $gateway) {
                        if ($gateway->title == $order->payment_method_title) {
                            ?>
                            <header class="title">
                                <h3><?php _e('Payment Details', 'woocommerce'); ?></h3>
                            </header>
                            <div class="woocommerce-thankyou-order-details order_details">
                                <p class="order">
                                    <?php _e('Order Number:', 'woocommerce'); ?>
                                    <strong><?php echo $order->get_order_number(); ?></strong>
                                </p>
                                <p class="date">
                                    <?php _e('Date:', 'woocommerce'); ?>
                                    <strong><?php echo date_i18n(get_option('date_format'), strtotime($order->order_date)); ?></strong>
                                </p>
                                <p class="total">
                                    <?php _e('Total:', 'woocommerce'); ?>
                                    <strong><?php echo $order->get_formatted_order_total(); ?></strong>
                                </p>
                                <?php if ($order->payment_method_title) : ?>
                                    <p class="method">
                                        <?php _e('Payment Method:', 'woocommerce'); ?>
                                        <strong><?php echo $order->payment_method_title; ?></strong>
                                    </p>
                                <?php endif; ?>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
<?php
        }
}
do_action('woocommerce_after_thank_page');?>
