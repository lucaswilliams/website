<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

$post_slug = $_SERVER['REQUEST_URI'];
$parts = preg_split('/\//', $post_slug, -1, PREG_SPLIT_NO_EMPTY);
$args = array(
    'name'        => end($parts),
    'post_type'   => 'product',
    'post_status' => 'publish',
    'numberposts' => 1
);
$my_posts = get_posts($args);
if( $my_posts ) {
    $page_title = $my_posts[0]->post_title;
}
$product = new WC_product(get_the_ID());
$attachment_ids = $product->get_gallery_image_ids();

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
    <div class="row product-flash <?php echo(count($attachment_ids) > 0 ? 'gallery' : ''); ?>">
        <div class="col-12 d-md-none text-right">
            <?php dynamic_sidebar('cart-info'); ?>
        </div>
        <div class="col-12 col-md-6">
            <?php
                /**
                 * Hook: woocommerce_before_single_product_summary.
                 *
                 * @hooked woocommerce_show_product_sale_flash - 10
                 * @hooked woocommerce_show_product_images - 20
                 */
                do_action( 'woocommerce_before_single_product_summary' );
            ?>
        </div>
        <div class="col-12 col-md-6">
            <div class="summary entry-summary">
                <div class="row">
                    <div class="col-12 col-md-10">

                        <h2 class="product-title"><?php echo (strlen($page_title) > 0 ? $page_title : woocommerce_page_title()); ?></h2>
                    </div>
                    <div class="col-2 d-none d-md-block text-right">
                        <?php dynamic_sidebar('cart-info'); ?>
                    </div>
                </div>
                <?php
                woocommerce_template_single_excerpt();

                //woocommerce_template_single_meta();
                //woocommerce_template_single_sharing();

                woocommerce_template_single_price();
                woocommerce_template_single_add_to_cart();
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <?php
                woocommerce_product_description_tab();
            ?>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
